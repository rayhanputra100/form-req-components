<?php

namespace App\Models;

use CodeIgniter\Model;

class M_Departement extends Model
{
    protected $table = 'departement';
    protected $allowedFields = ['id_dept', 'name_dept'];
    protected $useTimestamps = true;

    public function getData($id = false)
    {
        if ($id == false) {
            return $this->findAll();
        }

        return $this->where(['id_dept' => $id])->first();
    }
}
