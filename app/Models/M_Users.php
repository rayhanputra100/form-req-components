<?php

namespace App\Models;

use CodeIgniter\Model;

class M_Users extends Model
{
    protected $table = 'users';
    protected $allowedFields = ['id_users', 'id_dept', 'level', 'username', 'password', 'ket'];
    protected $useTimestamps = true;

    public function getData($id = false)
    {
        if ($id == false) {
            return $this->findAll();
        }

        return $this->where(['id_users' => $id])->first();
    }
}
