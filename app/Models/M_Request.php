<?php

namespace App\Models;

use CodeIgniter\Model;

class M_Request extends Model
{
    protected $table = 'request';
    protected $primaryKey = 'id_request';
    protected $allowedFields = ['no_registrasi', 'date_plan', 'usr_pemohon', 'id_pemohon', 'scope', 'dept_pemohon', 'detail', 'pn', 'description', 'wh_tujuan', 'wh_asal', 'jumlah_diminta', 'jumlah_disetujui', 'bukti_tr', 'usr_admin_wh_asal', 'usr_kasie_ppic', 'usr_kasie_pemohon', 'usr_admin_seksi_pemohon', 'approve_admin_wh_asal', 'approve_kasie_ppic', 'approve_kasie_pemohon', 'approve_admin_seksi_pemohon', 'qr'];
    protected $useTimestamps = true;

    public function getData($dates, $id_pemohon)
    {
        return $this->where('date_plan', $dates)
            ->where('id_pemohon', $id_pemohon)
            ->findAll();
    }

    public function getAllData($dates = null)
    {
        if ($dates != null) {
            return $this->where('date_plan', $dates)->orderBy('created_at', 'DESC')
                ->findAll();
        }

        return $this->orderBy('created_at', 'DESC')->findAll();
    }

    public function getDataById($id_request)
    {
        return $this->where('id_request', $id_request)->first();
    }

    public function updateData($id_request, $data)
    {
        // Example usage: $this->m_req->updateData($id_request, $data);
        $this->set($data);
        $this->where('id_request', $id_request);
        $this->update();
    }

    public function deleteRequest($id_request)
    {
        return $this->delete($id_request);
    }
}
