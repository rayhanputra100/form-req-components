<?php

namespace App\Models;

use CodeIgniter\Model;

class M_PartNumber extends Model
{
    protected $table = 'part_number';
    protected $allowedFields = ['id_pn', 'pn', 'desc'];
    protected $useTimestamps = true;

    public function getData($id = false)
    {
        if ($id == false) {
            return $this->findAll();
        }

        return $this->where(['id_pn' => $id])->first();
    }

    public function getPN($desc)
    {
        return $this->select('pn')
            ->where('description', $desc)
            ->get()
            ->getResultArray();
    }

    public function getDesc($pn)
    {
        return $this->select('description')
            ->where('pn', $pn)
            ->get()
            ->getResultArray();
    }
}
