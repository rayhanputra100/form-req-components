<?php

namespace App\Models;

use CodeIgniter\Model;

class M_Warehouse extends Model
{
    protected $table = 'warehouse';
    protected $allowedFields = ['id_wh', 'wh'];
    protected $useTimestamps = true;

    public function getData($id = false)
    {
        if ($id == false) {
            return $this->findAll();
        }

        return $this->where(['id_wh' => $id])->first();
    }
}
