<?php

namespace App\Models;

use CodeIgniter\Model;

class M_Scope extends Model
{
    protected $table = 'scope';
    protected $allowedFields = ['id_scope', 'scope'];
    protected $useTimestamps = true;

    public function getData($id = false)
    {
        if ($id == false) {
            return $this->findAll();
        }

        return $this->where(['id_scope' => $id])->first();
    }

}
