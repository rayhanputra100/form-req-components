<?= $this->extend('template/layout'); ?>

<?= $this->section('content'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <div class="container-full">
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xl-12 col-12">
          <div class="row">
            <div class="col-12 col-xl-12">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="text-center">FORM EDIT REQUEST COMPONENT</h4>
                </div>
                <form id="request-form" action="<?= base_url('edit_data_request/' . $req['id_request']) ?>" method="post" enctype="multipart/form-data">
                  <?= csrf_field(); ?>
                  <div class="row mt-3">
                    <div class="col-3 col-xl-3">
                      <div class="box-header with-border">
                        <div class="form-group">
                          <label class="form-label">ID Pemohon</label>
                          <div class="col-md-10">
                            <input class="form-control" type="text" name="id_pemohon" value="<?= $req['id_pemohon']; ?>">
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="form-label">Username Pemohon</label>
                          <div class="col-md-10">
                            <input class="form-control" type="text" name="usr_pemohon" value="<?= $req['usr_pemohon']; ?>">
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="form-label">Date</label>
                          <div class="col-md-10">
                            <input class="form-control" type="date" value="<?= $req['date_plan'] ?>" name="date" id="filterTanggal">
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="form-label">No Registrasi</label>
                          <div class="col-md-10">
                            <input class="form-control" type="text" name="no_registrasi" value="<?= sprintf('%06d', $req['no_registrasi']); ?>" readonly>
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="form-label">Departement Pemohon</label>
                          <select class="form-control select2" name="dept_pemohon">
                            <?php foreach ($dept as $v) : ?>
                              <?php $selected = ($v['name_dept'] === $req['dept_pemohon']) ? 'selected' : ''; ?>
                              <option value="<?= $v['name_dept']; ?>" <?= $selected; ?>>
                                <?= $v['name_dept']; ?>
                              </option>
                            <?php endforeach ?>
                          </select>
                        </div>

                        <div class="form-group">
                          <label class="form-label">Warehouse Tujuan</label>
                          <select class="form-control select2" name="warehouse_tujuan">
                            <?php foreach ($wh as $v) : ?>
                              <?php $selected = ($v['wh'] === $req['wh_tujuan']) ? 'selected' : ''; ?>
                              <option value="<?= $v['wh']; ?>" <?= $selected; ?>>
                                <?= $v['wh']; ?>
                              </option>
                            <?php endforeach ?>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-6 col-xl-6"></div>
                    <div class="col-3 col-xl-3">
                      <div class="box-header with-border">
                        <div class="form-group">
                          <label class="form-label">Scope</label>
                          <select class="form-control select2" name="scope">
                            <?php foreach ($scope as $v) : ?>
                              <?php $selected = ($v['scope'] === $req['scope']) ? 'selected' : ''; ?>
                              <option value="<?= $v['scope']; ?>" <?= $selected; ?>>
                                <?= $v['scope']; ?>
                              </option>
                            <?php endforeach ?>
                          </select>
                        </div>

                        <div class="form-group">
                          <label class="col-form-label col-md-2">Detail</label>
                          <div class="col-md-10">
                            <textarea class="form-control" placeholder="Ketik Detailnya" name="detail"><?= $req['detail']; ?></textarea>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="box-body">
                    <div class="table-responsive">
                      <table class="table table-bordered table-striped" style="width:100%">
                        <thead>
                          <tr class="text-center">
                            <th>No</th>
                            <th>Part Number</th>
                            <th>Description</th>
                            <th>Warehouse Asal</th>
                            <th>Jumlah Diminta</th>
                            <th>Jumlah Disetujui</th>
                            <th>Bukti TR</th>
                          </tr>
                        </thead>
                        <tbody id="table-body">
                        </tbody>
                      </table>
                    </div>
                  </div>
                </form>
                <div class="text-end">
                  <button type="submit" id="submit-button" form="request-form" class="btn btn-success col-2 col-xl-2">Submit</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
</div>
<!-- /.content-wrapper -->

<?= $this->endSection(); ?>

<?= $this->section('script'); ?>
<script>
  const pn = <?= json_encode($pn) ?>;
  const wh = <?= json_encode($wh) ?>;
  const reqDescription = <?= json_encode($req['description']) ?>;
  const reqPN = <?= json_encode($req['pn']) ?>;
  const whAsal = <?= json_encode($req['wh_asal']) ?>;
  const jumDiminta = <?= json_encode($req['jumlah_diminta']) ?>;
  const jumDisetujui = <?= json_encode($req['jumlah_disetujui']) ?>;
  const buktiTR = <?= json_encode($req['bukti_tr']) ?>;


  $(document).ready(function() {
    let rowData = []; // Menyimpan data baris
    let no = 1;

    // Fungsi untuk mengisi data ke dalam tabel
    function addDataInput() {
      const tableBody = $('#table-body');
      const newRow = $('<tr>');

      // Kolom "No" dengan tag <p> dan nomor yang terus bertambah
      const noCell = $('<td>').append($('<p>', {
        class: 'text-center',
        text: no++
      }));
      newRow.append(noCell);

      const part_number = $('<td>').append(
        $('<select>', {
          class: 'form-control select2',
          name: 'part_number[]'
        }).append(
          $.map(pn, function(item) {
            return $('<option>', {
              value: item.pn,
              text: item.pn,
              selected: item.pn === reqPN
            });
          })
        )
      );
      newRow.append(part_number);

      const desc = $('<td>').append(
        $('<select>', {
          class: 'form-control select2',
          name: 'description[]'
        }).append(
          $.map(pn, function(item) {
            return $('<option>', {
              value: item.description,
              text: item.description,
              selected: item.description === reqDescription
            });
          })
        )
      );
      newRow.append(desc);

      const warehouse_asal = $('<td>').append(
        $('<select>', {
          class: 'form-control select2',
          name: 'warehouse_asal[]'
        }).append(
          $.map(wh, function(item) {
            return $('<option>', {
              value: item.wh,
              text: item.wh,
              selected: item.wh === whAsal
            });
          })
        )
      );
      newRow.append(warehouse_asal);

      const jumlah_diminta = $('<td>').append($('<input>', {
        type: 'text',
        class: 'form-control',
        name: 'jumlah_diminta[]',
        value: jumDiminta
      }));
      newRow.append(jumlah_diminta);

      const jumlah_disetujui = $('<td>').append($('<input>', {
        type: 'text',
        class: 'form-control',
        name: 'jumlah_disetujui[]',
        value: jumDisetujui
      }));
      newRow.append(jumlah_disetujui);

      const bukti_tr = $('<td>').append($('<input>', {
        type: 'file',
        class: 'form-control',
        name: 'bukti_tr[]',
      }));
      newRow.append(bukti_tr);

      tableBody.append(newRow);

      // Tambahkan data baru ke dalam variabel rowData
      const newData = {
        part_number: '',
        description: '',
        warehouse_asal: '',
        jumlah_diminta: '',
        jumlah_disetujui: '',
        bukti_tr: ''
      };
      rowData.push(newData);
    }

    // Fungsi untuk menangani submit data
    $('#submit-button').on('click', function() {
      const formData = new FormData();

      // Menambahkan data dari setiap baris ke objek FormData
      rowData.forEach(function(data, index) {
        formData.append(`part_number_${index}`, data.part_number);
        formData.append(`description_${index}`, data.description);
        formData.append(`warehouse_asal_${index}`, data.warehouse_asal);
        formData.append(`jumlah_diminta_${index}`, data.jumlah_diminta);
        formData.append(`jumlah_disetujui_${index}`, data.jumlah_disetujui);
        formData.append(`bukti_tr_${index}`, data.bukti_tr);
      });
    });

    // Event listener untuk langsung menambahkan row saat dokumen di-load
    addDataInput();
    // Tampilkan modal
    $('.bs-example-modal-lg').modal('show');
  });
</script>

<script>
  // Event listener untuk perubahan nilai pada part_number
  $(document).on('change', '[name="part_number[]"]', function() {
    var selectedPN = $(this).val();
    var currentRow = $(this).closest('tr'); // Simpan referensi ke baris saat ini
    console.log(selectedPN);
    // Kirim permintaan AJAX untuk mendapatkan deskripsi berdasarkan part_number
    $.ajax({
      type: "POST",
      url: '<?= base_url('getDesc') ?>',
      dataType: 'json',
      data: {
        part_number: selectedPN
      },
      success: function(data) {
        // console.log(data);
        // console.log(data.description + ' asd');
        // Update nilai deskripsi pada baris terkait
        // console.log(currentRow.find('[name="description[]"]').val() + ' dsa');
        currentRow.find('[name="description[]"]').val(data[0].description);
      },
      error: function() {
        console.error('Error while fetching description');
      }
    });
  });

  // Event listener untuk perubahan nilai pada description
  $(document).on('change', '[name="description[]"]', function() {
    var selectedDesc = $(this).val();
    var currentRow = $(this).closest('tr'); // Simpan referensi ke baris saat ini
    console.log(selectedDesc);
    // Kirim permintaan AJAX untuk mendapatkan deskripsi berdasarkan part_number
    $.ajax({
      type: "POST",
      url: '<?= base_url('getPN') ?>',
      dataType: 'json',
      data: {
        description: selectedDesc
      },
      success: function(data) {
        // console.log(data[0]);
        // console.log(data.description + ' asd');
        // Update nilai deskripsi pada baris terkait
        // console.log(currentRow.find('[name="part_number[]"]').val() + ' dsa');
        currentRow.find('[name="part_number[]"]').val(data[0].pn);
      },
      error: function() {
        console.error('Error while fetching description');
      }
    });
  });
</script>


<?= $this->endSection(); ?>