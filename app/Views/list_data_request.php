<?= $this->extend('template/layout'); ?>

<?= $this->section('content'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xl-12 col-12">
                    <div class="row">
                        <div class="col-12 col-xl-12">
                            <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="text-center">LIST DATA REQUEST COMPONENT</h4>
                                </div>
                                <div class="box-body">
                                    <a href="<?= base_url('form/' . date('Y-m-d')); ?>"><button type=" button" class="btn-md btn-success">New Request</button></a>
                                    <br />
                                    <br />
                                    <div class="table-responsive">
                                        <table id="data_rak" class="table table-bordered table-striped" style="width:100%">
                                            <thead>
                                                <tr class="text-center">
                                                    <th>No</th>
                                                    <th>Date Request</th>
                                                    <th>Dept Pemohon</th>
                                                    <th>Part Number</th>
                                                    <th>Description</th>
                                                    <th>Warehouse Asal</th>
                                                    <th>Warehouse Tujuan</th>
                                                    <th>Scope</th>
                                                    <th>Jumlah Diminta</th>
                                                    <th>Jumlah Disetujui</th>
                                                    <th>Bukti TR</th>
                                                    <th>Detail</th>
                                                    <th>Approve</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="table-body">
                                                <?php $no = 1;
                                                foreach ($req as $v) : ?>
                                                    <tr class="text-center">
                                                        <td><?= $no++; ?></td>
                                                        <td><?= $v['date_plan']; ?></td>
                                                        <td><?= $v['dept_pemohon']; ?></td>
                                                        <td><?= $v['pn']; ?></td>
                                                        <td><?= $v['description']; ?></td>
                                                        <td><?= $v['wh_asal']; ?></td>
                                                        <td><?= $v['wh_tujuan']; ?></td>
                                                        <td><?= $v['scope']; ?></td>
                                                        <td><?= $v['jumlah_diminta']; ?></td>
                                                        <td><?= $v['jumlah_disetujui']; ?></td>
                                                        <td><img class="img-fluid" src="<?= base_url('assets/images/' . $v['bukti_tr']); ?>" alt="<?= $v['bukti_tr']; ?>"></td>
                                                        <td><?= $v['detail']; ?></td>
                                                        <td>
                                                            <button type=" button" class="btn-sm btn-primary btn-approved" data-bs-toggle="modal" data-bs-target=".bs-example-modal-lg" data-id-request="<?= $v['id_request'] ?>">
                                                                Aprroved
                                                            </button>
                                                        </td>
                                                        <td>
                                                            <a href="<?= base_url('formEdit/' . $v['id_request']); ?>"><button type="button" class="btn-sm btn-primary btn-edit">Edit</button></a>
                                                            <a href="<?= base_url('deleteRequest/' . $v['id_request']); ?>"><button type="button" class="btn-sm btn-danger" id="delete"">Delete</button></a>
                                                        </td>
                                                    </tr>
                                                <?php endforeach ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-approve -->
            <div class=" modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                                                    <div class="modal-dialog modal-lg">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h4 class="modal-title" id="myLargeModalLabel">Approved</h4>
                                                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <table class="table table-bordered table-striped" style="width:100%">
                                                                                    <thead>
                                                                                        <tr class="text-center">
                                                                                            <th>No</th>
                                                                                            <th>Admin WH Asal</th>
                                                                                            <th>Kasie PPIC</th>
                                                                                            <th>Kasie Pemohon</th>
                                                                                            <th>Admin Seksi Pemohon</th>
                                                                                            <th>Pemohon</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody id="data_approved">
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                                                                            </div>
                                                                        </div>
                                                                        <!-- /.modal-content -->
                                                                    </div>
                                                                    <!-- /.modal-dialog -->
                                    </div>
        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?= $this->endSection(); ?>

<?= $this->section('script'); ?>
<script>
    $(document).ready(function() {
        $('#data_rak').DataTable({
            "responsive": true,
            "autoWidth": false
        });
        $('.modal .select2').select2({
            dropdownParent: $('.modal')
        });

    });

    // Event listener untuk tombol "Approved"
    $(document).on('click', '.btn-approved', function() {
        // Ambil data ID Request dari tombol
        var idRequest = $(this).data('id-request');

        // Kirim permintaan AJAX untuk mendapatkan data approve berdasarkan ID Request
        $.ajax({
            type: "POST",
            url: '<?= base_url('getDataReqById') ?>',
            dataType: 'json',
            data: {
                id_request: idRequest
            },
            success: function(data) {
                console.log(data);
                var htmlRows = '<tr class="text-center">' +
                    '<td>' + '1' + '</td>' +
                    '<td>' + (data.approve_admin_wh_asal != null ? 'Approved' : 'Not Approved') + '<br />' + (data.approve_admin_wh_asal != null ? data.approve_admin_wh_asal : '-') + '<br />' + '<br />' + (data.usr_admin_wh_asal != null ? data.usr_admin_wh_asal : '-') + '</td>' +
                    '<td>' + (data.approve_kasie_ppic != null ? 'Approved' : 'Not Approved') + '<br />' + (data.approve_kasie_ppic != null ? data.approve_kasie_ppic : '-') + '<br />' + '<br />' + (data.usr_kasie_ppic != null ? data.usr_kasie_ppic : '-') + '</td>' +
                    '<td>' + (data.approve_kasie_pemohon != null ? 'Approved' : 'Not Approved') + '<br />' + (data.approve_kasie_pemohon != null ? data.approve_kasie_pemohon : '-') + '<br />' + '<br />' + (data.usr_kasie_pemohon != null ? data.usr_kasie_pemohon : '-') + '</td>' +
                    '<td>' + (data.approve_admin_seksi_pemohon != null ? 'Approved' : 'Not Approved') + '<br />' + (data.approve_admin_seksi_pemohon != null ? data.approve_admin_seksi_pemohon : '-') + '<br />' + '<br />' + (data.usr_admin_seksi_pemohon != null ? data.usr_admin_seksi_pemohon : '-') + '</td>' +
                    '<td>' + (data.created_at != null ? 'Approved' : 'Not Approved') + '<br />' + (data.created_at != null ? data.created_at : '-') + '<br />' + '<br />' + (data.usr_pemohon != null ? data.usr_pemohon : '-') + '</td>' +
                    '</tr>';
                // Menggantikan isi tbody modal dengan baris-baris yang dibuat
                $('#data_approved').html(htmlRows);

                // Tampilkan modal
                $('.bs-example-modal-lg').modal('show');
            },
            error: function() {
                console.error('Error while fetching approve data');
            }
        });
    });
</script>

<?= $this->endSection(); ?>