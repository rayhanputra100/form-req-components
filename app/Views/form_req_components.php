<?= $this->extend('template/layout'); ?>

<?= $this->section('content'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <div class="container-full">
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xl-12 col-12">
          <div class="row">
            <div class="col-12 col-xl-12">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="text-center">FORM REQUEST COMPONENT</h4>
                </div>
                <form id="request-form" action="<?= base_url() ?>input_req_component" method="post" enctype="multipart/form-data">
                  <?= csrf_field(); ?>
                  <div class="row mt-3">
                    <div class="col-3 col-xl-3">
                      <div class="box-header with-border">
                        <div  class="form-group">
                          <label class="form-label">ID Pemohon</label>
                          <div class="col-md-10">
                            <input class="form-control" type="text" name="id_pemohon" value="<?= session()->get('id_user'); ?>">
                          </div>
                        </div>

                        <div  class="form-group">
                          <label class="form-label">Username Pemohon</label>
                          <div class="col-md-10">
                            <input class="form-control" type="text" name="usr_pemohon" value="<?= session()->get('username'); ?>">
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="form-label">Date</label>
                          <div class="col-md-10">
                            <input class="form-control" type="date" value="<?= $date ?>" onchange="filterByDate()" name="date" id="filterTanggal">
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="form-label">No Registrasi</label>
                          <div class="col-md-10">
                            <input class="form-control" type="text" name="no_registrasi" value="<?= $total_regist; ?>" readonly>
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="form-label">Departement Pemohon</label>
                          <select class="form-control select2" name="dept_pemohon">
                            <option value="">--PILIH DEPARTEMENT--</option>
                            <?php foreach ($dept as $v) : ?>
                              <option value="<?= $v['name_dept']; ?>"><?= $v['name_dept']; ?></option>
                            <?php endforeach ?>
                          </select>
                        </div>

                        <div class="form-group">
                          <label class="form-label">Warehouse Tujuan</label>
                          <select class="form-control select2" name="warehouse_tujuan">
                            <option value="">--PILIH WAREHOUSE--</option>
                            <?php foreach ($wh as $v) : ?>
                              <option value="<?= $v['wh']; ?>"><?= $v['wh']; ?></option>
                            <?php endforeach ?>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-6 col-xl-6"></div>
                    <div class="col-3 col-xl-3">
                      <div class="box-header with-border">
                        <div class="form-group">
                          <label class="form-label">Scope</label>
                          <select class="form-control select2" name="scope">
                            <option value="">--PILIH SCOPE--</option>
                            <?php foreach ($scope as $v) : ?>
                              <option value="<?= $v['scope']; ?>"><?= $v['scope']; ?></option>
                            <?php endforeach ?>
                          </select>
                        </div>

                        <div class="form-group">
                          <label class="col-form-label col-md-2">Detail</label>
                          <div class="col-md-10">
                            <textarea class="form-control" placeholder="Ketik Detailnya" name="detail"></textarea>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row mt-3">
                    <div class="col-4 col-xl-2">
                      <div class="box-header with-border">
                        <div class="form-group">
                          <label class="form-label">Add & Delete Row</label>
                          <br />
                          <button id="add-button" type="button" form="request-form" class="btn btn-primary mt-3">+</button>
                          <button id="delete-button" type="button" form="request-form" class="btn btn-danger mt-3">-</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="box-body">
                    <div class="table-responsive">
                      <table id="data_rak" class="table table-bordered table-striped" style="width:100%">
                        <thead>
                          <tr class="text-center">
                            <th>Part Number</th>
                            <th>Description</th>
                            <th>Warehouse Asal</th>
                            <th>Jumlah Diminta</th>
                            <th>Jumlah Disetujui</th>
                            <th>Bukti TR</th>
                            <!-- <th>Action</th> -->
                          </tr>
                        </thead>
                        <tbody id="table-body">
                        </tbody>
                      </table>
                    </div>
                  </div>
                </form>
                <div class=" text-end">
                  <button type="submit" id="submit-button" form="request-form" class="btn btn-success col-2 col-xl-2">Submit</button>
                  <!-- <button type="button" id="submit-button" form="request-form" class="btn btn-primary col-2 col-xl-2">Submit</button> -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
</div>
<!-- /.content-wrapper -->

<?= $this->endSection(); ?>

<?= $this->section('script'); ?>
<script>
  const pn = <?= json_encode($pn) ?>;
  const wh = <?= json_encode($wh) ?>;

  $(document).ready(function() {
    $('#data_rak').DataTable({
      "responsive": true,
      "autoWidth": false
    });
    $('.modal .select2').select2({
      dropdownParent: $('.modal')
    });
  });
</script>

<script>
  $(document).ready(function() {
    let rowData = []; // Menyimpan data baris

    // Fungsi untuk mengisi data ke dalam tabel
    function addDataInput(data) {
      const tableBody = $('#table-body');
      const newRow = $('<tr>');

      const part_number = $('<td>').append(
        $('<select>', {
          class: 'form-control select2',
          name: 'part_number[]'
        }).append(
          $.map(pn, function(item) {
            return $('<option>', {
              value: item.pn,
              text: item.pn
            });
          })
        )
      );
      newRow.append(part_number);
      // Inisialisasi Select2 setelah elemen ditambahkan ke DOM
      part_number.find('select').select2();

      const desc = $('<td>').append(
        $('<select>', {
          class: 'form-control select2',
          name: 'description[]'
        }).append(
          $.map(pn, function(item) {
            return $('<option>', {
              value: item.description,
              text: item.description
            });
          })
        )
      );
      newRow.append(desc);
      desc.find('select').select2();

      const warehouse_asal = $('<td>').append(
        $('<select>', {
          class: 'form-control select2',
          name: 'warehouse_asal[]'
        }).append(
          $.map(wh, function(item) {
            return $('<option>', {
              value: item.wh,
              text: item.wh
            });
          })
        )
      );
      newRow.append(warehouse_asal);
      warehouse_asal.find('select').select2();


      const jumlah_diminta = $('<td>').append($('<input>', {
        type: 'number',
        class: 'form-control',
        name: 'jumlah_diminta[]'
      }));
      newRow.append(jumlah_diminta);

      const jumlah_disetujui = $('<td>').append($('<input>', {
        type: 'number',
        class: 'form-control',
        name: 'jumlah_disetujui[]'
      }));
      newRow.append(jumlah_disetujui);

      const bukti_tr = $('<td>').append($('<input>', {
        type: 'file',
        class: 'form-control',
        name: 'bukti_tr[]',
      }));
      newRow.append(bukti_tr);

      // const actionCell = $('<td>', {
      //   class: 'text-center'
      // }).append('-');
      // newRow.append(actionCell);

      tableBody.append(newRow);

      // Tambahkan data baru ke dalam variabel rowData
      const newData = {};
      cells.forEach(function(cell) {
        newData[cell] = newRow.find('[name="' + cell + '[]"]').val();
      });
      rowData.push(newData);

      // Reset nilai input
      cells.forEach(function(cell) {
        newRow.find('[name="' + cell + '[]"]').val('');
      });
    }

    // Fungsi untuk menambahkan input baru saat tombol "Add" diklik
    function addRowInput() {
      addDataInput([]);
    }

    // Fungsi untuk memperbarui nomor baris setelah menghapus
    function updateRowNumbers() {
      $('#table-body').children().each(function(index) {
        $(this).find('p').text('#');
      });
    }

    // Fungsi untuk menghapus baris saat tombol "Delete Row" diklik
    function deleteRow() {
      const tableBody = $('#table-body');
      if (tableBody.children().length > 0) {
        tableBody.children().last().remove();
        // updateRowNumbers(); // Perbarui nomor baris setelah menghapus
      }
    }

    // Fungsi untuk menangani submit data
    $('#submit-button').on('click', function() {
      const formData = new FormData();

      // Menambahkan data dari setiap baris ke objek FormData
      rowData.forEach(function(data, index) {
        console.log(data);
        formData.append(`part_number_${index}`, data.part_number);
        formData.append(`description_${index}`, data.description);
        formData.append(`warehouse_asal_${index}`, data.warehouse_asal);
        formData.append(`jumlah_diminta_${index}`, data.jumlah_diminta);
        formData.append(`jumlah_disetujui_${index}`, data.jumlah_disetujui);
        formData.append(`bukti_tr_${index}`, data.bukti_tr);
      });
    });

    // Event listener untuk tombol "Add"
    $('#add-button').on('click', addRowInput);

    // Event listener untuk tombol "Delete Row"
    $('#delete-button').on('click', deleteRow);
  });

  // Event listener untuk perubahan nilai pada part_number
  $(document).on('change', '[name="part_number[]"]', function() {
    var selectedPN = $(this).val();
    var currentRow = $(this).closest('tr');
    var descriptionSelect = currentRow.find('[name="description[]"]');

    console.log('ini part number : ' + selectedPN);

    $.ajax({
      type: "POST",
      url: '<?= base_url('getDesc') ?>',
      dataType: 'json',
      data: {
        part_number: selectedPN
      },
      success: function(data) {
        console.log('ini hasil fetch parnumber : ' + data[0].description);

        // Clear existing options
        descriptionSelect.empty();

        // Add new options based on the fetched data
        $.map(pn, function(item) {
          var option = $('<option>', {
            value: item.description, // Set value to data[0].description
            text: item.description
          });

          // Set selected if the option matches the fetched description
          if (item.description === data[0].description) {
            option.prop('selected', true);
          }

          descriptionSelect.append(option);
        });

        // // Trigger Select2 to update the UI
        // descriptionSelect.trigger('change');
      },
      error: function() {
        console.error('Error while fetching description');
      }
    });
  });



  // Event listener untuk perubahan nilai pada description
  $(document).on('change', '[name="description[]"]', function() {
    var selectedDesc = $(this).val();
    var currentRow = $(this).closest('tr'); // Simpan referensi ke baris saat ini
    var partNumberSelect = currentRow.find('[name="part_number[]"]');
    // console.log(selectedDesc);
    // Kirim permintaan AJAX untuk mendapatkan deskripsi berdasarkan part_number
    $.ajax({
      type: "POST",
      url: '<?= base_url('getPN') ?>',
      dataType: 'json',
      data: {
        description: selectedDesc
      },
      success: function(data) {
        console.log('ini hasil fetch desc : ' + data[0].pn);

        // Clear existing options
        partNumberSelect.empty();

        // Add new options based on the fetched data
        $.map(pn, function(item) {
          var option = $('<option>', {
            value: item.pn, // Set value to data[0].pn
            text: item.pn
          });

          // Set selected if the option matches the fetched pn
          if (item.pn === data[0].pn) {
            option.prop('selected', true);
          }

          partNumberSelect.append(option);
        });

        // // Trigger Select2 to update the UI
        // partNumberSelect.trigger('change');
      },
      error: function() {
        console.error('Error while fetching description');
      }
    });
  });

  // Event listener untuk tombol "Approved"
  $(document).on('click', '.btn-approved', function() {
    // Ambil data ID Request dari tombol
    var idRequest = $(this).data('id-request');

    // Kirim permintaan AJAX untuk mendapatkan data approve berdasarkan ID Request
    $.ajax({
      type: "POST",
      url: '<?= base_url('getDataReqById') ?>',
      dataType: 'json',
      data: {
        id_request: idRequest
      },
      success: function(data) {
        console.log(data);
        var htmlRows = '<tr class="text-center">' +
          '<td>' + '1' + '</td>' +
          '<td>' + (data.approve_admin_wh_asal != null ? 'Approved' : 'Not Approved') + '<br />' + (data.approve_admin_wh_asal != null ? data.approve_admin_wh_asal : '-') + '<br />' + '<br />' + (data.usr_admin_wh_asal != null ? data.usr_admin_wh_asal : '-') + '</td>' +
          '<td>' + (data.approve_kasie_ppic != null ? 'Approved' : 'Not Approved') + '<br />' + (data.approve_kasie_ppic != null ? data.approve_kasie_ppic : '-') + '<br />' + '<br />' + (data.usr_kasie_ppic != null ? data.usr_kasie_ppic : '-') + '</td>' +
          '<td>' + (data.approve_kasie_pemohon != null ? 'Approved' : 'Not Approved') + '<br />' + (data.approve_kasie_pemohon != null ? data.approve_kasie_pemohon : '-') + '<br />' + '<br />' + (data.usr_kasie_pemohon != null ? data.usr_kasie_pemohon : '-') + '</td>' +
          '<td>' + (data.approve_admin_seksi_pemohon != null ? 'Approved' : 'Not Approved') + '<br />' + (data.approve_admin_seksi_pemohon != null ? data.approve_admin_seksi_pemohon : '-') + '<br />' + '<br />' + (data.usr_admin_seksi_pemohon != null ? data.usr_admin_seksi_pemohon : '-') + '</td>' +
          '<td>' + (data.created_at != null ? 'Approved' : 'Not Approved') + '<br />' + (data.created_at != null ? data.created_at : '-') + '<br />' + '<br />' + (data.usr_pemohon != null ? data.usr_pemohon : '-') + '</td>' +
          '</tr>';
        // Menggantikan isi tbody modal dengan baris-baris yang dibuat
        $('#data_approved').html(htmlRows);

        // Tampilkan modal
        $('.bs-example-modal-lg').modal('show');
      },
      error: function() {
        console.error('Error while fetching approve data');
      }
    });
  });

  function filterByDate() {
    var tanggal = $('#filterTanggal').val();
    window.location.replace('<?= base_url(); ?>form/' + tanggal);
  }
</script>


<?= $this->endSection(); ?>