
<!doctype html>
<html lang="en" class="no-focus">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

	<title>Print Request Component</title>

	<!-- Icons -->
	<!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
	<link rel="shortcut icon" href="<?= base_url() . 'assets/images/favicons/favicon.png' ?>">
	<link rel="icon" type="image/png" sizes="192x192" href="<?= base_url() . 'assets/images/favicons/favicon-192x192.png' ?>">
	<link rel="apple-touch-icon" sizes="180x180" href="<?= base_url() . 'assets/images/favicons/apple-touch-icon-180x180.png' ?>">
	<!-- END Icons -->

	<link rel="stylesheet" href="<?= base_url() ?>assets/dataTables.dateTime.min.css">


	<!-- Vendors Style-->
	<link rel="stylesheet" href="<?= base_url() ?>assets/template/main/css/vendors_css.css">

	<!-- Style-->
	<link rel="stylesheet" href="<?= base_url() ?>assets/template/main/css/style.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/template/main/css/skin_color.css">

</head>

<body onload="window.print()">
	<div id="page-container" class="sidebar-mini sidebar-o sidebar-inverse enable-page-overlay side-scroll page-header-fixed page-header-glass page-header-inverse main-content-boxed">
		<div class="row d-flex justify-content-center">
			<div class="col-2"></div>
			<div class="col-6">
				<h2 class="text-center">Print Request Component</h2>
				<P class="text-center">*************************************************************************************</P>
				<div class="row">
					<div class="col-4">
						<h5 class="text-center">Scope :</h5>
						<p class="text-center font-size-md"><?= $req['scope']; ?></p>
						<br />
						<h5 class="text-center">Date Plan :</h5>
						<p class="text-center font-size-md"><?= $req['date_plan']; ?></p>
					</div>
					<div class="col-4">
						<h5 class="text-center">WH Asal :</h5>
						<p class="text-center font-size-md"><?= $req['wh_asal']; ?></p>
						<br />
						<h5 class="text-center">WH Tujuan :</h5>
						<p class="text-center font-size-md"><?= $req['wh_tujuan']; ?></p>
					</div>
					<div class="col-4">
						<h5 class="text-center">Diminta :</h5>
						<p class="text-center font-size-md"><?= $req['jumlah_diminta']; ?></p>
						<br />
						<h5 class="text-center">Disetujui :</h5>
						<p class="text-center font-size-md"><?= $req['jumlah_disetujui']; ?></p>
					</div>
				</div>
			</div>
			<div class="col-4 align-self-center">
				<div class="text-center">
					<!-- <img class="img-fluid" src="https://chart.googleapis.com/chart?chs=320x320&cht=qr&chl=<?= $req['id_request']; ?>&choe=UTF-8" alt=""> -->
					<img src="<?= base_url('assets/images/qrcode/item-' . $req['qr'] . '.png') ?>" alt="">
				</div>

				<h4 class="text-center"><?= $req['pn']; ?></h4>
			</div>
		</div>

	</div>
	<!-- END Page Container -->
<!-- Vendor JS -->
<script src="<?= base_url() ?>assets/template/main/js/vendors.min.js"></script>
	<script src="<?= base_url() ?>assets/template/main/js/pages/chat-popup.js"></script>
	<script src="<?= base_url() ?>assets/template/assets/icons/feather-icons/feather.min.js"></script>

	<script src="<?= base_url() ?>assets/template/assets/vendor_components/apexcharts-bundle/dist/apexcharts.js"></script>
	<script src="<?= base_url() ?>assets/template/assets/vendor_components/moment/min/moment.min.js"></script>
	<script src="<?= base_url() ?>assets/template/assets/vendor_components/fullcalendar/fullcalendar.js"></script>
	<script src="<?= base_url() ?>assets/template/assets/vendor_components/datatable/datatables.min.js"></script>
	<script src="<?= base_url() ?>assets/template/assets/vendor_components/select2/dist/js/select2.full.js"></script>
	<script src="<?= base_url() ?>assets/template/assets/vendor_components/sweetalert/sweetalert.min.js"></script>
    <script src="<?= base_url() ?>assets/template/assets/vendor_components/sweetalert/jquery.sweet-alert.custom.js"></script>

	<!-- EduAdmin App -->
	<script src="<?= base_url() ?>assets/template/main/js/template.js"></script>
	<script src="<?= base_url() ?>assets/template/main/js/pages/dashboard.js"></script>
	<script src="<?= base_url() ?>assets/template/main/js/pages/calendar.js"></script>
	<!-- <script src="<?= base_url() ?>assets/template/main/js/pages/data-table.js"></script> -->
	<script src="<?= base_url() ?>assets/template/main/js/pages/advanced-form-element.js"></script>



</body>

</html>
