<?= $this->extend('template/layout'); ?>

<?= $this->section('content'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xl-12 col-12">
                    <div class="row">
                        <div class="col-12 col-xl-12">
                            <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="text-center">FORM APPROVE REQUEST COMPONENT</h4>
                                </div>
                                <form id="request-form" action="<?= base_url() ?>approved" method="post">
                                    <?= csrf_field(); ?>
                                    <div class="form-group">
                                        <label class="form-label">Date</label>
                                        <div class="col-md-10">
                                            <input class="form-control" type="date" value="<?= $date ?>" onchange="filterByDate()" name="date" id="filterTanggal">
                                        </div>
                                    </div>
                                </form>
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table id="data_rak" class="table table-bordered table-striped" style="width:100%">
                                            <thead>
                                                <tr class="text-center">
                                                    <th>No</th>
                                                    <th>Date Request</th>
                                                    <th>Dept Pemohon</th>
                                                    <th>Part Number</th>
                                                    <th>Description</th>
                                                    <th>Warehouse Asal</th>
                                                    <th>Warehouse Tujuan</th>
                                                    <th>Scope</th>
                                                    <th>Jumlah Diminta</th>
                                                    <th>Jumlah Disetujui</th>
                                                    <th>Bukti TR</th>
                                                    <th>Detail</th>
                                                    <th>Approve</th>
                                                </tr>
                                            </thead>
                                            <tbody id="table-body">
                                                <?php $no = 1;
                                                foreach ($req as $v) : ?>
                                                    <tr class="text-center">
                                                        <td><?= $no++; ?></td>
                                                        <td><?= $v['date_plan']; ?></td>
                                                        <td><?= $v['dept_pemohon']; ?></td>
                                                        <td><?= $v['pn']; ?></td>
                                                        <td><?= $v['description']; ?></td>
                                                        <td><?= $v['wh_asal']; ?></td>
                                                        <td><?= $v['wh_tujuan']; ?></td>
                                                        <td><?= $v['scope']; ?></td>
                                                        <td><?= $v['jumlah_diminta']; ?></td>
                                                        <td><?= $v['jumlah_disetujui']; ?></td>
                                                        <td><img class="img-fluid" src="<?= base_url('assets/images/' . $v['bukti_tr']); ?>" alt="<?= $v['bukti_tr']; ?>"></td>
                                                        <td><?= $v['detail']; ?></td>
                                                        <?php if ($keterangan === 'ADMIN WH ASAL') : ?>
                                                            <?php if ($v['usr_admin_wh_asal'] === null && $v['approve_admin_wh_asal'] === null) : ?>
                                                                <td>
                                                                    <a href="<?= base_url('approved/' . $v['id_request'] . '/' . $v['date_plan']); ?>"><button class="btn-sm btn-success">Approve</button></a>
                                                                </td>
                                                            <?php else : ?>
                                                                <td class="bg-success text-center">
                                                                    Approved <br />
                                                                    <a target="_blank" href="<?= base_url('printCom/' . $v['id_request']); ?>"><button class="btn-sm btn-primary">Print</button></a>
                                                                </td>
                                                            <?php endif; ?>
                                                        <?php elseif ($keterangan === 'KASIE PPIC') : ?>
                                                            <?php if ($v['usr_kasie_ppic'] === null && $v['approve_kasie_ppic'] === null) : ?>
                                                                <td>
                                                                    <a href="<?= base_url('approved/' . $v['id_request'] . '/' . $v['date_plan']); ?>"><button class="btn-sm btn-success">Approve</button></a>
                                                                </td>
                                                            <?php else : ?>
                                                                <td class="bg-success">Approved</td>
                                                            <?php endif; ?>
                                                        <?php elseif ($keterangan === 'KASIE PEMOHON') : ?>
                                                            <?php if ($v['usr_kasie_pemohon'] === null && $v['approve_kasie_pemohon'] === null) : ?>
                                                                <td>
                                                                    <a href="<?= base_url('approved/' . $v['id_request'] . '/' . $v['date_plan']); ?>"><button class="btn-sm btn-success">Approve</button></a>
                                                                </td>
                                                            <?php else : ?>
                                                                <td class="bg-success">Approved</td>
                                                            <?php endif; ?>
                                                        <?php elseif ($keterangan === 'ADMIN SEKSI PEMOHON') : ?>
                                                            <?php if ($v['usr_admin_seksi_pemohon'] === null && $v['approve_admin_seksi_pemohon'] === null) : ?>
                                                                <td>
                                                                    <a href="<?= base_url('approved/' . $v['id_request'] . '/' . $v['date_plan']); ?>"><button class="btn-sm btn-success">Approve</button></a>
                                                                </td>
                                                            <?php else : ?>
                                                                <td class="bg-success">Approved</td>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </tr>
                                                <?php endforeach ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?= $this->endSection(); ?>

<?= $this->section('script'); ?>
<script>
    $(document).ready(function() {
        $('#data_rak').DataTable({
            "responsive": true,
            "autoWidth": false
        });
        $('.modal .select2').select2({
            dropdownParent: $('.modal')
        });

    });

    function filterByDate() {
        var tanggal = $('#filterTanggal').val();
        window.location.replace('<?= base_url(); ?>approve/' + tanggal);
    }
</script>

<?= $this->endSection(); ?>