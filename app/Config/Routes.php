<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */

// LOGIN
$routes->get('/', 'Login::index');
$routes->post('/proses_login', 'Login::proses_login');
$routes->get('/logout', 'Login::logout');

// LIST DATA
$routes->get('/list_data', 'ListData::index');

// FORM INPUT
$routes->get('/form/(:any)', 'Form::index/$1');
$routes->post('/input_req_component', 'Form::input_data_request');

// FORM EDIT
$routes->get('/formEdit/(:any)', 'Form::formEdit/$1');
$routes->post('/edit_data_request/(:any)', 'Form::edit_data_request/$1');

// DELETE
$routes->get('/deleteRequest/(:any)', 'Form::deleteRequest/$1');

// FORM APPROVE
$routes->get('/approve/(:any)', 'Form::formApprove/$1');
$routes->get('/approved/(:any)/(:any)', 'Form::approved/$1/$2');

// API
$routes->post('/getDesc', 'Form::getDesc');
$routes->post('/getPN', 'Form::getPN');
$routes->post('/getDataReqById', 'Form::getDataReqById');

// PRINT
$routes->get('/printCom/(:any)', 'Prints::index/$1');