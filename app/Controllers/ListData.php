<?php

namespace App\Controllers;

use App\Models\M_Request;

class ListData extends BaseController
{
    protected $m_req;

    public function __construct()
    {
        $this->m_req = new M_Request();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index(): string
    {
        if (session()->get('id_user') == null || session()->get('id_user') == '') {
            return redirect()->to(base_url('login'));
        } else {
            $dataReq = $this->m_req->getAllData();
            // dd($dataReq);
            $datas = [
                'req' => $dataReq,
            ];

            return view('list_data_request', $datas);
        }
    }
}
