<?php

namespace App\Controllers;

use App\Models\M_Request;

class Prints extends BaseController
{
    protected $m_req;

    public function __construct()
    {
        $this->m_req = new M_Request();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index($id_request): string
    {
        $dataReq = $this->m_req->getDataById($id_request);
        // dd($dataReq);

        $datas = [
            'req' => $dataReq,
        ];

        return view('print_request', $datas);
    }
}