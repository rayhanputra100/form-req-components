<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\M_Login;
use App\Models\M_Request;

class Login extends BaseController
{
    protected $M_Login;
    protected $m_req;
    protected $session;

    public function __construct()
    {
        $this->M_Login = new M_Login();
        $this->m_req = new M_Request();
        $this->session = \Config\Services::session();
        date_default_timezone_set("Asia/Jakarta");
    }
    public function index()
    {
        return view('login');
    }

    public function proses_login()
    {
        $username = strtoupper($this->request->getPost('username'));
        $password = $this->request->getPost('password');

        $date =  date('Y-m-d');

        $data = $this->M_Login->cek_login($username, $password);

        if (!empty($data)) {
            $session_data = [
                'id_user' => $data['id_user'],
                'username' => $data['username'],
                'level' => $data['level'],
                'id_dept' => $data['id_dept'],
                'keterangan' => $data['keterangan'],
                'is_login' => true
            ];
            $this->session->set($session_data);

            if ($data['level'] == 1) {
                return redirect()->to(base_url('approve/' . $date));
            } else {
                return redirect()->to(base_url('form/' . $date));
            }
        } else {
            $this->session->setFlashdata('error', 'Username atau Password Salah');
            return redirect()->to(base_url());
        }
    }

    public function logout()
    {
        $this->session->destroy();
        return redirect()->to(base_url());
    }
}
