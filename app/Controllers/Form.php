<?php

namespace App\Controllers;

use App\Models\M_Departement;
use App\Models\M_PartNumber;
use App\Models\M_Scope;
use App\Models\M_Warehouse;
use App\Models\M_Users;
use App\Models\M_Request;
use Endroid\QrCode\Color\Color;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelLow;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use Endroid\QrCode\Writer\PngWriter;

class Form extends BaseController
{
    protected $m_dept;
    protected $m_pn;
    protected $m_scope;
    protected $m_wh;
    protected $m_users;
    protected $m_req;

    public function __construct()
    {
        $this->m_dept = new M_Departement();
        $this->m_pn = new M_PartNumber();
        $this->m_req = new M_Request();
        $this->m_scope = new M_Scope();
        $this->m_wh = new M_Warehouse();
        $this->m_users = new M_Users();

        date_default_timezone_set("Asia/Jakarta");
    }

    public function index($dates = null): string
    {
        if (session()->get('id_user') == null || session()->get('id_user') == '') {
            return redirect()->to(base_url('login'));
        } else {
            $id_pemohon = session()->get('id_user');
            $date = ($dates === null) ? date('Y-m-d') : $dates;

            $dataReq = $this->m_req->getData($date, $id_pemohon);
            $dataAllReq = $this->m_req->getAllData();
            // Hitung jumlah data
            $totalData = count($dataAllReq);

            // Format angka dengan panjang tetap enam digit
            $formattedTotal = sprintf('%06d', $totalData + 1);
            // dd($formattedTotal);

            $datas = [
                'dept' => $this->m_dept->getData(),
                'wh' => $this->m_wh->getData(),
                'scope' => $this->m_scope->getData(),
                'pn' => $this->m_pn->getData(),
                'date' => $date,
                'req' => $dataReq,
                'total_regist' => $formattedTotal
            ];

            return view('form_req_components', $datas);
        }
    }

    public function formApprove($dates = null): string
    {
        if (session()->get('id_user') == null || session()->get('id_user') == '') {
            return redirect()->to(base_url('login'));
        } else {
            $keterangan = session()->get('keterangan');
            $date = ($dates === null) ? date('Y-m-d') : $dates;

            $dataReq = $this->m_req->getAllData($date);
            // dd($dataReq);
            $datas = [
                'date' => $date,
                'req' => $dataReq,
                'keterangan' => $keterangan
            ];

            return view('approve_request', $datas);
        }
    }

    public function formEdit($id_request): string
    {
        if (session()->get('id_user') == null || session()->get('id_user') == '') {
            return redirect()->to(base_url('login'));
        } else {
            $dataReq = $this->m_req->getDataById($id_request);
            // dd($dataReq);
            $datas = [
                'dept' => $this->m_dept->getData(),
                'wh' => $this->m_wh->getData(),
                'scope' => $this->m_scope->getData(),
                'pn' => $this->m_pn->getData(),
                'req' => $dataReq,
            ];

            return view('form_edit_req_components', $datas);
        }
    }


    public function getDesc()
    {
        $pn = $this->request->getPost('part_number') ?? [];

        if (!empty($pn)) {
            $data = $this->m_pn->getDesc($pn);
            echo json_encode($data);
        } else {
            echo json_encode(['description' => '']);
        }
    }

    public function getPN()
    {
        $desc = $this->request->getPost('description');
        if (!empty($desc)) {
            $data = $this->m_pn->getPN($desc);
            echo json_encode($data);
        } else {
            echo json_encode(['part_number' => '']);
        }
    }

    public function getDataReqById()
    {
        $id_request = $this->request->getPost('id_request');

        if (!empty($id_request)) {
            $data = $this->m_req->getDataById($id_request);
            echo json_encode($data);
        } else {
            echo json_encode(['id_request' => '']);
        }
    }

    public function approved($id_request)
    {
        // dd($id_request);
        $dataAllReq = $this->m_req->getDataById($id_request);

        $keterangan = session()->get('keterangan');
        $approve_admin_wh_asal = $dataAllReq['approve_admin_wh_asal'];
        $approve_kasie_ppic = $dataAllReq['approve_kasie_ppic'];
        $approve_kasie_pemohon = $dataAllReq['approve_kasie_pemohon'];
        $approve_admin_seksi_pemohon = $dataAllReq['approve_admin_seksi_pemohon'];

        $user = session()->get('username');
        $datetime = date('Y-m-d H:i:s');

        $dateApporve = date('Y-m-d', strtotime($dataAllReq['created_at']));

        $year = substr(date('Y', strtotime($dateApporve)), -2);
        $month = date('m', strtotime($dateApporve));

        $combineCodeQR = $id_request . $dataAllReq['id_pemohon'] . $month . $year;

        $data = [];

        if ($keterangan === 'ADMIN SEKSI PEMOHON') {
            if ($approve_admin_seksi_pemohon == null && $approve_kasie_pemohon == null && $approve_kasie_ppic == null && $approve_admin_wh_asal == null) {
                $data['usr_admin_seksi_pemohon'] = $user;
                $data['approve_admin_seksi_pemohon'] = $datetime;
                $this->m_req->updateData($id_request, $data);
                return "<script type='text/javascript'>
                alert('Approve Success');
                window.location.href = '" . base_url() . "approve/" . $dateApporve . "';
              </script>";
            } else {
                return "<script type='text/javascript'>
                alert('Approve Gagal');
                window.location.href = '" . base_url() . "approve/" . $dateApporve . "';
              </script>";
            }
        } elseif ($keterangan === 'KASIE PEMOHON') {
            if ($approve_admin_seksi_pemohon == !null && $approve_kasie_pemohon == null && $approve_kasie_ppic == null && $approve_admin_wh_asal == null) {
                $data['usr_kasie_pemohon'] = $user;
                $data['approve_kasie_pemohon'] = $datetime;
                $this->m_req->updateData($id_request, $data);
                return "<script type='text/javascript'>
                alert('Approve Success');
                window.location.href = '" . base_url() . "approve/" . $dateApporve . "';
              </script>";
            } else {
                if ($approve_admin_seksi_pemohon == null) {
                    return "<script type='text/javascript'>
                    alert('Approve Gagal Karena ADMIN SEKSI PEMOHON Belum Melakukan Approval');
                    window.location.href = '" . base_url() . "approve/" . $dateApporve . "';
                  </script>";
                }
            }
        } elseif ($keterangan === 'KASIE PPIC') {
            if ($approve_admin_seksi_pemohon == !null && $approve_kasie_pemohon == !null && $approve_kasie_ppic == null && $approve_admin_wh_asal == null) {
                $data['usr_kasie_ppic'] = $user;
                $data['approve_kasie_ppic'] = $datetime;
                $this->m_req->updateData($id_request, $data);
                return "<script type='text/javascript'>
                alert('Approve Success');
                window.location.href = '" . base_url() . "approve/" . $dateApporve . "';
              </script>";
            } else {
                if ($approve_admin_seksi_pemohon == null) {
                    return "<script type='text/javascript'>
                    alert('Approve Gagal Karena ADMIN SEKSI PEMOHON Belum Melakukan Approval');
                    window.location.href = '" . base_url() . "approve/" . $dateApporve . "';
                  </script>";
                } elseif ($approve_kasie_pemohon == null) {
                    return "<script type='text/javascript'>
                    alert('Approve Gagal Karena KASIE PEMOHON Belum Melakukan Approval');
                    window.location.href = '" . base_url() . "approve/" . $dateApporve . "';
                  </script>";
                } else {
                    return "<script type='text/javascript'>
                    alert('Approve Gagal Karena ADMIN SEKSI PEMOHON / KASIE PEMOHON Belum Melakukan Approval');
                    window.location.href = '" . base_url() . "approve/" . $dateApporve . "';
                  </script>";
                }
            }
        } elseif ($keterangan === 'ADMIN WH ASAL') {
            if ($approve_admin_seksi_pemohon == !null && $approve_kasie_pemohon == !null && $approve_kasie_ppic == !null && $approve_admin_wh_asal == null) {
                $data['usr_admin_wh_asal'] = $user;
                $data['approve_admin_wh_asal'] = $datetime;
                $data['qr'] = $combineCodeQR;

                $writer = new PngWriter();

                // Create QR code
                $qrCode = QrCode::create($id_request)
                    ->setEncoding(new Encoding('UTF-8'))
                    ->setErrorCorrectionLevel(new ErrorCorrectionLevelLow())
                    ->setSize(300)
                    ->setMargin(10)
                    ->setRoundBlockSizeMode(new RoundBlockSizeModeMargin())
                    ->setForegroundColor(new Color(0, 0, 0))
                    ->setBackgroundColor(new Color(255, 255, 255));

                $result = $writer->write($qrCode);
                $result->saveToFile('assets/images/qrcode/item-' . $combineCodeQR . '.png');

                $this->m_req->updateData($id_request, $data);
                return "<script type='text/javascript'>
                alert('Approve Success');
                window.location.href = '" . base_url() . "approve/" . $dateApporve . "';
              </script>";
            } else {
                if ($approve_admin_seksi_pemohon == null) {
                    return "<script type='text/javascript'>
                    alert('Approve Gagal Karena ADMIN SEKSI PEMOHON Belum Melakukan Approval');
                    window.location.href = '" . base_url() . "approve/" . $dateApporve . "';
                  </script>";
                } elseif ($approve_kasie_ppic == null) {
                    return "<script type='text/javascript'>
                    alert('Approve Gagal Karena KASIE PPIC PEMOHON Belum Melakukan Approval');
                    window.location.href = '" . base_url() . "approve/" . $dateApporve . "';
                  </script>";
                } elseif ($approve_kasie_pemohon == null) {
                    return "<script type='text/javascript'>
                    alert('Approve Gagal Karena KASIE PEMOHON Belum Melakukan Approval');
                    window.location.href = '" . base_url() . "approve/" . $dateApporve . "';
                  </script>";
                } else {
                    return "<script type='text/javascript'>
                    alert('Approve Gagal Karena ADMIN SEKSI PEMOHON / KASIE PEMOHON / KASIE PPIC Belum Melakukan Approval');
                    window.location.href = '" . base_url() . "approve/" . $dateApporve . "';
                  </script>";
                }
            }
        }
    }

    public function input_data_request()
    {
        // Tangkap data dari formulir
        $partNumber = $this->request->getPost('part_number') ?? [];
        $idPemohon = $this->request->getPost('id_pemohon') ?? [];
        $usrPemohon = $this->request->getPost('usr_pemohon') ?? [];
        $description = $this->request->getPost('description') ?? [];
        $warehouseAsal = $this->request->getPost('warehouse_asal') ?? [];
        $jumlahDiminta = $this->request->getPost('jumlah_diminta') ?? [];
        $jumlahDisetujui = $this->request->getPost('jumlah_disetujui') ?? [];
        $date = $this->request->getPost('date') ?? [];
        $dept_pemohon = $this->request->getPost('dept_pemohon') ?? [];
        $scope = $this->request->getPost('scope') ?? [];
        $detail = $this->request->getPost('detail') ?? [];
        $warehouse_tujuan = $this->request->getPost('warehouse_tujuan') ?? [];

        $imagefile = $this->request->getFiles();
        $dataAllReq = $this->m_req->getAllData();
        $totalData = count($dataAllReq);
        $savedData = [];

        // Iterasi untuk menyimpan file dan data lainnya
        foreach ($partNumber as $i => $part) {

            // Tangkap file yang diunggah
            $img = $imagefile['bukti_tr'][$i];

            // Periksa apakah file valid dan belum dipindahkan
            if ($img->isValid() && !$img->hasMoved()) {
                // Pindahkan file ke folder penyimpanan
                $newName = $img->getName();
                $img->move(WRITEPATH . '../public/assets/images/', $newName);

                // Data yang akan disimpan
                $data = [
                    'usr_pemohon' => $usrPemohon,
                    'id_pemohon' => $idPemohon,
                    'no_registrasi' => sprintf('%06d', ++$totalData),
                    'date_plan' => $date,
                    'dept_pemohon' => $dept_pemohon,
                    'scope' => $scope,
                    'detail' => $detail,
                    'wh_tujuan' => $warehouse_tujuan,
                    'pn' => $part,
                    'description' => $description[$i],
                    'wh_asal' => $warehouseAsal[$i],
                    'jumlah_diminta' => $jumlahDiminta[$i],
                    'jumlah_disetujui' => $jumlahDisetujui[$i],
                    'bukti_tr' => $newName,
                ];

                // Simpan data ke dalam database atau lakukan operasi lainnya
                $this->m_req->insert($data);

                // Tambahkan data yang berhasil disimpan ke dalam array
                $savedData[] = $data;
            } else {
                // Data yang akan disimpan
                $data = [
                    'usr_pemohon' => $usrPemohon,
                    'id_pemohon' => $idPemohon,
                    'no_registrasi' => sprintf('%06d', ++$totalData),
                    'date_plan' => $date,
                    'dept_pemohon' => $dept_pemohon,
                    'scope' => $scope,
                    'detail' => $detail,
                    'wh_tujuan' => $warehouse_tujuan,
                    'pn' => $part,
                    'description' => $description[$i],
                    'wh_asal' => $warehouseAsal[$i],
                    'jumlah_diminta' => $jumlahDiminta[$i],
                    'jumlah_disetujui' => $jumlahDisetujui[$i],
                ];

                // Simpan data ke dalam database atau lakukan operasi lainnya
                $this->m_req->insert($data);

                // Tambahkan data yang berhasil disimpan ke dalam array
                $savedData[] = $data;
            }
        }

        return redirect()->to(base_url('list_data'));
    }

    public function edit_data_request($id_request)
    {
        // Tangkap data dari formulir
        $partNumber = $this->request->getPost('part_number') ?? [];
        $idPemohon = $this->request->getPost('id_pemohon') ?? [];
        $usrPemohon = $this->request->getPost('usr_pemohon') ?? [];
        $description = $this->request->getPost('description') ?? [];
        $warehouseAsal = $this->request->getPost('warehouse_asal') ?? [];
        $jumlahDiminta = $this->request->getPost('jumlah_diminta') ?? [];
        $jumlahDisetujui = $this->request->getPost('jumlah_disetujui') ?? [];
        $date = $this->request->getPost('date') ?? [];
        $dept_pemohon = $this->request->getPost('dept_pemohon') ?? [];
        $scope = $this->request->getPost('scope') ?? [];
        $detail = $this->request->getPost('detail') ?? [];
        $warehouse_tujuan = $this->request->getPost('warehouse_tujuan') ?? [];

        $imagefile = $this->request->getFiles();
        $savedData = [];

        // Iterasi untuk menyimpan file dan data lainnya
        foreach ($partNumber as $i => $part) {
            // Tangkap file yang diunggah
            $img = $imagefile['bukti_tr'][$i];

            // Periksa apakah file valid dan belum dipindahkan
            if ($img->isValid() && !$img->hasMoved()) {
                // Pindahkan file ke folder penyimpanan
                $newName = $img->getName();
                $img->move(WRITEPATH . '../public/assets/images/', $newName);

                // Data yang akan disimpan
                $data = [
                    'usr_pemohon' => $usrPemohon,
                    'id_pemohon' => $idPemohon,
                    'date_plan' => $date,
                    'dept_pemohon' => $dept_pemohon,
                    'scope' => $scope,
                    'detail' => $detail,
                    'wh_tujuan' => $warehouse_tujuan,
                    'pn' => $part,
                    'description' => $description[$i],
                    'wh_asal' => $warehouseAsal[$i],
                    'jumlah_diminta' => $jumlahDiminta[$i],
                    'jumlah_disetujui' => $jumlahDisetujui[$i],
                    'bukti_tr' => $newName,
                ];

                // Simpan data ke dalam database atau lakukan operasi lainnya
                $this->m_req->updateData($id_request, $data);

                // Tambahkan data yang berhasil disimpan ke dalam array
                $savedData[] = $data;
            } else {
                // Data yang akan disimpan
                $data = [
                    'usr_pemohon' => $usrPemohon,
                    'id_pemohon' => $idPemohon,
                    'date_plan' => $date,
                    'dept_pemohon' => $dept_pemohon,
                    'scope' => $scope,
                    'detail' => $detail,
                    'wh_tujuan' => $warehouse_tujuan,
                    'pn' => $part,
                    'description' => $description[$i],
                    'wh_asal' => $warehouseAsal[$i],
                    'jumlah_diminta' => $jumlahDiminta[$i],
                    'jumlah_disetujui' => $jumlahDisetujui[$i],
                ];

                // Simpan data ke dalam database atau lakukan operasi lainnya
                $this->m_req->updateData($id_request, $data);

                // Tambahkan data yang berhasil disimpan ke dalam array
                $savedData[] = $data;
            }
        }

        // dd($savedData);
        return redirect()->to(base_url('list_data'));
    }

    public function deleteRequest($id_request)
    {
        // dd($id_request);
        $this->m_req->deleteRequest($id_request);
        return redirect()->to(base_url('list_data'));
    }
}
