USE [db_component]
GO
/****** Object:  Table [dbo].[departement]    Script Date: 11/20/2023 11:55:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[departement](
	[id_dept] [int] IDENTITY(1,1) NOT NULL,
	[name_dept] [varchar](50) NULL,
	[created_at] [datetime] NULL,
 CONSTRAINT [PK_dapartement] PRIMARY KEY CLUSTERED 
(
	[id_dept] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[part_number]    Script Date: 11/20/2023 11:55:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[part_number](
	[id_pn] [varchar](50) NOT NULL,
	[pn] [varchar](50) NULL,
	[description] [varchar](50) NULL,
	[created_at] [datetime] NULL,
 CONSTRAINT [PK_part_number] PRIMARY KEY CLUSTERED 
(
	[id_pn] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[request]    Script Date: 11/20/2023 11:55:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[request](
	[id_request] [int] IDENTITY(1,1) NOT NULL,
	[no_registrasi] [int] NULL,
	[date_plan] [date] NULL,
	[id_pemohon] [int] NULL,
	[usr_pemohon] [varchar](50) NULL,
	[scope] [varchar](50) NULL,
	[dept_pemohon] [varchar](50) NULL,
	[detail] [text] NULL,
	[pn] [varchar](50) NULL,
	[description] [varchar](50) NULL,
	[wh_tujuan] [varchar](50) NULL,
	[wh_asal] [varchar](50) NULL,
	[bukti_tr] [varchar](50) NULL,
	[jumlah_diminta] [int] NULL,
	[jumlah_disetujui] [int] NULL,
	[usr_admin_wh_asal] [varchar](50) NULL,
	[approve_admin_wh_asal] [datetime] NULL,
	[usr_kasie_ppic] [varchar](50) NULL,
	[approve_kasie_ppic] [datetime] NULL,
	[usr_kasie_pemohon] [varchar](50) NULL,
	[approve_kasie_pemohon] [datetime] NULL,
	[usr_admin_seksi_pemohon] [varchar](50) NULL,
	[approve_admin_seksi_pemohon] [datetime] NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
 CONSTRAINT [PK_request] PRIMARY KEY CLUSTERED 
(
	[id_request] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[scope]    Script Date: 11/20/2023 11:55:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[scope](
	[id_scope] [int] IDENTITY(1,1) NOT NULL,
	[scope] [varchar](50) NULL,
	[created_at] [datetime] NULL,
 CONSTRAINT [PK_scope] PRIMARY KEY CLUSTERED 
(
	[id_scope] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[users]    Script Date: 11/20/2023 11:55:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[users](
	[id_user] [int] IDENTITY(1,1) NOT NULL,
	[id_dept] [int] NOT NULL,
	[level] [int] NULL,
	[username] [varchar](50) NULL,
	[password] [varchar](50) NULL,
	[keterangan] [varchar](50) NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
 CONSTRAINT [PK_users] PRIMARY KEY CLUSTERED 
(
	[id_user] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[warehouse]    Script Date: 11/20/2023 11:55:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[warehouse](
	[id_wh] [int] IDENTITY(1,1) NOT NULL,
	[wh] [varchar](50) NULL,
	[created_at] [datetime] NULL,
 CONSTRAINT [PK_warehouse] PRIMARY KEY CLUSTERED 
(
	[id_wh] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[departement] ON 
GO
INSERT [dbo].[departement] ([id_dept], [name_dept], [created_at]) VALUES (1, N'INDUSTRIAL SYSTEM & DEVELOPMENT', CAST(N'2023-11-17T23:13:34.133' AS DateTime))
GO
INSERT [dbo].[departement] ([id_dept], [name_dept], [created_at]) VALUES (2, N'PRODUCT ENGINEERING', CAST(N'2023-11-17T23:13:34.270' AS DateTime))
GO
INSERT [dbo].[departement] ([id_dept], [name_dept], [created_at]) VALUES (3, N'MAINTENANCE', CAST(N'2023-11-17T23:13:34.347' AS DateTime))
GO
INSERT [dbo].[departement] ([id_dept], [name_dept], [created_at]) VALUES (4, N'PROCESS ENGINEERING', CAST(N'2023-11-17T23:13:34.417' AS DateTime))
GO
INSERT [dbo].[departement] ([id_dept], [name_dept], [created_at]) VALUES (5, N'PPIC', CAST(N'2023-11-17T23:13:34.500' AS DateTime))
GO
INSERT [dbo].[departement] ([id_dept], [name_dept], [created_at]) VALUES (6, N'PRODUCTION ENGINEERING', CAST(N'2023-11-17T23:13:34.577' AS DateTime))
GO
INSERT [dbo].[departement] ([id_dept], [name_dept], [created_at]) VALUES (7, N'PRODUCTION 1', CAST(N'2023-11-17T23:13:34.650' AS DateTime))
GO
INSERT [dbo].[departement] ([id_dept], [name_dept], [created_at]) VALUES (8, N'PRODUCTION 2', CAST(N'2023-11-17T23:13:34.720' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[departement] OFF
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1', N'K-TC09-S100XXX-GLFS-NL-0000', N'TOP COVER S100 GREY', CAST(N'2023-11-17T23:48:16.333' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'10', N'K-CV00-566LXXX-J00N-NL-0000', N'CV 566L NATURAL HD-NATURAL', CAST(N'2023-11-17T23:48:17.137' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'100', N'K-SCCV-345X050-11V0-BL-MFA0', N'STC CVR NO BRAND MF 345X50 MM', CAST(N'2023-11-17T23:48:24.943' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1000', N'W-CP00-G51XXXX-0880-IN-XPD0', N'CP G51 LC-880 NAT INCOE XP MLY', CAST(N'2023-11-17T23:49:45.697' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1001', N'W-CP00-G51XXXX-0880-OH-HDB0', N'CP G51 LC-880 NAT OHAYO HD MII', CAST(N'2023-11-17T23:49:45.777' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1002', N'W-CP00-D23XXXX-0315-AS-HPA0', N'CP D23 LC-315 NAT ASAHI H-PERF', CAST(N'2023-11-17T23:49:45.860' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1003', N'W-CP00-B24XXXX-0331-AP-PRB0', N'CP B24 LC331 NAT ASPIRA PRE AO', CAST(N'2023-11-17T23:49:45.940' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1004', N'W-CP00-B24XXXX-0331-AS-HPA0', N'CP B24 LC-331 NAT ASAHI H-PERF', CAST(N'2023-11-17T23:49:46.020' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1005', N'W-CP00-B24XXXX-0331-AS-HPB0', N'CP B24 LC-331 NAT ASAHI HP MII', CAST(N'2023-11-17T23:49:46.100' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1006', N'W-CP00-B24XXXX-0331-AS-MEA0', N'CP B24 LC-331 NAT ASAHI MEANS', CAST(N'2023-11-17T23:49:46.190' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1007', N'W-CP00-B24XXXX-0331-BL-00A0', N'CP B24 LC-331 NAT NO BRAND', CAST(N'2023-11-17T23:49:46.277' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1008', N'W-CP00-B24XXXX-0331-IN-GOB0', N'CP B24 LC-331 NAT INCOE GOLD', CAST(N'2023-11-17T23:49:46.360' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1009', N'W-CP00-B24XXXX-0331-IN-PPA0', N'CP B24 LC-331 NAT INCOE PPO', CAST(N'2023-11-17T23:49:46.440' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'101', N'K-SCCV-345X050-11V0-LU-PRA0', N'STC COVER LUCAS PRE 345X50 MM', CAST(N'2023-11-17T23:48:25.020' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1010', N'W-CP00-B24XXXX-0331-IN-XPA0', N'CP B24 LC-331 NAT INCOE XP', CAST(N'2023-11-17T23:49:46.530' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1011', N'W-CP00-B24XXXX-0331-IN-XPB0', N'CP B24 LC-331 NAT INCOE XP SUD', CAST(N'2023-11-17T23:49:46.610' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1012', N'W-CP00-B24XXXX-0331-IN-XPC0', N'CP B24 LC-331 NAT INCOE XP UAE', CAST(N'2023-11-17T23:49:46.693' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1013', N'W-CP00-B24XXXX-0331-IN-XPD0', N'CP B24 LC-331 NAT INCOE XP MLY', CAST(N'2023-11-17T23:49:46.770' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1014', N'W-CP00-B24XXXX-0331-OH-HDB0', N'CP B24 LC-331 NAT OHAYO HD MII', CAST(N'2023-11-17T23:49:46.853' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1015', N'W-CP00-B24XXXX-0331-QU-00A0', N'CP B24 LC-331 NAT QUANTUM', CAST(N'2023-11-17T23:49:46.933' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1016', N'W-CP00-B24XXXX-0331-QU-00B0', N'CP B24 LC-331 NAT QUANTUM MII', CAST(N'2023-11-17T23:49:47.013' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1017', N'W-CP00-B24XXXX-0331-QU-00C0', N'CP B24 LC-331 NAT QUANTUM RFH', CAST(N'2023-11-17T23:49:47.093' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1018', N'W-CP00-B24XXXX-0331-RA-00A0', N'CP B24 LC-331 NAT RAPTOR', CAST(N'2023-11-17T23:49:47.177' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1019', N'W-CP00-B24XXXX-0331-UL-00A0', N'CP B24 LC-331 NAT ULTRA-X', CAST(N'2023-11-17T23:49:47.267' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'102', N'K-SCCV-345X050-11V0-NI-PRA0', N'STIC COVER NIKO PRE 345X50 MM', CAST(N'2023-11-17T23:48:25.103' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1020', N'W-CP00-B24XXXX-0331-UL-00B0', N'CP B24 LC-331 NAT ULTRA-X MII', CAST(N'2023-11-17T23:49:47.347' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1021', N'W-CP00-D31XXXX-0421-AS-HPB0', N'CP D31 LC-421 NAT ASAHI HP MII', CAST(N'2023-11-17T23:49:47.430' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1022', N'W-CP00-NS60LXX-0331-NI-00A0', N'CP NS60L LC-331 NAT NIKO', CAST(N'2023-11-17T23:49:47.510' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1023', N'W-CP00-NS60XXX-0232-NI-00A0', N'CP NS60 LC-232 NAT NIKO', CAST(N'2023-11-17T23:49:47.593' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1024', N'W-CP00-NS60XXX-0331-NI-00A0', N'CP NS60 LC-331 NAT NIKO', CAST(N'2023-11-17T23:49:47.677' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1025', N'W-CP00-NS70LXX-0257-NI-00A0', N'CP NS70L LC-257 NAT NIKO', CAST(N'2023-11-17T23:49:47.757' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1026', N'W-CP00-NS70XXX-0257-NI-00A0', N'CP NS70 LC-257 NAT NIKO', CAST(N'2023-11-17T23:49:47.840' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1027', N'W-CP00-NS70XXX-0313-IS-00A0', N'CP NS70 LC-313 NAT ISUZU DG', CAST(N'2023-11-17T23:49:47.920' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1028', N'W-CP02-115D31R-0433-AM-HDA0', N'CP 115D31R LC-433 BLA AM HDT', CAST(N'2023-11-17T23:49:48.000' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1029', N'W-CP02-115E41R-0563-AM-HDA0', N'CP 115E41R LC-563 BLA AM HDT', CAST(N'2023-11-17T23:49:48.083' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'103', N'K-SCCV-345X050-11V0-OH-MFB0', N'STC CVR OHAYO MF UAE 345X50 MM', CAST(N'2023-11-17T23:48:25.187' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1030', N'W-CP00-G51XXXX-0880-QU-00A0', N'CP G51 LC-880 NAT QUANTUM', CAST(N'2023-11-17T23:49:48.160' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1031', N'W-CP00-G51XXXX-0880-QU-00B0', N'CP G51 LC-880 NAT QUANTUM MII', CAST(N'2023-11-17T23:49:48.240' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1032', N'W-CP00-G51XXXX-0880-RA-00A0', N'CP G51 LC-880 NAT RAPTOR', CAST(N'2023-11-17T23:49:48.323' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1033', N'W-CP00-G51XXXX-0880-UL-00A0', N'CP G51 LC-880 NAT ULTRA-X', CAST(N'2023-11-17T23:49:48.403' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1034', N'W-CP00-G51XXXX-0880-UL-00B0', N'CP G51 LC-880 NAT ULTRA-X MII', CAST(N'2023-11-17T23:49:48.483' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1035', N'W-CP00-H52XXXX-1060-AM-HDA0', N'CP H52 LC-1060 NAT AM H-DUTY', CAST(N'2023-11-17T23:49:48.567' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1036', N'W-CP00-H52XXXX-1060-AM-PRA0', N'CP H52 LC-1060 NAT AM PREMIUM', CAST(N'2023-11-17T23:49:48.653' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1037', N'W-CP00-H52XXXX-1060-AP-DCA0', N'CP H52 LC-1060 NAT ASPIRA DEEP', CAST(N'2023-11-17T23:49:48.747' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1038', N'W-CP00-H52XXXX-1060-AP-PRA0', N'CP H52 LC-1060 NAT ASPIRA PRE', CAST(N'2023-11-17T23:49:48.840' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1039', N'W-CP00-H52XXXX-1060-AP-PRC0', N'CP H52 LC1060 NAT ASPIRA PR AO', CAST(N'2023-11-17T23:49:48.933' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'104', N'K-SCCV-345X050-11V0-QU-MFA0', N'STC CVR QUANTUM MF 345X50 MM', CAST(N'2023-11-17T23:48:25.273' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1040', N'W-CP00-H52XXXX-1060-AS-HPA0', N'CP H52 LC1060 NAT ASAHI H-PERF', CAST(N'2023-11-17T23:49:49.037' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1041', N'W-CP00-H52XXXX-1060-AS-HPB0', N'CP H52 LC1060 NAT ASAHI HP MII', CAST(N'2023-11-17T23:49:49.133' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1042', N'W-CP00-H52XXXX-1060-AS-MCA0', N'CP H52 L1060 NAT ASAHI ME DEEP', CAST(N'2023-11-17T23:49:49.230' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1043', N'W-CP00-H52XXXX-1060-AS-MEA0', N'CP H52 LC-1060 NAT ASAHI MEANS', CAST(N'2023-11-17T23:49:49.323' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1044', N'W-CP00-H52XXXX-1060-AU-00A0', N'CP H52 LC-1060 NAT AURORA', CAST(N'2023-11-17T23:49:49.403' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1045', N'W-CP00-H52XXXX-1060-IN-PPA0', N'CP H52 LC-1060 NAT INCOE PPO', CAST(N'2023-11-17T23:49:49.487' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1046', N'W-CP00-H52XXXX-1060-IN-XPA0', N'CP H52 LC-1060 NAT INCOE XP', CAST(N'2023-11-17T23:49:49.567' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1047', N'W-CP00-H52XXXX-1060-IN-XPB0', N'CP H52 LC1060 NAT INCOE XP SUD', CAST(N'2023-11-17T23:49:49.650' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1048', N'W-CP00-H52XXXX-1060-IN-XPC0', N'CP H52 LC1060 NAT INCOE XP UAE', CAST(N'2023-11-17T23:49:49.730' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1049', N'W-CP00-H52XXXX-1060-IN-XPD0', N'CP H52 LC1060 NAT INCOE XP MLY', CAST(N'2023-11-17T23:49:49.810' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'105', N'K-SCCV-345X050-11VJ-AS-MFA0', N'STIC CVR ASAHI MF 345X50 MM', CAST(N'2023-11-17T23:48:25.353' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1050', N'W-CP00-H52XXXX-1060-OH-HDB0', N'CP H52 LC1060 NAT OHAYO HD MII', CAST(N'2023-11-17T23:49:49.893' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1051', N'W-CP00-H52XXXX-1060-QU-00A0', N'CP H52 LC-1060 NAT QUANTUM', CAST(N'2023-11-17T23:49:49.973' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1052', N'W-CP00-H52XXXX-1060-QU-00B0', N'CP H52 LC-1060 NAT QUANTUM MII', CAST(N'2023-11-17T23:49:50.057' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1053', N'W-CP00-G51XXXX-0880-IN-XPA0', N'CP G51 LC-880 NAT INCOE XP', CAST(N'2023-11-17T23:49:50.137' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1054', N'W-CP00-H52XXXX-1060-UL-00A0', N'CP H52 LC-1060 NAT ULTRA-X', CAST(N'2023-11-17T23:49:50.217' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1055', N'W-CP00-H52XXXX-1060-UL-00B0', N'CP H52 LC-1060 NAT ULTRA-X MII', CAST(N'2023-11-17T23:49:50.300' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1056', N'W-CP00-N100TXX-0562-AS-HPA0', N'CP N100T LC562 NAT ASAHI H-PER', CAST(N'2023-11-17T23:49:50.380' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1057', N'W-CP00-N100TXX-0562-IN-XPA0', N'CP N100T LC-562 NAT INCOE XP', CAST(N'2023-11-17T23:49:50.460' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1058', N'W-CP00-N100TXX-0562-OH-HDB0', N'CP N100T LC-562 NAT OHA HD MII', CAST(N'2023-11-17T23:49:50.543' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1059', N'W-CP00-N100TXX-0562-QU-00A0', N'CP N100T LC-562 NAT QUANTUM', CAST(N'2023-11-17T23:49:50.623' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'106', N'K-SCCV-345X050-11VJ-IN-MFA0', N'STIC COVER INCOE MF 345X50 MM', CAST(N'2023-11-17T23:48:25.437' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1060', N'W-CP00-N100TXX-0562-QU-00B0', N'CP N100T LC562 NAT QUANTUM MII', CAST(N'2023-11-17T23:49:50.707' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1061', N'W-CP00-N100TXX-0562-UL-00B0', N'CP N100T LC562 NAT ULTRA-X MII', CAST(N'2023-11-17T23:49:50.787' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1062', N'W-CP00-N150TXX-0880-AS-HPA0', N'CP N150T LC880 NAT ASAHI H-PER', CAST(N'2023-11-17T23:49:50.867' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1063', N'W-CP00-N150TXX-0880-IN-XPA0', N'CP N150T LC-880 NAT INCOE XP', CAST(N'2023-11-17T23:49:50.947' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1064', N'W-CP00-N150TXX-0880-OH-HDB0', N'CP N150T LC-880 NAT OHA HD MII', CAST(N'2023-11-17T23:49:51.030' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1065', N'W-CP00-N150TXX-0880-QU-00A0', N'CP N150T LC-880 NAT QUANTUM', CAST(N'2023-11-17T23:49:51.117' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1066', N'W-CP00-N150TXX-0880-QU-00B0', N'CP N150T LC880 NAT QUANTUM MII', CAST(N'2023-11-17T23:49:51.200' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1067', N'W-CP00-N150TXX-0880-UL-00B0', N'CP N150T LC880 NAT ULTRA-X MII', CAST(N'2023-11-17T23:49:51.280' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1068', N'W-CP00-N200TXX-1060-AS-HPA0', N'CP N200T LC1060 NAT ASAHI H-PE', CAST(N'2023-11-17T23:49:51.363' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1069', N'W-CP00-N200TXX-1060-IN-XPA0', N'CP N200T LC1060 NAT INCOE XP', CAST(N'2023-11-17T23:49:51.443' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'107', N'K-SCCV-345X050-11VJ-OH-MFA0', N'STIC COVER OHAYO MF 345X50 MM', CAST(N'2023-11-17T23:48:25.520' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1070', N'W-CP00-N200TXX-1060-OH-HDB0', N'CP N200T LC1060 NAT OHA HD MII', CAST(N'2023-11-17T23:49:51.523' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1071', N'W-CP00-N200TXX-1060-QU-00A0', N'CP N200T LC1060 NAT QUANTUM', CAST(N'2023-11-17T23:49:51.603' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1072', N'W-CP00-N200TXX-1060-QU-00B0', N'CP N200T LC1060NAT QUANTUM MII', CAST(N'2023-11-17T23:49:51.683' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1073', N'W-CP00-N70TXXX-0421-AS-HPA0', N'CP N70T LC421 NAT ASAHI H-PERF', CAST(N'2023-11-17T23:49:51.767' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1074', N'W-CP00-N70TXXX-0421-IN-XPA0', N'CP N70T LC-421 NAT INCOE XP', CAST(N'2023-11-17T23:49:51.847' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1075', N'W-CP00-N70TXXX-0421-OH-HDB0', N'CP N70T LC-421 NAT OHA HD MII', CAST(N'2023-11-17T23:49:51.930' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1076', N'W-CP00-N70TXXX-0421-QU-00A0', N'CP N70T LC-421 NAT QUANTUM', CAST(N'2023-11-17T23:49:52.010' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1077', N'W-CP00-N70TXXX-0421-QU-00B0', N'CP N70T LC-421 NAT QUANTUM MII', CAST(N'2023-11-17T23:49:52.090' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1078', N'W-CP00-N70TXXX-0421-UL-00B0', N'CP N70T LC-421 NAT ULTRA-X MII', CAST(N'2023-11-17T23:49:52.170' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1079', N'W-CP00-N70ZXXX-0375-IS-00A0', N'CP N70Z LC-375 NAT ISUZU DG', CAST(N'2023-11-17T23:49:52.253' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'108', N'K-SCCV-252X050-15V0-IN-MFB0', N'STC CVR INCOE MF UAE 252X50 MM', CAST(N'2023-11-17T23:48:25.607' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1080', N'W-CP02-115F51X-0672-AM-PRA0', N'CP 115F51 LC-672 BLA AM PRE MF', CAST(N'2023-11-17T23:49:52.330' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1081', N'W-CP02-115F51X-0672-TN-HDA0', N'CP 115F51 LC672 BLA TRAKNUS HD', CAST(N'2023-11-17T23:49:52.413' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1082', N'W-CP02-145F51X-0672-AM-HDA0', N'CP 145F51 LC-672 BLA AM HDT', CAST(N'2023-11-17T23:49:52.493' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1083', N'W-CP02-145G51X-0880-AM-PRA0', N'CP 145G51 LC-880 BLA AM PRE MF', CAST(N'2023-11-17T23:49:52.577' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1084', N'W-CP02-145G51X-0880-TN-HDA0', N'CP 145G51 LC880 BLA TRAKNUS HD', CAST(N'2023-11-17T23:49:52.657' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1085', N'W-CP02-180G51X-0880-AM-HDA0', N'CP 180G51 LC-880 BLA AM HDT', CAST(N'2023-11-17T23:49:52.740' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1086', N'W-CP02-190H52X-1060-AM-PRA0', N'CP 190H52 LC1060 BLA AM PRE MF', CAST(N'2023-11-17T23:49:52.820' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1087', N'W-CP02-190H52X-1060-TN-HDA0', N'CP 190H52 LC1060 BLA TRAKNU HD', CAST(N'2023-11-17T23:49:52.900' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1088', N'W-CP02-210H52X-1060-AM-HDA0', N'CP 210H52 LC-1060 BLA AM HDT', CAST(N'2023-11-17T23:49:52.983' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1089', N'W-CP02-48D26RX-0210-AM-PRA0', N'CP 48D26R LC-210 BLA AM PRE MF', CAST(N'2023-11-17T23:49:53.063' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'109', N'K-SCCV-265X070-11V0-AP-MFA0', N'STIC CVR ASPIRA MF 265X70 MM', CAST(N'2023-11-17T23:48:25.690' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1090', N'W-CP02-48D26RX-0210-TN-MFA0', N'CP 48D26R LC210 BLA TRAKNUS MF', CAST(N'2023-11-17T23:49:53.143' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1091', N'W-CP02-4D150RX-1575-AM-HDA0', N'CP 4D150R LC-1575 BLA AM HDT', CAST(N'2023-11-17T23:49:53.227' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1092', N'W-CP02-4D170RX-1575-AM-HDA0', N'CP 4D170R LC-1575 BLA AM HDT', CAST(N'2023-11-17T23:49:53.310' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1093', N'W-CP02-4D200RX-1575-AM-HDA0', N'CP 4D200R LC-1575 BLA AM HDT', CAST(N'2023-11-17T23:49:53.390' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1094', N'W-CP02-544XXXX-0284-AP-MFA0', N'CP 544 LC-284 BLA ASPIRA MF', CAST(N'2023-11-17T23:49:53.470' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1095', N'W-CP02-544XXXX-0284-AS-MEA0', N'CP 544 LC-284 BLA ASAHI MEN MF', CAST(N'2023-11-17T23:49:53.553' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1096', N'W-CP02-544XXXX-0284-IN-MFA0', N'CP 544 LC-284 BLA INCOE MF', CAST(N'2023-11-17T23:49:53.633' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1097', N'W-CP02-544XXXX-0284-IN-MFB0', N'CP 544 LC-284 BLA INCOE MF UAE', CAST(N'2023-11-17T23:49:53.717' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1098', N'W-CP02-544XXXX-0284-IN-MFC0', N'CP 544 LC-284 BLA INCOE MF AOP', CAST(N'2023-11-17T23:49:53.797' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1099', N'W-CP02-D31XXXX-0386-IN-MFA0', N'CP D31 LC-386 BLA INCOE MF', CAST(N'2023-11-17T23:49:53.877' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'11', N'K-CV00-588LXXX-K00N-NL-0000', N'CV 588L NATURAL HD-NATURAL', CAST(N'2023-11-17T23:48:17.223' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'110', N'K-SCCV-265X070-11V0-BL-MFA0', N'STC CVR NO BRAND MF 265X70 MM', CAST(N'2023-11-17T23:48:25.773' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1100', N'W-CT00-G51XXXX-0880-NH-0000', N'CONTAINER G51 LC-880 NATURAL', CAST(N'2023-11-17T23:49:53.960' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1101', N'W-CT00-H52XXXX-1060-NH-0000', N'CONTAINER H52 LC-1060 NATURAL', CAST(N'2023-11-17T23:49:54.040' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1102', N'W-CT00-N12DXXX-0613-BL-0000', N'CONTAINER N12D LC-613 NATURAL', CAST(N'2023-11-17T23:49:54.123' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1103', N'W-CP00-NS40ZLX-0203-NI-00A0', N'CP NS40ZL LC-203 NAT NIKO', CAST(N'2023-11-17T23:49:54.203' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1104', N'W-CP02-H52XXXX-1060-IN-MFC0', N'CP H52 LC1060 BLA INCOE MF AOP', CAST(N'2023-11-17T23:49:54.283' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1105', N'W-CP02-NS50XXX-0343-IN-MFC0', N'CP NS50 LC343 BLA INCOE MF AOP', CAST(N'2023-11-17T23:49:54.367' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1106', N'W-CP00-D23XXXX-0315-AS-HPB0', N'CP D23 LC-315 NAT ASAHI HP MII', CAST(N'2023-11-17T23:49:54.450' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1107', N'W-CP00-D23XXXX-0315-BL-00A0', N'CP D23 LC-315 NAT NO BRAND', CAST(N'2023-11-17T23:49:54.530' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1108', N'W-CP00-D26XXXX-0209-UL-00A0', N'CP D26 LC-209 NAT ULTRA-X', CAST(N'2023-11-17T23:49:54.610' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1109', N'W-CP00-D26XXXX-0209-UL-00B0', N'CP D26 LC-209 NAT ULTRA-X MII', CAST(N'2023-11-17T23:49:54.693' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'111', N'K-SCCV-265X070-11V0-LU-PRA0', N'STC COVER LUCAS PRE 265X70 MM', CAST(N'2023-11-17T23:48:25.857' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1110', N'W-CP00-D26XXXX-0257-AP-PRA0', N'CP D26 LC-257 NAT ASPIRA PRE', CAST(N'2023-11-17T23:49:54.773' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1111', N'W-CP00-D26XXXX-0257-AP-PRB0', N'CP D26 LC257 NAT ASPIRA PRE AO', CAST(N'2023-11-17T23:49:54.857' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1112', N'W-CP00-D26XXXX-0257-AS-HPA0', N'CP D26 LC-257 NAT ASAHI H-PERF', CAST(N'2023-11-17T23:49:54.937' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1113', N'W-CP00-D26XXXX-0257-AS-HPB0', N'CP D26 LC-257 NAT ASAHI HP MII', CAST(N'2023-11-17T23:49:55.020' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1114', N'W-CP00-D26XXXX-0257-AU-00A0', N'CP D26 LC-257 NAT AURORA', CAST(N'2023-11-17T23:49:55.103' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1115', N'W-CP00-D26XXXX-0257-BL-00A0', N'CP D26 LC-257 NAT NO BRAND', CAST(N'2023-11-17T23:49:55.183' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1116', N'W-CT02-4DXXXXX-1575-NH-RH00', N'CONTAINER 4D LC-1575 BLACK', CAST(N'2023-11-17T23:49:55.263' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1117', N'W-CT02-544XXXX-0284-BA-0000', N'CONTAINER 544 LC-284 BLACK', CAST(N'2023-11-17T23:49:55.347' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1118', N'W-CT02-555XXXX-0344-BA-0000', N'CONTAINER 555 LC-344 BLACK', CAST(N'2023-11-17T23:49:55.430' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1119', N'W-CT02-566XXXX-0403-BA-0000', N'CONTAINER 566 LC-403 BLACK', CAST(N'2023-11-17T23:49:55.510' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'112', N'K-SCCV-265X070-11V0-NI-PRA0', N'STIC COVER NIKO PRE 265X70 MM', CAST(N'2023-11-17T23:48:25.940' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1120', N'W-CT02-588XXXX-0529-BA-0000', N'CONTAINER 588 LC-529 BLACK', CAST(N'2023-11-17T23:49:55.590' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1121', N'W-CT02-A19XXXX-0242-NH-0000', N'CONTAINER A19 LC-242 BLACK', CAST(N'2023-11-17T23:49:55.670' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1122', N'W-CT02-B20XXXX-0273-NH-0000', N'CONTAINER B20 LC-273 BLACK', CAST(N'2023-11-17T23:49:55.753' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1123', N'W-CT02-B24XXXX-0232-NH-0000', N'CONTAINER B24 LC-232 BLACK', CAST(N'2023-11-17T23:49:55.833' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1124', N'W-CT02-B24XXXX-0331-NH-0000', N'CONTAINER B24 LC-331 BLACK', CAST(N'2023-11-17T23:49:55.917' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1125', N'W-CT02-D23XXXX-0315-BL-0000', N'CONTAINER D23 LC-315 BLACK', CAST(N'2023-11-17T23:49:55.997' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1126', N'W-CT02-D26XXXX-0210-BL-0000', N'CONTAINER D26 LC-210 BLACK', CAST(N'2023-11-17T23:49:56.080' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1127', N'W-CT02-D26XXXX-0252-BL-0000', N'CONTAINER D26 LC-252 BLACK', CAST(N'2023-11-17T23:49:56.157' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1128', N'W-CT02-D26XXXX-0310-BL-0000', N'CONTAINER D26 LC-310 BLACK', CAST(N'2023-11-17T23:49:56.237' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1129', N'W-CT02-D26XXXX-0329-BL-0000', N'CONTAINER D26 LC-329 BLACK', CAST(N'2023-11-17T23:49:56.320' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'113', N'K-SCCV-265X070-11V0-OH-MFB0', N'STC CVR OHAYO MF UAE 265X70 MM', CAST(N'2023-11-17T23:48:26.023' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1130', N'W-CT02-D26XXXX-0375-BL-0000', N'CONTAINER D26 LC-375 BLACK', CAST(N'2023-11-17T23:49:56.400' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1131', N'W-CT02-D31XXXX-0329-BL-0000', N'CONTAINER D31 LC-329 BLACK', CAST(N'2023-11-17T23:49:56.480' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1132', N'W-CT02-D31XXXX-0386-BL-0000', N'CONTAINER D31 LC-386 BLACK', CAST(N'2023-11-17T23:49:56.570' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1133', N'W-CT02-D31XXXX-0433-BL-0000', N'CONTAINER D31 LC-433 BLACK', CAST(N'2023-11-17T23:49:56.650' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1134', N'W-CT02-E41XXXX-0563-NH-0000', N'CONTAINER E41 LC-563 BLACK', CAST(N'2023-11-17T23:49:56.730' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1135', N'W-CT02-F51XXXX-0672-NH-0000', N'CONTAINER F51 LC-672 BLACK', CAST(N'2023-11-17T23:49:56.810' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1136', N'W-CT02-G51XXXX-0880-NH-0000', N'CONTAINER G51 LC-880 BLACK', CAST(N'2023-11-17T23:49:56.890' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1137', N'W-CT02-H52XXXX-1060-NH-0000', N'CONTAINER H52 LC-1060 BLACK', CAST(N'2023-11-17T23:49:56.970' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1138', N'W-CT02-N03XXXX-0535-BS-0000', N'CONTAINER N03 LC-535 BLACK', CAST(N'2023-11-17T23:49:57.053' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1139', N'W-CT02-NS50XXX-0343-BL-0000', N'CONTAINER NS50 LC-343 BLACK', CAST(N'2023-11-17T23:49:57.130' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'114', N'K-SCCV-265X070-11V0-QU-MFA0', N'STC CVR QUANTUM MF 265X70 MM', CAST(N'2023-11-17T23:48:26.103' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1140', N'W-CP00-D31XXXX-0283-AP-PRB0', N'CP D31 LC283 NAT ASPIRA PRE AO', CAST(N'2023-11-17T23:49:57.213' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1141', N'W-CP00-D31XXXX-0283-AS-HPA0', N'CP D31 LC-283 NAT ASAHI H-PERF', CAST(N'2023-11-17T23:49:57.293' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1142', N'W-CP00-D23XXXX-0315-IN-GOB0', N'CP D23 LC-315 NAT INCOE GOLD', CAST(N'2023-11-17T23:49:57.377' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1143', N'W-CP00-D23XXXX-0315-IN-PPA0', N'CP D23 LC-315 NAT INCOE PPO', CAST(N'2023-11-17T23:49:57.457' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1144', N'W-CP00-D23XXXX-0315-IN-XPA0', N'CP D23 LC-315 NAT INCOE XP', CAST(N'2023-11-17T23:49:57.543' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1145', N'W-CP00-D23XXXX-0315-IN-XPD0', N'CP D23 LC-315 NAT INCOE XP MLY', CAST(N'2023-11-17T23:49:57.623' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1146', N'W-CP00-D23XXXX-0315-OH-HDB0', N'CP D23 LC-315 NAT OHAYO HD MII', CAST(N'2023-11-17T23:49:57.703' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1147', N'W-CP00-D23XXXX-0315-QU-00A0', N'CP D23 LC-315 NAT QUANTUM', CAST(N'2023-11-17T23:49:57.783' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1148', N'W-CP00-D23XXXX-0315-QU-00B0', N'CP D23 LC-315 NAT QUANTUM MII', CAST(N'2023-11-17T23:49:57.863' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1149', N'W-CP00-D23XXXX-0315-UL-00A0', N'CP D23 LC-315 NAT ULTRA-X', CAST(N'2023-11-17T23:49:57.947' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'115', N'K-SCCV-265X070-11VJ-AS-MFA0', N'STIC CVR ASAHI MF 265X70 MM', CAST(N'2023-11-17T23:48:26.190' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1150', N'W-CP00-D23XXXX-0315-UL-00B0', N'CP D23 LC-315 NAT ULTRA-X MII', CAST(N'2023-11-17T23:49:58.027' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1151', N'W-CP00-D26XXXX-0209-AM-PRA0', N'CP D26 LC-209 NAT AM PREMIUM', CAST(N'2023-11-17T23:49:58.107' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1152', N'W-CP00-D26XXXX-0209-AP-PRA0', N'CP D26 LC-209 NAT ASPIRA PRE', CAST(N'2023-11-17T23:49:58.187' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1153', N'W-CP00-D26XXXX-0209-AP-PRB0', N'CP D26 LC209 NAT ASPIRA PRE AO', CAST(N'2023-11-17T23:49:58.270' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1154', N'W-CP00-D26XXXX-0209-AS-HPA0', N'CP D26 LC-209 NAT ASAHI H-PERF', CAST(N'2023-11-17T23:49:58.350' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1155', N'W-CP00-D26XXXX-0209-AS-HPB0', N'CP D26 LC-209 NAT ASAHI HP MII', CAST(N'2023-11-17T23:49:58.430' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1156', N'W-CP00-D26XXXX-0209-AS-MEA0', N'CP D26 LC-209 NAT ASAHI MEANS', CAST(N'2023-11-17T23:49:58.510' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1157', N'W-CP00-D26XXXX-0209-BL-00A0', N'CP D26 LC-209 NAT NO BRAND', CAST(N'2023-11-17T23:49:58.593' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1158', N'W-CP00-D26XXXX-0209-IN-GOB0', N'CP D26 LC-209 NAT INCOE GOLD', CAST(N'2023-11-17T23:49:58.673' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1159', N'W-CP00-D26XXXX-0209-IN-PPA0', N'CP D26 LC-209 NAT INCOE PPO', CAST(N'2023-11-17T23:49:58.757' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'116', N'K-SCCV-265X070-11VJ-IN-MFA0', N'STIC COVER INCOE MF 265X70 MM', CAST(N'2023-11-17T23:48:26.273' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1160', N'W-CP00-D26XXXX-0209-IN-XPA0', N'CP D26 LC-209 NAT INCOE XP', CAST(N'2023-11-17T23:49:58.837' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1161', N'W-CP00-D26XXXX-0209-IN-XPB0', N'CP D26 LC-209 NAT INCOE XP SUD', CAST(N'2023-11-17T23:49:58.917' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1162', N'W-CP00-D26XXXX-0209-IN-XPC0', N'CP D26 LC-209 NAT INCOE XP UAE', CAST(N'2023-11-17T23:49:58.997' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1163', N'W-CP00-D26XXXX-0209-IN-XPD0', N'CP D26 LC-209 NAT INCOE XP MLY', CAST(N'2023-11-17T23:49:59.080' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1164', N'W-CP00-D26XXXX-0209-OH-HDB0', N'CP D26 LC-209 NAT OHAYO HD MII', CAST(N'2023-11-17T23:49:59.160' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1165', N'W-CP00-D26XXXX-0209-QU-00A0', N'CP D26 LC-209 NAT QUANTUM', CAST(N'2023-11-17T23:49:59.243' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1166', N'W-CP00-D26XXXX-0257-IN-GOB0', N'CP D26 LC-257 NAT INCOE GOLD', CAST(N'2023-11-17T23:49:59.330' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1167', N'W-CP00-D26XXXX-0257-IN-PPA0', N'CP D26 LC-257 NAT INCOE PPO', CAST(N'2023-11-17T23:49:59.420' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1168', N'W-CP00-D26XXXX-0257-IN-XPA0', N'CP D26 LC-257 NAT INCOE XP', CAST(N'2023-11-17T23:49:59.520' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1169', N'W-CP00-D26XXXX-0257-IN-XPB0', N'CP D26 LC-257 NAT INCOE XP SUD', CAST(N'2023-11-17T23:49:59.617' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'117', N'K-SCCV-265X070-11VJ-OH-MFA0', N'STIC COVER OHAYO MF 265X70 MM', CAST(N'2023-11-17T23:48:26.360' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1170', N'W-CP00-D26XXXX-0257-IN-XPC0', N'CP D26 LC-257 NAT INCOE XP UAE', CAST(N'2023-11-17T23:49:59.710' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1171', N'W-CP00-D26XXXX-0257-IN-XPD0', N'CP D26 LC-257 NAT INCOE XP MLY', CAST(N'2023-11-17T23:49:59.803' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1172', N'W-CP00-D26XXXX-0257-OH-HDB0', N'CP D26 LC-257 NAT OHAYO HD MII', CAST(N'2023-11-17T23:49:59.900' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1173', N'W-CP00-D26XXXX-0257-QU-00A0', N'CP D26 LC-257 NAT QUANTUM', CAST(N'2023-11-17T23:50:00.050' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1174', N'W-CP00-D26XXXX-0257-QU-00B0', N'CP D26 LC-257 NAT QUANTUM MII', CAST(N'2023-11-17T23:50:00.147' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1175', N'W-CP00-D26XXXX-0257-RA-00A0', N'CP D26 LC-257 NAT RAPTOR', CAST(N'2023-11-17T23:50:00.230' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1176', N'W-CP00-D26XXXX-0257-UL-00A0', N'CP D26 LC-257 NAT ULTRA-X', CAST(N'2023-11-17T23:50:00.310' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1177', N'W-CP00-D26XXXX-0257-UL-00B0', N'CP D26 LC-257 NAT ULTRA-X MII', CAST(N'2023-11-17T23:50:00.393' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1178', N'W-CP00-D26XXXX-0313-AP-PRA0', N'CP D26 LC-313 NAT ASPIRA PRE', CAST(N'2023-11-17T23:50:00.473' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1179', N'W-CP00-D26XXXX-0313-AP-PRB0', N'CP D26 LC313 NAT ASPIRA PRE AO', CAST(N'2023-11-17T23:50:00.553' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'118', N'K-SCCV-220X045-15V0-IN-MFB0', N'STC CVR INCOE MF UAE 220X45 MM', CAST(N'2023-11-17T23:48:26.443' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1180', N'W-CP00-D26XXXX-0313-AS-HPA0', N'CP D26 LC-313 NAT ASAHI H-PERF', CAST(N'2023-11-17T23:50:00.633' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1181', N'W-CP00-D26XXXX-0313-AS-HPB0', N'CP D26 LC-313 NAT ASAHI HP MII', CAST(N'2023-11-17T23:50:00.720' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1182', N'W-CP00-D26XXXX-0313-BL-00A0', N'CP D26 LC-313 NAT NO BRAND', CAST(N'2023-11-17T23:50:00.803' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1183', N'W-CP00-D26XXXX-0313-IN-GOB0', N'CP D26 LC-313 NAT INCOE GOLD', CAST(N'2023-11-17T23:50:00.880' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1184', N'W-CP00-D26XXXX-0313-IN-PPA0', N'CP D26 LC-313 NAT INCOE PPO', CAST(N'2023-11-17T23:50:00.963' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1185', N'W-CP00-D31XXXX-0421-AU-00A0', N'CP D31 LC-421 NAT AURORA', CAST(N'2023-11-17T23:50:01.043' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1186', N'W-CP00-D31XXXX-0421-BL-00A0', N'CP D31 LC-421 NAT NO BRAND', CAST(N'2023-11-17T23:50:01.127' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1187', N'W-CP00-D31XXXX-0421-IN-GOB0', N'CP D31 LC-421 NAT INCOE GOLD', CAST(N'2023-11-17T23:50:01.210' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1188', N'W-CP00-D31XXXX-0421-IN-PPA0', N'CP D31 LC-421 NAT INCOE PPO', CAST(N'2023-11-17T23:50:01.290' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1189', N'W-CP00-D31XXXX-0421-IN-XPA0', N'CP D31 LC-421 NAT INCOE XP', CAST(N'2023-11-17T23:50:01.370' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'119', N'K-SCCV-240X045-11V0-BL-MFA0', N'STC CVR NO BRAND MF 240X45 MM', CAST(N'2023-11-17T23:48:26.533' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1190', N'W-CP00-D31XXXX-0421-IN-XPB0', N'CP D31 LC-421 NAT INCOE XP SUD', CAST(N'2023-11-17T23:50:01.453' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1191', N'W-CP00-D31XXXX-0421-IN-XPC0', N'CP D31 LC-421 NAT INCOE XP UAE', CAST(N'2023-11-17T23:50:01.533' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1192', N'W-CP00-D31XXXX-0283-AS-HPB0', N'CP D31 LC-283 NAT ASAHI HP MII', CAST(N'2023-11-17T23:50:01.613' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1193', N'W-CP00-D31XXXX-0283-AS-MEA0', N'CP D31 LC-283 NAT ASAHI MEANS', CAST(N'2023-11-17T23:50:01.697' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1194', N'W-CP00-D31XXXX-0283-BL-00A0', N'CP D31 LC-283 NAT NO BRAND', CAST(N'2023-11-17T23:50:01.777' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1195', N'W-CP00-D31XXXX-0283-IN-GOB0', N'CP D31 LC-283 NAT INCOE GOLD', CAST(N'2023-11-17T23:50:01.857' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1196', N'W-CP00-D31XXXX-0283-IN-PPA0', N'CP D31 LC-283 NAT INCOE PPO', CAST(N'2023-11-17T23:50:01.940' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1197', N'W-CP00-D31XXXX-0283-IN-XPA0', N'CP D31 LC-283 NAT INCOE XP', CAST(N'2023-11-17T23:50:02.020' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1198', N'W-CP00-D31XXXX-0283-IN-XPB0', N'CP D31 LC-283 NAT INCOE XP SUD', CAST(N'2023-11-17T23:50:02.103' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1199', N'W-CP00-D31XXXX-0283-IN-XPC0', N'CP D31 LC-283 NAT INCOE XP UAE', CAST(N'2023-11-17T23:50:02.183' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'12', N'K-CV00-DINBLXX-0MHN-NL-0000', N'CV DIN-B-L NATURAL', CAST(N'2023-11-17T23:48:17.313' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'120', N'K-SCCV-240X045-11V0-LU-PRA0', N'STC COVER LUCAS PRE 240X45 MM', CAST(N'2023-11-17T23:48:26.630' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1200', N'W-CP00-D31XXXX-0283-IN-XPD0', N'CP D31 LC-283 NAT INCOE XP MLY', CAST(N'2023-11-17T23:50:02.270' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1201', N'W-CP00-D31XXXX-0283-OH-HDB0', N'CP D31 LC-283 NAT OHAYO HD MII', CAST(N'2023-11-17T23:50:02.350' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1202', N'W-CP00-D31XXXX-0283-QU-00A0', N'CP D31 LC-283 NAT QUANTUM', CAST(N'2023-11-17T23:50:02.437' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1203', N'W-CP00-D31XXXX-0283-QU-00B0', N'CP D31 LC-283 NAT QUANTUM MII', CAST(N'2023-11-17T23:50:02.520' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1204', N'W-CP00-D31XXXX-0283-UL-00A0', N'CP D31 LC-283 NAT ULTRA-X', CAST(N'2023-11-17T23:50:02.603' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1205', N'W-CP00-D31XXXX-0283-UL-00B0', N'CP D31 LC-283 NAT ULTRA-X MII', CAST(N'2023-11-17T23:50:02.690' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1206', N'W-CP00-D31XXXX-0300-AM-PRA0', N'CP D31 LC-300 NAT AM PRMEIUM', CAST(N'2023-11-17T23:50:02.773' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1207', N'W-CP00-D31XXXX-0300-AP-PRA0', N'CP D31 LC-300 NAT ASPIRA PRE', CAST(N'2023-11-17T23:50:02.857' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1208', N'W-CP00-D31XXXX-0300-AP-PRB0', N'CP D31 LC300 NAT ASPIRA PRE AO', CAST(N'2023-11-17T23:50:02.940' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1209', N'W-CP00-D31XXXX-0300-AS-HPA0', N'CP D31 LC-300 NAT ASAHI H-PERF', CAST(N'2023-11-17T23:50:03.027' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'121', N'K-SCCV-240X045-11V0-OH-MFB0', N'STC CVR OHAYO MF UAE 240X45 MM', CAST(N'2023-11-17T23:48:26.713' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1210', N'W-CP00-D31XXXX-0300-AS-HPB0', N'CP D31 LC-300 NAT ASAHI HP MII', CAST(N'2023-11-17T23:50:03.107' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1211', N'W-CP00-D31XXXX-0300-AS-MEA0', N'CP D31 LC-300 NAT ASAHI MEANS', CAST(N'2023-11-17T23:50:03.190' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1212', N'W-CP00-D31XXXX-0300-BL-00A0', N'CP D31 LC-300 NAT NO BRAND', CAST(N'2023-11-17T23:50:03.273' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1213', N'W-CP00-D31XXXX-0300-IN-GOB0', N'CP D31 LC-300 NAT INCOE GOLD', CAST(N'2023-11-17T23:50:03.357' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1214', N'W-CP00-D31XXXX-0300-IN-PPA0', N'CP D31 LC-300 NAT INCOE PPO', CAST(N'2023-11-17T23:50:03.440' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1215', N'W-CP00-D31XXXX-0300-IN-XPA0', N'CP D31 LC-300 NAT INCOE XP', CAST(N'2023-11-17T23:50:03.527' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1216', N'W-CP00-D31XXXX-0300-IN-XPB0', N'CP D31 LC-300 NAT INCOE XP SUD', CAST(N'2023-11-17T23:50:03.610' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1217', N'W-CT02-TZ5SXXX-0160-NH-0000', N'CONTAINER TZ5S LC-160 BLACK', CAST(N'2023-11-17T23:50:03.693' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1218', N'W-CP00-B20XXXX-0220-QU-00B0', N'CP B20 LC-220 NAT QUANTUM MII', CAST(N'2023-11-17T23:50:03.777' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1219', N'W-CP00-D31XXXX-0421-IN-XPD0', N'CP D31 LC-421 NAT INCOE XP MLY', CAST(N'2023-11-17T23:50:03.860' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'122', N'K-SCCV-240X045-11V0-QU-MFA0', N'STC CVR QUANTUM MF 240X45 MM', CAST(N'2023-11-17T23:48:26.797' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1220', N'W-CP00-D31XXXX-0421-OH-HDB0', N'CP D31 LC-421 NAT OHAYO HD MII', CAST(N'2023-11-17T23:50:03.947' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1221', N'W-CP00-D31XXXX-0421-QU-00A0', N'CP D31 LC-421 NAT QUANTUM', CAST(N'2023-11-17T23:50:04.027' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1222', N'W-CP00-D31XXXX-0421-QU-00B0', N'CP D31 LC-421 NAT QUANTUM MII', CAST(N'2023-11-17T23:50:04.110' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1223', N'W-CP00-D31XXXX-0421-RA-00A0', N'CP D31 LC-421 NAT RAPTOR', CAST(N'2023-11-17T23:50:04.190' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1224', N'W-CP00-D31XXXX-0421-UL-00A0', N'CP D31 LC-421 NAT ULTRA-X', CAST(N'2023-11-17T23:50:04.277' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1225', N'W-CP00-D31XXXX-0421-UL-00B0', N'CP D31 LC-421 NAT ULTRA-X MII', CAST(N'2023-11-17T23:50:04.370' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1226', N'W-CP00-E41XXXX-0393-AM-PRA0', N'CP E41 LC-393 NAT AM PREMIUM', CAST(N'2023-11-17T23:50:04.453' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1227', N'W-CP00-E41XXXX-0393-AP-PRA0', N'CP E41 LC-393 NAT ASPIRA PRE', CAST(N'2023-11-17T23:50:04.533' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1228', N'W-CP00-E41XXXX-0393-AP-PRC0', N'CP E41 LC393 NAT ASPIRA PRE AO', CAST(N'2023-11-17T23:50:04.613' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1229', N'W-CP00-E41XXXX-0393-AS-HPA0', N'CP E41 LC-393 NAT ASAHI H-PERF', CAST(N'2023-11-17T23:50:04.697' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'123', N'K-SCCV-170X045-11VJ-IN-MFA0', N'STIC COVER INCOE MF 170X45 MM', CAST(N'2023-11-17T23:48:26.880' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1230', N'W-CP00-E41XXXX-0393-AS-HPB0', N'CP E41 LC-393 NAT ASAHI HP MII', CAST(N'2023-11-17T23:50:04.777' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1231', N'W-CP00-E41XXXX-0393-AS-MEA0', N'CP E41 LC-393 NAT ASAHI MEANS', CAST(N'2023-11-17T23:50:04.860' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1232', N'W-CP00-E41XXXX-0393-BL-00A0', N'CP E41 LC-393 NAT NO BRAND', CAST(N'2023-11-17T23:50:04.937' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1233', N'W-CP00-E41XXXX-0393-IN-PPA0', N'CP E41 LC-393 NAT INCOE PPO', CAST(N'2023-11-17T23:50:05.017' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1234', N'W-CP00-E41XXXX-0393-IN-XPA0', N'CP E41 LC-393 NAT INCOE XP', CAST(N'2023-11-17T23:50:05.100' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1235', N'W-CP00-E41XXXX-0393-IN-XPB0', N'CP E41 LC-393 NAT INCOE XP SUD', CAST(N'2023-11-17T23:50:05.187' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1236', N'W-CP00-E41XXXX-0393-IN-XPC0', N'CP E41 LC-393 NAT INCOE XP UAE', CAST(N'2023-11-17T23:50:05.267' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1237', N'W-CP00-E41XXXX-0393-IN-XPD0', N'CP E41 LC-393 NAT INCOE XP MLY', CAST(N'2023-11-17T23:50:05.347' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1238', N'W-CP00-E41XXXX-0393-OH-HDB0', N'CP E41 LC-393 NAT OHAYO HD MII', CAST(N'2023-11-17T23:50:05.430' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1239', N'W-CP00-555XXXX-0338-QU-00A0', N'CP 555 LC-338 NAT QUANTUM', CAST(N'2023-11-17T23:50:05.510' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'124', N'K-SCCV-170X045-15V0-IN-MFB0', N'STC CVR INCOE MF UAE 170X45 MM', CAST(N'2023-11-17T23:48:26.970' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1240', N'W-CT02-TZ6VXXX-0165-NH-0000', N'CONTAINER TZ6V LC-165 BLACK', CAST(N'2023-11-17T23:50:05.590' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1241', N'W-CT08-S100CTX-0771-NH-RH00', N'CONTAINER S100CT LC-771 GREY', CAST(N'2023-11-17T23:50:05.673' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1242', N'W-CP02-TZ5SXXX-0160-AP-00A0', N'CP TZ-5S LC-160 BLA ASPIRA KIT', CAST(N'2023-11-17T23:50:05.753' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1243', N'W-CP02-TZ5SXXX-0160-QU-FPA0', N'CP TZ-5S LC160 BLA QUANTUM FED', CAST(N'2023-11-17T23:50:05.837' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1244', N'W-CP02-TZ5SXXX-0160-TR-00A0', N'CP TZ-5S LC-160 BLA TRANSPORT', CAST(N'2023-11-17T23:50:05.917' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1245', N'W-CP02-TZ6VXXX-0165-AP-00A0', N'CP TZ-6V LC-165 BLA ASPIRA KIT', CAST(N'2023-11-17T23:50:05.997' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1246', N'K-CT00-GC8VXXX-0590-NH-0000', N'CONTAINER GC-8V LC-590 NATURAL', CAST(N'2023-11-17T23:50:06.080' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1247', N'K-CP02-115D31R-0433-AM-HDA0', N'CP 115D31R LC-433 BLA AM HD MF', CAST(N'2023-11-17T23:50:06.170' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1248', N'W-CP00-D26XXXX-0209-AP-HYA0', N'CP D26 LC-209 NAT ASPIRA HYB', CAST(N'2023-11-17T23:50:06.250' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1249', N'W-CP00-D26XXXX-0257-AP-HYA0', N'CP D26 LC-257 NAT ASPIRA HYB', CAST(N'2023-11-17T23:50:06.330' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'125', N'K-SCCV-180X045-11V0-AP-EFA0', N'STIC CVR ASPIRA EFB 180X45 MM', CAST(N'2023-11-17T23:48:27.050' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1250', N'W-CP02-D26XXXX-0252-UL-MFA0', N'CP D26 LC-252 BLA ULTRA MF', CAST(N'2023-11-17T23:50:06.413' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1251', N'W-CP00-D31XXXX-0283-AP-HYA0', N'CP D31 LC-283 NAT ASPIRA HYB', CAST(N'2023-11-17T23:50:06.493' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1252', N'W-CP00-D31XXXX-0375-AP-HYA0', N'CP D31 LC-375 NAT ASPIRA HYB', CAST(N'2023-11-17T23:50:06.577' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1253', N'W-CP00-B20XXXX-0273-AP-HYA0', N'CP B20 LC-273 NAT ASPIRA HYB', CAST(N'2023-11-17T23:50:06.657' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1254', N'W-CP00-B20XXXX-0203-AP-HYA0', N'CP B20 LC-203 NAT ASPIRA HYB', CAST(N'2023-11-17T23:50:06.740' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1255', N'W-CP02-B20XXXX-0220-UL-MFA0', N'CP B20 LC-220 BLA ULTRA MF', CAST(N'2023-11-17T23:50:06.820' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1256', N'W-CP00-B24XXXX-0331-AP-HYA0', N'CP B24 LC-331 NAT ASPIRA HYB', CAST(N'2023-11-17T23:50:06.900' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1257', N'W-CP02-B24XXXX-0232-UL-MFA0', N'CP B24 LC-232 BLA ULTRA MF', CAST(N'2023-11-17T23:50:06.983' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1258', N'W-CP02-4D170LX-1575-AM-HDA0', N'CP 4D170L LC-1575 BLA AM HDT', CAST(N'2023-11-17T23:50:07.067' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1259', N'W-CP02-B20XXXX-0220-IN-MFA0', N'CP B20 LC-220 BLA INCOE MF', CAST(N'2023-11-17T23:50:07.147' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'126', N'K-SCCV-180X045-11V0-AP-MFA0', N'STIC CVR ASPIRA MF 180X45 MM', CAST(N'2023-11-17T23:48:27.137' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1260', N'W-CP08-SG100CT-0771-IN-00A0', N'CP SG100-CT LC-771 GREY INCOE', CAST(N'2023-11-17T23:50:07.227' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1261', N'W-CT02-B20XXXX-0220-NH-0000', N'CONTAINER B20 LC-220 BLACK', CAST(N'2023-11-17T23:50:07.310' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1262', N'W-FOPL-CG87NXX-T3A0-02-7S00', N'FORM PLATE CG87 NEG VT-3A', CAST(N'2023-11-17T23:50:07.390' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1263', N'W-FOPL-CM84APX-L870-02-7S00', N'FORM PLATE CM84-A POS L8-7', CAST(N'2023-11-17T23:50:07.473' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1264', N'W-FOPL-CM8786N-T3A0-02-7S00', N'FORM PLATE CM87/86 NEG VT-3A', CAST(N'2023-11-17T23:50:07.553' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1265', N'W-FOPL-CR82PXX-L870-02-7S00', N'FORM PLATE CR82 POS L8-7', CAST(N'2023-11-17T23:50:07.637' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1266', N'W-FOPL-CR87NXX-T3A0-02-7S00', N'FORM PLATE CR87 NEG VT-3A', CAST(N'2023-11-17T23:50:07.723' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1267', N'W-FOPL-DF72PXX-L34A-01-7S00', N'FORM PLATE DF72 POS L3-4A', CAST(N'2023-11-17T23:50:07.803' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1268', N'W-FOPL-DF78NXX-T3S0-01-7S0A', N'FORM PLATE DF78 NEG VT-3S', CAST(N'2023-11-17T23:50:07.887' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1269', N'W-FOPL-M8783PX-L87C-08-9S0A', N'FORM PLATE M87/83 POS L8-7C', CAST(N'2023-11-17T23:50:07.967' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'127', N'K-SCCV-180X045-11V0-BL-MFA0', N'STC CVR NO BRAND MF 180X45 MM', CAST(N'2023-11-17T23:48:27.220' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1270', N'W-FOPL-M8786NX-T3AC-08-9S0A', N'FORM PLATE M87/86 NEG VT-3AC', CAST(N'2023-11-17T23:50:08.047' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1271', N'W-FOPL-YC62PXX-L65D-02-9S0A', N'FORM PLATE YC62 POS L6-5D', CAST(N'2023-11-17T23:50:08.130' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1272', N'W-FOPL-YC70NXX-T3AD-02-9S0A', N'FORM PLATE YC70 NEG VT-3AD', CAST(N'2023-11-17T23:50:08.213' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1273', N'W-FOPL-YH84PXX-L87C-06-9S0A', N'FORM PLATE YH84 POS L8-7C', CAST(N'2023-11-17T23:50:08.293' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1274', N'W-FOPL-YH86NXX-T3AC-06-9S0A', N'FORM PLATE YH86 NEG VT-3AC', CAST(N'2023-11-17T23:50:08.373' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1275', N'W-FOPL-YS67PXX-L65D-01-9S0A', N'FORM PLATE YS67 POS L6-5D', CAST(N'2023-11-17T23:50:08.457' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1276', N'W-FOPL-YS76NXX-T3AD-01-9S0A', N'FORM PLATE YS76 NEG VT-3AD', CAST(N'2023-11-17T23:50:08.537' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1277', N'W-FOPL-YT71PXX-L65D-01-9S0A', N'FORM PLATE YT71 POS L6-5D', CAST(N'2023-11-17T23:50:08.620' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1278', N'W-FOPL-YT80NXX-T3AD-01-9S0A', N'FORM PLATE YT80 NEG VT-3AD', CAST(N'2023-11-17T23:50:08.703' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1279', N'W-FOPL-YU67PXX-L65D-02-9S0A', N'FORM PLATE YU67 POS L6-5D', CAST(N'2023-11-17T23:50:08.783' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'128', N'K-SCCV-180X045-11V0-IN-EFA0', N'STIC COVER INCOE EFB 180X45 MM', CAST(N'2023-11-17T23:48:27.303' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1280', N'W-FOPL-YU76NXX-T3AD-02-9S0A', N'FORM PLATE YU76 NEG VT-3AD', CAST(N'2023-11-17T23:50:08.867' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1281', N'W-FOPL-YW73PXX-L65D-06-9S0A', N'FORM PLATE YW73 POS L6-5D', CAST(N'2023-11-17T23:50:09.127' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1282', N'W-FOPL-YW75PXX-L65D-06-9S0A', N'FORM PLATE YW75 POS L6-5D', CAST(N'2023-11-17T23:50:10.437' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1283', N'W-FOPL-YW82NXX-T3AD-06-9S0A', N'FORM PLATE YW82 NEG VT-3AD', CAST(N'2023-11-17T23:50:11.547' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1284', N'W-FOCP-DF78NXX-T3S0-01-7S0A', N'FORM CUT PLATE DF78 NEG VT-3S', CAST(N'2023-11-17T23:50:12.540' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1285', N'W-FOCP-M8783PX-L87C-08-9S0A', N'FORM CUT PLATE M87/83 POS L87C', CAST(N'2023-11-17T23:50:13.540' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1286', N'W-FOCP-M8786NX-T3AC-08-9S0A', N'UNF CUT PLATE M87/86 NEG VT3AC', CAST(N'2023-11-17T23:50:14.153' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1287', N'W-FOCP-YC62PXX-L65D-02-9S0A', N'FORM CUT PLATE YC62 POS L6-5D', CAST(N'2023-11-17T23:50:14.240' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1288', N'W-FOCP-YC70NXX-T3AD-02-9S0A', N'FORM CUT PLATE YC70 NEG VT-3AD', CAST(N'2023-11-17T23:50:14.323' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1289', N'W-FOCP-YH84PXX-L87C-06-9S0A', N'FORM CUT PLATE YH84 POS L8-7C', CAST(N'2023-11-17T23:50:14.403' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'129', N'K-SCCV-180X045-11V0-IN-MFC0', N'STC CVR INCOE MF AOP 180X45 MM', CAST(N'2023-11-17T23:48:27.390' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1290', N'W-FOCP-YH86NXX-T3AC-06-9S0A', N'FORM CUT PLATE YH86 NEG VT-3AC', CAST(N'2023-11-17T23:50:14.487' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1291', N'W-FOCP-YS67PXX-L65D-01-9S0A', N'FORM CUT PLATE YS67 POS L6-5D', CAST(N'2023-11-17T23:50:14.570' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1292', N'W-FOCP-YS76NXX-T3AD-01-9S0A', N'FORM CUT PLATE YS76 NEG VT-3AD', CAST(N'2023-11-17T23:50:14.653' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1293', N'W-FOCP-YT71PXX-L65D-01-9S0A', N'FORM CUT PLATE YT71 POS L6-5D', CAST(N'2023-11-17T23:50:14.733' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1294', N'W-FOCP-YT80NXX-T3AD-01-9S0A', N'FORM CUT PLATE YT80 NEG VT-3AD', CAST(N'2023-11-17T23:50:14.813' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1295', N'W-FOCP-YU67PXX-L65D-02-9S0A', N'FORM CUT PLATE YU67 POS L6-5D', CAST(N'2023-11-17T23:50:14.893' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1296', N'W-FOCP-YU76NXX-T3AD-02-9S0A', N'FORM CUT PLATE YU76 NEG VT-3AD', CAST(N'2023-11-17T23:50:14.977' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1297', N'W-FOCP-YW73PXX-L65D-06-9S0A', N'FORM CUT PLATE YW73 POS L6-5D', CAST(N'2023-11-17T23:50:15.057' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1298', N'W-FOCP-YW75PXX-L65D-06-9S0A', N'FORM CUT PLATE YW75 POS L6-5D', CAST(N'2023-11-17T23:50:15.140' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1299', N'W-FOCP-YW82NXX-T3AD-06-9S0A', N'FORM CUT PLATE YW82 NEG VT-3AD', CAST(N'2023-11-17T23:50:15.220' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'13', N'K-CV00-DINCLXX-0MHN-NL-0000', N'CV DIN-C-L NATURAL', CAST(N'2023-11-17T23:48:17.403' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'130', N'K-SCCV-180X045-11V0-LU-EFA0', N'STC COVER LUCAS EFB 180X45 MM', CAST(N'2023-11-17T23:48:27.470' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1300', N'W-FOPL-CG79PXX-L34A-02-7S00', N'FORM PLATE CG79 POS L3-4A', CAST(N'2023-11-17T23:50:15.303' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1301', N'W-FOPL-CG80PXX-L870-02-7S00', N'FORM PLATE CG80 POS L8-7', CAST(N'2023-11-17T23:50:15.390' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1302', N'W-FOPL-CG82PXX-L870-02-7S00', N'FORM PLATE CG82 POS L8-7', CAST(N'2023-11-17T23:50:15.477' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1303', N'W-FOPL-CG84NXX-T3S0-02-7S0A', N'FORM PLATE CG84 NEG VT-3S', CAST(N'2023-11-17T23:50:15.560' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1304', N'W-FOPL-CG85NXX-T3A0-02-7S00', N'FORM PLATE CG85 NEG VT-3A', CAST(N'2023-11-17T23:50:15.640' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1305', N'W-FOPL-CG85PXX-L870-02-7S00', N'FORM PLATE CG85 POS L8-7', CAST(N'2023-11-17T23:50:15.723' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1306', N'W-FOCP-CG79PXX-L34A-02-7S00', N'FORM CUT PLATE CG79 POS L3-4A', CAST(N'2023-11-17T23:50:15.803' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1307', N'W-FOCP-CG80PXX-L870-02-7S00', N'FORM CUT PLATE CG80 POS L8-7', CAST(N'2023-11-17T23:50:15.887' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1308', N'W-FOCP-CG82PXX-L870-02-7S00', N'FORM CUT PLATE CG82 POS L8-7', CAST(N'2023-11-17T23:50:15.970' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1309', N'W-FOCP-CG84NXX-T3S0-02-7S0A', N'FORM CUT PLATE CG84 NEG VT-3S', CAST(N'2023-11-17T23:50:16.050' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'131', N'K-SCCV-180X045-11V0-LU-PRA0', N'STC COVER LUCAS PRE 180X45 MM', CAST(N'2023-11-17T23:48:27.560' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1310', N'W-FOCP-CG85NXX-T3A0-02-7S00', N'FORM CUT PLATE CG85 NEG VT-3A', CAST(N'2023-11-17T23:50:16.130' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1311', N'W-FOCP-CG85PXX-L870-02-7S00', N'FORM CUT PLATE CG85 POS L8-7', CAST(N'2023-11-17T23:50:16.213' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1312', N'W-UNCP-CG85EPX-L870-02-7C00', N'UNF CUT PLATE CG85-E POS L8-7', CAST(N'2023-11-17T23:50:16.293' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1313', N'W-UNCP-CG85NXX-T3A0-02-7S00', N'UNF CUT PLATE CG85 NEG VT-3A', CAST(N'2023-11-17T23:50:16.373' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1314', N'W-UNCP-CG85PXX-L870-02-7S00', N'UNF CUT PLATE CG85 POS L8-7', CAST(N'2023-11-17T23:50:16.460' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1315', N'W-UNCP-CG87NXX-T3A0-02-7S00', N'UNF CUT PLATE CG87 NEG VT-3A', CAST(N'2023-11-17T23:50:16.547' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1316', N'W-UNCP-CM84APX-L870-02-7S00', N'UNF CUT PLATE CM84-A POS L8-7', CAST(N'2023-11-17T23:50:16.627' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1317', N'W-UNCP-CM8786N-T3A0-02-7S00', N'UNF CUT PLATE CM87/86 NEG VT3A', CAST(N'2023-11-17T23:50:16.710' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1318', N'W-UNCP-DF72PXX-L34A-01-7S00', N'UNF CUT PLATE DF72 POS L3-4A', CAST(N'2023-11-17T23:50:16.790' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1319', N'W-UNCP-DF78NXX-T3S0-01-7S0A', N'UNF CUT PLATE DF78 NEG VT-3S', CAST(N'2023-11-17T23:50:16.873' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'132', N'K-SCCV-180X045-11V0-OH-MFB0', N'STC CVR OHAYO MF UAE 180X45 MM', CAST(N'2023-11-17T23:48:27.643' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1320', N'W-UNCP-WG83PXX-L65F-00-7C00', N'UNF CUT PLATE WG83 POS L6-5F', CAST(N'2023-11-17T23:50:16.957' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1321', N'W-UNCP-WG87NXX-T3AF-00-7C00', N'UNF CUT PLATE WG87 NEG VT-3AF', CAST(N'2023-11-17T23:50:17.037' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1322', N'W-UNCP-WM84ESP-L03B-00-7S0A', N'UNF CUT PLATE WM84ES POS L0-3B', CAST(N'2023-11-17T23:50:17.120' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1323', N'W-UNCP-WM84PXX-L65F-00-7S00', N'UNF CUT PLATE WM84 POS L6-5F', CAST(N'2023-11-17T23:50:17.200' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1324', N'W-UNCP-WM85NXX-T3AF-00-7S00', N'UNF CUT PLATE WM85 NEG VT-3AF', CAST(N'2023-11-17T23:50:17.283' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1325', N'W-UNCP-WM87ESN-T3B0-00-7S0A', N'UNF CUT PLATE WM87ES NEG VT-3B', CAST(N'2023-11-17T23:50:17.363' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1326', N'W-UNCP-YA82ESP-L03B-02-7S0A', N'UNF CUT PLATE YA82ES POS L0-3B', CAST(N'2023-11-17T23:50:17.447' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1327', N'W-UNCP-YA85ESN-T3B0-02-7S0A', N'UNF CUT PLATE YA85ES NEG VT-3B', CAST(N'2023-11-17T23:50:17.527' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1328', N'W-UNCP-CG79PXX-L34A-02-7S00', N'UNF CUT PLATE CG79 POS L3-4A', CAST(N'2023-11-17T23:50:17.610' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1329', N'W-UNCP-CG80PXX-L870-02-7S00', N'UNF CUT PLATE CG80 POS L8-7', CAST(N'2023-11-17T23:50:17.690' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'133', N'K-SCCV-180X045-11V0-QU-EFA0', N'STC CVR QUANTUM EFB 180X45 MM', CAST(N'2023-11-17T23:48:27.723' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1330', N'W-UNCP-CG82PXX-L870-02-7S00', N'UNF CUT PLATE CG82 POS L8-7', CAST(N'2023-11-17T23:50:17.770' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1331', N'W-UNCP-CG84NXX-T3S0-02-7S0A', N'UNF CUT PLATE CG84 NEG VT-3S', CAST(N'2023-11-17T23:50:17.850' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1332', N'W-UNCP-YM84NXX-T3A0-02-7S00', N'UNF CUT PLATE YM84 NEG VT-3A', CAST(N'2023-11-17T23:50:17.933' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1333', N'W-FOCP-CG87NXX-T3A0-02-7S00', N'FORM CUT PLATE CG87 NEG VT-3A', CAST(N'2023-11-17T23:50:18.017' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1334', N'W-FOCP-CM84APX-L870-02-7S00', N'FORM CUT PLATE CM84-A POS L8-7', CAST(N'2023-11-17T23:50:18.097' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1335', N'W-FOCP-CM8786N-T3A0-02-7S00', N'FOR CUT PLATE CM87/86 NEG VT3A', CAST(N'2023-11-17T23:50:18.177' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1336', N'W-FOCP-CR82PXX-L870-02-7S00', N'FORM CUT PLATE CR82 POS L8-7', CAST(N'2023-11-17T23:50:18.260' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1337', N'W-FOCP-CR87NXX-T3A0-02-7S00', N'FORM CUT PLATE CR87 NEG VT-3A', CAST(N'2023-11-17T23:50:18.340' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1338', N'W-FOCP-DF72PXX-L34A-01-7S00', N'FORM CUT PLATE DF72 POS L3-4A', CAST(N'2023-11-17T23:50:18.420' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1339', N'W-UNPL-YG85NXX-T3A0-02-7S00', N'UNFORM PLATE YG85 NEG VT-3A', CAST(N'2023-11-17T23:50:18.503' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'134', N'K-SCCV-180X045-11V0-QU-MFA0', N'STC CVR QUANTUM MF 180X45 MM', CAST(N'2023-11-17T23:48:27.810' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1340', N'W-UNPL-WG83PXX-L65F-00-7C00', N'UNFORM PLATE WG83 POS L6-5F', CAST(N'2023-11-17T23:50:18.583' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1341', N'W-UNPL-WG87NXX-T3AF-00-7C00', N'UNFORM PLATE WG87 NEG VT-3AF', CAST(N'2023-11-17T23:50:18.663' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1342', N'W-UNPL-WM84ESP-L03B-00-7S0A', N'UNFORM PLATE WM84ES POS L0-3B', CAST(N'2023-11-17T23:50:18.743' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1343', N'W-UNPL-WM84PXX-L65F-00-7S00', N'UNFORM PLATE WM84 POS L6-5F', CAST(N'2023-11-17T23:50:18.823' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1344', N'W-UNPL-WM85NXX-T3AF-00-7S00', N'UNFORM PLATE WM85 NEG VT-3AF', CAST(N'2023-11-17T23:50:18.907' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1345', N'W-UNPL-WM87ESN-T3B0-00-7S0A', N'UNFORM PLATE WM87ES NEG VT-3B', CAST(N'2023-11-17T23:50:18.987' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1346', N'W-UNPL-CG79PXX-L34A-02-7S00', N'UNFORM PLATE CG79 POS L3-4A', CAST(N'2023-11-17T23:50:19.067' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1347', N'W-UNPL-CG80PXX-L870-02-7S00', N'UNFORM PLATE CG80 POS L8-7', CAST(N'2023-11-17T23:50:19.150' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1348', N'W-UNPL-CG82PXX-L870-02-7S00', N'UNFORM PLATE CG82 POS L8-7', CAST(N'2023-11-17T23:50:19.240' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1349', N'W-UNPL-CG84NXX-T3S0-02-7S0A', N'UNFORM PLATE CG84 NEG VT-3S', CAST(N'2023-11-17T23:50:19.320' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'135', N'K-SCCV-180X045-11VJ-AS-EFA0', N'STIC CVR ASAHI EFB 180X45 MM', CAST(N'2023-11-17T23:48:27.890' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1350', N'W-UNPL-CG85EPX-L870-02-7C00', N'UNFORM PLATE CG85-E POS L8-7', CAST(N'2023-11-17T23:50:19.400' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1351', N'W-UNPL-CG85NXX-T3A0-02-7S00', N'UNFORM PLATE CG85 NEG VT-3A', CAST(N'2023-11-17T23:50:19.483' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1352', N'W-UNPL-CG85PXX-L870-02-7S00', N'UNFORM PLATE CG85 POS L8-7', CAST(N'2023-11-17T23:50:19.567' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1353', N'W-UNPL-CG87NXX-T3A0-02-7S00', N'UNFORM PLATE CG87 NEG VT-3A', CAST(N'2023-11-17T23:50:19.647' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1354', N'W-UNPL-CM84APX-L870-02-7S00', N'UNFORM PLATE CM84-A POS L8-7', CAST(N'2023-11-17T23:50:19.727' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1355', N'W-UNPL-CM8786N-T3A0-02-7S00', N'UNFORM PLATE CM87/86 NEG VT-3A', CAST(N'2023-11-17T23:50:19.807' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1356', N'W-UNPL-CR82PXX-L870-02-7S00', N'UNFORM PLATE CR82 POS L8-7', CAST(N'2023-11-17T23:50:19.890' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1357', N'W-UNPL-CR87NXX-T3A0-02-7S00', N'UNFORM PLATE CR87 NEG VT-3A', CAST(N'2023-11-17T23:50:19.970' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1358', N'W-UNPL-DF72PXX-L34A-01-7S00', N'UNFORM PLATE DF72 POS L3-4A', CAST(N'2023-11-17T23:50:20.050' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1359', N'W-UNPL-DF78NXX-T3S0-01-7S0A', N'UNFORM PLATE DF78 NEG VT-3S', CAST(N'2023-11-17T23:50:20.133' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'136', N'K-SCCV-180X045-11VJ-AS-MFA0', N'STIC CVR ASAHI MF 180X45 MM', CAST(N'2023-11-17T23:48:27.973' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1360', N'W-UNPL-M8783PX-L87C-08-9S0A', N'UNFORM PLATE M87/83 POS L8-7C', CAST(N'2023-11-17T23:50:20.220' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1361', N'W-UNPL-M8786NX-T3AC-08-9S0A', N'UNFORM PLATE M87/86 NEG VT-3AC', CAST(N'2023-11-17T23:50:20.303' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1362', N'W-UNPL-YA82ESP-L03B-02-7S0A', N'UNFORM PLATE YA82ES POS L0-3B', CAST(N'2023-11-17T23:50:20.383' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1363', N'W-UNPL-YA85ESN-T3B0-02-7S0A', N'UNFORM PLATE YA85ES NEG VT-3B', CAST(N'2023-11-17T23:50:20.467' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1364', N'W-UNPL-YC62PXX-L65D-02-9S0A', N'UNFORM PLATE YC62 POS L6-5D', CAST(N'2023-11-17T23:50:20.550' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1365', N'W-UNPL-YC70NXX-T3AD-02-9S0A', N'UNFORM PLATE YC70 NEG VT-3AD', CAST(N'2023-11-17T23:50:20.630' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1366', N'W-UNPL-YD84PXX-L870-02-7S00', N'UNFORM PLATE YD84 POS L8-7', CAST(N'2023-11-17T23:50:20.750' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1367', N'W-UNPL-YD85NXX-T3A0-02-7S00', N'UNFORM PLATE YD85 NEG VT-3A', CAST(N'2023-11-17T23:50:20.830' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1368', N'W-UNPL-YG79HDP-L650-02-7S00', N'UNFORM PLATE YG79HD POS L6-5', CAST(N'2023-11-17T23:50:20.917' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1369', N'W-UNPL-YG8079P-L340-02-7S00', N'UNFORM PLATE YG80/79 POS L3-4', CAST(N'2023-11-17T23:50:21.010' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'137', N'K-SCCV-180X045-11VJ-IN-MFA0', N'STIC COVER INCOE MF 180X45 MM', CAST(N'2023-11-17T23:48:28.057' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1370', N'W-UNPL-YG8079P-L650-02-7S00', N'UNFORM PLATE YG80/79 POS L6-5', CAST(N'2023-11-17T23:50:21.107' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1371', N'W-UNPL-YG82HDP-L650-02-7S00', N'UNFORM PLATE YG82HD POS L6-5', CAST(N'2023-11-17T23:50:21.200' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1372', N'W-UNPL-YG82PXX-L650-02-7S00', N'UNFORM PLATE YG82 POS L6-5', CAST(N'2023-11-17T23:50:21.297' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1373', N'W-UNCP-YD84PXX-L870-02-7S00', N'UNF CUT PLATE YD84 POS L8-7', CAST(N'2023-11-17T23:50:21.390' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1374', N'W-UNCP-YD85NXX-T3A0-02-7S00', N'UNF CUT PLATE YD85 NEG VT-3A', CAST(N'2023-11-17T23:50:21.483' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1375', N'W-UNCP-YG79HDP-L650-02-7S00', N'UNF CUT PLATE YG79HD POS L6-5', CAST(N'2023-11-17T23:50:21.577' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1376', N'W-UNCP-YG8079P-L340-02-7S00', N'UNF CUT PLATE YG80/79 POS L3-4', CAST(N'2023-11-17T23:50:21.657' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1377', N'W-UNCP-YG8079P-L650-02-7S00', N'UNF CUT PLATE YG80/79 POS L6-5', CAST(N'2023-11-17T23:50:21.737' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1378', N'W-UNCP-YG82HDP-L650-02-7S00', N'UNF CUT PLATE YG82HD POS L6-5', CAST(N'2023-11-17T23:50:21.820' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1379', N'W-UNCP-YG82PXX-L650-02-7S00', N'UNF CUT PLATE YG82 POS L6-5', CAST(N'2023-11-17T23:50:21.900' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'138', N'K-SCCV-180X045-11VJ-OH-EFA0', N'STIC COVER OHAYO EFB 180X45 MM', CAST(N'2023-11-17T23:48:28.140' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1380', N'W-UNCP-YG85NXX-T3A0-02-7S00', N'UNF CUT PLATE YG85 NEG VT-3A', CAST(N'2023-11-17T23:50:21.980' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1381', N'W-UNCP-YG85PXX-L650-02-7S00', N'UNF CUT PLATE YG85 POS L6-5', CAST(N'2023-11-17T23:50:22.060' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1382', N'W-UNCP-YG87NXX-T3A0-02-7S00', N'UNF CUT PLATE YG87 NEG VT-3A', CAST(N'2023-11-17T23:50:22.143' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1383', N'W-UNCP-YL80PXX-L650-02-7S00', N'UNF CUT PLATE YL80 POS L6-5', CAST(N'2023-11-17T23:50:22.223' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1384', N'W-UNCP-YL84NXX-T3A0-02-7S00', N'UNF CUT PLATE YL84 NEG VT-3A', CAST(N'2023-11-17T23:50:22.303' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1385', N'W-UNPL-YG85PXX-L650-02-7S00', N'UNFORM PLATE YG85 POS L6-5', CAST(N'2023-11-17T23:50:22.387' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1386', N'W-UNPL-YG87NXX-T3A0-02-7S00', N'UNFORM PLATE YG87 NEG VT-3A', CAST(N'2023-11-17T23:50:22.467' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1387', N'W-UNPL-YH84PXX-L87C-06-9S0A', N'UNFORM PLATE YH84 POS L8-7C', CAST(N'2023-11-17T23:50:22.550' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1388', N'W-UNPL-YH86NXX-T3AC-06-9S0A', N'UNFORM PLATE YH86 NEG VT-3AC', CAST(N'2023-11-17T23:50:22.630' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1389', N'W-UNPL-YL80PXX-L650-02-7S00', N'UNFORM PLATE YL80 POS L6-5', CAST(N'2023-11-17T23:50:22.713' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'139', N'K-SCCV-180X045-11VJ-OH-MFA0', N'STIC COVER OHAYO MF 180X45 MM', CAST(N'2023-11-17T23:48:28.223' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1390', N'W-UNPL-YL84NXX-T3A0-02-7S00', N'UNFORM PLATE YL84 NEG VT-3A', CAST(N'2023-11-17T23:50:22.793' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1391', N'W-UNPL-YM84NXX-T3A0-02-7S00', N'UNFORM PLATE YM84 NEG VT-3A', CAST(N'2023-11-17T23:50:22.877' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1392', N'W-UNPL-YS67PXX-L65D-01-9S0A', N'UNFORM PLATE YS67 POS L6-5D', CAST(N'2023-11-17T23:50:22.960' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1393', N'W-UNPL-YS76NXX-T3AD-01-9S0A', N'UNFORM PLATE YS76 NEG VT-3AD', CAST(N'2023-11-17T23:50:23.040' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1394', N'W-UNPL-YT71PXX-L65D-01-9S0A', N'UNFORM PLATE YT71 POS L6-5D', CAST(N'2023-11-17T23:50:23.120' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1395', N'W-UNPL-YT80NXX-T3AD-01-9S0A', N'UNFORM PLATE YT80 NEG VT-3AD', CAST(N'2023-11-17T23:50:23.200' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1396', N'W-UNPL-YU67PXX-L65D-02-9S0A', N'UNFORM PLATE YU67 POS L6-5D', CAST(N'2023-11-17T23:50:23.280' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1397', N'W-UNPL-YU76NXX-T3AD-02-9S0A', N'UNFORM PLATE YU76 NEG VT-3AD', CAST(N'2023-11-17T23:50:23.360' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1398', N'W-UNPL-YW73PXX-L65D-06-9S0A', N'UNFORM PLATE YW73 POS L6-5D', CAST(N'2023-11-17T23:50:23.443' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1399', N'W-UNPL-YW75PXX-L65D-06-9S0A', N'UNFORM PLATE YW75 POS L6-5D', CAST(N'2023-11-17T23:50:23.527' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'14', N'K-CV02-DINBLXX-0MHN-NL-0000', N'CV DIN-B-L BLACK', CAST(N'2023-11-17T23:48:17.490' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'140', N'K-SCCV-180X045-15V0-IN-MFB0', N'STC CVR INCOE MF UAE 180X45 MM', CAST(N'2023-11-17T23:48:28.307' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1400', N'W-UNPL-YW82NXX-T3AD-06-9S0A', N'UNFORM PLATE YW82 NEG VT-3AD', CAST(N'2023-11-17T23:50:23.607' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1401', N'W-UNCP-YF78NXX-T3ED-01-7S0A', N'UNF CUT PLATE YF78 NEG VT-ED', CAST(N'2023-11-17T23:50:23.687' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1402', N'W-UNPL-YF78NXX-T3ED-01-7S0A', N'UNFORM PLATE YF78 NEG VT-3ED', CAST(N'2023-11-17T23:50:23.767' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1403', N'W-ACDP-65D31RX-B3C3-IN-PPAR', N'65D31R STD DC INC PRE-SAW REP', CAST(N'2023-11-17T23:50:23.850' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1404', N'W-ACUP-46B24RX-B1C4-AP-PRB0', N'WIP 46B24R SUB-1 UC ASPIRA PRE', CAST(N'2023-11-17T23:50:23.930' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1405', N'W-ACUP-65D26LX-B1C4-AS-HPC0', N'WIP 65D26L SUB-1 UC ASA HP - C', CAST(N'2023-11-17T23:50:24.010' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1406', N'W-ACUP-65D26RX-B1C4-AS-HPC0', N'WIP 65D26R SUB-1 UC ASA HP - C', CAST(N'2023-11-17T23:50:24.093' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1407', N'W-ACUP-95D31LX-B5C4-AP-PRB0', N'WIP 95D31L SUB-1 UC ASPIRA PRE', CAST(N'2023-11-17T23:50:24.170' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1408', N'W-ACUP-95D31LX-B5C4-AS-HPC0', N'WIP 95D31L SUB-1 UC ASA HP - C', CAST(N'2023-11-17T23:50:24.250' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1409', N'W-ACUP-95D31RX-B5C4-AP-PRB0', N'WIP 95D31R SUB-1 UC ASPIRA PRE', CAST(N'2023-11-17T23:50:24.333' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'141', N'K-SCCV-188X050-11V0-AP-MFA0', N'STIC CVR ASPIRA MF 188X50 MM', CAST(N'2023-11-17T23:48:28.390' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1410', N'W-ACUP-95D31RX-B5C4-AS-HPC0', N'WIP 95D31R SUB-1 UC ASA HP - C', CAST(N'2023-11-17T23:50:24.420' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1411', N'W-ACUP-95E41LX-B5C4-AS-HPC0', N'WIP 95E41L SUB-1 UC ASA HP - C', CAST(N'2023-11-17T23:50:24.500' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1412', N'W-ACUP-95E41RX-B5C4-AP-PRB0', N'WIP 95E41R SUB-1 UC ASPIRA PRE', CAST(N'2023-11-17T23:50:24.580' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1413', N'W-ACUP-95E41RX-B5C4-AS-HPC0', N'WIP 95E41R SUB-1 UC ASA HP - C', CAST(N'2023-11-17T23:50:24.660' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1414', N'W-ACUR-8D90XXX-B4C3-IN-00A0', N'WIP 8D90 STD UC INCOE', CAST(N'2023-11-17T23:50:24.743' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1415', N'W-ACWR-8D90XXX-B4C3-IN-00A0', N'WIP 8D90 STD WC INCOE', CAST(N'2023-11-17T23:50:24.827' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1416', N'W-ACWR-8D90XXX-B4C3-IN-00AV', N'WIP 8D90 STD WC-V INCOE', CAST(N'2023-11-17T23:50:24.910' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1417', N'W-AHUP-32B20LS-A8A3-IN-GOB0', N'WIP 32B20LS STD UC INCOE GOLD', CAST(N'2023-11-17T23:50:24.993' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1418', N'W-AHUP-32B20LX-A8A3-IN-GOB0', N'WIP 32B20L STD UC INCOE GOLD', CAST(N'2023-11-17T23:50:25.077' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1419', N'W-AHUP-32B20RS-A8A3-IN-GOB0', N'WIP 32B20RS STD UC INCOE GOLD', CAST(N'2023-11-17T23:50:25.160' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'142', N'K-SCCV-188X050-11V0-BL-MFA0', N'STC CVR NO BRAND MF 188X50 MM', CAST(N'2023-11-17T23:48:28.477' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1420', N'W-AHUP-32B20RX-A8A3-IN-GOB0', N'WIP 32B20R STD UC INCOE GOLD', CAST(N'2023-11-17T23:50:25.247' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1421', N'W-AHUP-34B20LX-B0A3-IN-GOB0', N'WIP 34B20L STD UC INCOE GOLD', CAST(N'2023-11-17T23:50:25.327' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1422', N'W-AHUP-34B20LX-B0C3-IN-GOA0', N'WIP 34B20L STD UC INCOE GOL OE', CAST(N'2023-11-17T23:50:25.410' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1423', N'W-AHUP-34B20RX-B0A3-IN-GOB0', N'WIP 34B20R STD UC INCOE GOLD', CAST(N'2023-11-17T23:50:25.490' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1424', N'W-AHUP-36B20LS-B0A3-IN-GOB0', N'WIP 36B20LS STD UC INCOE GOLD', CAST(N'2023-11-17T23:50:25.577' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1425', N'W-AHUP-36B20LX-B0A3-IN-GOB0', N'WIP 36B20L STD UC INCOE GOLD', CAST(N'2023-11-17T23:50:25.657' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1426', N'W-AMDF-SG100XX-B1C3-IN-00A0', N'WIP SG-100 STD DC INCOE', CAST(N'2023-11-17T23:50:25.740' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1427', N'W-AMUP-210H52X-D4P3-BL-MFA0', N'W-MF 210H52 STD UC NON BRAND', CAST(N'2023-11-17T23:50:25.820' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1428', N'W-AMUP-210H52X-D4P3-IN-MFA0', N'W-MF 210H52 STD UC INCOE MF-A', CAST(N'2023-11-17T23:50:25.900' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1429', N'W-AMUP-210H52X-D4P3-IN-MFB0', N'W-MF 210H52 STD UC INCOE MF-B', CAST(N'2023-11-17T23:50:25.983' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'143', N'K-SCCV-188X050-11V0-OH-MFB0', N'STC CVR OHAYO MF UAE 188X50 MM', CAST(N'2023-11-17T23:48:28.560' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1430', N'W-AMUP-210H52X-D4P3-IN-MFC0', N'W-MF 210H52 STD UC INCOE MF-C', CAST(N'2023-11-17T23:50:26.067' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1431', N'W-AMUP-210H52X-D4P3-IN-MFD0', N'W-MF 210H52 STD UC INCOE SUD', CAST(N'2023-11-17T23:50:26.147' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1432', N'W-AMUP-210H52X-D4P3-IN-MFE0', N'W-MF 210H52 STD UC INCOE PHI', CAST(N'2023-11-17T23:50:26.227' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1433', N'W-AMUP-210H52X-D4P3-IN-MFH0', N'W-MF 210H52 STD UC INCOE VIE', CAST(N'2023-11-17T23:50:26.310' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1434', N'W-AMDF-SG200XX-C3C3-BL-00A0', N'WIP SG-200 STD DC NO BRAND', CAST(N'2023-11-17T23:50:26.390' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1435', N'W-AMDF-SG45XXX-A7C3-AM-00A0', N'WIP SG-45 STD DC ALL MAKES', CAST(N'2023-11-17T23:50:26.470' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1436', N'W-AMDF-SG65XXX-B1C3-IN-00A0', N'WIP SG-65 STD DC INCOE', CAST(N'2023-11-17T23:50:26.560' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1437', N'W-AMUP-100D31L-B4P3-AP-MFA0', N'W-MF 100D31L STD UC ASPIRA MFA', CAST(N'2023-11-17T23:50:26.647' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1438', N'W-AMUP-100D31L-B4P3-AS-HPB0', N'W-MF 100D31L STD UC ASAHI HP-B', CAST(N'2023-11-17T23:50:26.730' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1439', N'W-AMUP-100D31L-B4P3-BL-MFA0', N'W-MF 100D31L STD UC NON BRAND', CAST(N'2023-11-17T23:50:26.810' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'144', N'K-SCCV-188X050-11V0-QU-MFA0', N'STC CVR QUANTUM MF 188X50 MM', CAST(N'2023-11-17T23:48:28.647' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1440', N'W-AMUP-100D31L-B4P3-IN-MFA0', N'W-MF 100D31L STD UC INCOE MF-A', CAST(N'2023-11-17T23:50:26.890' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1441', N'W-AMUP-100D31L-B4P3-IN-MFB0', N'W-MF 100D31L STD UC INCOE MF-B', CAST(N'2023-11-17T23:50:26.970' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1442', N'W-AMUP-100D31L-B4P3-IN-MFC0', N'W-MF 100D31L STD UC INCOE MF-C', CAST(N'2023-11-17T23:50:27.050' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1443', N'W-AMUP-100D31L-B4P3-IN-MFH0', N'W-MF 100D31L STD UC INCOE VIE', CAST(N'2023-11-17T23:50:27.133' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1444', N'W-AMUP-100D31L-B4P3-IN-MFJ0', N'W-MF 100D31L STD UC INCOE UAE', CAST(N'2023-11-17T23:50:27.213' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1445', N'W-AMUP-100D31L-B4P3-OH-MFA0', N'W-MF 100D31L STD UC OHAYO MF-A', CAST(N'2023-11-17T23:50:27.297' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1446', N'W-AMUP-100D31L-B4P3-OH-MFB0', N'W-MF 100D31L STD UC OHAYO UAE', CAST(N'2023-11-17T23:50:27.377' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1447', N'W-AMUP-100D31R-B4P3-AP-MFA0', N'W-MF 100D31R STD UC ASPIRA MFA', CAST(N'2023-11-17T23:50:27.460' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1448', N'W-AMUP-100D31R-B4P3-AS-HPB0', N'W-MF 100D31R STD UC ASAHI HP-B', CAST(N'2023-11-17T23:50:27.543' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1449', N'W-AMUP-100D31R-B4P3-BL-MFA0', N'W-MF 100D31R STD UC NON BRAND', CAST(N'2023-11-17T23:50:27.623' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'145', N'K-SCCV-188X050-11VE-AS-MFA0', N'STIC CVR ASAHI MF 188X50 MM', CAST(N'2023-11-17T23:48:28.730' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1450', N'W-AMUP-100D31R-B4P3-IN-MFA0', N'W-MF 100D31R STD UC INCOE MF-A', CAST(N'2023-11-17T23:50:27.707' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1451', N'W-AMUP-100D31R-B4P3-IN-MFB0', N'W-MF 100D31R STD UC INCOE MF-B', CAST(N'2023-11-17T23:50:27.787' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1452', N'W-AMUP-100D31R-B4P3-IN-MFC0', N'W-MF 100D31R STD UC INCOE MF-C', CAST(N'2023-11-17T23:50:27.870' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1453', N'W-AHUP-36B20RX-B0A3-IN-GOB0', N'WIP 36B20R STD UC INCOE GOLD', CAST(N'2023-11-17T23:50:27.950' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1454', N'W-AHUP-46B24LS-B1A3-IN-GOB0', N'WIP 46B24LS STD UC INCOE GOLD', CAST(N'2023-11-17T23:50:28.030' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1455', N'W-AHUP-46B24LX-B1A3-IN-GOB0', N'WIP 46B24L STD UC INCOE GOLD', CAST(N'2023-11-17T23:50:28.110' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1456', N'W-AHUP-46B24RS-B1A3-IN-GOB0', N'WIP 46B24RS STD UC INCOE GOLD', CAST(N'2023-11-17T23:50:28.190' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1457', N'W-AMUP-170G51X-C7C3-IN-MFH0', N'W-MF 170G51 STD UC INCOE VIE', CAST(N'2023-11-17T23:50:28.270' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1458', N'W-AMUP-170G51X-C7C3-IN-MFJ0', N'W-MF 170G51 STD UC INCOE UAE', CAST(N'2023-11-17T23:50:28.353' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1459', N'W-AMUP-170G51X-C7C3-LU-00A0', N'W-MF 170G51 STD UC LUCAS', CAST(N'2023-11-17T23:50:28.437' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'146', N'W-CV01-D23LXXX-D01N-NL-DQ00', N'CV D23L BLUE HD-BLUE DG-QR', CAST(N'2023-11-17T23:48:28.813' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1460', N'W-AMUP-170G51X-C7C3-NI-MFA0', N'W-MF 170G51 STD UC NIKO MF-A', CAST(N'2023-11-17T23:50:28.517' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1461', N'W-AMUP-170G51X-C7C3-OH-MFA0', N'W-MF 170G51 STD UC OHAYO MF-A', CAST(N'2023-11-17T23:50:28.597' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1462', N'W-AMUP-180G51X-D1C1-AM-HDA0', N'W-MF 180G51 HD UC AMK HDT', CAST(N'2023-11-17T23:50:28.680' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1463', N'W-AMUP-180G51X-D1C1-IN-MFF0', N'W-MF 180G51 HD UC INCOE AOP', CAST(N'2023-11-17T23:50:28.760' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1464', N'W-AMUP-190H52X-D4P3-AM-PRA0', N'W-MF 190H52 STD UC AMK PRE', CAST(N'2023-11-17T23:50:28.843' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1465', N'W-AMUP-190H52X-D4P3-IN-MFF0', N'W-MF 190H52 STD UC INCOE AOP', CAST(N'2023-11-17T23:50:28.923' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1466', N'W-AMUP-190H52X-D4P3-OH-MFB0', N'W-MF 190H52 STD UC OHAYO UAE', CAST(N'2023-11-17T23:50:29.007' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1467', N'W-AMUP-190H52X-D4P3-QU-MFA0', N'W-MF 190H52 STD UC QUANTUM-A', CAST(N'2023-11-17T23:50:29.087' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1468', N'W-AMUP-190H52X-D4P3-QU-MFB0', N'W-MF 190H52 STD UC QUANTUM-B', CAST(N'2023-11-17T23:50:29.170' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1469', N'W-AMUP-190H52X-D4P3-QU-MFC0', N'W-MF 190H52 STD UC QUANTUM-C', CAST(N'2023-11-17T23:50:29.257' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'147', N'W-CV01-D23RXXX-D01N-NL-DG00', N'CV D23R BLUE HD-BLUE', CAST(N'2023-11-17T23:48:28.897' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1470', N'W-AMUP-190H52X-D4P3-QU-MFD0', N'W-MF 190H52 STD UC QUANTUM-D', CAST(N'2023-11-17T23:50:29.337' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1471', N'W-AMUP-190H52X-D4P3-QU-MFF0', N'W-MF 190H52 STD UC QUANTUM-F', CAST(N'2023-11-17T23:50:29.420' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1472', N'W-AMUP-190H52X-E0C1-TN-HDA0', N'W-MF 190H52 HD UC TRAKNUS HD', CAST(N'2023-11-17T23:50:29.500' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1473', N'W-AMUP-210H52X-D4P3-AP-MFA0', N'W-MF 210H52 STD UC ASPIRA MF-A', CAST(N'2023-11-17T23:50:29.580' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1474', N'W-AMUP-210H52X-D4P3-AS-HPB0', N'W-MF 210H52 STD UC ASAHI HP-B', CAST(N'2023-11-17T23:50:29.660' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1475', N'W-AMUP-210H52X-D4P3-AS-HPC0', N'W-MF 210H52 STD UC ASAHI HP-C', CAST(N'2023-11-17T23:50:29.743' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1476', N'W-AMUP-210H52X-D4P3-AS-HPD0', N'W-MF 210H52 STD UC ASAHI HP-D', CAST(N'2023-11-17T23:50:29.823' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1477', N'W-AMUP-100D31R-B4P3-IN-MFH0', N'W-MF 100D31R STD UC INCOE VIE', CAST(N'2023-11-17T23:50:29.910' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1478', N'W-AMUP-100D31R-B4P3-IN-MFJ0', N'W-MF 100D31R STD UC INCOE UAE', CAST(N'2023-11-17T23:50:29.990' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1479', N'W-AMUP-100D31R-B4P3-OH-MFA0', N'W-MF 100D31R STD UC OHAYO MF-A', CAST(N'2023-11-17T23:50:30.070' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'148', N'W-CV01-D23RXXX-D01N-NL-DQ00', N'CV D23R BLUE HD-BLUE DG-QR', CAST(N'2023-11-17T23:48:28.980' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1480', N'W-AMUP-105D31L-B6P3-AS-MEA0', N'W-MF 105D31L STD UC ASAHI MEN', CAST(N'2023-11-17T23:50:30.150' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1481', N'W-AMUP-105D31L-B6P3-IN-MFK0', N'W-MF 105D31L STD UC INCOE DSP', CAST(N'2023-11-17T23:50:30.243' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1482', N'W-AMUP-105D31L-B6P3-QU-MFA0', N'W-MF 105D31L STD UC QUANTUM-A', CAST(N'2023-11-17T23:50:30.323' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1483', N'W-AMUP-105D31L-B6P3-QU-MFB0', N'W-MF 105D31L STD UC QUANTUM-B', CAST(N'2023-11-17T23:50:30.407' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1484', N'W-AMUP-45B20LX-B0P3-BL-MFD0', N'W-MF 45B20L STD UC N-BRA AMK', CAST(N'2023-11-17T23:50:30.487' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1485', N'W-AMUP-115E41R-B7C3-AS-HPD0', N'W-MF 115E41R STD UC ASAHI HP-D', CAST(N'2023-11-17T23:50:30.570' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1486', N'W-AMUP-110D31R-B6P3-IN-MFA0', N'W-MF 110D31R STD UC INCOE MF-A', CAST(N'2023-11-17T23:50:30.650' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1487', N'W-AMUP-40B20LX-A9P3-IN-MFA0', N'W-MF 40B20L STD UC INCOE MF-A', CAST(N'2023-11-17T23:50:30.730' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1488', N'W-AMUP-40B20LX-A9P3-IN-MFB0', N'W-MF 40B20L STD UC INCOE MF-B', CAST(N'2023-11-17T23:50:30.813' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1489', N'W-AMUP-40B20LX-A9P3-IN-MFC0', N'W-MF 40B20L STD UC INCOE MF-C', CAST(N'2023-11-17T23:50:30.893' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'149', N'W-CV01-D26LXXX-C01N-NL-DG00', N'CV D26L BLUE HD-BLUE', CAST(N'2023-11-17T23:48:29.070' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1490', N'W-AMUP-40B20LX-A9P3-IN-MFD0', N'W-MF 40B20L STD UC INCOE SUD', CAST(N'2023-11-17T23:50:30.977' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1491', N'W-AMUP-40B20LX-A9P3-IN-MFJ0', N'W-MF 40B20L STD UC INCOE UAE', CAST(N'2023-11-17T23:50:31.057' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1492', N'W-AMUP-40B20LX-A9P3-OH-MFA0', N'W-MF 40B20L STD UC OHAYO MF-A', CAST(N'2023-11-17T23:50:31.137' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1493', N'W-AMUP-40B20RS-A9P3-BL-MFA0', N'W-MF 40B20RS STD UC NON BRAND', CAST(N'2023-11-17T23:50:31.217' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1494', N'W-AMUP-40B20RS-A9P3-IN-MFA0', N'W-MF 40B20RS STD UC INCOE MF-A', CAST(N'2023-11-17T23:50:31.300' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1495', N'W-AMUP-40B20RX-A9P2-IN-MFB0', N'W-MF 40B20R C20 UC INCOE MF-B', CAST(N'2023-11-17T23:50:31.380' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1496', N'W-AMUP-40B20RX-A9P3-AP-MFA0', N'W-MF 40B20R STD UC ASPIRA MF-A', CAST(N'2023-11-17T23:50:31.463' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1497', N'W-AMUP-40B20RX-A9P3-AS-HPB0', N'W-MF 40B20R STD UC ASAHI HP-B', CAST(N'2023-11-17T23:50:31.543' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1498', N'W-AMUP-40B20RX-A9P3-BL-MFA0', N'W-MF 40B20R STD UC NON BRAND', CAST(N'2023-11-17T23:50:31.630' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1499', N'W-AMUP-40B20RX-A9P3-IN-MFA0', N'W-MF 40B20R STD UC INCOE MF-A', CAST(N'2023-11-17T23:50:31.723' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'15', N'K-CV02-DINCLXX-0MHN-NL-0000', N'CV DIN-C-L BLACK', CAST(N'2023-11-17T23:48:17.580' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'150', N'W-CV01-D26LXXX-C03N-NL-DG00', N'CV D26L BLUE HD-ORANGE', CAST(N'2023-11-17T23:48:29.157' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1500', N'W-AMUP-40B20RX-A9P3-IN-MFB0', N'W-MF 40B20R STD UC INCOE MF-B', CAST(N'2023-11-17T23:50:31.820' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1501', N'W-AMUP-40B20RX-A9P3-IN-MFC0', N'W-MF 40B20R STD UC INCOE MF-C', CAST(N'2023-11-17T23:50:31.917' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1502', N'W-AMUP-40B20RX-A9P3-IN-MFD0', N'W-MF 40B20R STD UC INCOE SUD', CAST(N'2023-11-17T23:50:32.010' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1503', N'W-AMUP-40B20RX-A9P3-IN-MFJ0', N'W-MF 40B20R STD UC INCOE UAE', CAST(N'2023-11-17T23:50:32.103' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1504', N'W-AMUP-40B20RX-A9P3-OH-MFA0', N'W-MF 40B20R STD UC OHAYO MF-A', CAST(N'2023-11-17T23:50:32.193' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1505', N'W-AMUP-45B20LS-B0P3-IN-MFH0', N'W-MF 45B20LS STD UC INCOE VIE', CAST(N'2023-11-17T23:50:32.283' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1506', N'W-AMUP-45B20LS-B0P3-OH-MFA0', N'W-MF 45B20LS STD UC OHAYO MF-A', CAST(N'2023-11-17T23:50:32.367' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1507', N'W-AMUP-45B20LS-B0P3-OH-MFB0', N'W-MF 45B20LS STD UC OHAYO UAE', CAST(N'2023-11-17T23:50:32.447' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1508', N'W-ACUP-46B24LX-B1C4-AP-PRB0', N'WIP 46B24L SUB-1 UC ASPIRA PRE', CAST(N'2023-11-17T23:50:32.530' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1509', N'W-ACUP-46B24RS-B1C4-AP-PRB0', N'WIP 46B24RS SUB-1 UC ASPIRA PR', CAST(N'2023-11-17T23:50:32.610' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'151', N'W-CV01-D26LXXX-C03N-NL-DQ00', N'CV D26L BLUE HD-ORANGE DG-QR', CAST(N'2023-11-17T23:48:29.243' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1510', N'W-AMUP-115F51X-C5C1-TN-HDA0', N'W-MF 115F51 HD UC TRAKNUS HD', CAST(N'2023-11-17T23:50:32.693' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1511', N'W-AMUP-125D31L-B8P3-AP-MFA0', N'W-MF 125D31L STD UC ASPIRA MFA', CAST(N'2023-11-17T23:50:32.773' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1512', N'W-AMUP-125D31L-B8P3-AS-HPD0', N'W-MF 125D31L STD UC ASAHI HP-D', CAST(N'2023-11-17T23:50:32.857' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1513', N'W-AMUP-145F51X-C5C1-AM-HDA0', N'W-MF 145F51 HD UC AMK HDT', CAST(N'2023-11-17T23:50:32.937' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1514', N'W-AMUP-145F51X-C5C1-IN-MFF0', N'W-MF 145F51 HD UC INCOE AOP', CAST(N'2023-11-17T23:50:33.017' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1515', N'W-AMUP-145G51X-C5C4-IN-MFB0', N'W-MF 145G51 SUB-1 UC INC MF-B', CAST(N'2023-11-17T23:50:33.097' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1516', N'W-AMUP-145G51X-C5C4-OH-MFA0', N'W-MF 145G51 SUB-1 UC OHA MF-A', CAST(N'2023-11-17T23:50:33.180' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1517', N'W-AMUP-145G51X-C5C4-QU-MFA0', N'W-MF 145G51 SUB-1 UC QUANTUM-A', CAST(N'2023-11-17T23:50:33.260' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1518', N'W-AMUP-145G51X-C7C3-AM-PRA0', N'W-MF 145G51 STD UC AMK PRE', CAST(N'2023-11-17T23:50:33.343' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1519', N'W-AMUP-145G51X-C7C3-BL-MFC0', N'W-MF 145G51 STD UC N-BRAND -C', CAST(N'2023-11-17T23:50:33.423' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'152', N'W-CV01-D26LXXX-C06N-NL-DG00', N'CV D26L BLUE HD-WHITE', CAST(N'2023-11-17T23:48:29.327' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1520', N'W-AMUP-145G51X-C7C3-IN-MFF0', N'W-MF 145G51 STD UC INCOE AOP', CAST(N'2023-11-17T23:50:33.503' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1521', N'W-AMUP-145G51X-C7C3-OH-MFB0', N'W-MF 145G51 STD UC OHAYO UAE', CAST(N'2023-11-17T23:50:33.587' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1522', N'W-AMUP-145G51X-C7C3-QU-MFA0', N'W-MF 145G51 STD UC QUANTUM-A', CAST(N'2023-11-17T23:50:33.667' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1523', N'W-AMUP-145G51X-C7C3-QU-MFB0', N'W-MF 145G51 STD UC QUANTUM-B', CAST(N'2023-11-17T23:50:33.747' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1524', N'W-AMUP-145G51X-C7C3-QU-MFC0', N'W-MF 145G51 STD UC QUANTUM-C', CAST(N'2023-11-17T23:50:33.830' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1525', N'W-AMUP-145G51X-C7C3-QU-MFD0', N'W-MF 145G51 STD UC QUANTUM-D', CAST(N'2023-11-17T23:50:33.913' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1526', N'W-AMUP-145G51X-C7C3-QU-MFF0', N'W-MF 145G51 STD UC QUANTUM-F', CAST(N'2023-11-17T23:50:33.993' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1527', N'W-AMUP-145G51X-D1C1-TN-HDA0', N'W-MF 145G51 HD UC TRAKNUS HD', CAST(N'2023-11-17T23:50:34.073' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1528', N'W-AMUP-170G51X-C7C3-AP-MFA0', N'W-MF 170G51 STD UC ASPIRA MF-A', CAST(N'2023-11-17T23:50:34.153' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1529', N'W-AMUP-170G51X-C7C3-AS-HPB0', N'W-MF 170G51 STD UC ASAHI HP-B', CAST(N'2023-11-17T23:50:34.237' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'153', N'W-CV01-D26RXXX-C01N-NL-DG00', N'CV D26R BLUE HD-BLUE', CAST(N'2023-11-17T23:48:29.417' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1530', N'W-AMUP-170G51X-C7C3-AS-HPC0', N'W-MF 170G51 STD UC ASAHI HP-C', CAST(N'2023-11-17T23:50:34.317' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1531', N'W-AMUP-170G51X-C7C3-AS-HPD0', N'W-MF 170G51 STD UC ASAHI HP-D', CAST(N'2023-11-17T23:50:34.400' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1532', N'W-AMUP-170G51X-C7C3-BL-MFA0', N'W-MF 170G51 STD UC NON BRAND', CAST(N'2023-11-17T23:50:34.480' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1533', N'W-AMUP-170G51X-C7C3-IN-MFA0', N'W-MF 170G51 STD UC INCOE MF-A', CAST(N'2023-11-17T23:50:34.563' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1534', N'W-AMUP-170G51X-C7C3-IN-MFB0', N'W-MF 170G51 STD UC INCOE MF-B', CAST(N'2023-11-17T23:50:34.643' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1535', N'W-AMUP-58827LX-B6C3-AS-HPB0', N'W-MF 588-27L STD UC ASAHI HP-B', CAST(N'2023-11-17T23:50:34.727' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1536', N'W-AMUP-58827LX-B6C3-AS-MEA0', N'W-MF 588-27L STD UC ASAHI MEN', CAST(N'2023-11-17T23:50:34.810' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1537', N'W-AMUP-58827LX-B6C3-BL-MFA0', N'W-MF 588-27L STD UC NON BRAND', CAST(N'2023-11-17T23:50:34.890' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1538', N'W-AMUP-50B24LX-B1P3-BL-MFB0', N'W-MF 50B24L STD UC N-BRAND - B', CAST(N'2023-11-17T23:50:34.970' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1539', N'W-AMUP-65D26LX-B0P4-QU-MFA0', N'W-MF 65D26L SUB-1 UC QUANTUM-A', CAST(N'2023-11-17T23:50:35.053' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'154', N'W-CV01-D26RXXX-C03N-NL-DG00', N'CV D26R BLUE HD-ORANGE', CAST(N'2023-11-17T23:48:29.500' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1540', N'W-AMUP-65D26RX-B0P4-AS-HPB0', N'W-MF 65D26R SUB-1 UC ASAHI HPB', CAST(N'2023-11-17T23:50:35.133' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1541', N'W-AMUP-65D26RX-B0P4-IN-MFB0', N'W-MF 65D26R SUB-1 UC INCOE MFB', CAST(N'2023-11-17T23:50:35.213' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1542', N'W-AMUP-65D26RX-B0P4-OH-MFA0', N'W-MF 65D26R SUB-1 UC OHAYO MFA', CAST(N'2023-11-17T23:50:35.293' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1543', N'W-AMUP-65D26RX-B0P4-QU-MFA0', N'W-MF 65D26R SUB-1 UC QUANTUM-A', CAST(N'2023-11-17T23:50:35.377' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1544', N'W-AMUP-65D31LX-B2P3-AP-MFD0', N'W-MF 65D31L STD UC ASPIRA MF-D', CAST(N'2023-11-17T23:50:35.457' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1545', N'W-AMUP-65D31LX-B2P3-AS-HPD0', N'W-MF 65D31L STD UC ASAHI HP-D', CAST(N'2023-11-17T23:50:35.540' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1546', N'W-AMUP-65D31LX-B2P3-AS-MEA0', N'W-MF 65D31L STD UC ASAHI MEN', CAST(N'2023-11-17T23:50:35.623' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1547', N'W-AMUP-65D31LX-B2P3-IN-MFE0', N'W-MF 65D31L STD UC INCOE PHI', CAST(N'2023-11-17T23:50:35.700' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1548', N'W-AMUP-65D31LX-B2P3-IN-MFF0', N'W-MF 65D31L STD UC INCOE AOP', CAST(N'2023-11-17T23:50:35.783' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1549', N'W-AMUP-65D31LX-B2P3-QU-MFA0', N'W-MF 65D31L STD UC QUANTUM-A', CAST(N'2023-11-17T23:50:35.863' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'155', N'W-CV01-D26RXXX-C03N-NL-DQ00', N'CV D26R BLUE HD-ORANGE DG-QR', CAST(N'2023-11-17T23:48:29.583' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1550', N'W-AMUP-65D31LX-B2P3-QU-MFB0', N'W-MF 65D31L STD UC QUANTUM-B', CAST(N'2023-11-17T23:50:35.947' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1551', N'W-AMUP-65D31LX-B2P3-QU-MFC0', N'W-MF 65D31L STD UC QUANTUM-C', CAST(N'2023-11-17T23:50:36.027' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1552', N'W-AMUP-65D31LX-B2P3-QU-MFD0', N'W-MF 65D31L STD UC QUANTUM-D', CAST(N'2023-11-17T23:50:36.110' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1553', N'W-AMUP-65D31LX-B3P2-IN-MFE0', N'W-MF 65D31L C20 UC INCOE PHI', CAST(N'2023-11-17T23:50:36.200' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1554', N'W-AMUP-65D31RX-B1P4-BL-MFA0', N'W-MF 65D31R SUB-1 UC NON BRAND', CAST(N'2023-11-17T23:50:36.280' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1555', N'W-AMUP-65D31RX-B1P4-IN-MFB0', N'W-MF 65D31R SUB-1 UC INCOE MFB', CAST(N'2023-11-17T23:50:36.360' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1556', N'W-AMUP-65D31RX-B1P4-QU-MFA0', N'W-MF 65D31R SUB-1 UC QUANTUM-A', CAST(N'2023-11-17T23:50:36.443' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1557', N'W-AMUP-65D31RX-B2P3-AM-PRA0', N'W-MF 65D31R STD UC AMK PRE', CAST(N'2023-11-17T23:50:36.523' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1558', N'W-AMUP-65D31RX-B2P3-AS-MEA0', N'W-MF 65D31R STD UC ASAHI MEN', CAST(N'2023-11-17T23:50:36.603' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1559', N'W-AMUP-65D31RX-B2P3-IN-MFF0', N'W-MF 65D31R STD UC INCOE AOP', CAST(N'2023-11-17T23:50:36.687' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'156', N'W-CV01-D26RXXX-C06N-NL-DG00', N'CV D26R BLUE HD-WHITE', CAST(N'2023-11-17T23:48:29.670' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1560', N'W-AMUP-65D31RX-B2P3-QU-MFA0', N'W-MF 65D31R STD UC QUANTUM-A', CAST(N'2023-11-17T23:50:36.770' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1561', N'W-AMUP-EFBQ85L-B3C3-LU-00A0', N'W-EFB Q-85L STD UC LUCAS', CAST(N'2023-11-17T23:50:36.847' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1562', N'W-AMUP-105D31L-B6P3-QU-MFC0', N'W-MF 105D31L STD UC QUANTUM-C', CAST(N'2023-11-17T23:50:36.927' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1563', N'W-AMUP-115E41R-B7C3-BL-MFA0', N'W-MF 115E41R STD UC NON BRAND', CAST(N'2023-11-17T23:50:37.010' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1564', N'W-AMUP-115E41R-B7C3-IN-MFA0', N'W-MF 115E41R STD UC INCOE MF-A', CAST(N'2023-11-17T23:50:37.090' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1565', N'W-AMUP-115E41R-B7C3-IN-MFB0', N'W-MF 115E41R STD UC INCOE MF-B', CAST(N'2023-11-17T23:50:37.170' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1566', N'W-AMUP-115E41R-B7C3-IN-MFC0', N'W-MF 115E41R STD UC INCOE MF-C', CAST(N'2023-11-17T23:50:37.253' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1567', N'W-AMUP-115E41R-B7C3-IN-MFH0', N'W-MF 115E41R STD UC INCOE VIE', CAST(N'2023-11-17T23:50:37.330' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1568', N'W-AMUP-115E41R-B7C3-IN-MFJ0', N'W-MF 115E41R STD UC INCOE UAE', CAST(N'2023-11-17T23:50:37.410' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1569', N'W-AMUP-115E41R-B7C3-LU-00A0', N'W-MF 115E41R STD UC LUCAS', CAST(N'2023-11-17T23:50:37.497' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'157', N'W-CV01-D31LXXX-C01N-NL-DG00', N'CV D31L BLUE HD-BLUE', CAST(N'2023-11-17T23:48:29.753' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1570', N'W-AMUP-115E41R-B7C3-NI-MFA0', N'W-MF 115E41R STD UC NIKO MF-A', CAST(N'2023-11-17T23:50:37.577' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1571', N'W-AMUP-115E41R-B7C3-OH-MFA0', N'W-MF 115E41R STD UC OHAYO MF-A', CAST(N'2023-11-17T23:50:37.653' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1572', N'W-AMUP-115E41R-C1C1-AM-HDA0', N'W-MF 115E41R HD UC AMK HDT', CAST(N'2023-11-17T23:50:37.737' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1573', N'W-AMUP-115E41R-C1C1-IN-MFF0', N'W-MF 115E41R HD UC INCOE AOP', CAST(N'2023-11-17T23:50:37.820' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1574', N'W-AMUP-115F51X-B9C4-IN-MFB0', N'W-MF 115F51 SUB-1 UC INCOE MFB', CAST(N'2023-11-17T23:50:37.903' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1575', N'W-AMUP-115F51X-B9C4-OH-MFA0', N'W-MF 115F51 SUB-1 UC OHAYO MFA', CAST(N'2023-11-17T23:50:37.980' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1576', N'W-AMUP-115F51X-B9C4-QU-MFA0', N'W-MF 115F51 SUB-1 UC QUANTUM-A', CAST(N'2023-11-17T23:50:38.060' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1577', N'W-AMUP-115F51X-C1C3-AM-PRA0', N'W-MF 115F51 STD UC AMK PRE', CAST(N'2023-11-17T23:50:38.143' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1578', N'W-AMUP-115F51X-C1C3-AS-HPD0', N'W-MF 115F51 STD UC ASAHI HP-D', CAST(N'2023-11-17T23:50:38.220' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1579', N'W-AMUP-115F51X-C1C3-BL-MFC0', N'W-MF 115F51 STD UC N-BRAND -C', CAST(N'2023-11-17T23:50:38.303' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'158', N'W-CV01-D31LXXX-C03N-NL-DG00', N'CV D31L BLUE HD-ORANGE', CAST(N'2023-11-17T23:48:29.840' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1580', N'W-AMUP-170G51X-C7C3-IN-MFC0', N'W-MF 170G51 STD UC INCOE MF-C', CAST(N'2023-11-17T23:50:38.387' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1581', N'W-AMUP-170G51X-C7C3-IN-MFD0', N'W-MF 170G51 STD UC INCOE SUD', CAST(N'2023-11-17T23:50:38.470' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1582', N'W-AMUP-110D31R-B6P3-IN-MFB0', N'W-MF 110D31R STD UC INCOE MF-B', CAST(N'2023-11-17T23:50:38.550' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1583', N'W-AMUP-110D31R-B6P3-IN-MFC0', N'W-MF 110D31R STD UC INCOE MF-C', CAST(N'2023-11-17T23:50:38.630' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1584', N'W-AMUP-110D31R-B6P3-IN-MFD0', N'W-MF 110D31R STD UC INCOE SUD', CAST(N'2023-11-17T23:50:38.710' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1585', N'W-AMUP-110D31R-B6P3-IN-MFE0', N'W-MF 110D31R STD UC INCOE PHI', CAST(N'2023-11-17T23:50:38.790' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1586', N'W-AMUP-110D31R-B6P3-IN-MFH0', N'W-MF 110D31R STD UC INCOE VIE', CAST(N'2023-11-17T23:50:38.870' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1587', N'W-AMUP-110D31R-B6P3-IN-MFJ0', N'W-MF 110D31R STD UC INCOE UAE', CAST(N'2023-11-17T23:50:38.950' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1588', N'W-AMUP-110D31R-B6P3-OH-MFA0', N'W-MF 110D31R STD UC OHAYO MF-A', CAST(N'2023-11-17T23:50:39.033' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1589', N'W-AMUP-110D31R-B6P3-OH-MFB0', N'W-MF 110D31R STD UC OHAYO UAE', CAST(N'2023-11-17T23:50:39.113' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'159', N'W-CV01-D31LXXX-C03N-NL-DQ00', N'CV D31L BLUE HD-ORANGE DG-QR', CAST(N'2023-11-17T23:48:29.923' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1590', N'W-AMUP-115D31L-B8P3-IN-MFL0', N'W-MF 115D31L STD UC INCOE HYD', CAST(N'2023-11-17T23:50:39.197' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1591', N'W-AMUP-115D31R-B8P3-AM-HDA0', N'W-MF 115D31R STD UC AMK HDT', CAST(N'2023-11-17T23:50:39.277' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1592', N'W-AMUP-115D31R-B8P3-AP-MFC0', N'W-MF 115D31R STD UC ASPIRA AOP', CAST(N'2023-11-17T23:50:39.360' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1593', N'W-AMUP-115D31R-B8P3-IN-MFL0', N'W-MF 115D31R STD UC INCOE HYD', CAST(N'2023-11-17T23:50:39.440' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1594', N'W-AMUP-115E41L-B7C3-AP-MFA0', N'W-MF 115E41L STD UC ASPIRA MFA', CAST(N'2023-11-17T23:50:39.523' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1595', N'W-AMUP-115E41L-B7C3-AS-HPB0', N'W-MF 115E41L STD UC ASAHI HP-B', CAST(N'2023-11-17T23:50:39.607' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1596', N'W-AMUP-115E41L-B7C3-AS-HPC0', N'W-MF 115E41L STD UC ASAHI HP-C', CAST(N'2023-11-17T23:50:39.683' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1597', N'W-AMUP-115E41L-B7C3-AS-HPD0', N'W-MF 115E41L STD UC ASAHI HP-D', CAST(N'2023-11-17T23:50:39.763' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1598', N'W-AMUP-115E41L-B7C3-BL-MFA0', N'W-MF 115E41L STD UC NON BRAND', CAST(N'2023-11-17T23:50:39.847' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1599', N'W-AMUP-115E41L-B7C3-IN-MFA0', N'W-MF 115E41L STD UC INCOE MF-A', CAST(N'2023-11-17T23:50:39.927' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'16', N'K-CV02-GC6VXXX-0MHN-NL-0000', N'CV GC-6V BLACK (SPINCAP)', CAST(N'2023-11-17T23:48:17.663' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'160', N'W-CV01-D31LXXX-C06N-NL-DG00', N'CV D31L BLUE HD-WHITE', CAST(N'2023-11-17T23:48:30.003' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1600', N'W-AMUP-115E41L-B7C3-IN-MFB0', N'W-MF 115E41L STD UC INCOE MF-B', CAST(N'2023-11-17T23:50:40.010' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1601', N'W-AMUP-115E41L-B7C3-IN-MFC0', N'W-MF 115E41L STD UC INCOE MF-C', CAST(N'2023-11-17T23:50:40.127' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1602', N'W-AMUP-115E41L-B7C3-IN-MFE0', N'W-MF 115E41L STD UC INCOE PHI', CAST(N'2023-11-17T23:50:40.210' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1603', N'W-AMUP-115E41L-B7C3-OH-MFA0', N'W-MF 115E41L STD UC OHAYO MF-A', CAST(N'2023-11-17T23:50:40.290' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1604', N'W-AMUP-115F51X-C1C3-IN-MFE0', N'W-MF 115F51 STD UC INCOE PHI', CAST(N'2023-11-17T23:50:40.373' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1605', N'W-AMUP-115F51X-C1C3-IN-MFF0', N'W-MF 115F51 STD UC INCOE AOP', CAST(N'2023-11-17T23:50:40.453' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1606', N'W-AMUP-115F51X-C1C3-OH-MFB0', N'W-MF 115F51 STD UC OHAYO UAE', CAST(N'2023-11-17T23:50:40.537' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1607', N'W-AMUP-115F51X-C1C3-QU-MFA0', N'W-MF 115F51 STD UC QUANTUM-A', CAST(N'2023-11-17T23:50:40.617' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1608', N'W-AMUP-115F51X-C1C3-QU-MFB0', N'W-MF 115F51 STD UC QUANTUM-B', CAST(N'2023-11-17T23:50:40.700' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1609', N'W-AMUP-115F51X-C1C3-QU-MFC0', N'W-MF 115F51 STD UC QUANTUM-C', CAST(N'2023-11-17T23:50:40.780' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'161', N'W-CV01-D31RXXX-C01N-NL-DG00', N'CV D31R BLUE HD-BLUE', CAST(N'2023-11-17T23:48:30.090' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1610', N'W-AMUP-115F51X-C1C3-QU-MFD0', N'W-MF 115F51 STD UC QUANTUM-D', CAST(N'2023-11-17T23:50:40.860' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1611', N'W-AMUP-115F51X-C1C3-QU-MFF0', N'W-MF 115F51 STD UC QUANTUM-F', CAST(N'2023-11-17T23:50:40.940' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1612', N'W-AMUP-125D31L-B8P3-IN-MFA0', N'W-MF 125D31L STD UC INCOE MF-A', CAST(N'2023-11-17T23:50:41.020' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1613', N'W-AMUP-125D31L-B8P3-IN-MFB0', N'W-MF 125D31L STD UC INCOE MF-B', CAST(N'2023-11-17T23:50:41.103' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1614', N'W-AMUP-125D31L-B8P3-IN-MFH0', N'W-MF 125D31L STD UC INCOE VIE', CAST(N'2023-11-17T23:50:41.183' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1615', N'W-AMUP-125D31L-B8P3-IN-MFJ0', N'W-MF 125D31L STD UC INCOE UAE', CAST(N'2023-11-17T23:50:41.263' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1616', N'W-AMUP-125D31L-B8P3-LU-00A0', N'W-MF 125D31L STD UC LUCAS', CAST(N'2023-11-17T23:50:41.343' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1617', N'W-AMUP-125D31L-B8P3-OH-MFA0', N'W-MF 125D31L STD UC OHAYO MF-A', CAST(N'2023-11-17T23:50:41.427' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1618', N'W-AMUP-115E41R-B7C3-AP-MFA0', N'W-MF 115E41R STD UC ASPIRA MFA', CAST(N'2023-11-17T23:50:41.507' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1619', N'W-AMUP-115E41R-B7C3-AS-HPB0', N'W-MF 115E41R STD UC ASAHI HP-B', CAST(N'2023-11-17T23:50:41.590' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'162', N'W-CV01-D31RXXX-C03N-NL-DG00', N'CV D31R BLUE HD-ORANGE', CAST(N'2023-11-17T23:48:30.170' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1620', N'W-AMUP-115E41R-B7C3-AS-HPC0', N'W-MF 115E41R STD UC ASAHI HP-C', CAST(N'2023-11-17T23:50:41.670' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1621', N'W-AMUP-36B20LX-B0P3-IN-MFF0', N'W-MF 36B20L STD UC INCOE AOP', CAST(N'2023-11-17T23:50:41.753' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1622', N'W-AMUP-45C24LX-B0P3-IN-MFE0', N'W-MF 45C24L STD UC INCOE PHI', CAST(N'2023-11-17T23:50:41.833' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1623', N'W-AMUP-45C24LX-B0P3-IN-MFJ0', N'W-MF 45C24L STD UC INCOE UAE', CAST(N'2023-11-17T23:50:41.913' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1624', N'W-AMUP-45C24LX-B0P3-OH-MFA0', N'W-MF 45C24L STD UC OHAYO MF-A', CAST(N'2023-11-17T23:50:41.997' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1625', N'W-AMUP-45C24RX-B0P2-IN-MFB0', N'W-MF 45C24R C20 UC INCOE MF-B', CAST(N'2023-11-17T23:50:42.077' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1626', N'W-AMUP-45C24RX-B0P3-AP-MFA0', N'W-MF 45C24R STD UC ASPIRA MF-A', CAST(N'2023-11-17T23:50:42.157' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1627', N'W-AMUP-45C24RX-B0P3-AS-HPB0', N'W-MF 45C24R STD UC ASAHI HP-B', CAST(N'2023-11-17T23:50:42.243' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1628', N'W-AMUP-45C24RX-B0P3-IN-MFA0', N'W-MF 45C24R STD UC INCOE MF-A', CAST(N'2023-11-17T23:50:42.333' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1629', N'W-AMUP-45C24RX-B0P3-IN-MFB0', N'W-MF 45C24R STD UC INCOE MF-B', CAST(N'2023-11-17T23:50:42.430' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'163', N'W-CV01-D31RXXX-C03N-NL-DQ00', N'CV D31R BLUE HD-ORANGE DG-QR', CAST(N'2023-11-17T23:48:30.257' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1630', N'W-AMUP-45C24RX-B0P3-IN-MFC0', N'W-MF 45C24R STD UC INCOE MF-C', CAST(N'2023-11-17T23:50:42.527' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1631', N'W-AMUP-45C24RX-B0P3-IN-MFJ0', N'W-MF 45C24R STD UC INCOE UAE', CAST(N'2023-11-17T23:50:42.620' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1632', N'W-AMUP-45C24RX-B0P3-OH-MFA0', N'W-MF 45C24R STD UC OHAYO MF-A', CAST(N'2023-11-17T23:50:42.713' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1633', N'W-AMUP-46B24LS-A9P5-OH-MFA0', N'W-MF 46B24LS SUB-2 UC OHA MF-2', CAST(N'2023-11-17T23:50:42.813' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1634', N'W-AMUP-46B24LS-A9P5-QU-MFA0', N'W-MF 46B24LS SUB-2 UC QUANTU-A', CAST(N'2023-11-17T23:50:42.907' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1635', N'W-AMUP-46B24LS-B0P4-AS-HPB0', N'W-MF 46B24LS SUB-1 UC ASA HP-B', CAST(N'2023-11-17T23:50:42.993' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1636', N'W-AMUP-46B24LS-B0P4-IN-MFB0', N'W-MF 46B24LS SUB-1 UC INC MFB', CAST(N'2023-11-17T23:50:43.077' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1637', N'W-AMUP-46B24LS-B0P4-QU-MFA0', N'W-MF 46B24LS SUB-1 UC QUANTU-A', CAST(N'2023-11-17T23:50:43.160' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1638', N'W-AMUP-46B24LS-B1P3-AS-MEA0', N'W-MF 46B24LS STD UC ASAHI MEN', CAST(N'2023-11-17T23:50:43.237' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1639', N'W-AMUP-46B24LS-B1P3-IN-MFE0', N'W-MF 46B24LS STD UC INCOE PHI', CAST(N'2023-11-17T23:50:43.317' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'164', N'W-CV01-D31RXXX-C06N-NL-DG00', N'CV D31R BLUE HD-WHITE', CAST(N'2023-11-17T23:48:30.340' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1640', N'W-AMUP-46B24LS-B1P3-IN-MFF0', N'W-MF 46B24LS STD UC INCOE AOP', CAST(N'2023-11-17T23:50:43.397' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1641', N'W-AMUP-46B24LS-B1P3-IN-MFK0', N'W-MF 46B24LS STD UC INCOE DSP', CAST(N'2023-11-17T23:50:43.480' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1642', N'W-AMUP-50B24LX-B1P3-IN-MFA0', N'W-MF 50B24L STD UC INCOE MF-A', CAST(N'2023-11-17T23:50:43.560' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1643', N'W-AMUP-50B24LX-B1P3-IN-MFB0', N'W-MF 50B24L STD UC INCOE MF-B', CAST(N'2023-11-17T23:50:43.640' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1644', N'W-AMUP-50B24LX-B1P3-IN-MFC0', N'W-MF 50B24L STD UC INCOE MF-C', CAST(N'2023-11-17T23:50:43.720' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1645', N'W-AMUP-50B24LX-B1P3-IN-MFH0', N'W-MF 50B24L STD UC INCOE VIE', CAST(N'2023-11-17T23:50:43.803' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1646', N'W-AMUP-125D31R-B8P3-AP-MFA0', N'W-MF 125D31R STD UC ASPIRA MFA', CAST(N'2023-11-17T23:50:43.883' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1647', N'W-AMUP-125D31R-B8P3-IN-MFA0', N'W-MF 125D31R STD UC INCOE MF-A', CAST(N'2023-11-17T23:50:43.967' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1648', N'W-AMUP-125D31R-B8P3-IN-MFB0', N'W-MF 125D31R STD UC INCOE MF-B', CAST(N'2023-11-17T23:50:44.047' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1649', N'W-AMUP-125D31R-B8P3-IN-MFH0', N'W-MF 125D31R STD UC INCOE VIE', CAST(N'2023-11-17T23:50:44.130' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'165', N'W-CV01-E41LXXX-0MHN-NL-DG00', N'CV E41L BLUE', CAST(N'2023-11-17T23:48:30.423' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1650', N'W-AMUP-125D31R-B8P3-IN-MFJ0', N'W-MF 125D31R STD UC INCOE UAE', CAST(N'2023-11-17T23:50:44.210' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1651', N'W-AMUP-125D31R-B8P3-LU-00A0', N'W-MF 125D31R STD UC LUCAS', CAST(N'2023-11-17T23:50:44.293' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1652', N'W-AMUP-125D31R-B8P3-OH-MFA0', N'W-MF 125D31R STD UC OHAYO MF-A', CAST(N'2023-11-17T23:50:44.373' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1653', N'W-AMUP-135F51X-C1C3-AP-MFA0', N'W-MF 135F51 STD UC ASPIRA MF-A', CAST(N'2023-11-17T23:50:44.453' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1654', N'W-AMUP-135F51X-C1C3-AS-HPB0', N'W-MF 135F51 STD UC ASAHI HP-B', CAST(N'2023-11-17T23:50:44.533' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1655', N'W-AMUP-135F51X-C1C3-AS-HPC0', N'W-MF 135F51 STD UC ASAHI HP-C', CAST(N'2023-11-17T23:50:44.617' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1656', N'W-AMUP-135F51X-C1C3-BL-MFA0', N'W-MF 135F51 STD UC NON BRAND', CAST(N'2023-11-17T23:50:44.697' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1657', N'W-AMUP-135F51X-C1C3-IN-MFA0', N'W-MF 135F51 STD UC INCOE MF-A', CAST(N'2023-11-17T23:50:44.777' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1658', N'W-AMUP-135F51X-C1C3-IN-MFB0', N'W-MF 135F51 STD UC INCOE MF-B', CAST(N'2023-11-17T23:50:44.857' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1659', N'W-AMUP-135F51X-C1C3-IN-MFC0', N'W-MF 135F51 STD UC INCOE MF-C', CAST(N'2023-11-17T23:50:44.940' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'166', N'W-CV01-E41LXXX-0MHN-NL-DQ00', N'CV E41L BLUE DG-QR', CAST(N'2023-11-17T23:48:30.510' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1660', N'W-AMUP-135F51X-C1C3-IN-MFD0', N'W-MF 135F51 STD UC INCOE SUD', CAST(N'2023-11-17T23:50:45.020' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1661', N'W-AMUP-135F51X-C1C3-IN-MFH0', N'W-MF 135F51 STD UC INCOE VIE', CAST(N'2023-11-17T23:50:45.100' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1662', N'W-AMUP-135F51X-C1C3-IN-MFJ0', N'W-MF 135F51 STD UC INCOE UAE', CAST(N'2023-11-17T23:50:45.180' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1663', N'W-AMUP-105D31L-B6P3-QU-MFD0', N'W-MF 105D31L STD UC QUANTUM-D', CAST(N'2023-11-17T23:50:45.270' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1664', N'W-AMUP-105D31L-B6P3-QU-MFE0', N'W-MF 105D31L STD UC QUANTUM-E', CAST(N'2023-11-17T23:50:45.350' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1665', N'W-AMUP-105D31L-B6P3-QU-MFF0', N'W-MF 105D31L STD UC QUANTUM-F', CAST(N'2023-11-17T23:50:45.430' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1666', N'W-AMUP-105D31R-B6P3-IN-MFK0', N'W-MF 105D31R STD UC INCOE DSP', CAST(N'2023-11-17T23:50:45.513' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1667', N'W-AMUP-105D31R-B6P3-QU-MFA0', N'W-MF 105D31R STD UC QUANTUM-A', CAST(N'2023-11-17T23:50:45.597' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1668', N'W-AMUP-105D31R-B6P3-QU-MFB0', N'W-MF 105D31R STD UC QUANTUM-B', CAST(N'2023-11-17T23:50:45.677' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1669', N'W-AMUP-95D26RX-B3P3-IN-MFB0', N'W-MF 95D26R STD UC INCOE MF-B', CAST(N'2023-11-17T23:50:45.757' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'167', N'W-CV01-E41RXXX-0MHN-NL-DG00', N'CV E41R BLUE', CAST(N'2023-11-17T23:48:30.597' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1670', N'W-AMUP-95D26RX-B3P3-IN-MFH0', N'W-MF 95D26R STD UC INCOE VIE', CAST(N'2023-11-17T23:50:45.837' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1671', N'W-AMUP-95D26RX-B3P3-LU-00A0', N'W-MF 95D26R STD UC LUCAS', CAST(N'2023-11-17T23:50:45.920' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1672', N'W-AMUP-95D26RX-B3P3-OH-MFA0', N'W-MF 95D26R STD UC OHAYO MF-A', CAST(N'2023-11-17T23:50:46.000' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1673', N'W-AMUP-95D31LX-B2P5-OH-MFA0', N'W-MF 95D31L SUB-2 UC OHAYO MFA', CAST(N'2023-11-17T23:50:46.080' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1674', N'W-AMUP-95D31LX-B2P5-QU-MFA0', N'W-MF 95D31L SUB-2 UC QUANTUM-A', CAST(N'2023-11-17T23:50:46.163' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1675', N'W-AMUP-95D31LX-B3P3-AP-MFA0', N'W-MF 95D31L STD UC ASPIRA MF-A', CAST(N'2023-11-17T23:50:46.240' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1676', N'W-AMUP-95D31LX-B3P3-AS-HPB0', N'W-MF 95D31L STD UC ASAHI HP-B', CAST(N'2023-11-17T23:50:46.323' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1677', N'W-AMUP-95D31LX-B3P3-AS-HPC0', N'W-MF 95D31L STD UC ASAHI HP-C', CAST(N'2023-11-17T23:50:46.403' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1678', N'W-AMUP-95D31LX-B3P3-AS-HPD0', N'W-MF 95D31L STD UC ASAHI HP-D', CAST(N'2023-11-17T23:50:46.487' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1679', N'W-AMUP-95D31LX-B3P3-BL-MFA0', N'W-MF 95D31L STD UC NON BRAND', CAST(N'2023-11-17T23:50:46.567' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'168', N'W-CV01-E41RXXX-0MHN-NL-DQ00', N'CV E41R BLUE DG-QR', CAST(N'2023-11-17T23:48:30.677' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1680', N'W-AMUP-95D31LX-B3P3-IN-MFA0', N'W-MF 95D31L STD UC INCOE MF-A', CAST(N'2023-11-17T23:50:46.650' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1681', N'W-AMUP-95D31LX-B3P3-IN-MFB0', N'W-MF 95D31L STD UC INCOE MF-B', CAST(N'2023-11-17T23:50:46.730' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1682', N'W-AMUP-95D31LX-B3P3-IN-MFC0', N'W-MF 95D31L STD UC INCOE MF-C', CAST(N'2023-11-17T23:50:46.813' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1683', N'W-AMUP-95D31LX-B3P4-IN-MFB0', N'W-MF 95D31L SUB-1 UC INCOE MFB', CAST(N'2023-11-17T23:50:46.893' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1684', N'W-AMUP-95D31LX-B3P4-QU-MFA0', N'W-MF 95D31L SUB-1 UC QUANTUM-A', CAST(N'2023-11-17T23:50:46.977' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1685', N'W-AMUP-95D31LX-B3P4-QU-MFE0', N'W-MF 95D31L SUB-1 UC QUANTUM-E', CAST(N'2023-11-17T23:50:47.060' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1686', N'W-AMUP-95D31LX-B4P3-AP-MFB0', N'W-MF 95D31L STD UC ASPIRA MF-B', CAST(N'2023-11-17T23:50:47.140' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1687', N'W-AMUP-95D31LX-B4P3-AS-MEA0', N'W-MF 95D31L STD UC ASAHI MEN', CAST(N'2023-11-17T23:50:47.230' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1688', N'W-AMUP-95D31LX-B4P3-IN-MFF0', N'W-MF 95D31L STD UC INCOE AOP', CAST(N'2023-11-17T23:50:47.310' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1689', N'W-AMUP-95D31LX-B4P3-IN-MFI0', N'W-MF 95D31L STD UC INCOE GREE', CAST(N'2023-11-17T23:50:47.390' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'169', N'W-CV01-F51XXXX-0MHN-NL-DG00', N'CV F51 BLUE', CAST(N'2023-11-17T23:48:30.763' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1690', N'W-AMUP-95D31LX-B4P3-IN-MFL0', N'W-MF 95D31L STD UC INCOE HYD', CAST(N'2023-11-17T23:50:47.473' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1691', N'W-AMUP-105D31R-B6P3-QU-MFC0', N'W-MF 105D31R STD UC QUANTUM-C', CAST(N'2023-11-17T23:50:47.557' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1692', N'W-AMUP-105D31R-B6P3-QU-MFE0', N'W-MF 105D31R STD UC QUANTUM-E', CAST(N'2023-11-17T23:50:47.637' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1693', N'W-AMUP-105D31R-B6P3-QU-MFF0', N'W-MF 105D31R STD UC QUANTUM-F', CAST(N'2023-11-17T23:50:47.720' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1694', N'W-AMUP-110D31L-B6P3-AP-MFA0', N'W-MF 110D31L STD UC ASIRA MF-A', CAST(N'2023-11-17T23:50:47.800' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1695', N'W-AMUP-110D31L-B6P3-AS-HPB0', N'W-MF 110D31L STD UC ASAHI HP-B', CAST(N'2023-11-17T23:50:47.883' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1696', N'W-AMUP-110D31L-B6P3-AS-HPC0', N'W-MF 110D31L STD UC ASAHI HP-C', CAST(N'2023-11-17T23:50:47.963' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1697', N'W-AMUP-110D31L-B6P3-BL-MFA0', N'W-MF 110D31L STD UC NON BRAND', CAST(N'2023-11-17T23:50:48.043' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1698', N'W-AMUP-110D31L-B6P3-IN-MFA0', N'W-MF 110D31L STD UC INCOE MF-A', CAST(N'2023-11-17T23:50:48.123' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1699', N'W-AMUP-110D31L-B6P3-IN-MFB0', N'W-MF 110D31L STD UC INCOE MF-B', CAST(N'2023-11-17T23:50:48.203' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'17', N'K-CV02-GC8VXXX-0MHN-NL-0000', N'CV GC-8V BLACK (SPINCAP)', CAST(N'2023-11-17T23:48:17.750' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'170', N'W-CV01-F51XXXX-0MHN-NL-DQ00', N'CV F51 BLUE DG-QR', CAST(N'2023-11-17T23:48:30.850' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1700', N'W-AMUP-110D31L-B6P3-IN-MFC0', N'W-MF 110D31L STD UC INCOE MF-C', CAST(N'2023-11-17T23:50:48.287' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1701', N'W-AMUP-110D31L-B6P3-IN-MFD0', N'W-MF 110D31L STD UC INCOE SUD', CAST(N'2023-11-17T23:50:48.367' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1702', N'W-AMUP-110D31L-B6P3-IN-MFE0', N'W-MF 110D31L STD UC INCOE PHI', CAST(N'2023-11-17T23:50:48.447' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1703', N'W-AMUP-110D31L-B6P3-IN-MFH0', N'W-MF 110D31L STD UC INCOE VIE', CAST(N'2023-11-17T23:50:48.530' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1704', N'W-AMUP-110D31L-B6P3-IN-MFJ0', N'W-MF 110D31L STD UC INCOE UAE', CAST(N'2023-11-17T23:50:48.610' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1705', N'W-AMUP-110D31L-B6P3-OH-MFA0', N'W-MF 110D31L STD UC OHAYO MF-A', CAST(N'2023-11-17T23:50:48.690' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1706', N'W-AMUP-110D31L-B6P3-OH-MFB0', N'W-MF 110D31L STD UC OHAYO UAE', CAST(N'2023-11-17T23:50:48.770' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1707', N'W-AMUP-110D31R-B6P3-AP-MFA0', N'W-MF 110D31R STD UC ASIRA MF-A', CAST(N'2023-11-17T23:50:48.853' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1708', N'W-AMUP-110D31R-B6P3-AS-HPB0', N'W-MF 110D31R STD UC ASAHI HP-B', CAST(N'2023-11-17T23:50:48.933' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1709', N'W-AMUP-110D31R-B6P3-AS-HPC0', N'W-MF 110D31R STD UC ASAHI HP-C', CAST(N'2023-11-17T23:50:49.010' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'171', N'W-CV01-G51LXXX-0MHN-NL-DG00', N'CV G51L BLUE', CAST(N'2023-11-17T23:48:30.933' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1710', N'W-AMUP-210H52X-D4P3-IN-MFJ0', N'W-MF 210H52 STD UC INCOE UAE', CAST(N'2023-11-17T23:50:49.093' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1711', N'W-AMUP-36B20LX-B0P3-QU-MFA0', N'W-MF 36B20L STD UC QUANTUM-A', CAST(N'2023-11-17T23:50:49.177' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1712', N'W-AMUP-36B20LX-B0P3-QU-MFB0', N'W-MF 36B20L STD UC QUANTUM-B', CAST(N'2023-11-17T23:50:49.260' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1713', N'W-AMUP-36B20LX-B0P3-QU-MFC0', N'W-MF 36B20L STD UC QUANTUM-C', CAST(N'2023-11-17T23:50:49.507' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1714', N'W-AMUP-36B20LX-B0P3-QU-MFD0', N'W-MF 36B20L STD UC QUANTUM-D', CAST(N'2023-11-17T23:50:49.810' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1715', N'W-AMUP-36B20LX-B0P3-QU-MFF0', N'W-MF 36B20L STD UC QUANTUM-F', CAST(N'2023-11-17T23:50:49.890' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1716', N'W-AMUP-36B20RS-A8P4-IN-MFB0', N'W-MF 36B20RS SUB-1 UC INC MF-B', CAST(N'2023-11-17T23:50:49.973' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1717', N'W-AMUP-57519LX-B4C3-IN-MFH0', N'W-MF 575-19L STD UC INCOE VIE', CAST(N'2023-11-17T23:50:50.060' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1718', N'W-AMUP-N70ZDXX-B5C3-IN-MFE0', N'W-MF N70Z-D STD UC INCOE PHI', CAST(N'2023-11-17T23:50:50.143' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1719', N'W-AMUP-N70ZDXX-B5C3-IN-MFF0', N'W-MF N70Z-D STD UC INCOE AOP', CAST(N'2023-11-17T23:50:50.227' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'172', N'W-CV01-G51LXXX-0MHN-NL-DQ00', N'CV G51L BLUE DG-QR', CAST(N'2023-11-17T23:48:31.013' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1720', N'W-AMUP-N70ZDXX-B5C3-OH-MFA0', N'W-MF N70Z-D STD UC OHAYO MF-A', CAST(N'2023-11-17T23:50:50.310' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1721', N'W-AMUP-NS50LXX-B4C3-IN-MFL0', N'W-MF NS50L STD UC INCOE HYD', CAST(N'2023-11-17T23:50:50.390' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1722', N'W-AMUP-95D26LX-B3P3-IN-MFE0', N'W-MF 95D26L STD UC INCOE MF-E', CAST(N'2023-11-17T23:50:50.477' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1723', N'W-AMUP-95D26LX-B3P3-IN-MFH0', N'W-MF 95D26L STD UC INCOE VIE', CAST(N'2023-11-17T23:50:50.557' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1724', N'W-AMUP-95D26LX-B3P3-LU-00A0', N'W-MF 95D26L STD UC LUCAS', CAST(N'2023-11-17T23:50:50.640' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1725', N'W-AMUP-95D26LX-B3P3-OH-MFA0', N'W-MF 95D26L STD UC OHAYO MF-A', CAST(N'2023-11-17T23:50:50.723' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1726', N'W-AMUP-36B20RX-A7P5-OH-MFA0', N'W-MF 36B20R SUB-2 UC OHAYO MFA', CAST(N'2023-11-17T23:50:50.807' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1727', N'W-AMUP-36B20RX-A7P5-QU-MFA0', N'W-MF 36B20R SUB-2 UC QUANTUM-A', CAST(N'2023-11-17T23:50:50.887' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1728', N'W-AMUP-36B20RX-A8P4-QU-MFA0', N'W-MF 36B20R SUB-1 UC QUANTUM-A', CAST(N'2023-11-17T23:50:50.970' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1729', N'W-AMUP-36B20RX-B0P3-IN-MFF0', N'W-MF 36B20R STD UC INCOE AOP', CAST(N'2023-11-17T23:50:51.050' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'173', N'W-CV01-G51XXXX-0MHN-NL-DG00', N'CV G51 BLUE', CAST(N'2023-11-17T23:48:31.100' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1730', N'W-AMUP-36B20RX-B0P3-IN-MFK0', N'W-MF 36B20R STD UC INCOE DSP', CAST(N'2023-11-17T23:50:51.130' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1731', N'W-AMUP-36B20RX-B0P3-QU-MFA0', N'W-MF 36B20R STD UC QUANTUM-A', CAST(N'2023-11-17T23:50:51.210' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1732', N'W-AMUP-36B20RX-B0P3-QU-MFB0', N'W-MF 36B20R STD UC QUANTUM-B', CAST(N'2023-11-17T23:50:51.293' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1733', N'W-AMUP-36B20RX-B0P3-QU-MFC0', N'W-MF 36B20R STD UC QUANTUM-C', CAST(N'2023-11-17T23:50:51.373' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1734', N'W-AMUP-36B20RX-B0P3-QU-MFF0', N'W-MF 36B20R STD UC QUANTUM-F', CAST(N'2023-11-17T23:50:51.453' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1735', N'W-AMUP-40B20LS-A9P3-AS-HPB0', N'W-MF 40B20LS STD UC ASAHI HP-B', CAST(N'2023-11-17T23:50:51.537' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1736', N'W-AMUP-40B20LS-A9P3-IN-MFA0', N'W-MF 40B20LS STD UC INCOE MF-A', CAST(N'2023-11-17T23:50:51.617' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1737', N'W-AMUP-40B20LX-A9P2-IN-MFB0', N'W-MF 40B20L C20 UC INCOE MF-B', CAST(N'2023-11-17T23:50:51.697' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1738', N'W-AMUP-40B20LX-A9P3-AP-MFA0', N'W-MF 40B20L STD UC ASPIRA MF-A', CAST(N'2023-11-17T23:50:51.780' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1739', N'W-AMUP-40B20LX-A9P3-AP-MFD0', N'W-MF 40B20L STD UC ASPIRA MF-D', CAST(N'2023-11-17T23:50:51.860' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'174', N'W-CV01-G51XXXX-0MHN-NL-DQ00', N'CV G51 BLUE DG-QR', CAST(N'2023-11-17T23:48:31.180' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1740', N'W-AMUP-40B20LX-A9P3-AS-HPB0', N'W-MF 40B20L STD UC ASAHI HP-B', CAST(N'2023-11-17T23:50:51.943' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1741', N'W-AMUP-40B20LX-A9P3-AS-HPD0', N'W-MF 40B20L STD UC ASAHI HP-D', CAST(N'2023-11-17T23:50:52.023' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1742', N'W-AMUP-65D26LX-B0P4-OH-MFA0', N'W-MF 65D26L SUB-1 UC OHAYO MFA', CAST(N'2023-11-17T23:50:52.107' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1743', N'W-AMUP-50B24LX-B1P3-IN-MFJ0', N'W-MF 50B24L STD UC INCOE UAE', CAST(N'2023-11-17T23:50:52.187' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1744', N'W-AMUP-50B24LX-B1P3-LU-00A0', N'W-MF 50B24L STD UC LUCAS', CAST(N'2023-11-17T23:50:52.267' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1745', N'W-AMUP-50B24LX-B1P3-OH-MFA0', N'W-MF 50B24L STD UC OHAYO MF-A', CAST(N'2023-11-17T23:50:52.347' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1746', N'W-AMUP-50B24RS-B1P2-IN-MFB0', N'W-MF 50B24RS C20 UC INCOE MF-B', CAST(N'2023-11-17T23:50:52.430' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1747', N'W-AMUP-50B24RS-B1P3-AS-HPB0', N'W-MF 50B24RS STD UC ASAHI HP-B', CAST(N'2023-11-17T23:50:52.510' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1748', N'W-AMUP-50B24RS-B1P3-AS-HPC0', N'MF 50B24RS STD UC ASAHI HP-C', CAST(N'2023-11-17T23:50:52.590' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1749', N'W-AMUP-50B24RS-B1P3-BL-MFA0', N'W-MF 50B24RS STD UC NON BRAND', CAST(N'2023-11-17T23:50:52.673' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'175', N'W-CV01-H52XXXX-0MHN-NL-DG00', N'CV H52 BLUE', CAST(N'2023-11-17T23:48:31.267' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1750', N'W-AMUP-50B24RS-B1P3-BL-MFB0', N'W-MF 50B24RS STD UC N-BRAND -B', CAST(N'2023-11-17T23:50:52.753' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1751', N'W-AMUP-50B24RS-B1P3-IN-MFA0', N'W-MF 50B24RS STD UC INCOE MF-A', CAST(N'2023-11-17T23:50:52.837' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1752', N'W-AMUP-50B24RS-B1P3-IN-MFB0', N'W-MF 50B24RS STD UC INCOE MF-B', CAST(N'2023-11-17T23:50:52.917' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1753', N'W-AMUP-50B24RS-B1P3-OH-MFA0', N'W-MF 50B24RS STD UC OHAYO MF-A', CAST(N'2023-11-17T23:50:53.007' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1754', N'W-AMUP-50B24RX-B1P2-IN-MFB0', N'W-MF 50B24R C20 UC INCOE MF-B', CAST(N'2023-11-17T23:50:53.100' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1755', N'W-AMUP-50B24RX-B1P3-AP-MFA0', N'W-MF 50B24R STD UC ASPIRA MF-A', CAST(N'2023-11-17T23:50:53.193' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1756', N'W-AMUP-50B24RX-B1P3-AS-HPB0', N'W-MF 50B24R STD UC ASAHI HP-B', CAST(N'2023-11-17T23:50:53.290' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1757', N'W-AMUP-50B24RX-B1P3-AS-HPC0', N'W-MF 50B24R STD UC ASAHI HP-C', CAST(N'2023-11-17T23:50:53.387' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1758', N'W-AMUP-50B24RX-B1P3-BL-MFA0', N'W-MF 50B24R STD UC NON BRAND', CAST(N'2023-11-17T23:50:53.480' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1759', N'W-AMUP-50B24RX-B1P3-BL-MFB0', N'W-MF 50B24R STD UC N-BRAND - B', CAST(N'2023-11-17T23:50:53.583' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'176', N'W-CV02-E41RXXX-0MHN-NL-DG00', N'CV E41R BLACK', CAST(N'2023-11-17T23:48:31.350' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1760', N'W-AMUP-50B24RX-B1P3-IN-MFA0', N'W-MF 50B24R STD UC INCOE MF-A', CAST(N'2023-11-17T23:50:53.670' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1761', N'W-AMUP-50B24RX-B1P3-IN-MFB0', N'W-MF 50B24R STD UC INCOE MF-B', CAST(N'2023-11-17T23:50:53.753' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1762', N'W-AMUP-50B24RX-B1P3-IN-MFC0', N'W-MF 50B24R STD UC INCOE MF-C', CAST(N'2023-11-17T23:50:53.833' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1763', N'W-AMUP-50B24RX-B1P3-IN-MFH0', N'W-MF 50B24R STD UC INCOE VIE', CAST(N'2023-11-17T23:50:53.913' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1764', N'W-AMUP-50B24RX-B1P3-IN-MFJ0', N'W-MF 50B24R STD UC INCOE UAE', CAST(N'2023-11-17T23:50:53.993' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1765', N'W-AMUP-EFBQ85L-B3C3-OH-MFA0', N'W-EFB Q-85L STD UC OHAYO MF-A', CAST(N'2023-11-17T23:50:54.077' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1766', N'W-AMUP-EFBQ85L-B3C3-QU-MFA0', N'W-EFB Q-85L STD UC QUA MF-A', CAST(N'2023-11-17T23:50:54.160' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1767', N'W-AMUP-N50ZDXX-B1C3-IN-MFE0', N'W-MF N50Z-D STD UC INCOE PHI', CAST(N'2023-11-17T23:50:54.240' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1768', N'W-AMUP-N50ZDXX-B1C3-IN-MFF0', N'W-MF N50Z-D STD UC INCOE AOP', CAST(N'2023-11-17T23:50:54.323' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1769', N'W-AMUP-210H52X-D4P3-LU-00A0', N'W-MF 210H52 STD UC LUCAS', CAST(N'2023-11-17T23:50:54.407' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'177', N'W-CV02-E41RXXX-0MHS-NL-00T0', N'CV E41R MF BLACK', CAST(N'2023-11-17T23:48:31.437' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1770', N'W-AMUP-210H52X-D4P3-NI-MFA0', N'W-MF 210H52 STD UC NIKO MF-A', CAST(N'2023-11-17T23:50:54.487' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1771', N'W-AMUP-210H52X-D4P3-OH-MFA0', N'W-MF 210H52 STD UC OHAYO MF-A', CAST(N'2023-11-17T23:50:54.570' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1772', N'W-AMUP-210H52X-E0C1-AM-HDA0', N'W-MF 120H52 HD UC AMK HDT', CAST(N'2023-11-17T23:50:54.653' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1773', N'W-AMUP-210H52X-E0C1-IN-MFF0', N'W-MF 210H52 HD UC INCOE AOP', CAST(N'2023-11-17T23:50:54.737' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1774', N'W-AMUP-26A19LX-A9C3-BL-MFA0', N'W-MF 26A19L STD UC NON BRAND', CAST(N'2023-11-17T23:50:54.820' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1775', N'W-AMUP-26A19LX-A9C3-BO-MFA0', N'W-MF 26A19L STD UC BOND', CAST(N'2023-11-17T23:50:54.903' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1776', N'W-AMUP-26A19LX-A9C3-IN-MFB0', N'W-MF 26A19L STD UC INCOE MF-B', CAST(N'2023-11-17T23:50:54.987' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1777', N'W-AMUP-26A19RX-A9C3-BL-MFA0', N'W-MF 26A19R STD UC NON BRAND', CAST(N'2023-11-17T23:50:55.067' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1778', N'W-AMUP-26A19RX-A9C3-BO-MFA0', N'W-MF 26A19R STD UC BOND', CAST(N'2023-11-17T23:50:55.150' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1779', N'W-AMUP-26A19RX-A9C3-IN-MFB0', N'W-MF 26A19R STD UC INCOE MF-B', CAST(N'2023-11-17T23:50:55.223' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'178', N'W-CV02-F51LXXX-0MHN-NL-DG00', N'CV F51L BLACK', CAST(N'2023-11-17T23:48:31.520' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1780', N'W-AMUP-26A19RX-A9C3-IN-MFF0', N'W-MF 26A19R STD UC INCOE AOP', CAST(N'2023-11-17T23:50:55.307' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1781', N'W-AMUP-32B20LX-A7P4-AS-HPB0', N'W-MF 32B20L SUB-1 UC ASAHI HPB', CAST(N'2023-11-17T23:50:55.390' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1782', N'W-AMUP-32B20LX-A7P4-QU-MFA0', N'W-MF 32B20L SUB-1 UC QUANTUM-A', CAST(N'2023-11-17T23:50:55.470' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1783', N'W-AMUP-32B20LX-A9P3-AS-MEA0', N'W-MF 32B20L STD UC ASAHI MEN', CAST(N'2023-11-17T23:50:55.553' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1784', N'W-AMUP-32B20LX-A9P3-IN-MFF0', N'W-MF 32B20L STD UC INCOE AOP', CAST(N'2023-11-17T23:50:55.633' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1785', N'W-AMUP-32B20LX-A9P3-IN-MFK0', N'W-MF 32B20L STD UC INCOE DSP', CAST(N'2023-11-17T23:50:55.713' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1786', N'W-AMUP-32B20LX-A9P3-QU-MFA0', N'W-MF 32B20L STD UC QUANTUM-A', CAST(N'2023-11-17T23:50:55.797' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1787', N'W-AMUP-32B20LX-A9P3-QU-MFB0', N'W-MF 32B20L STD UC QUANTUM-B', CAST(N'2023-11-17T23:50:55.880' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1788', N'W-AMUP-32B20LX-A9P3-QU-MFC0', N'W-MF 32B20L STD UC QUANTUM-C', CAST(N'2023-11-17T23:50:55.960' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1789', N'W-AMUP-32B20LX-A9P3-QU-MFD0', N'W-MF 32B20L STD UC QUANTUM-D', CAST(N'2023-11-17T23:50:56.040' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'179', N'W-CV01-H52XXXX-0MHN-NL-DQ00', N'CV H52 BLUE DG-QR', CAST(N'2023-11-17T23:48:31.607' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1790', N'W-AMUP-32B20RX-A7P4-AS-HPB0', N'W-MF 32B20R SUB-1 ASAHI HP-B', CAST(N'2023-11-17T23:50:56.120' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1791', N'W-AMUP-57520RX-B4C3-IN-MFA0', N'W-MF 575-20R STD UC INCOE MF-A', CAST(N'2023-11-17T23:50:56.200' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1792', N'W-AMUP-58024LX-B4C3-AP-MFA0', N'W-MF 580-24L STD UC ASPIRA MFA', CAST(N'2023-11-17T23:50:56.283' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1793', N'W-AMUP-58024LX-B4C3-AS-HPD0', N'W-MF 580-24L STD UC ASAHI HP-D', CAST(N'2023-11-17T23:50:56.367' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1794', N'W-AMUP-58024LX-B4C3-AS-MEA0', N'W-MF 580-24L STD UC ASAHI MEN', CAST(N'2023-11-17T23:50:56.450' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1795', N'W-AMUP-58024LX-B4C3-BL-MFA0', N'W-MF 580-24L STD UC NON BRAND', CAST(N'2023-11-17T23:50:56.530' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1796', N'W-AMUP-58024LX-B4C3-IN-MFB0', N'W-MF 580-24L STD UC INCOE MF-B', CAST(N'2023-11-17T23:50:56.617' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1797', N'W-AMUP-58024LX-B4C3-IN-MFC0', N'W-MF 580-24L STD UC INCOE MF-C', CAST(N'2023-11-17T23:50:56.697' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1798', N'W-AMUP-58024LX-B4C3-IN-MFF0', N'W-MF 580-24L STD UC INCOE AOP', CAST(N'2023-11-17T23:50:56.780' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1799', N'W-AMUP-58024LX-B4C3-IN-MFH0', N'W-MF 580-24L STD UC INCOE VIE', CAST(N'2023-11-17T23:50:56.860' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'18', N'K-CV09-S100XXX-0MHN-NL-00M0', N'CV S100 GREY (ABS)', CAST(N'2023-11-17T23:48:17.830' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'180', N'W-CV02-145G51L-0MHN-NL-DG00', N'CV 145G51L BLACK', CAST(N'2023-11-17T23:48:31.690' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1800', N'W-AMUP-58024LX-B4C3-IN-MFJ0', N'W-MF 580-24L STD UC INCOE UAE', CAST(N'2023-11-17T23:50:56.940' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1801', N'W-AMUP-58024LX-B4C3-IN-MFK0', N'W-MF 580-24L STD UC INCOE DSP', CAST(N'2023-11-17T23:50:57.020' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1802', N'W-AMUP-58024LX-B4C3-OH-MFA0', N'W-MF 580-24L STD UC OHAYO MF-A', CAST(N'2023-11-17T23:50:57.103' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1803', N'W-AMUP-58821RX-B6C3-IN-MFA0', N'W-MF 588-21R STD UC INCOE MF-A', CAST(N'2023-11-17T23:50:57.183' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1804', N'W-AMUP-58821RX-B6C3-QU-MFA0', N'W-MF 588-21R STD UC QUANTUM-A', CAST(N'2023-11-17T23:50:57.277' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1805', N'W-AMUP-58827LX-B6C3-AP-MFA0', N'W-MF 588-27L STD UC ASPIRA MFA', CAST(N'2023-11-17T23:50:57.357' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1806', N'W-AMUP-32B20RX-A7P4-BL-MFA0', N'W-MF 32B20R SUB-1 NON BRAND', CAST(N'2023-11-17T23:50:57.437' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1807', N'W-AMUP-32B20RX-A7P4-QU-MFA0', N'W-MF 32B20R SUB-1 QUANTUM-A', CAST(N'2023-11-17T23:50:57.517' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1808', N'W-AMUP-32B20RX-A9P3-BL-MFC0', N'W-MF 32B20R STD UC N-BRAND -C', CAST(N'2023-11-17T23:50:57.600' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1809', N'W-AMUP-32B20RX-A9P3-IN-MFF0', N'W-MF 32B20R STD UC INCOE AOP', CAST(N'2023-11-17T23:50:57.683' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'181', N'W-CV02-F51XXXX-0MHN-NL-DG00', N'CV F51 BLACK', CAST(N'2023-11-17T23:48:31.773' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1810', N'W-AMUP-45B20LX-B0P2-BL-MFD0', N'W-MF 45B20L C20 UC N-BRA AMK', CAST(N'2023-11-17T23:50:57.763' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1811', N'W-AMUP-45B20LX-B0P3-AP-MFA0', N'W-MF 45B20L STD UC ASPIRA MF-A', CAST(N'2023-11-17T23:50:57.847' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1812', N'W-AMUP-45B20LX-B0P3-AS-HPB0', N'W-MF 45B20L STD UC ASAHI HP-B', CAST(N'2023-11-17T23:50:57.927' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1813', N'W-AMUP-45B20LX-B0P3-AS-HPC0', N'W-MF 45B20L STD UC ASAHI HP-C', CAST(N'2023-11-17T23:50:58.010' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1814', N'W-AHUP-48D26LX-A8A3-IN-GOB0', N'WIP 48D26L STD UC INCOE GOLD', CAST(N'2023-11-17T23:50:58.090' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1815', N'W-AHUP-48D26RX-A8A3-IN-GOB0', N'WIP 48D26R STD UC INCOE GOLD', CAST(N'2023-11-17T23:50:58.170' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1816', N'W-AHUP-48D26RX-A8A3-IN-GOBR', N'WIP 48D26R STD UC INCOE GOL -R', CAST(N'2023-11-17T23:50:58.250' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1817', N'W-AHUP-55D23LX-B0A3-IN-GOB0', N'WIP 55D23L STD UC INCOE GOLD', CAST(N'2023-11-17T23:50:58.330' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1818', N'W-AHUP-55D23RX-B0A3-IN-GOB0', N'WIP 55D23R STD UC INCOE GOLD', CAST(N'2023-11-17T23:50:58.410' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1819', N'W-AHUP-55D26LX-B0A3-IN-GOB0', N'WIP 55D26L STD UC INCOE GOLD', CAST(N'2023-11-17T23:50:58.493' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'182', N'W-CV02-F51XXXX-E02S-NL-00T0', N'CV F51 MF BLACK', CAST(N'2023-11-17T23:48:31.860' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1820', N'W-AHUP-55D26RX-B0A3-IN-GOB0', N'WIP 55D26R STD UC INCOE GOLD', CAST(N'2023-11-17T23:50:58.570' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1821', N'W-AHUP-65D26LX-B2A3-IN-GOB0', N'WIP 65D26L STD UC INCOE GOLD', CAST(N'2023-11-17T23:50:58.653' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1822', N'W-AHUP-65D26RX-B2A3-IN-GOB0', N'WIP 65D26R STD UC INCOE GOLD', CAST(N'2023-11-17T23:50:58.733' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1823', N'W-AHUP-65D31LX-B2A3-IN-GOB0', N'WIP 65D31L STD UC INCOE GOLD', CAST(N'2023-11-17T23:50:58.817' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1824', N'W-AHUP-65D31RX-B2A3-IN-GOB0', N'WIP 65D31R STD UC INCOE GOLD', CAST(N'2023-11-17T23:50:58.900' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1825', N'W-AHUP-75D31LX-B4A3-IN-GOB0', N'WIP 75D31L STD UC INCOE GOLD', CAST(N'2023-11-17T23:50:58.980' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1826', N'W-AHUP-75D31RX-B4A3-IN-GOB0', N'WIP 75D31R STD UC INCOE GOLD', CAST(N'2023-11-17T23:50:59.060' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1827', N'W-AHUP-80D26LX-B3A3-IN-GOB0', N'WIP 80D26L STD UC INCOE GOLD', CAST(N'2023-11-17T23:50:59.143' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1828', N'W-AHUP-80D26RX-B3A3-IN-GOB0', N'WIP 80D26R STD UC INCOE GOLD', CAST(N'2023-11-17T23:50:59.227' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1829', N'W-AHUP-95D31LX-B6A3-IN-GOB0', N'WIP 95D31L STD UC INCOE GOLD', CAST(N'2023-11-17T23:50:59.310' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'183', N'W-CV02-G51LXXX-0MHN-NL-DG00', N'CV G51L BLACK', CAST(N'2023-11-17T23:48:31.947' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1830', N'W-AHUP-95D31RX-B6A3-IN-GOB0', N'WIP 95D31R STD UC INCOE GOLD', CAST(N'2023-11-17T23:50:59.390' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1831', N'W-AMDA-SA100XX-B1C3-AM-00A0', N'WIP SA-100 STD DC ALL MAKES', CAST(N'2023-11-17T23:50:59.473' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1832', N'W-AMDA-SA100XX-B1C3-IN-00A0', N'WIP SA-100 STD DC INCOE', CAST(N'2023-11-17T23:50:59.553' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1833', N'W-AMDA-SA105FT-B1C3-AM-00A0', N'WIP SA-105FT STD DC ALL MAKES', CAST(N'2023-11-17T23:50:59.680' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1834', N'W-AMDA-SA105FT-B1C3-IN-00A0', N'WIP SA-105FT STD DC INCOE', CAST(N'2023-11-17T23:50:59.760' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1835', N'W-AMDA-SA12XXX-B1C3-AM-00A0', N'WIP SA-12 STD DC ALL MAKES', CAST(N'2023-11-17T23:50:59.843' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1836', N'W-AMUP-45B20LX-B0P3-IN-MFA0', N'W-MF 45B20L STD UC INCOE MF-A', CAST(N'2023-11-17T23:50:59.980' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1837', N'W-AMUP-45B20LX-B0P3-IN-MFB0', N'W-MF 45B20L STD UC INCOE MF-B', CAST(N'2023-11-17T23:51:00.077' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1838', N'W-AMUP-45B20LX-B0P3-IN-MFC0', N'W-MF 45B20L STD UC INCOE MF-C', CAST(N'2023-11-17T23:51:00.157' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1839', N'W-AMUP-45B20LX-B0P3-IN-MFE0', N'W-MF 45B20L STD UC INCOE PHI', CAST(N'2023-11-17T23:51:00.240' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'184', N'W-CV02-G51XXXX-0MHN-NL-DG00', N'CV G51 BLACK', CAST(N'2023-11-17T23:48:32.030' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1840', N'W-AMUP-45B20LX-B0P3-IN-MFH0', N'W-MF 45B20L STD UC INCOE VIE', CAST(N'2023-11-17T23:51:00.323' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1841', N'W-AMUP-45B20LX-B0P3-IN-MFJ0', N'W-MF 45B20L STD UC INCOE UAE', CAST(N'2023-11-17T23:51:00.403' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1842', N'W-AMUP-45B20LX-B0P3-LU-00A0', N'W-MF 45B20L STD UC LUCAS', CAST(N'2023-11-17T23:51:00.483' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1843', N'W-AMUP-45B20LX-B0P3-OH-MFA0', N'W-MF 45B20L STD UC OHAYO MF-A', CAST(N'2023-11-17T23:51:00.570' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1844', N'W-AMUP-45B20RS-B0P3-AS-HPC0', N'W-MF 45B20RS STD UC ASAHI HP-C', CAST(N'2023-11-17T23:51:00.650' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1845', N'W-AMUP-45B20RS-B0P3-IN-MFA0', N'W-MF 45B20RS STD UC INCOE MF-A', CAST(N'2023-11-17T23:51:00.737' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1846', N'W-AMUP-45B20RS-B0P3-IN-MFB0', N'W-MF 45B20RS STD UC INCOE MF-B', CAST(N'2023-11-17T23:51:00.817' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1847', N'W-AMUP-45B20RS-B0P3-IN-MFH0', N'W-MF 45B20RS STD UC INCOE VIE', CAST(N'2023-11-17T23:51:00.897' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1848', N'W-AMUP-45B20RX-B0P3-AS-HPB0', N'W-MF 45B20R STD UC ASAHI HP-B', CAST(N'2023-11-17T23:51:00.980' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1849', N'W-AMUP-45B20RX-B0P3-AS-HPC0', N'W-MF 45B20R STD UC ASAHI HP-C', CAST(N'2023-11-17T23:51:01.060' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'185', N'W-CV02-G51XXXX-F02S-NL-00T0', N'CV G51 MF BLACK', CAST(N'2023-11-17T23:48:32.113' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1850', N'W-AMUP-45B20RX-B0P3-IN-MFA0', N'W-MF 45B20R STD UC INCOE MF-A', CAST(N'2023-11-17T23:51:01.140' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1851', N'W-AMUP-45B20RX-B0P3-IN-MFB0', N'W-MF 45B20R STD UC INCOE MF-B', CAST(N'2023-11-17T23:51:01.223' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1852', N'W-AMUP-45B20RX-B0P3-IN-MFC0', N'W-MF 45B20R STD UC INCOE MF-C', CAST(N'2023-11-17T23:51:01.303' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1853', N'W-AMUP-45B20RX-B0P3-IN-MFE0', N'W-MF 45B20R STD UC INCOE PHI', CAST(N'2023-11-17T23:51:01.387' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1854', N'W-AMUP-45B20RX-B0P3-IN-MFH0', N'W-MF 45B20R STD UC INCOE VIE', CAST(N'2023-11-17T23:51:01.467' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1855', N'W-AMUP-45B20RX-B0P3-OH-MFA0', N'W-MF 45B20R STD UC OHAYO MF-A', CAST(N'2023-11-17T23:51:01.550' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1856', N'W-AMUP-45C24LX-B0P2-IN-MFE0', N'W-MF 45C24L C20 UC INCOE PHI', CAST(N'2023-11-17T23:51:01.630' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1857', N'W-AMUP-45C24LX-B0P3-AP-MFA0', N'W-MF 45C24L STD UC ASPIRA MF-A', CAST(N'2023-11-17T23:51:01.710' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1858', N'W-AMUP-45C24LX-B0P3-AP-MFD0', N'W-MF 45C24L STD UC ASPIRA MF-D', CAST(N'2023-11-17T23:51:01.793' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1859', N'W-AMUP-45C24LX-B0P3-AS-HPB0', N'W-MF 45C24L STD UC ASAHI HP-B', CAST(N'2023-11-17T23:51:01.877' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'186', N'W-CV02-H52XXXX-0MHN-NL-DG00', N'CV H52 BLACK', CAST(N'2023-11-17T23:48:32.197' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1860', N'W-AMUP-45C24LX-B0P3-AS-HPD0', N'W-MF 45C24L STD UC ASAHI HP-D', CAST(N'2023-11-17T23:51:01.957' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1861', N'W-AMDA-SA65XXX-B1C3-IN-00A0', N'WIP SA-65 STD DC INCOE', CAST(N'2023-11-17T23:51:02.037' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1862', N'W-AMDA-SA9XXXX-A9C3-AM-00A0', N'WIP SA-9 STD DC INCOE', CAST(N'2023-11-17T23:51:02.127' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1863', N'W-AMDF-SG100CT-B3C3-AM-00A0', N'WIP SG-100CT STD ALL MAKES', CAST(N'2023-11-17T23:51:02.203' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1864', N'W-AMDF-SG100CT-B3C3-AP-00A0', N'WIP SG-100CT STD ASPIRA', CAST(N'2023-11-17T23:51:02.290' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1865', N'W-AMDF-SG100XX-B1C3-AM-00A0', N'WIP SG-100 STD DC ALL MAKES', CAST(N'2023-11-17T23:51:02.367' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1866', N'W-AMUP-32B20RX-A9P3-IN-MFM0', N'W-MF 32B20R STD UC INCOE VEL', CAST(N'2023-11-17T23:51:02.447' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1867', N'W-AMUP-45C24LX-B0P3-IN-MFA0', N'W-MF 45C24L STD UC INCOE MF-A', CAST(N'2023-11-17T23:51:02.530' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1868', N'W-ACDP-115F51X-B9C4-AS-HPC0', N'WIP 115F51 SUB-1 DC ASA HP - C', CAST(N'2023-11-17T23:51:02.613' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1869', N'W-ACDP-145G51X-C3C5-AS-HPC0', N'WIP 145G51 SUB-2 DC ASA HP - C', CAST(N'2023-11-17T23:51:02.693' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'187', N'W-CV02-165G51L-0MHN-NL-DG00', N'CV 165G51L BLACK', CAST(N'2023-11-17T23:48:32.280' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1870', N'W-ACDP-190H52X-D1C6-AS-HPC0', N'WIP 190H52 SUB-3 DC ASA HP - C', CAST(N'2023-11-17T23:51:02.777' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1871', N'W-ACDP-65D26LX-B1C4-AS-HPC0', N'WIP 65D26L SUB-1 DC ASA HP - C', CAST(N'2023-11-17T23:51:02.857' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1872', N'W-ACDP-65D26RX-B1C4-AS-HPC0', N'WIP 65D26R SUB-1 DC ASA HP - C', CAST(N'2023-11-17T23:51:02.937' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1873', N'W-ACDP-95D31LX-B5C4-AS-HPC0', N'WIP 95D31L SUB-1 DC ASA HP - C', CAST(N'2023-11-17T23:51:03.017' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1874', N'W-ACDP-95D31RX-B5C4-AS-HPC0', N'WIP 95D31R SUB-1 DC ASA HP - C', CAST(N'2023-11-17T23:51:03.100' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1875', N'W-ACDP-95E41LX-B5C4-AS-HPC0', N'WIP 95E41L SUB-1 DC ASA HP - C', CAST(N'2023-11-17T23:51:03.180' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1876', N'W-ACDP-95E41RX-B5C4-AS-HPC0', N'WIP 95E41R SUB-1 DC ASA HP - C', CAST(N'2023-11-17T23:51:03.270' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1877', N'W-ACDR-6D115XX-B7C3-BO-00A0', N'WIP 6D115 STD DC BOND', CAST(N'2023-11-17T23:51:03.350' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1878', N'W-ACDR-8D90XXX-B4C3-IN-00A0', N'WIP 8D90 STD DC INCOE', CAST(N'2023-11-17T23:51:03.430' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1879', N'W-ACUP-115F51X-B9C4-AP-PRB0', N'WIP 115F51 SUB-1 UC ASPIRA PRE', CAST(N'2023-11-17T23:51:03.513' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'188', N'W-CV02-4DRXXXX-0MHN-AM-00T0', N'CV 4D-R MF BLACK ALL MAKES', CAST(N'2023-11-17T23:48:32.367' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1880', N'W-ACUP-115F51X-B9C4-AS-HPC0', N'WIP 115F51 SUB-1 UC ASA HP - C', CAST(N'2023-11-17T23:51:03.593' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1881', N'W-ACUP-145G51X-C3C5-AP-PRB0', N'WIP 145G51 SUB-2 UC ASPIRA PRE', CAST(N'2023-11-17T23:51:03.680' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1882', N'W-ACUP-145G51X-C3C5-AS-HPC0', N'WIP 145G51 SUB-2 UC ASA HP - C', CAST(N'2023-11-17T23:51:03.770' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1883', N'W-ACUP-190H52X-D1C6-AP-PRB0', N'WIP 190H52 SUB-3 UC ASPIRA PRE', CAST(N'2023-11-17T23:51:03.870' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1884', N'W-ACUP-190H52X-D1C6-AS-HPC0', N'WIP 190H52 SUB-3 UC ASA HP - C', CAST(N'2023-11-17T23:51:03.963' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1885', N'W-ACUP-36B20LX-A9C4-AP-PRB0', N'WIP 36B20L SUB-1 UC ASPIRA PRE', CAST(N'2023-11-17T23:51:04.060' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1886', N'W-AMUP-170G51X-C7C3-IN-MFE0', N'W-MF 170G51 STD UC INCOE PHI', CAST(N'2023-11-17T23:51:04.153' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1887', N'W-AMUP-40B20LX-A9P3-BL-MFA0', N'W-MF 40B20L STD UC NON BRAND', CAST(N'2023-11-17T23:51:04.247' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1888', N'W-AHUP-46B24RX-B1A3-IN-GOB0', N'WIP 46B24R STD UC INCOE GOLD', CAST(N'2023-11-17T23:51:04.350' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1889', N'W-AMUP-32B20RX-A9P3-QU-MFA0', N'W-MF 32B20R STD UC QUANTUM-A', CAST(N'2023-11-17T23:51:04.430' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'189', N'W-CV02-544LXXX-D02C-NL-00T0', N'CV 544L MF BLACK', CAST(N'2023-11-17T23:48:32.450' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1890', N'W-AMUP-32B20RX-A9P3-QU-MFB0', N'W-MF 32B20R STD UC QUANTUM-B', CAST(N'2023-11-17T23:51:04.510' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1891', N'W-AMUP-32B20RX-A9P3-QU-MFC0', N'W-MF 32B20R STD UC QUANTUM-C', CAST(N'2023-11-17T23:51:04.593' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1892', N'W-AMUP-32C24LX-A9P4-IN-MFB0', N'W-MF 32C24L SUB-1 UC INCOE MFB', CAST(N'2023-11-17T23:51:04.673' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1893', N'W-AMUP-32C24LX-A9P4-QU-MFA0', N'W-MF 32C24L SUB-1 UC QUANTUM-A', CAST(N'2023-11-17T23:51:04.757' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1894', N'W-AMUP-32C24LX-B0P3-AS-MEA0', N'W-MF 32C24L STD UC ASAHI MEN', CAST(N'2023-11-17T23:51:04.837' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1895', N'W-AMUP-32C24LX-B0P3-QU-MFA0', N'W-MF 32C24L STD UC QUANTUM-A', CAST(N'2023-11-17T23:51:04.917' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1896', N'W-AMUP-32C24LX-B0P3-QU-MFB0', N'W-MF 32C24L STD UC QUANTUM-B', CAST(N'2023-11-17T23:51:05.000' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1897', N'W-AMUP-32C24LX-B0P3-QU-MFD0', N'W-MF 32C24L STD UC QUANTUM-D', CAST(N'2023-11-17T23:51:05.080' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1898', N'W-AMUP-32C24RX-A9P4-IN-MFB0', N'W-MF 32C24R SUB-1 UC INCOE MFB', CAST(N'2023-11-17T23:51:05.160' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1899', N'W-AMUP-32C24RX-A9P4-QU-MFA0', N'W-MF 32C24R SUB-1 UC QUANTUM-A', CAST(N'2023-11-17T23:51:05.240' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'19', N'K-CV09-S105XXX-0MHN-NL-00M0', N'CV S105 GREY (ABS)', CAST(N'2023-11-17T23:48:17.913' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'190', N'W-CV02-544RXXX-D02C-NL-00T0', N'CV 544R MF BLACK', CAST(N'2023-11-17T23:48:32.533' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1900', N'W-AMUP-32C24RX-B0P3-IN-MFF0', N'W-MF 32C24R STD UC INCOE AOP', CAST(N'2023-11-17T23:51:05.323' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1901', N'W-AMUP-32C24RX-B0P3-QU-MFA0', N'W-MF 32C24R STD UC QUANTUM-A', CAST(N'2023-11-17T23:51:05.403' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1902', N'W-AMUP-32C24RX-B0P3-QU-MFB0', N'W-MF 32C24R STD UC QUANTUM-B', CAST(N'2023-11-17T23:51:05.487' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1903', N'W-AMUP-32C24RX-B0P3-QU-MFC0', N'W-MF 32C24R STD UC QUANTUM-C', CAST(N'2023-11-17T23:51:05.567' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1904', N'W-AMUP-36B20LS-B0P3-IN-MFF0', N'W-MF 36B20LS STD UC INCOE AOP', CAST(N'2023-11-17T23:51:05.647' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1905', N'W-AMUP-36B20LX-A7P5-OH-MFA0', N'W-MF 36B20L SUB-2 UC OHAYO MFA', CAST(N'2023-11-17T23:51:05.730' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1906', N'W-AMUP-36B20LX-A7P5-QU-MFA0', N'W-MF 36B20L SUB-2 UC QUANTUM-A', CAST(N'2023-11-17T23:51:05.810' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1907', N'W-AMUP-36B20LX-A8P4-IN-MFB0', N'W-MF 36B20L SUB-1 UC INC MF-B', CAST(N'2023-11-17T23:51:05.890' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1908', N'W-AMUP-36B20LX-A8P4-QU-MFA0', N'W-MF 36B20L SUB-1 UC QUANTUM-A', CAST(N'2023-11-17T23:51:05.970' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1909', N'W-AMUP-36B20LX-B0P2-IN-MFL0', N'W-MF 36B20L C20 UC INCOE HYD', CAST(N'2023-11-17T23:51:06.050' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'191', N'W-CV02-555LXXX-D02C-NL-00T0', N'CV 555L MF BLACK', CAST(N'2023-11-17T23:48:32.617' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1910', N'W-AMUP-36B20LX-B0P3-AS-MEA0', N'W-MF 36B20L STD UC ASAHI MEN', CAST(N'2023-11-17T23:51:06.137' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1911', N'W-AMUP-55559LX-B0C3-AS-HPC0', N'W-MF 555-59L STD UC ASAHI HP-C', CAST(N'2023-11-17T23:51:06.223' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1912', N'W-AMUP-135F51X-C1C3-LU-00A0', N'W-MF 135F51 STD UC LUCAS', CAST(N'2023-11-17T23:51:06.303' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1913', N'W-AMUP-135F51X-C1C3-NI-MFA0', N'W-MF 135F51 STD UC NIKO MF-A', CAST(N'2023-11-17T23:51:06.383' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1914', N'W-AMUP-135F51X-C1C3-OH-MFA0', N'W-MF 135F51 STD UC OHAYO MF-A', CAST(N'2023-11-17T23:51:06.463' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1915', N'W-AMUP-95E41RX-B7C3-QU-MFB0', N'W-MF 95E41R STD UC QUANTUM-B', CAST(N'2023-11-17T23:51:06.547' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1916', N'W-AMUP-95E41RX-B7C3-QU-MFC0', N'W-MF 95E41R STD UC QUANTUM-C', CAST(N'2023-11-17T23:51:06.627' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1917', N'W-AMUP-95E41RX-B7C3-QU-MFD0', N'W-MF 95E41R STD UC QUANTUM-D', CAST(N'2023-11-17T23:51:06.710' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1918', N'W-AMUP-95E41RX-B7C3-QU-MFF0', N'W-MF 95E41R STD UC QUANTUM-F', CAST(N'2023-11-17T23:51:06.797' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1919', N'W-AMUP-95E41RX-B7C3-TN-00A0', N'W-MF 95E41R STD UC TAKNUS', CAST(N'2023-11-17T23:51:06.877' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'192', N'W-CV02-555RXXX-D02C-NL-00T0', N'CV 555R MF BLACK', CAST(N'2023-11-17T23:48:32.700' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1920', N'W-AMUP-EFBM42L-B2P3-AP-MFA0', N'W-EFB M-42L STD UC ASPIRA MF-A', CAST(N'2023-11-17T23:51:06.957' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1921', N'W-AMUP-EFBM42L-B2P3-AS-HPC0', N'W-EFB M-42L STD UC ASAHI HP-C', CAST(N'2023-11-17T23:51:07.037' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1922', N'W-AMUP-EFBM42L-B2P3-LU-00A0', N'W-EFB M-42L STD UC LUCAS', CAST(N'2023-11-17T23:51:07.117' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1923', N'W-AMUP-EFBM42L-B2P3-OH-MFA0', N'W-EFB M-42L STD UC OHAYO MF-A', CAST(N'2023-11-17T23:51:07.200' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1924', N'W-AMUP-EFBM42L-B2P3-QU-MFA0', N'W-EFB M-42L STD UC QUA MF-A', CAST(N'2023-11-17T23:51:07.277' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1925', N'W-AMUP-EFBQ85L-B3C3-AP-MFA0', N'W-EFB Q-85L STD UC ASPPIRA MFA', CAST(N'2023-11-17T23:51:07.357' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1926', N'W-AMUP-EFBQ85L-B3C3-IN-MFB0', N'W-EFB Q-85L STD UC INCOE MF-B', CAST(N'2023-11-17T23:51:07.440' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1927', N'W-AMUP-EFBQ85L-B3C3-IN-MFH0', N'W-EFB Q-85L STD UC INCOE VIE', CAST(N'2023-11-17T23:51:07.520' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1928', N'W-AMUP-55D26RX-B0P3-QU-MFB0', N'W-MF 55D26R STD UC QUANTUM-B', CAST(N'2023-11-17T23:51:07.603' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1929', N'W-AMUP-55D26RX-B0P3-QU-MFC0', N'W-MF 55D26R STD UC QUANTUM-C', CAST(N'2023-11-17T23:51:07.683' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'193', N'W-CV02-566LXXX-D02C-NL-00T0', N'CV 566L MF BLACK', CAST(N'2023-11-17T23:48:32.783' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1930', N'W-AMUP-55D26RX-B0P3-QU-MFE0', N'W-MF 55D26R STD UC QUANTUM-E', CAST(N'2023-11-17T23:51:07.763' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1931', N'W-AMUP-56638LX-B2C3-IN-MFC0', N'W-MF 566-38L STD UC INCOE MF-C', CAST(N'2023-11-17T23:51:07.847' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1932', N'W-AMUP-56638LX-B2C3-IN-MFJ0', N'W-MF 566-38L STD UC INCOE UAE', CAST(N'2023-11-17T23:51:07.923' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1933', N'W-AMUP-56638LX-B2C3-LU-00A0', N'W-MF 566-38L STD UC LUCAS', CAST(N'2023-11-17T23:51:08.007' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1934', N'W-AMUP-56638LX-B2C3-OH-MFA0', N'W-MF 566-38L STD UC OHAYO MF-A', CAST(N'2023-11-17T23:51:08.087' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1935', N'W-AMUP-56638LX-B2C3-OH-MFB0', N'W-MF 566-38L STD UC OHAYO UAE', CAST(N'2023-11-17T23:51:08.170' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1936', N'W-AMUP-90D26LX-B3P3-AS-HPD0', N'W-MF 90D26L STD UC ASAHI HP-D', CAST(N'2023-11-17T23:51:08.247' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1937', N'W-AMUP-90D26LX-B3P3-BL-MFA0', N'W-MF 90D26L STD UC NON BRAND', CAST(N'2023-11-17T23:51:08.327' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1938', N'W-AMUP-90D26LX-B3P3-IN-MFA0', N'W-MF 90D26L STD UC INCOE MF-A', CAST(N'2023-11-17T23:51:08.410' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1939', N'W-AMUP-90D26LX-B3P3-IN-MFB0', N'W-MF 90D26L STD UC INCOE MF-B', CAST(N'2023-11-17T23:51:08.490' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'194', N'W-CV02-566RXXX-D02C-NL-00T0', N'CV 566R MF BLACK', CAST(N'2023-11-17T23:48:32.867' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1940', N'W-AMUP-90D26LX-B3P3-IN-MFC0', N'W-MF 90D26L STD UC INCOE MF-C', CAST(N'2023-11-17T23:51:08.573' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1941', N'W-AMUP-90D26LX-B3P3-IN-MFD0', N'W-MF 90D26L STD UC INCOE SUD', CAST(N'2023-11-17T23:51:08.653' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1942', N'W-AMUP-90D26LX-B3P3-IN-MFE0', N'W-MF 90D26L STD UC INCOE PHI', CAST(N'2023-11-17T23:51:08.737' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1943', N'W-AMUP-90D26LX-B3P3-IN-MFH0', N'W-MF 90D26L STD UC INCOE VIE', CAST(N'2023-11-17T23:51:08.820' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1944', N'W-AMUP-90D26LX-B3P3-IN-MFJ0', N'W-MF 90D26L STD UC INCOE UAE', CAST(N'2023-11-17T23:51:08.900' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1945', N'W-AMUP-90D26LX-B3P3-OH-MFA0', N'W-MF 90D26L STD UC OHAYO MF-A', CAST(N'2023-11-17T23:51:08.980' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1946', N'W-AMUP-90D26LX-B3P3-OH-MFB0', N'W-MF 90D26L STD UC OHAYO UAE', CAST(N'2023-11-17T23:51:09.060' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1947', N'W-AMUP-90D26LX-B3P3-QU-MFB0', N'W-MF 90D26L STD UC QUANTUM-B', CAST(N'2023-11-17T23:51:09.143' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1948', N'W-AMUP-90D26LX-B3P3-QU-MFE0', N'W-MF 90D26L STD UC QUANTUM-E', CAST(N'2023-11-17T23:51:09.227' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1949', N'W-AMUP-55565RX-B0C3-OH-MFA0', N'W-MF 555-65R STD UC OHAYO MF-A', CAST(N'2023-11-17T23:51:09.310' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'195', N'W-CV02-588LXXX-D02C-NL-00T0', N'CV 588L MF BLACK', CAST(N'2023-11-17T23:48:32.950' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1950', N'W-AMUP-55565RX-B0C3-QU-MFA0', N'W-MF 555-65R STD UC QUANTUM-A', CAST(N'2023-11-17T23:51:09.393' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1951', N'W-AMUP-55565RX-B0C3-QU-MFB0', N'W-MF 555-65R STD UC QUANTUM-B', CAST(N'2023-11-17T23:51:09.470' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1952', N'W-AMUP-55565RX-B2C2-AS-HPB0', N'W-MF 555-65R C20 UC ASAHI HP-B', CAST(N'2023-11-17T23:51:09.553' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1953', N'W-AMUP-55565RX-B2C2-IN-MFB0', N'W-MF 555-65R C20 UC INCOE MF-B', CAST(N'2023-11-17T23:51:09.637' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1954', N'W-AMUP-55565RX-B2C2-IN-MFG0', N'W-MF 555-65R C20 UC INCOE KSA', CAST(N'2023-11-17T23:51:09.713' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1955', N'W-AMUP-55D23LX-B0P3-AS-MEA0', N'W-MF 55D23L STD UC ASAHI MEN', CAST(N'2023-11-17T23:51:09.797' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1956', N'W-AMUP-55D23LX-B0P3-IN-MFF0', N'W-MF 55D23L STD UC INCOE AOP', CAST(N'2023-11-17T23:51:09.877' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1957', N'W-AMUP-55D23LX-B0P3-QU-MFA0', N'W-MF 55D23L STD UC QUANTUM-A', CAST(N'2023-11-17T23:51:09.957' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1958', N'W-AMUP-55D23LX-B0P3-QU-MFB0', N'W-MF 55D23L STD UC QUANTUM-B', CAST(N'2023-11-17T23:51:10.037' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1959', N'W-AMUP-55D23LX-B0P3-QU-MFD0', N'W-MF 55D23L STD UC QUANTUM-D', CAST(N'2023-11-17T23:51:10.120' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'196', N'W-CV02-588RXXX-D02C-NL-00T0', N'CV 588R MF BLACK', CAST(N'2023-11-17T23:48:33.033' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1960', N'W-AMUP-55D23LX-B0P3-QU-MFF0', N'W-MF 55D23L STD UC QUANTUM-F', CAST(N'2023-11-17T23:51:10.200' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1961', N'W-AMUP-55D23RX-B0P3-AS-MEA0', N'W-MF 55D23R STD UC ASAHI MEN', CAST(N'2023-11-17T23:51:10.280' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1962', N'W-AMUP-55D23RX-B0P3-IN-MFF0', N'W-MF 55D23R STD UC INCOE AOP', CAST(N'2023-11-17T23:51:10.363' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1963', N'W-AMUP-55D23RX-B0P3-QU-MFA0', N'W-MF 55D23R STD UC QUANTUM-A', CAST(N'2023-11-17T23:51:10.443' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1964', N'W-AMUP-55D23RX-B0P3-QU-MFB0', N'W-MF 55D23R STD UC QUANTUM-B', CAST(N'2023-11-17T23:51:10.527' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1965', N'W-AMUP-55D23RX-B0P3-QU-MFF0', N'W-MF 55D23R STD UC QUANTUM-F', CAST(N'2023-11-17T23:51:10.607' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1966', N'W-AMUP-55D26LX-B0P3-AS-MEA0', N'W-MF 55D26L STD UC ASAHI MEN', CAST(N'2023-11-17T23:51:10.687' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1967', N'W-AMUP-55D26LX-B0P3-IN-MFF0', N'W-MF 55D26L STD UC INCOE AOP', CAST(N'2023-11-17T23:51:10.770' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1968', N'W-AMUP-55D26LX-B0P3-QU-MFA0', N'W-MF 55D26L STD UC QUANTUM-A', CAST(N'2023-11-17T23:51:10.850' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1969', N'W-AMUP-55D26LX-B0P3-QU-MFB0', N'W-MF 55D26L STD UC QUANTUM-B', CAST(N'2023-11-17T23:51:10.930' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'197', N'W-CV02-A19LXXX-D02C-NL-00M0', N'CV A19L MF BLACK', CAST(N'2023-11-17T23:48:33.117' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1970', N'W-AMUP-55D26LX-B0P3-QU-MFC0', N'W-MF 55D26L STD UC QUANTUM-C', CAST(N'2023-11-17T23:51:11.013' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1971', N'W-AMUP-55D26LX-B0P3-QU-MFF0', N'W-MF 55D26L STD UC QUANTUM-F', CAST(N'2023-11-17T23:51:11.097' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1972', N'W-AMUP-46B24LS-B1P3-IN-MFL0', N'W-MF 46B24LS STD UC INCOE HYD', CAST(N'2023-11-17T23:51:11.177' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1973', N'W-AMUP-46B24LS-B1P3-QU-MFA0', N'W-MF 46B24LS STD UC QUANTUM-A', CAST(N'2023-11-17T23:51:11.257' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1974', N'W-AMUP-58827LX-B6C3-IN-MFB0', N'W-MF 588-27L STD UC INCOE MF-B', CAST(N'2023-11-17T23:51:11.340' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1975', N'W-AMUP-55D26RX-B0P3-QU-MFF0', N'W-MF 55D26R STD UC QUANTUM-F', CAST(N'2023-11-17T23:51:11.423' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1976', N'W-AMUP-56073LX-B1C3-AP-MFA0', N'W-MF 560-73L STD UC ASPIRA MFA', CAST(N'2023-11-17T23:51:11.503' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1977', N'W-AMUP-56073LX-B1C3-IN-MFB0', N'W-MF 560-73L STD UC INCOE MF-B', CAST(N'2023-11-17T23:51:11.583' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1978', N'W-AMUP-56219LX-B2C3-AP-MFA0', N'W-MF 562-19L STD UC ASPIRA MFA', CAST(N'2023-11-17T23:51:11.667' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1979', N'W-AMUP-56219LX-B2C3-IN-MFB0', N'W-MF 562-19L STD UC INCOE MF-B', CAST(N'2023-11-17T23:51:11.747' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'198', N'W-CV02-A19RXXX-D02C-NL-00M0', N'CV A19R MF BLACK', CAST(N'2023-11-17T23:48:33.200' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1980', N'W-AMUP-56219LX-B2C3-IN-MFD0', N'W-MF 562-19L STD UC INCOE SUD', CAST(N'2023-11-17T23:51:11.830' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1981', N'W-AMUP-56219LX-B2C3-IN-MFE0', N'W-MF 562-19L STD UC INCOE PHI', CAST(N'2023-11-17T23:51:11.910' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1982', N'W-AMUP-56219LX-B2C3-IN-MFF0', N'W-MF 562-19L STD UC INCOE AOP', CAST(N'2023-11-17T23:51:11.990' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1983', N'W-AMUP-56219LX-B2C3-IN-MFH0', N'W-MF 562-19L STD UC INCOE VIE', CAST(N'2023-11-17T23:51:12.070' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1984', N'W-AMUP-56219LX-B2C3-IN-MFL0', N'W-MF 562-19L STD UC INCOE HYD', CAST(N'2023-11-17T23:51:12.153' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1985', N'W-AMUP-56219LX-B2C3-LU-00A0', N'W-MF 562-19L STD UC LUCAS', CAST(N'2023-11-17T23:51:12.240' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1986', N'W-AMUP-56219LX-B2C3-OH-MFA0', N'W-MF 562-19L STD UC OHAYO MF-A', CAST(N'2023-11-17T23:51:12.320' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1987', N'W-AMUP-56219LX-B2C3-QU-MFB0', N'W-MF 562-19L STD UC QUANTUM-B', CAST(N'2023-11-17T23:51:12.400' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1988', N'W-AMUP-56221RX-B2C3-AP-MFA0', N'W-MF 562-21R STD UC ASPIRA MFA', CAST(N'2023-11-17T23:51:12.487' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1989', N'W-AMUP-56221RX-B2C3-IN-MFB0', N'W-MF 562-21R STD UC INCOE MF-B', CAST(N'2023-11-17T23:51:12.563' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'199', N'W-CV02-B20LSXX-A02N-NL-DG00', N'CV B20LS BLACK HD1-BLACK', CAST(N'2023-11-17T23:48:33.287' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1990', N'W-AMUP-56221RX-B2C3-LU-00A0', N'W-MF 562-21R STD UC LUCAS', CAST(N'2023-11-17T23:51:12.647' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1991', N'W-AMUP-56221RX-B2C3-OH-MFA0', N'W-MF 562-21R STD UC OHAYO MF-A', CAST(N'2023-11-17T23:51:12.727' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1992', N'W-AMUP-56638LX-B2C3-AP-MFA0', N'W-MF 566-38L STD UC ASPIRA MFA', CAST(N'2023-11-17T23:51:12.810' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1993', N'W-AMUP-56638LX-B2C3-AS-HPC0', N'W-MF 566-38L STD UC ASAHI HP-C', CAST(N'2023-11-17T23:51:12.890' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1994', N'W-AMUP-56638LX-B2C3-BL-MFC0', N'W-MF 566-38L STD UC N-BRAND -C', CAST(N'2023-11-17T23:51:12.970' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1995', N'W-AMUP-56638LX-B2C3-IN-MFA0', N'W-MF 566-38L STD UC INCOE MF-A', CAST(N'2023-11-17T23:51:13.050' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1996', N'W-AMUP-46B24RS-B0P4-QU-MFA0', N'W-MF 46B24RS SUB-1 UC QUANTU-A', CAST(N'2023-11-17T23:51:13.130' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1997', N'W-AMUP-90D26RX-B3P3-AP-MFA0', N'W-MF 90D26R STD UC ASPIRA MF-A', CAST(N'2023-11-17T23:51:13.213' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1998', N'W-AMUP-90D26RX-B3P3-AS-HPB0', N'W-MF 90D26R STD UC ASAHI HP-B', CAST(N'2023-11-17T23:51:13.293' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'1999', N'W-AMUP-90D26RX-B3P3-AS-HPC0', N'W-MF 90D26R STD UC ASAHI HP-C', CAST(N'2023-11-17T23:51:13.377' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2', N'K-TC09-S105XXX-GLFS-NL-0000', N'TOP COVER S105 GREY', CAST(N'2023-11-17T23:48:16.433' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'20', N'K-CV09-S200XXX-0MHN-NL-00M0', N'CV S200 GREY (ABS)', CAST(N'2023-11-17T23:48:17.990' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'200', N'W-CV02-B20LSXX-D02C-NL-00T0', N'CV B20LS MF BLACK', CAST(N'2023-11-17T23:48:33.367' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2000', N'W-AMUP-90D26RX-B3P3-BL-MFA0', N'W-MF 90D26R STD UC NON BRAND', CAST(N'2023-11-17T23:51:13.457' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2001', N'W-AMUP-90D26RX-B3P3-IN-MFA0', N'W-MF 90D26R STD UC INCOE MF-A', CAST(N'2023-11-17T23:51:13.540' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2002', N'W-AMUP-90D26RX-B3P3-IN-MFB0', N'W-MF 90D26R STD UC INCOE MF-B', CAST(N'2023-11-17T23:51:13.620' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2003', N'W-AMUP-90D26RX-B3P3-IN-MFC0', N'W-MF 90D26R STD UC INCOE MF-C', CAST(N'2023-11-17T23:51:13.703' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2004', N'W-AMUP-90D26RX-B3P3-IN-MFD0', N'W-MF 90D26R STD UC INCOE SUD', CAST(N'2023-11-17T23:51:13.787' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2005', N'W-AMUP-90D26RX-B3P3-IN-MFH0', N'W-MF 90D26R STD UC INCOE VIE', CAST(N'2023-11-17T23:51:13.867' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2006', N'W-AMUP-90D26RX-B3P3-IN-MFJ0', N'W-MF 90D26R STD UC INCOE UAE', CAST(N'2023-11-17T23:51:13.947' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2007', N'W-AMUP-90D26RX-B3P3-OH-MFA0', N'W-MF 90D26R STD UC OHAYO MF-A', CAST(N'2023-11-17T23:51:14.030' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2008', N'W-AMUP-90D26RX-B3P3-OH-MFB0', N'W-MF 90D26R STD UC OHAYO UAE', CAST(N'2023-11-17T23:51:14.110' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2009', N'W-AMUP-95D26LX-B3P3-IN-MFB0', N'W-MF 95D26L STD UC INCOE MF-B', CAST(N'2023-11-17T23:51:14.190' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'201', N'W-CV02-B20LXXX-A02N-NL-DG00', N'CV B20L BLACK HD1-BLACK', CAST(N'2023-11-17T23:48:33.453' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2010', N'W-AMUP-58827LX-B6C3-IN-MFC0', N'W-MF 588-27L STD UC INCOE MF-C', CAST(N'2023-11-17T23:51:14.270' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2011', N'W-AMUP-58827LX-B6C3-IN-MFF0', N'W-MF 588-27L STD UC INCOE AOP', CAST(N'2023-11-17T23:51:14.353' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2012', N'W-AMUP-58827LX-B6C3-IN-MFJ0', N'W-MF 588-27L STD UC INCOE UAE', CAST(N'2023-11-17T23:51:14.447' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2013', N'W-AMUP-58827LX-B6C3-OH-MFA0', N'W-MF 588-27L STD UC OHAYO MF-A', CAST(N'2023-11-17T23:51:14.550' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2014', N'W-AMUP-58827LX-B6C3-OH-MFB0', N'W-MF 588-27L STD UC OHAYO UAE', CAST(N'2023-11-17T23:51:15.100' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2015', N'W-AMUP-58827LX-B6C3-QU-MFA0', N'W-MF 588-27L STD UC QUANTUM-A', CAST(N'2023-11-17T23:51:16.087' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2016', N'W-AMUP-58827LX-B6C3-QU-MFD0', N'W-MF 588-27L STD UC QUANTUM-D', CAST(N'2023-11-17T23:51:17.070' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2017', N'W-AMUP-60038LX-B8C3-AP-MFA0', N'W-MF 600-38L STD UC ASPIRA MFA', CAST(N'2023-11-17T23:51:18.053' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2018', N'W-AMUP-60038LX-B8C3-AS-HPB0', N'W-MF 600-38L STD UC ASAHI HP-B', CAST(N'2023-11-17T23:51:19.023' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2019', N'W-AMUP-60038LX-B8C3-AS-HPC0', N'W-MF 600-38L STD UC ASAHI HP-C', CAST(N'2023-11-17T23:51:19.920' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'202', N'W-CV02-B20LXXX-D02C-NL-00T0', N'CV B20L MF BLACK', CAST(N'2023-11-17T23:48:33.543' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2020', N'W-AMUP-60038LX-B8C3-BL-MFA0', N'W-MF 600-38L STD UC NON BRAND', CAST(N'2023-11-17T23:51:20.023' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2021', N'W-AMUP-60038LX-B8C3-IN-MFA0', N'W-MF 600-38L STD UC INCOE MF-A', CAST(N'2023-11-17T23:51:20.110' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2022', N'W-AMUP-60038LX-B8C3-IN-MFB0', N'W-MF 600-38L STD UC INCOE MF-B', CAST(N'2023-11-17T23:51:20.193' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2023', N'W-AMUP-60038LX-B8C3-IN-MFC0', N'W-MF 600-38L STD UC INCOE MF-C', CAST(N'2023-11-17T23:51:20.273' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2024', N'W-AMUP-60038LX-B8C3-IN-MFE0', N'W-MF 600-38L STD UC INCOE PHI', CAST(N'2023-11-17T23:51:20.357' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2025', N'W-AMUP-60038LX-B8C3-IN-MFF0', N'W-MF 600-38L STD UC INCOE AOP', CAST(N'2023-11-17T23:51:20.440' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2026', N'W-AMUP-60038LX-B8C3-IN-MFH0', N'W-MF 600-38L STD UC INCOE VIE', CAST(N'2023-11-17T23:51:20.520' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2027', N'W-AMUP-60038LX-B8C3-IN-MFJ0', N'W-MF 600-38L STD UC INCOE UAE', CAST(N'2023-11-17T23:51:20.603' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2028', N'W-AMUP-60038LX-B8C3-IN-MFL0', N'W-MF 600-38L STD UC INCOE HYD', CAST(N'2023-11-17T23:51:20.723' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2029', N'W-AMUP-60038LX-B8C3-LU-00A0', N'W-MF 600-38L STD UC LUCAS', CAST(N'2023-11-17T23:51:20.810' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'203', N'W-CV02-B20RSXX-A02N-NL-DG00', N'CV B20RS BLACK HD1-BLACK', CAST(N'2023-11-17T23:48:33.640' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2030', N'W-AMUP-60038LX-B8C3-OH-MFA0', N'W-MF 600-38L STD UC OHAYO MF-A', CAST(N'2023-11-17T23:51:20.893' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2031', N'W-AMUP-60038LX-B8C3-OH-MFB0', N'W-MF 600-38L STD UC OHAYO UAE', CAST(N'2023-11-17T23:51:20.973' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2032', N'W-AMUP-60038LX-B8C3-QU-MFA0', N'W-MF 600-38L STD UC QUANTUM-A', CAST(N'2023-11-17T23:51:21.057' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2033', N'W-AMUP-60038LX-B8C3-QU-MFB0', N'W-MF 600-38L STD UC QUANTUM-B', CAST(N'2023-11-17T23:51:21.140' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2034', N'W-AMUP-60038LX-B8C3-QU-MFF0', N'W-MF 600-38L STD UC QUANTUM-F', CAST(N'2023-11-17T23:51:21.220' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2035', N'W-AMUP-60038LX-C0C2-OH-MFA0', N'W-MF 600-38L C20 UC OHAYO MF-A', CAST(N'2023-11-17T23:51:21.300' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2036', N'W-AMUP-50B24RX-B1P3-LU-00A0', N'W-MF 50B24R STD UC LUCAS', CAST(N'2023-11-17T23:51:21.383' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2037', N'W-AMUP-65D31RX-B2P3-QU-MFC0', N'W-MF 65D31R STD UC QUANTUM-C', CAST(N'2023-11-17T23:51:21.470' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2038', N'W-AMUP-60D26LX-A8P3-AP-MFA0', N'W-MF 60D26L STD UC ASPIRA MF-A', CAST(N'2023-11-17T23:51:21.583' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2039', N'W-AMUP-75D23RX-B3P3-QU-MFF0', N'W-MF 75D23R STD UC QUANTUM-F', CAST(N'2023-11-17T23:51:21.663' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'204', N'W-CV02-B20RSXX-D02C-NL-00T0', N'CV B20RS MF BLACK', CAST(N'2023-11-17T23:48:33.743' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2040', N'W-AMUP-75D31LX-B2P4-BL-MFA0', N'W-MF 75D31L SUB-1 UC NON BRAND', CAST(N'2023-11-17T23:51:21.747' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2041', N'W-AMUP-75D31LX-B2P4-IN-MFB0', N'W-MF 75D31L SUB-1 UC INCOE MFB', CAST(N'2023-11-17T23:51:21.827' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2042', N'W-AMUP-75D31LX-B2P4-QU-MFA0', N'W-MF 75D31L SUB-1 UC QUANTUM-A', CAST(N'2023-11-17T23:51:21.910' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2043', N'W-AMUP-75D31LX-B3P3-AM-PRA0', N'W-MF 75D31L STD UC AMK PRE', CAST(N'2023-11-17T23:51:21.993' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2044', N'W-AMUP-65D31RX-B4P1-TN-HDA0', N'W-MF 65D31R HD UC TRAKNUS HD', CAST(N'2023-11-17T23:51:22.080' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2045', N'W-AMUP-67018LX-D5C3-BL-MFA0', N'W-MF 670-18L STD UC NON BRNAD', CAST(N'2023-11-17T23:51:22.160' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2046', N'W-AMUP-67018LX-D5C3-IN-MFE0', N'W-MF 670-18L STD UC INCOE PHI', CAST(N'2023-11-17T23:51:22.240' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2047', N'W-AMUP-67018LX-D5C3-IN-MFF0', N'W-MF 670-18L STD UC INCOE AOP', CAST(N'2023-11-17T23:51:22.323' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2048', N'W-AMUP-67018LX-D5C3-OH-MFA0', N'W-MF 670-18L STD UC OHAYO MF-A', CAST(N'2023-11-17T23:51:22.410' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2049', N'W-AMUP-70D26LX-B0P3-AP-MFA0', N'W-MF 70D26L STD UC ASPIRA MF-A', CAST(N'2023-11-17T23:51:22.490' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'205', N'W-CV02-B20RXXX-A02N-NL-DG00', N'CV B20R BLACK HD1-BLACK', CAST(N'2023-11-17T23:48:33.843' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2050', N'W-AMUP-70D26LX-B0P3-AS-HPB0', N'W-MF 70D26L STD UC ASAHI HP-B', CAST(N'2023-11-17T23:51:22.573' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2051', N'W-AMUP-70D26LX-B0P3-BL-MFA0', N'W-MF 70D26L STD UC NON BRAND', CAST(N'2023-11-17T23:51:22.653' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2052', N'W-AMUP-70D26LX-B0P3-IN-MFA0', N'W-MF 70D26L STD UC INCOE MF-A', CAST(N'2023-11-17T23:51:22.737' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2053', N'W-AMUP-70D26LX-B0P3-IN-MFB0', N'W-MF 70D26L STD UC INCOE MF-B', CAST(N'2023-11-17T23:51:22.817' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2054', N'W-AMUP-70D26LX-B0P3-IN-MFC0', N'W-MF 70D26L STD UC INCOE MF-C', CAST(N'2023-11-17T23:51:22.900' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2055', N'W-AMUP-70D26LX-B0P3-OH-MFA0', N'W-MF 70D26L STD UC OHAYO MF-A', CAST(N'2023-11-17T23:51:22.980' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2056', N'W-AMUP-70D26RX-B0P3-AP-MFA0', N'W-MF 70D26R STD UC ASPIRA MF-A', CAST(N'2023-11-17T23:51:23.063' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2057', N'W-AMUP-70D26RX-B0P3-AS-HPB0', N'W-MF 70D26R STD UC ASAHI HP-B', CAST(N'2023-11-17T23:51:23.147' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2058', N'W-AMUP-70D26RX-B0P3-BL-MFA0', N'W-MF 70D26R STD UC NON BRAND', CAST(N'2023-11-17T23:51:23.227' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2059', N'W-AMUP-70D26RX-B0P3-IN-MFA0', N'W-MF 70D26R STD UC INCOE MF-A', CAST(N'2023-11-17T23:51:23.307' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'206', N'W-CV02-B20RXXX-D02C-NL-00T0', N'CV B20R MF BLACK', CAST(N'2023-11-17T23:48:33.967' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2060', N'W-AMUP-70D26RX-B0P3-IN-MFB0', N'W-MF 70D26R STD UC INCOE MF-B', CAST(N'2023-11-17T23:51:23.387' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2061', N'W-AMUP-70D26RX-B0P3-IN-MFC0', N'W-MF 70D26R STD UC INCOE MF-C', CAST(N'2023-11-17T23:51:23.470' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2062', N'W-AMUP-70D26RX-B0P3-OH-MFA0', N'W-MF 70D26R STD UC OHAYO MF-A', CAST(N'2023-11-17T23:51:23.553' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2063', N'W-AMUP-72018LX-E3C3-AM-HDA0', N'W-MF 720-18L STD UC AMK HDT', CAST(N'2023-11-17T23:51:23.633' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2064', N'W-AMUP-72018LX-E3C3-IN-MFB0', N'W-MF 720-18L STD UC INCOE MF-B', CAST(N'2023-11-17T23:51:23.720' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2065', N'W-AMUP-72018LX-E3C3-OH-MFA0', N'W-MF 720-18L STD UC OHAYO MF-A', CAST(N'2023-11-17T23:51:23.803' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2066', N'W-AMUP-75D23LX-B3P3-QU-MFB0', N'W-MF 75D23L STD UC QUANTUM-B', CAST(N'2023-11-17T23:51:23.883' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2067', N'W-AMUP-75D23LX-B3P3-QU-MFE0', N'W-MF 75D23L STD UC QUANTUM-E', CAST(N'2023-11-17T23:51:23.967' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2068', N'W-AMUP-75D23LX-B3P3-QU-MFF0', N'W-MF 75D23L STD UC QUANTUM-F', CAST(N'2023-11-17T23:51:24.050' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2069', N'W-AMUP-75D31LX-B3P3-AS-MEA0', N'W-MF 75D31L STD UC ASAHI MEN', CAST(N'2023-11-17T23:51:24.130' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'207', N'W-CV02-B24LSXX-A02N-NL-DG00', N'CV B24LS BLACK HD1-BLACK', CAST(N'2023-11-17T23:48:34.077' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2070', N'W-AMUP-75D31LX-B3P3-IN-MFF0', N'W-MF 75D31L STD UC INCOE AOP', CAST(N'2023-11-17T23:51:24.210' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2071', N'W-AMUP-75D31LX-B3P3-QU-MFA0', N'W-MF 75D31L STD UC QUANTUM-A', CAST(N'2023-11-17T23:51:24.293' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2072', N'W-AMUP-75D31LX-B3P3-QU-MFB0', N'W-MF 75D31L STD UC QUANTUM-B', CAST(N'2023-11-17T23:51:24.373' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2073', N'W-AMUP-75D31LX-B3P3-QU-MFD0', N'W-MF 75D31L STD UC QUANTUM-D', CAST(N'2023-11-17T23:51:24.453' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2074', N'W-AMUP-75D31LX-B3P3-QU-MFF0', N'W-MF 75D31L STD UC QUANTUM-F', CAST(N'2023-11-17T23:51:24.537' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2075', N'W-AMUP-75D31LX-B3P3-TN-00A0', N'W-MF 75D31L STD UC TRAKNUS', CAST(N'2023-11-17T23:51:24.617' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2076', N'W-AMUP-75D31RX-B2P4-BL-MFA0', N'W-MF 75D31R SUB-1 UC NON BRAND', CAST(N'2023-11-17T23:51:24.700' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2077', N'W-AMUP-75D31RX-B2P4-IN-MFB0', N'W-MF 75D31R SUB-1 UC INCOE MFB', CAST(N'2023-11-17T23:51:24.780' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2078', N'W-AMUP-75D31RX-B2P4-QU-MFA0', N'W-MF 75D31R SUB-1 UC QUANTUM-A', CAST(N'2023-11-17T23:51:24.860' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2079', N'W-AMUP-75D31RX-B3P3-AM-PRA0', N'W-MF 75D31R STD UC AMK PRE', CAST(N'2023-11-17T23:51:24.940' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'208', N'W-CV02-B24LSXX-D02C-NL-00T0', N'CV B24LS MF BLACK', CAST(N'2023-11-17T23:48:34.180' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2080', N'W-AMUP-75D31RX-B3P3-IN-MFF0', N'W-MF 75D31R STD UC INCOE AOP', CAST(N'2023-11-17T23:51:25.023' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2081', N'W-AMUP-75D31RX-B3P3-QU-MFA0', N'W-MF 75D31R STD UC QUANTUM-A', CAST(N'2023-11-17T23:51:25.107' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2082', N'W-AMUP-75D31RX-B3P3-QU-MFB0', N'W-MF 75D31R STD UC QUANTUM-B', CAST(N'2023-11-17T23:51:25.200' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2083', N'W-AMUP-75D31RX-B3P3-QU-MFD0', N'W-MF 75D31R STD UC QUANTUM-D', CAST(N'2023-11-17T23:51:25.297' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2084', N'W-AMUP-75D31RX-B3P3-QU-MFF0', N'W-MF 75D31R STD UC QUANTUM-F', CAST(N'2023-11-17T23:51:25.393' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2085', N'W-AMUP-80D23LX-B3P3-AS-HPB0', N'W-MF 80D23L STD UC ASAHI HP-B', CAST(N'2023-11-17T23:51:25.487' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2086', N'W-AMUP-80D23LX-B3P3-AS-HPC0', N'W-MF 80D23L STD UC ASAHI HP-C', CAST(N'2023-11-17T23:51:25.587' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2087', N'W-AMUP-80D23LX-B3P3-BL-MFA0', N'W-MF 80D23L STD UC NON BRAND', CAST(N'2023-11-17T23:51:25.683' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2088', N'W-AMUP-80D23LX-B3P3-BL-MFB0', N'W-MF 80D23L STD UC N-BRAND - B', CAST(N'2023-11-17T23:51:25.777' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2089', N'W-AMUP-80D23LX-B3P3-IN-MFA0', N'W-MF 80D23L STD UC INCOE MF-A', CAST(N'2023-11-17T23:51:25.857' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'209', N'W-CV02-B24LXXX-A02N-NL-DG00', N'CV B24L BLACK HD1-BLACK', CAST(N'2023-11-17T23:48:34.280' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2090', N'W-AMUP-95D31LX-B4P3-QU-MFA0', N'W-MF 95D31L STD UC QUANTUM-A', CAST(N'2023-11-17T23:51:25.937' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2091', N'W-AMUP-45C24LX-B0P3-IN-MFB0', N'W-MF 45C24L STD UC INCOE MF-B', CAST(N'2023-11-17T23:51:26.017' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2092', N'W-AMUP-45C24LX-B0P3-IN-MFC0', N'W-MF 45C24L STD UC INCOE MF-C', CAST(N'2023-11-17T23:51:26.100' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2093', N'W-AMUP-46B24RS-B1P3-AS-MEA0', N'W-MF 46B24RS STD UC ASAHI MEN', CAST(N'2023-11-17T23:51:26.180' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2094', N'W-AMUP-46B24RS-B1P3-IN-MFF0', N'W-MF 46B24RS STD UC INCOE AOP', CAST(N'2023-11-17T23:51:26.263' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2095', N'W-AMUP-46B24RS-B1P3-QU-MFA0', N'W-MF 46B24RS STD UC QUANTUM-A', CAST(N'2023-11-17T23:51:26.343' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2096', N'W-AMUP-46B24RS-B1P3-QU-MFB0', N'W-MF 46B24RS STD UC QUANTUM-B', CAST(N'2023-11-17T23:51:26.427' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2097', N'W-AMUP-46B24RS-B1P3-QU-MFE0', N'W-MF 46B24RS STD UC QUANTUM-E', CAST(N'2023-11-17T23:51:26.510' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2098', N'W-AMUP-46B24LS-B1P3-QU-MFB0', N'W-MF 46B24LS STD UC QUANTUM-B', CAST(N'2023-11-17T23:51:26.593' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2099', N'W-AMUP-46B24LS-B1P3-QU-MFC0', N'W-MF 46B24LS STD UC QUANTUM-C', CAST(N'2023-11-17T23:51:26.677' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'21', N'K-CV09-S45XXXX-0MHN-NL-00M0', N'CV S45 GREY (ABS)', CAST(N'2023-11-17T23:48:18.080' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'210', N'W-CV02-B24LXXX-D02C-NL-00T0', N'CV B24L MF BLACK', CAST(N'2023-11-17T23:48:34.370' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2100', N'W-AMUP-46B24LS-B1P3-QU-MFD0', N'W-MF 46B24LS STD UC QUANTUM-D', CAST(N'2023-11-17T23:51:26.763' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2101', N'W-AMUP-46B24LS-B1P3-QU-MFE0', N'W-MF 46B24LS STD UC QUANTUM-E', CAST(N'2023-11-17T23:51:26.843' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2102', N'W-AMUP-46B24LX-B0P4-IN-MFB0', N'W-MF 46B24L SUB-1 UC INCOE MFB', CAST(N'2023-11-17T23:51:26.927' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2103', N'W-AMUP-46B24LX-B0P4-QU-MFA0', N'W-MF 46B24L SUB-1 UC QUANTUM-A', CAST(N'2023-11-17T23:51:27.007' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2104', N'W-AMUP-46B24LX-B1P3-AS-MEA0', N'W-MF 46B24L STD UC ASAHI MEN', CAST(N'2023-11-17T23:51:27.087' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2105', N'W-AMUP-46B24LX-B1P3-IN-MFE0', N'W-MF 46B24L STD UC INCOE PHI', CAST(N'2023-11-17T23:51:27.167' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2106', N'W-AMUP-46B24LX-B1P3-IN-MFF0', N'W-MF 46B24L STD UC INCOE AOP', CAST(N'2023-11-17T23:51:27.250' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2107', N'W-AMUP-46B24LX-B1P3-IN-MFK0', N'W-MF 46B24L STD UC INCOE DSP', CAST(N'2023-11-17T23:51:27.330' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2108', N'W-AMUP-46B24LX-B1P3-IN-MFL0', N'W-MF 46B24L STD UC INCOE HYD', CAST(N'2023-11-17T23:51:27.413' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2109', N'W-AMUP-46B24LX-B1P3-QU-MFA0', N'W-MF 46B24L STD UC QUANTUM-A', CAST(N'2023-11-17T23:51:27.493' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'211', N'W-CV02-B24RSXX-A02N-NL-DG00', N'CV B24RS BLACK HD1-BLACK', CAST(N'2023-11-17T23:48:34.453' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2110', N'W-AMUP-46B24LX-B1P3-QU-MFB0', N'W-MF 46B24L STD UC QUANTUM-B', CAST(N'2023-11-17T23:51:27.577' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2111', N'W-AMUP-46B24LX-B1P3-QU-MFC0', N'W-MF 46B24L STD UC QUANTUM-C', CAST(N'2023-11-17T23:51:27.657' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2112', N'W-AMUP-46B24LX-B1P3-QU-MFE0', N'W-MF 46B24L STD UC QUANTUM-E', CAST(N'2023-11-17T23:51:27.740' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2113', N'W-AMUP-46B24LX-B1P3-QU-MFF0', N'W-MF 46B24L STD UC QUANTUM-F', CAST(N'2023-11-17T23:51:27.820' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2114', N'W-AMUP-46B24RS-A9P5-OH-MFA0', N'W-MF 46B24RS SUB-2 UC OHA MF-A', CAST(N'2023-11-17T23:51:27.903' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2115', N'W-AMUP-46B24RS-A9P5-QU-MFA0', N'W-MF 46B24RS SUB-2 UC QUANTU-A', CAST(N'2023-11-17T23:51:27.987' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2116', N'W-AMUP-46B24RS-B0P4-AS-HPB0', N'W-MF 46B24RS SUB-1 UC ASA HP-B', CAST(N'2023-11-17T23:51:28.067' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2117', N'W-AMUP-46B24RX-B0P4-AS-HPB0', N'W-MF 46B24R SUB-1 UC ASAHI HPB', CAST(N'2023-11-17T23:51:28.150' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2118', N'W-AMUP-46B24RX-B0P4-IN-MFB0', N'W-MF 46B24R SUB-1 UC INCOE MFB', CAST(N'2023-11-17T23:51:28.230' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2119', N'W-AMUP-46B24RX-B0P4-QU-MFA0', N'W-MF 46B24R SUB-1 UC QUANTUM-A', CAST(N'2023-11-17T23:51:28.313' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'212', N'W-CV02-B24RSXX-D02C-NL-00T0', N'CV B24RS MF BLACK', CAST(N'2023-11-17T23:48:34.537' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2120', N'W-AMUP-46B24RX-B1P3-AS-MEA0', N'W-MF 46B24R STD UC ASAHI MEN', CAST(N'2023-11-17T23:51:28.393' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2121', N'W-AMUP-46B24RX-B1P3-IN-MFE0', N'W-MF 46B24R STD UC INCOE PHI', CAST(N'2023-11-17T23:51:28.473' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2122', N'W-AMUP-46B24RX-B1P3-IN-MFF0', N'W-MF 46B24R STD UC INCOE AOP', CAST(N'2023-11-17T23:51:28.557' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2123', N'W-AMUP-46B24RX-B1P3-QU-MFA0', N'W-MF 46B24R STD UC QUANTUM-A', CAST(N'2023-11-17T23:51:28.637' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2124', N'W-AMUP-46B24RX-B1P3-QU-MFB0', N'W-MF 46B24R STD UC QUANTUM-B', CAST(N'2023-11-17T23:51:28.720' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2125', N'W-AMUP-46B24RX-B1P3-QU-MFD0', N'W-MF 46B24R STD UC QUANTUM-D', CAST(N'2023-11-17T23:51:28.800' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2126', N'W-AMUP-46B24RX-B1P3-QU-MFE0', N'W-MF 46B24R STD UC QUANTUM-E', CAST(N'2023-11-17T23:51:28.880' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2127', N'W-AMUP-46B24RX-B1P3-QU-MFF0', N'W-MF 46B24R STD UC QUANTUM-F', CAST(N'2023-11-17T23:51:28.960' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2128', N'W-AMUP-48D26LX-A8P3-AS-MEA0', N'W-MF 48D26L STD UC ASAHI MEN', CAST(N'2023-11-17T23:51:29.040' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2129', N'W-AMUP-48D26LX-A8P3-IN-MFF0', N'W-MF 48D26L STD UC INCOE AOP', CAST(N'2023-11-17T23:51:29.123' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'213', N'W-CV02-B24RXXX-A02N-NL-DG00', N'CV B24R BLACK HD1-BLACK', CAST(N'2023-11-17T23:48:34.623' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2130', N'W-AMUP-48D26LX-A8P3-QU-MFA0', N'W-MF 48D26L STD UC QUANTUM-A', CAST(N'2023-11-17T23:51:29.203' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2131', N'W-AMUP-48D26LX-A8P3-QU-MFB0', N'W-MF 48D26L STD UC QUANTUM-B', CAST(N'2023-11-17T23:51:29.287' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2132', N'W-AMUP-48D26LX-A8P3-QU-MFD0', N'W-MF 48D26L STD UC QUANTUM-D', CAST(N'2023-11-17T23:51:29.377' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2133', N'W-AMUP-48D26RX-A8P3-AM-PRA0', N'W-MF 48D26R STD UC AMK PRE', CAST(N'2023-11-17T23:51:29.453' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2134', N'W-AMUP-48D26RX-A8P3-AS-MEA0', N'W-MF 48D26R STD UC ASAHI MEN', CAST(N'2023-11-17T23:51:29.537' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2135', N'W-AMUP-55559LX-B0C3-AS-MEA0', N'W-MF 555-59L STD UC ASAHI MEN', CAST(N'2023-11-17T23:51:29.620' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2136', N'W-AMUP-55559LX-B0C3-BL-MFA0', N'W-MF 555-59L STD UC NON BRAND', CAST(N'2023-11-17T23:51:29.700' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2137', N'W-AMUP-55559LX-B0C3-IN-MFA0', N'W-MF 555-59L STD UC INCOE MF-A', CAST(N'2023-11-17T23:51:29.783' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2138', N'W-AMUP-55559LX-B0C3-IN-MFB0', N'W-MF 555-59L STD UC INCOE MF-B', CAST(N'2023-11-17T23:51:29.863' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2139', N'W-AMUP-55559LX-B0C3-IN-MFC0', N'W-MF 555-59L STD UC INCOE MF-C', CAST(N'2023-11-17T23:51:29.950' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'214', N'W-CV02-B24RXXX-D02C-NL-00T0', N'CV B24R MF BLACK', CAST(N'2023-11-17T23:48:34.707' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2140', N'W-AMUP-55559LX-B0C3-IN-MFD0', N'W-MF 555-59L STD UC INCOE SUD', CAST(N'2023-11-17T23:51:30.030' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2141', N'W-AMUP-55559LX-B0C3-IN-MFF0', N'W-MF 555-59L STD UC INCOE AOP', CAST(N'2023-11-17T23:51:30.110' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2142', N'W-AMUP-55559LX-B0C3-IN-MFH0', N'W-MF 555-59L STD UC INCOE VIE', CAST(N'2023-11-17T23:51:30.193' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2143', N'W-AMUP-N70ZZXX-B6P3-IN-MFL0', N'W-MF N70ZZ STD UC INCOE HYD', CAST(N'2023-11-17T23:51:30.277' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2144', N'W-AMUP-75D26RX-B3P3-AS-HPB0', N'W-MF 75D26R STD UC ASAHI HP-B', CAST(N'2023-11-17T23:51:30.353' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2145', N'W-AMUP-75D26RX-B3P3-AP-MFA0', N'W-MF 75D26R STD UC ASPIRA MFA', CAST(N'2023-11-17T23:51:30.433' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2146', N'W-AMUP-65D26RX-B3P3-IN-MFK0', N'W-MF 65D26R STD UC INCOE DSP', CAST(N'2023-11-17T23:51:30.513' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2147', N'W-AMUP-75D26RX-B3P3-IN-MFA0', N'W-MF 75D26R STD UC INCOE MF-A', CAST(N'2023-11-17T23:51:30.597' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2148', N'W-AMUP-75D26RX-B3P3-IN-MFH0', N'W-MF 75D26R STD UC INCOE VIE', CAST(N'2023-11-17T23:51:30.680' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2149', N'W-AVUP-65D26RX-A9A3-UL-MFA0', N'W-MF 65D26R STD-V UC ULTRA MF', CAST(N'2023-11-17T23:51:30.757' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'215', N'W-CV02-H52XXXX-G02S-NL-00T0', N'CV H52 MF BLACK', CAST(N'2023-11-17T23:48:34.787' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2150', N'W-AMUP-55559LX-B0C3-IN-MFJ0', N'W-MF 555-59L STD UC INCOE UAE', CAST(N'2023-11-17T23:51:30.840' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2151', N'W-AMUP-55559LX-B0C3-IN-MFK0', N'W-MF 555-59L STD UC INCOE DSP', CAST(N'2023-11-17T23:51:30.920' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2152', N'W-AMUP-55559LX-B0C3-OH-MFA0', N'W-MF 555-59L STD UC OHAYO MF-A', CAST(N'2023-11-17T23:51:31.000' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2153', N'W-AMUP-55559LX-B0C3-OH-MFB0', N'W-MF 555-59L STD UC OHAYO UAE', CAST(N'2023-11-17T23:51:31.083' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2154', N'W-AMUP-55559LX-B0C3-QU-MFA0', N'W-MF 555-59L STD UC QUANTUM-A', CAST(N'2023-11-17T23:51:31.163' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2155', N'W-AMUP-55559LX-B0C3-QU-MFB0', N'W-MF 555-59L STD UC QUANTUM-B', CAST(N'2023-11-17T23:51:31.247' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2156', N'W-AMUP-55559LX-B0C3-QU-MFC0', N'W-MF 555-59L STD UC QUANTUM-C', CAST(N'2023-11-17T23:51:31.327' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2157', N'W-AMUP-55559LX-B0C3-QU-MFF0', N'W-MF 555-59L STD UC QUANTUM-F', CAST(N'2023-11-17T23:51:31.410' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2158', N'W-AMUP-55559LX-B2C2-IN-MFB0', N'W-MF 555-59L C20 UC INCOE MF-B', CAST(N'2023-11-17T23:51:31.490' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2159', N'W-AMUP-55559LX-B2C2-IN-MFG0', N'W-MF 555-59L C20 UC INCOE KSA', CAST(N'2023-11-17T23:51:31.573' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'216', N'W-CV02-N03XXXX-0MHN-NL-0000', N'CV N03 BLACK', CAST(N'2023-11-17T23:48:34.873' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2160', N'W-AMUP-55559LX-B2C2-OH-MFA0', N'W-MF 555-59L C20 UC OHAYO MF-A', CAST(N'2023-11-17T23:51:31.653' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2161', N'W-AMUP-55559LX-B2C2-QU-MFE0', N'W-MF 555-59L C20 UC QUANTUM-E', CAST(N'2023-11-17T23:51:31.740' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2162', N'W-AMUP-55565RX-B0C3-AP-MFA0', N'W-MF 555-65R STD UC ASPIRA MFA', CAST(N'2023-11-17T23:51:31.823' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2163', N'W-AMUP-55565RX-B0C3-AS-HPB0', N'W-MF 555-65R STD UC ASAHI HP-B', CAST(N'2023-11-17T23:51:31.903' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2164', N'W-AMUP-55565RX-B0C3-AS-HPC0', N'W-MF 555-65R STD UC ASAHI HP-C', CAST(N'2023-11-17T23:51:31.983' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2165', N'W-AMUP-55565RX-B0C3-BL-MFA0', N'W-MF 555-65R STD UC NON BRAND', CAST(N'2023-11-17T23:51:32.063' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2166', N'W-AMUP-55565RX-B0C3-IN-MFA0', N'W-MF 555-65R STD UC INCOE MF-A', CAST(N'2023-11-17T23:51:32.143' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2167', N'W-AVUP-36B20LX-A8A3-UL-MFA0', N'W-MF 36B20L STD-V UC ULTRA MF', CAST(N'2023-11-17T23:51:32.227' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2168', N'W-AVUP-46B24LS-A9A3-UL-MFA0', N'W-MF 46B24LS STD-V UC ULTRA MF', CAST(N'2023-11-17T23:51:32.307' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2169', N'W-AVUP-46B24RS-A9A3-UL-MFA0', N'W-MF 46B24RS STD-V UC ULTRA MF', CAST(N'2023-11-17T23:51:32.383' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'217', N'W-CV02-N12DXXX-0MHN-NL-0000', N'CV N12D BLACK', CAST(N'2023-11-17T23:48:34.957' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2170', N'W-AVUP-65D26RX-A9A3-QU-MFA0', N'W-MF 65D26R STD-V UC QUANTUM-A', CAST(N'2023-11-17T23:51:32.467' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2171', N'W-AVUP-65D26LX-A9A3-QU-MFA0', N'W-MF 65D26L STD-V UC QUANTUM-A', CAST(N'2023-11-17T23:51:32.550' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2172', N'W-AVUP-65D26LX-A9A3-UL-MFA0', N'W-MF 65D26L STD-V UC ULTRA MF', CAST(N'2023-11-17T23:51:32.630' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2173', N'W-AMUP-80D26RX-B3P3-QU-MFA0', N'W-MF 80D26R STD UC QUANTUM-A', CAST(N'2023-11-17T23:51:32.713' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2174', N'W-AMUP-80D26RX-B3P3-QU-MFB0', N'W-MF 80D26R STD UC QUANTUM-B', CAST(N'2023-11-17T23:51:32.793' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2175', N'W-AMUP-80D26RX-B3P3-QU-MFE0', N'W-MF 80D26R STD UC QUANTUM-E', CAST(N'2023-11-17T23:51:32.877' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2176', N'W-AMUP-80D26RX-B3P3-QU-MFF0', N'W-MF 80D26R STD UC QUANTUM-F', CAST(N'2023-11-17T23:51:32.957' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2177', N'W-AMUP-80D31LX-B2P3-AP-MFA0', N'W-MF 80D31L STD UC ASPIRA MF-A', CAST(N'2023-11-17T23:51:33.040' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2178', N'W-AMUP-80D31LX-B2P3-AS-HPB0', N'W-MF 80D31L STD UC ASAHI HP-B', CAST(N'2023-11-17T23:51:33.123' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2179', N'W-AMUP-80D31LX-B2P3-BL-MFA0', N'W-MF 80D31L STD UC NON BRAND', CAST(N'2023-11-17T23:51:33.203' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'218', N'W-CV02-TZ5SXXX-0MHN-NL-0000', N'CV TZ-5S BLACK KIT', CAST(N'2023-11-17T23:48:35.037' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2180', N'W-AMUP-80D31LX-B2P3-IN-MFA0', N'W-MF 80D31L STD UC INCOE MF-A', CAST(N'2023-11-17T23:51:33.283' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2181', N'W-AMUP-80D31LX-B2P3-IN-MFB0', N'W-MF 80D31L STD UC INCOE MF-B', CAST(N'2023-11-17T23:51:33.367' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2182', N'W-AMUP-80D31LX-B2P3-IN-MFC0', N'W-MF 80D31L STD UC INCOE MF-C', CAST(N'2023-11-17T23:51:33.450' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2183', N'W-AMUP-95D31LX-B4P3-QU-MFB0', N'W-MF 95D31L STD UC QUANTUM-B', CAST(N'2023-11-17T23:51:33.527' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2184', N'W-AMUP-95D31LX-B4P3-QU-MFC0', N'W-MF 95D31L STD UC QUANTUM-C', CAST(N'2023-11-17T23:51:33.610' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2185', N'W-AMUP-95D31LX-B4P3-QU-MFD0', N'W-MF 95D31L STD UC QUANTUM-D', CAST(N'2023-11-17T23:51:33.690' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2186', N'W-AMUP-95D31RX-B2P5-OH-MFA0', N'W-MF 95D31R SUB-2 UC OHAYO MFA', CAST(N'2023-11-17T23:51:33.773' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2187', N'W-AMUP-95D31RX-B2P5-QU-MFA0', N'W-MF 95D31R SUB-2 UC QUANTUM-A', CAST(N'2023-11-17T23:51:33.853' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2188', N'W-AMUP-95D31RX-B3P3-AP-MFA0', N'W-MF 95D31R STD UC ASPIRA MF-A', CAST(N'2023-11-17T23:51:33.937' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2189', N'W-AMUP-95D31RX-B3P3-AS-HPB0', N'W-MF 95D31R STD UC ASAHI HP-B', CAST(N'2023-11-17T23:51:34.020' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'219', N'W-CV04-B20LXXX-B04N-NL-DG00', N'CV B20L YELLOW HD2-YELLOW', CAST(N'2023-11-17T23:48:35.130' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2190', N'W-AMUP-95D31RX-B3P3-AS-HPC0', N'W-MF 95D31R STD UC ASAHI HP-C', CAST(N'2023-11-17T23:51:34.100' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2191', N'W-AMUP-95D31RX-B3P3-AS-HPD0', N'W-MF 95D31R STD UC ASAHI HP-D', CAST(N'2023-11-17T23:51:34.183' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2192', N'W-AMUP-95D31RX-B3P3-BL-MFA0', N'W-MF 95D31R STD UC NON BRAND', CAST(N'2023-11-17T23:51:34.263' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2193', N'W-AMUP-95D31RX-B3P3-IN-MFA0', N'W-MF 95D31R STD UC INCOE MF-A', CAST(N'2023-11-17T23:51:34.353' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2194', N'W-AMUP-95D31RX-B3P3-IN-MFB0', N'W-MF 95D31R STD UC INCOE MF-B', CAST(N'2023-11-17T23:51:34.437' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2195', N'W-AMUP-95D31RX-B3P3-IN-MFC0', N'W-MF 95D31R STD UC INCOE MF-C', CAST(N'2023-11-17T23:51:34.517' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2196', N'W-AMUP-95D31RX-B3P3-IN-MFH0', N'W-MF 95D31R STD UC INCOE VIE', CAST(N'2023-11-17T23:51:34.597' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2197', N'W-AMUP-95D31RX-B3P3-IN-MFJ0', N'W-MF 95D31R STD UC INCOE UAE', CAST(N'2023-11-17T23:51:34.680' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2198', N'W-AMUP-95D31RX-B3P4-IN-MFB0', N'W-MF 95D31R SUB-1 UC INCOE MFB', CAST(N'2023-11-17T23:51:34.763' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2199', N'W-AMUP-95D31RX-B3P4-QU-MFA0', N'W-MF 95D31R SUB-1 UC QUANTUM-A', CAST(N'2023-11-17T23:51:34.840' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'22', N'K-CV09-S65XXXX-0MHN-NL-00M0', N'CV S65 GREY (ABS)', CAST(N'2023-11-17T23:48:18.163' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'220', N'W-CV04-B20RXXX-B04N-NL-DG00', N'CV B20R YELLOW HD2-YELLOW', CAST(N'2023-11-17T23:48:35.213' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2200', N'W-AMUP-95D31RX-B3P4-QU-MFB0', N'W-MF 95D31R SUB-1 UC QUANTUM-B', CAST(N'2023-11-17T23:51:34.920' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2201', N'W-AMUP-95D31RX-B3P4-QU-MFE0', N'W-MF 95D31R SUB-1 UC QUANTUM-E', CAST(N'2023-11-17T23:51:35.000' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2202', N'W-AMUP-95D31RX-B4P3-AP-MFB0', N'W-MF 95D31R STD UC ASPIRA MFB', CAST(N'2023-11-17T23:51:35.083' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2203', N'W-AMUP-95D31RX-B4P3-AS-MEA0', N'W-MF 95D31R STD UC ASAHI MEN', CAST(N'2023-11-17T23:51:35.163' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2204', N'W-AMUP-95D31RX-B4P3-IN-MFF0', N'W-MF 95D31R STD UC INCOE AOP', CAST(N'2023-11-17T23:51:35.247' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2205', N'W-AMUP-95D31RX-B4P3-IN-MFI0', N'W-MF 95D31R STD UC INCOE GREE', CAST(N'2023-11-17T23:51:35.330' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2206', N'W-AMUP-95D31RX-B4P3-QU-MFA0', N'W-MF 95D31R STD UC QUANTUM-A', CAST(N'2023-11-17T23:51:35.407' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2207', N'W-AMUP-95D31RX-B4P3-QU-MFB0', N'W-MF 95D31R STD UC QUANTUM-B', CAST(N'2023-11-17T23:51:35.487' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2208', N'W-AMUP-80D31LX-B2P3-IN-MFD0', N'W-MF 80D31L STD UC INCOE SUD', CAST(N'2023-11-17T23:51:35.567' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2209', N'W-AMUP-80D31LX-B2P3-IN-MFJ0', N'W-MF 80D31L STD UC INCOE UAE', CAST(N'2023-11-17T23:51:35.647' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'221', N'W-CV04-B24LSXX-B04N-NL-DG00', N'CV B24LS YELLOW HD2-YELLOW', CAST(N'2023-11-17T23:48:35.293' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2210', N'W-AMUP-80D31LX-B2P3-OH-MFA0', N'W-MF 80D31L STD UC OHAYO MF-A', CAST(N'2023-11-17T23:51:35.730' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2211', N'W-AMUP-80D31RX-B2P3-AP-MFA0', N'W-MF 80D31R STD UC ASPIRA MF-A', CAST(N'2023-11-17T23:51:35.860' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2212', N'W-AMUP-80D31RX-B2P3-AS-HPB0', N'W-MF 80D31R STD UC ASAHI HP-B', CAST(N'2023-11-17T23:51:35.967' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2213', N'W-AMUP-80D31RX-B2P3-BL-MFA0', N'W-MF 80D31R STD UC NON BRAND', CAST(N'2023-11-17T23:51:36.057' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2214', N'W-AMUP-80D31RX-B2P3-IN-MFA0', N'W-MF 80D31R STD UC INCOE MF-A', CAST(N'2023-11-17T23:51:36.150' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2215', N'W-AMUP-80D31RX-B2P3-IN-MFB0', N'W-MF 80D31R STD UC INCOE MF-B', CAST(N'2023-11-17T23:51:36.250' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2216', N'W-AMUP-80D31RX-B2P3-IN-MFC0', N'W-MF 80D31R STD UC INCOE MF-C', CAST(N'2023-11-17T23:51:36.343' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2217', N'W-AMUP-80D31RX-B2P3-IN-MFD0', N'W-MF 80D31R STD UC INCOE SUD', CAST(N'2023-11-17T23:51:36.443' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2218', N'W-AMUP-80D31RX-B2P3-IN-MFH0', N'W-MF 80D31R STD UC INCOE VIE', CAST(N'2023-11-17T23:51:36.523' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2219', N'W-AMUP-80D31RX-B2P3-IN-MFJ0', N'W-MF 80D31R STD UC INCOE UAE', CAST(N'2023-11-17T23:51:36.607' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'222', N'W-CV04-B24LXXX-B04N-NL-DG00', N'CV B24L YELLOW HD2-YELLOW', CAST(N'2023-11-17T23:48:35.377' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2220', N'W-AMUP-80D31RX-B2P3-OH-MFA0', N'W-MF 80D31R STD UC OHAYO MF-A', CAST(N'2023-11-17T23:51:36.687' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2221', N'W-AMUP-80D31RX-B2P3-OH-MFB0', N'W-MF 80D31R STD UC OHAYO UAE', CAST(N'2023-11-17T23:51:36.767' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2222', N'W-AMUP-90D26LX-B3P3-AP-MFA0', N'W-MF 90D26L STD UC ASPIRA MF-A', CAST(N'2023-11-17T23:51:36.850' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2223', N'W-AMUP-90D26LX-B3P3-AS-HPB0', N'W-MF 90D26L STD UC ASAHI HP-B', CAST(N'2023-11-17T23:51:36.933' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2224', N'W-AMUP-90D26LX-B3P3-AS-HPC0', N'W-MF 90D26L STD UC ASAHI HP-C', CAST(N'2023-11-17T23:51:37.013' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2225', N'W-AMUP-75D26RX-B3P3-IN-MFB0', N'W-MF 75D26R STD UC INCOE MF-B', CAST(N'2023-11-17T23:51:37.097' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2226', N'W-AMUP-75D26RX-B3P3-IN-MFC0', N'W-MF 75D26R STD UC INCOE MF-C', CAST(N'2023-11-17T23:51:37.180' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2227', N'W-AMUP-65D26RX-B3P3-IN-MFF0', N'W-MF 65D26R STD UC INCOE AOP', CAST(N'2023-11-17T23:51:37.260' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2228', N'W-AMUP-75D26RX-B3P3-OH-MFA0', N'W-MF 75D26R STD UC OHAYO MF-A', CAST(N'2023-11-17T23:51:37.340' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2229', N'W-AMUP-65D26RX-B3P3-QU-MFA0', N'W-MF 65D26R STD UC QUANTUM-A', CAST(N'2023-11-17T23:51:37.420' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'223', N'W-CV04-B24RXXX-B04N-NL-DG00', N'CV B24R YELLOW HD2-YELLOW', CAST(N'2023-11-17T23:48:35.460' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2230', N'W-AMUP-65D26RX-B3P3-QU-MFB0', N'W-MF 65D26R STD UC QUANTUM-B', CAST(N'2023-11-17T23:51:37.500' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2231', N'W-AMUP-65D26RX-B3P3-QU-MFE0', N'W-MF 65D26R STD UC QUANTUM-E', CAST(N'2023-11-17T23:51:37.583' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2232', N'W-AMUP-75D26LX-B3P3-AS-HPB0', N'W-MF 75D26L STD UC ASAHI HP-B', CAST(N'2023-11-17T23:51:37.667' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2233', N'W-AMUP-65D26LX-B3P3-AS-MEA0', N'W-MF 65D26L STD UC ASAHI MEN', CAST(N'2023-11-17T23:51:37.750' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2234', N'W-AMUP-75D26LX-B3P3-AP-MFA0', N'W-MF 75D26L STD UC ASPIRA MFA', CAST(N'2023-11-17T23:51:37.830' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2235', N'W-AMUP-65D26LX-B3P3-IN-MFL0', N'W-MF 65D26L STD UC INCOE HYD', CAST(N'2023-11-17T23:51:37.907' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2236', N'W-AMUP-75D26LX-B3P3-IN-MFA0', N'W-MF 75D26L STD UC INCOE MF-A', CAST(N'2023-11-17T23:51:37.990' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2237', N'W-AMUP-75D26LX-B3P3-IN-MFH0', N'W-MF 75D26L STD UC INCOE MF-H', CAST(N'2023-11-17T23:51:38.070' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2238', N'W-AMUP-75D26LX-B3P3-IN-MFB0', N'W-MF 75D26L STD UC INCOE MF-B', CAST(N'2023-11-17T23:51:38.150' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2239', N'W-AMUP-75D26LX-B3P3-IN-MFC0', N'W-MF 75D26L STD UC INCOE MF-C', CAST(N'2023-11-17T23:51:38.233' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'224', N'W-CV04-D23LXXX-D04N-NL-DG00', N'CV 55D23L YELLOW HD-YELLOW', CAST(N'2023-11-17T23:48:35.550' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2240', N'W-AMUP-65D26LX-B3P3-IN-MFF0', N'W-MF 65D26L STD UC INCOE AOP', CAST(N'2023-11-17T23:51:38.313' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2241', N'W-AMUP-75D26LX-B3P3-OH-MFA0', N'W-MF 75D26L STD UC OHAYO MF-A', CAST(N'2023-11-17T23:51:38.390' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2242', N'W-AMUP-65D26LX-B3P3-QU-MFA0', N'W-MF 65D26L STD UC  QUANTUM-A', CAST(N'2023-11-17T23:51:38.477' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2243', N'W-AMUP-65D26LX-B3P3-QU-MFB0', N'W-MF 65D26L STD UC  QUANTUM-B', CAST(N'2023-11-17T23:51:38.557' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2244', N'W-AMUP-65D26LX-B3P3-QU-MFE0', N'W-MF 65D26L STD UC  QUANTUM-E', CAST(N'2023-11-17T23:51:38.637' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2245', N'W-AMUP-60D26LX-A8P3-AP-MFD0', N'W-MF 60D26L STD UC ASPIRA MF-D', CAST(N'2023-11-17T23:51:38.717' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2246', N'W-AMUP-60D26LX-A8P3-AS-HPB0', N'W-MF 60D26L STD UC ASAHI HP-B', CAST(N'2023-11-17T23:51:38.800' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2247', N'W-AMUP-60D26LX-A8P3-AS-HPD0', N'W-MF 60D26L STD UC ASAHI HP-D', CAST(N'2023-11-17T23:51:38.880' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2248', N'W-AMUP-60D26LX-A8P3-BL-MFA0', N'W-MF 60D26L STD UC NON BRAND', CAST(N'2023-11-17T23:51:38.960' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2249', N'W-AMUP-60D26LX-A8P3-IN-MFA0', N'W-MF 60D26L STD UC INCOE MF-A', CAST(N'2023-11-17T23:51:39.043' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'225', N'W-CV04-D26LXXX-C04N-NL-DG00', N'CV D26L YELLOW HD-YELLOW', CAST(N'2023-11-17T23:48:35.633' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2250', N'W-AMUP-60D26LX-A8P3-IN-MFB0', N'W-MF 60D26L STD UC INCOE MF-B', CAST(N'2023-11-17T23:51:39.127' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2251', N'W-AMUP-60D26LX-A8P3-IN-MFC0', N'W-MF 60D26L STD UC INCOE MF-C', CAST(N'2023-11-17T23:51:39.210' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2252', N'W-AMUP-60D26LX-A8P3-IN-MFE0', N'W-MF 60D26L STD UC INCOE PHI', CAST(N'2023-11-17T23:51:39.290' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2253', N'W-AMUP-60D26LX-A8P3-IN-MFJ0', N'W-MF 60D26L STD UC INCOE UAE', CAST(N'2023-11-17T23:51:39.373' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2254', N'W-AMUP-60D26LX-A8P3-OH-MFA0', N'W-MF 60D26L STD UC  OHAYO MF-A', CAST(N'2023-11-17T23:51:39.457' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2255', N'W-AMUP-60D26LX-B1P2-IN-MFE0', N'W-MF 60D26L C20 UC INCOE PHI', CAST(N'2023-11-17T23:51:39.537' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2256', N'W-AMUP-60D26RX-A8P3-AP-MFA0', N'W-MF 60D26R STD UC ASPIRA MF-A', CAST(N'2023-11-17T23:51:39.613' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2257', N'W-AMUP-60D26RX-A8P3-AS-HPB0', N'W-MF 60D26R STD UC ASAHI HP-B', CAST(N'2023-11-17T23:51:39.697' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2258', N'W-AMUP-60D26RX-A8P3-BL-MFA0', N'W-MF 60D26R STD UC NON BRAND', CAST(N'2023-11-17T23:51:39.777' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2259', N'W-AMUP-60D26RX-A8P3-IN-MFA0', N'W-MF 60D26R STD UC INCOE MF-A', CAST(N'2023-11-17T23:51:39.860' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'226', N'W-CV04-D26RXXX-C04N-NL-DG00', N'CV D26R YELLOW HD-YELLOW', CAST(N'2023-11-17T23:48:35.717' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2260', N'W-AMUP-60D26RX-A8P3-IN-MFB0', N'W-MF 60D26R STD UC INCOE MF-B', CAST(N'2023-11-17T23:51:39.940' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2261', N'W-AMUP-60D26RX-A8P3-IN-MFC0', N'W-MF 60D26R STD UC INCOE MF-C', CAST(N'2023-11-17T23:51:40.020' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2262', N'W-AMUP-60D26RX-A8P3-IN-MFD0', N'W-MF 60D26R STD UC INCOE SUD', CAST(N'2023-11-17T23:51:40.140' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2263', N'W-AMUP-60D26RX-A8P3-IN-MFJ0', N'W-MF 60D26R STD UC INCOE UAE', CAST(N'2023-11-17T23:51:40.223' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2264', N'W-AMUP-60D26RX-A8P3-OH-MFA0', N'W-MF 60D26R STD UC OHAYO MF-A', CAST(N'2023-11-17T23:51:40.303' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2265', N'W-AMUP-65D23LX-B0P3-AP-MFA0', N'W-MF 65D23L STD UC ASPIRA MF-A', CAST(N'2023-11-17T23:51:40.383' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2266', N'W-AMUP-65D23LX-B0P3-AS-HPB0', N'W-MF 65D23L STD UC ASAHI HP-B', CAST(N'2023-11-17T23:51:40.467' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2267', N'W-AMUP-65D23LX-B0P3-AS-HPC0', N'W-MF 65D23L STD UC ASAHI HP-C', CAST(N'2023-11-17T23:51:40.547' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2268', N'W-AMUP-65D23LX-B0P3-AS-HPD0', N'W-MF 65D23L STD UC ASAHI HP-D', CAST(N'2023-11-17T23:51:40.627' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2269', N'W-AMUP-65D23LX-B0P3-BL-MFA0', N'W-MF 65D23L STD UC NON BRAND', CAST(N'2023-11-17T23:51:40.707' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'227', N'W-CV04-D31LXXX-C04N-NL-DG00', N'CV D31L YELLOW HD-YELLOW', CAST(N'2023-11-17T23:48:35.800' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2270', N'W-AMUP-65D23LX-B0P3-BL-MFB0', N'W-MF 65D23L STD UC N-BRAND - B', CAST(N'2023-11-17T23:51:40.790' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2271', N'W-AMUP-56638LX-B2C3-QU-MFA0', N'W-MF 566-38L STD UC QUANTUM-A', CAST(N'2023-11-17T23:51:40.870' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2272', N'W-AMUP-56638LX-B2C3-QU-MFB0', N'W-MF 566-38L STD UC QUANTUM-B', CAST(N'2023-11-17T23:51:40.950' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2273', N'W-AMUP-56638LX-B2C3-QU-MFF0', N'W-MF 566-38L STD UC QUANTUM-F', CAST(N'2023-11-17T23:51:41.033' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2274', N'W-AMUP-56638LX-B4C2-IN-MFG0', N'W-MF 566-38L C20 UC INCOE KSA', CAST(N'2023-11-17T23:51:41.117' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2275', N'W-AMUP-56638LX-B4C2-OH-MFA0', N'W-MF 566-38L C20 UC OHAYO MF-A', CAST(N'2023-11-17T23:51:41.197' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2276', N'W-AMUP-57229LX-B4C3-IN-MFA0', N'W-MF 572-27L STD UC INCOE MF-A', CAST(N'2023-11-17T23:51:41.277' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2277', N'W-AMUP-57233RX-B4C3-IN-MFA0', N'W-MF 572-3R STD UC INCOE MF-A', CAST(N'2023-11-17T23:51:41.360' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2278', N'W-AMUP-57412LX-B4C3-AP-MFA0', N'W-MF 574-12L STD UC ASPIRA MFA', CAST(N'2023-11-17T23:51:41.440' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2279', N'W-AMUP-57412LX-B4C3-AS-HPD0', N'W-MF 574-12L STD UC ASAHI HP-D', CAST(N'2023-11-17T23:51:41.520' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'228', N'W-CV04-D31RXXX-C04N-NL-DG00', N'CV D31R YELLOW HD-YELLOW', CAST(N'2023-11-17T23:48:35.883' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2280', N'W-AMUP-57412LX-B4C3-IN-MFC0', N'W-MF 574-12L STD UC INCOE MF-C', CAST(N'2023-11-17T23:51:41.607' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2281', N'W-AMUP-57412LX-B4C3-IN-MFE0', N'W-MF 574-12L STD UC INCOE PHI', CAST(N'2023-11-17T23:51:41.687' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2282', N'W-AMUP-57412LX-B4C3-IN-MFJ0', N'W-MF 574-12L STD UC INCOE UAE', CAST(N'2023-11-17T23:51:41.767' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2283', N'W-AMUP-57412LX-B4C3-OH-MFA0', N'W-MF 574-12L STD UC OHAYO MF-A', CAST(N'2023-11-17T23:51:41.850' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2284', N'W-AMUP-57412LX-B4C3-OH-MFB0', N'W-MF 574-12L STD UC OHAYO UAE', CAST(N'2023-11-17T23:51:41.930' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2285', N'W-AMUP-57412LX-B4C3-QU-MFD0', N'W-MF 574-12L STD UC QUANTUM-D', CAST(N'2023-11-17T23:51:42.017' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2286', N'W-AMUP-57413RX-B4C3-OH-MFA0', N'W-MF 574-13R STD UC OHAYO MF-A', CAST(N'2023-11-17T23:51:42.100' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2287', N'W-AMUP-57519LX-B4C3-AS-HPC0', N'W-MF 575-19L STD UC ASAHI HP-C', CAST(N'2023-11-17T23:51:42.180' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2288', N'W-AMUP-57519LX-B4C3-IN-MFF0', N'W-MF 575-19L STD UC INCOE AOP', CAST(N'2023-11-17T23:51:42.263' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2289', N'W-AMUP-48D26RX-A8P3-IN-MFF0', N'W-MF 48D26R STD UC INCOE AOP', CAST(N'2023-11-17T23:51:42.347' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'229', N'W-CV08-S100CTX-0MHN-NL-0000', N'CV S100-CT GREY', CAST(N'2023-11-17T23:48:35.963' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2290', N'W-AMUP-65D23LX-B0P3-IN-MFB0', N'W-MF 65D23L STD UC INCOE MF-B', CAST(N'2023-11-17T23:51:42.427' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2291', N'W-AMUP-65D23LX-B0P3-IN-MFC0', N'W-MF 65D23L STD UC INCOE MF-C', CAST(N'2023-11-17T23:51:42.507' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2292', N'W-AMUP-65D23LX-B0P3-IN-MFD0', N'W-MF 65D23L STD UC INCOE SUD', CAST(N'2023-11-17T23:51:42.590' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2293', N'W-AMUP-65D23LX-B0P3-IN-MFE0', N'W-MF 65D23L STD UC INCOE PHI', CAST(N'2023-11-17T23:51:42.670' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2294', N'W-AMUP-65D23LX-B0P3-IN-MFH0', N'W-MF 65D23L STD UC INCOE VIE', CAST(N'2023-11-17T23:51:42.753' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2295', N'W-AMUP-65D23LX-B0P3-IN-MFJ0', N'W-MF 65D23L STD UC INCOE UAE', CAST(N'2023-11-17T23:51:42.830' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2296', N'W-AMUP-65D23LX-B0P3-OH-MFA0', N'W-MF 65D23L STD UC OHAYO MF-A', CAST(N'2023-11-17T23:51:42.913' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2297', N'W-AMUP-65D23LX-B0P3-OH-MFB0', N'W-MF 65D23L STD UC OHAYO UAE', CAST(N'2023-11-17T23:51:42.993' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2298', N'W-AMUP-65D23LX-B3P2-IN-MFB0', N'W-MF 65D23L C20 UC INCOE MF-B', CAST(N'2023-11-17T23:51:43.073' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2299', N'W-AMUP-65D23RX-B0P3-AP-MFA0', N'W-MF 65D23R STD UC ASPIRA MF-A', CAST(N'2023-11-17T23:51:43.157' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'23', N'K-CV10-S100XXX-0MHN-NL-00M0', N'CV S100 BLACK (ABS)', CAST(N'2023-11-17T23:48:18.247' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'230', N'W-TC02-A19XXXX-COFS-NL-0000', N'TOP COVER A19 BLACK', CAST(N'2023-11-17T23:48:36.050' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2300', N'W-AMUP-65D23RX-B0P3-AS-HPB0', N'W-MF 65D23R STD UC ASAHI HP-B', CAST(N'2023-11-17T23:51:43.237' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2301', N'W-AMUP-65D23RX-B0P3-AS-HPC0', N'W-MF 65D23R STD UC ASAHI HP-C', CAST(N'2023-11-17T23:51:43.320' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2302', N'W-AMUP-65D23RX-B0P3-BL-MFA0', N'W-MF 65D23R STD UC NON BRAND', CAST(N'2023-11-17T23:51:43.400' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2303', N'W-AMUP-65D23RX-B0P3-IN-MFA0', N'W-MF 65D23R STD UC INCOE MF-A', CAST(N'2023-11-17T23:51:43.480' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2304', N'W-AMUP-65D23RX-B0P3-IN-MFB0', N'W-MF 65D23R STD UC INCOE MF-B', CAST(N'2023-11-17T23:51:43.563' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2305', N'W-AMUP-65D23RX-B0P3-IN-MFC0', N'W-MF 65D23R STD UC INCOE MF-C', CAST(N'2023-11-17T23:51:43.647' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2306', N'W-AMUP-65D23RX-B0P3-IN-MFJ0', N'W-MF 65D23R STD UC INCOE UAE', CAST(N'2023-11-17T23:51:43.727' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2307', N'W-AMUP-65D23RX-B0P3-OH-MFA0', N'W-MF 65D23R STD UC OHAYO MF-A', CAST(N'2023-11-17T23:51:43.807' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2308', N'W-AMUP-65D23RX-B0P3-OH-MFB0', N'W-MF 65D23R STD UC OHAYO UAE', CAST(N'2023-11-17T23:51:43.890' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2309', N'W-AMUP-65D23RX-B3P2-IN-MFB0', N'W-MF 65D23R C20 UC INCOE MF-B', CAST(N'2023-11-17T23:51:43.970' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'231', N'W-CV01-D23LXXX-D01N-NL-DG00', N'CV D23L BLUE HD-BLUE', CAST(N'2023-11-17T23:48:36.133' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2310', N'W-AMUP-65D26LX-B0P4-AS-HPB0', N'W-MF 65D26L SUB-1 UC ASAHI HPB', CAST(N'2023-11-17T23:51:44.050' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2311', N'W-AMUP-65D26LX-B0P4-IN-MFB0', N'W-MF 65D26L SUB-1 UC INCOE MFB', CAST(N'2023-11-17T23:51:44.133' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2312', N'W-AMUP-55565RX-B0C3-IN-MFB0', N'W-MF 555-65R STD UC INCOE MF-B', CAST(N'2023-11-17T23:51:44.213' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2313', N'W-AMUP-80D23LX-B3P3-IN-MFB0', N'W-MF 80D23L STD UC INCOE MF-B', CAST(N'2023-11-17T23:51:44.293' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2314', N'W-AMUP-80D23LX-B3P3-IN-MFC0', N'W-MF 80D23L STD UC INCOE MF-C', CAST(N'2023-11-17T23:51:44.440' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2315', N'W-AMUP-80D23LX-B3P3-IN-MFE0', N'W-MF 80D23L STD UC INCOE PHI', CAST(N'2023-11-17T23:51:44.520' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2316', N'W-AMUP-80D23LX-B3P3-IN-MFH0', N'W-MF 80D23L STD UC INCOE VIE', CAST(N'2023-11-17T23:51:44.603' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2317', N'W-AMUP-80D23LX-B3P3-LU-00A0', N'W-MF 80D23L STD UC LUCAS', CAST(N'2023-11-17T23:51:44.687' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2318', N'W-AMUP-80D23LX-B3P3-OH-MFA0', N'W-MF 80D23L STD UC OHAYO MF-A', CAST(N'2023-11-17T23:51:44.767' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2319', N'W-AMUP-80D23LX-B3P3-OH-MFB0', N'W-MF 80D23L STD UC OHAYO UAE', CAST(N'2023-11-17T23:51:44.847' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'232', N'W-CV01-65D31RX-C06N-NL-DG00', N'CV 65D31R BLUE HD-WHITE', CAST(N'2023-11-17T23:48:36.217' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2320', N'W-AMUP-80D23RX-B3P3-AS-HPC0', N'W-MF 80D23R STD UC ASAHI HP-C', CAST(N'2023-11-17T23:51:44.927' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2321', N'W-AMUP-80D23RX-B3P3-BL-MFA0', N'W-MF 80D23R STD UC NON BRAND', CAST(N'2023-11-17T23:51:45.010' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2322', N'W-AMUP-80D23RX-B3P3-IN-MFB0', N'W-MF 80D23R STD UC INCOE MF-B', CAST(N'2023-11-17T23:51:45.097' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2323', N'W-AMUP-80D23RX-B3P3-IN-MFC0', N'W-MF 80D23R STD UC INCOE MF-C', CAST(N'2023-11-17T23:51:45.177' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2324', N'W-AMUP-80D26LX-B3P3-AP-MFB0', N'W-MF 80D26L STD UC ASPIRA MF-B', CAST(N'2023-11-17T23:51:45.257' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2325', N'W-AMUP-80D26LX-B3P3-AS-MEA0', N'W-MF 80D26L STD UC ASAHI MEN', CAST(N'2023-11-17T23:51:45.340' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2326', N'W-AMUP-80D26LX-B3P3-IN-MFF0', N'W-MF 80D26L STD UC INCOE AOP', CAST(N'2023-11-17T23:51:45.420' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2327', N'W-AMUP-80D26LX-B3P3-IN-MFI0', N'W-MF 80D26L STD UC INCOE GREE', CAST(N'2023-11-17T23:51:45.503' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2328', N'W-AMUP-80D26LX-B3P3-IN-MFL0', N'W-MF 80D26L STD UC INCOE HYD', CAST(N'2023-11-17T23:51:45.583' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2329', N'W-AMUP-80D26LX-B3P3-QU-MFA0', N'W-MF 80D26L STD UC QUANTUM-A', CAST(N'2023-11-17T23:51:45.667' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'233', N'W-CV02-D23LXXX-D02C-NL-00T0', N'CV D23L MF BLACK', CAST(N'2023-11-17T23:48:36.300' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2330', N'W-AMUP-80D26LX-B3P3-QU-MFB0', N'W-MF 80D26L STD UC QUANTUM-B', CAST(N'2023-11-17T23:51:45.750' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2331', N'W-AMUP-80D26LX-B3P3-QU-MFC0', N'W-MF 80D26L STD UC QUANTUM-C', CAST(N'2023-11-17T23:51:45.830' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2332', N'W-AMUP-80D26LX-B3P3-QU-MFE0', N'W-MF 80D26L STD UC QUANTUM-E', CAST(N'2023-11-17T23:51:45.910' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2333', N'W-AMUP-80D26LX-B3P3-QU-MFF0', N'W-MF 80D26L STD UC QUANTUM-F', CAST(N'2023-11-17T23:51:45.990' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2334', N'W-AMUP-80D26RX-B3P3-AM-PRA0', N'W-MF 80D26R STD UC AMK PRE', CAST(N'2023-11-17T23:51:46.073' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2335', N'W-AMUP-80D26RX-B3P3-AP-MFB0', N'W-MF 80D26R STD UC ASPIRA MF-B', CAST(N'2023-11-17T23:51:46.153' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2336', N'W-AMUP-80D26RX-B3P3-AS-MEA0', N'W-MF 80D26R STD UC ASAHI MEN', CAST(N'2023-11-17T23:51:46.237' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2337', N'W-AMUP-80D26RX-B3P3-IN-MFF0', N'W-MF 80D26R STD UC INCOE AOP', CAST(N'2023-11-17T23:51:46.320' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2338', N'W-AMUP-80D26RX-B3P3-IN-MFI0', N'W-MF 80D26R STD UC INCOE GREE', CAST(N'2023-11-17T23:51:46.410' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2339', N'W-AMUP-55565RX-B0C3-IN-MFD0', N'W-MF 555-65R STD UC INCOE SUD', CAST(N'2023-11-17T23:51:46.497' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'234', N'W-CV02-D23LXXX-D02N-NL-DG00', N'CV D23L BLACK HD-BLACK', CAST(N'2023-11-17T23:48:36.383' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2340', N'W-AMUP-55565RX-B0C3-IN-MFF0', N'W-MF 555-65R STD UC INCOE AOP', CAST(N'2023-11-17T23:51:46.593' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2341', N'W-AMUP-55D26RX-B0P3-IN-MFF0', N'W-MF 55D26R STD UC INCOE AOP', CAST(N'2023-11-17T23:51:46.687' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2342', N'W-AMUP-55D26RX-B0P3-QU-MFA0', N'W-MF 55D26R STD UC QUANTUM-A', CAST(N'2023-11-17T23:51:46.780' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2343', N'W-AMUP-50B24RX-B1P3-OH-MFA0', N'W-MF 50B24R STD UC OHAYO MF-A', CAST(N'2023-11-17T23:51:46.873' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2344', N'W-AMUP-56638LX-B2C3-IN-MFB0', N'W-MF 566-38L STD UC INCOE MF-B', CAST(N'2023-11-17T23:51:46.973' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2345', N'W-AMUP-48D26RX-A8P3-QU-MFA0', N'W-MF 48D26R STD UC QUANTUM-A', CAST(N'2023-11-17T23:51:47.063' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2346', N'W-AMUP-48D26RX-A8P3-QU-MFB0', N'W-MF 48D26R STD UC QUANTUM-B', CAST(N'2023-11-17T23:51:47.153' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2347', N'W-AMUP-48D26RX-A8P3-QU-MFC0', N'W-MF 48D26R STD UC QUANTUM-C', CAST(N'2023-11-17T23:51:47.233' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2348', N'W-AMUP-48D26RX-A8P3-QU-MFF0', N'W-MF 48D26R STD UC QUANTUM-F', CAST(N'2023-11-17T23:51:47.317' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2349', N'W-AMUP-48D26RX-A8P3-TN-00A0', N'W-MF 48D26R STD UC TRAKNUS', CAST(N'2023-11-17T23:51:47.397' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'235', N'W-CV02-D23LXXX-D02S-NL-00T0', N'CV D23L MF BLACK SIDE BA-IN', CAST(N'2023-11-17T23:48:36.467' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2350', N'W-AMUP-4D150RX-D1C3-AM-HDA0', N'W-MF 4D150R STD UC AMK HDT', CAST(N'2023-11-17T23:51:47.477' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2351', N'W-AMUP-4D170RX-D7C3-AM-HDA0', N'W-MF 4D170R STD UC AMK HDT', CAST(N'2023-11-17T23:51:47.560' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2352', N'W-AMUP-4D200RX-E0C3-AM-HDA0', N'W-MF 4D200R STD UC AMK HDT', CAST(N'2023-11-17T23:51:47.640' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2353', N'W-AMUP-50B24LS-B1P2-IN-MFB0', N'W-MF 50B24LS C20 UC INCOE MF-B', CAST(N'2023-11-17T23:51:47.720' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2354', N'W-AMUP-50B24LS-B1P3-AP-MFA0', N'W-MF 50B24LS STD UC ASPIRA MFA', CAST(N'2023-11-17T23:51:47.803' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2355', N'W-AMUP-50B24LS-B1P3-AS-HPB0', N'W-MF 50B24LS STD UC ASAHI HP-B', CAST(N'2023-11-17T23:51:47.883' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2356', N'W-AMUP-50B24LS-B1P3-AS-HPC0', N'W-MF 50B24LS STD UC ASAHI HP-C', CAST(N'2023-11-17T23:51:47.963' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2357', N'W-AMUP-50B24LS-B1P3-BL-MFA0', N'W-MF 50B24LS STD UC NON BRAND', CAST(N'2023-11-17T23:51:48.043' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2358', N'W-AMUP-58827LX-B6C3-IN-MFA0', N'W-MF 588-27L STD UC INCOE MF-A', CAST(N'2023-11-17T23:51:48.130' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2359', N'W-AMUP-54459LX-A8C3-AP-MFA0', N'W-MF 544-59L STD UC ASIPRA MFA', CAST(N'2023-11-17T23:51:48.210' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'236', N'W-CV02-D23RXXX-D02N-NL-DG00', N'CV D23R BLACK HD-BLACK', CAST(N'2023-11-17T23:48:36.553' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2360', N'W-AMUP-54459LX-A8C3-AS-HPB0', N'W-MF 544-59L STD UC ASAHI HP-B', CAST(N'2023-11-17T23:51:48.293' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2361', N'W-AMUP-54459LX-A8C3-AS-HPC0', N'W-MF 544-59L STD UC ASAHI HP-C', CAST(N'2023-11-17T23:51:48.373' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2362', N'W-AMUP-54459LX-A8C3-AS-MEA0', N'W-MF 544-59L STD UC ASAHI MEN', CAST(N'2023-11-17T23:51:48.450' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2363', N'W-AMUP-54459LX-A8C3-BL-MFA0', N'W-MF 544-59L STD UC NON BRAND', CAST(N'2023-11-17T23:51:48.533' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2364', N'W-AMUP-54459LX-A8C3-IN-MFA0', N'W-MF 544-59L STD UC INCOE MF-A', CAST(N'2023-11-17T23:51:48.617' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2365', N'W-AMUP-54459LX-A8C3-IN-MFB0', N'W-MF 544-59L STD UC INCOE MF-B', CAST(N'2023-11-17T23:51:48.697' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2366', N'W-AMUP-54459LX-A8C3-IN-MFC0', N'W-MF 544-59L STD UC INCOE MF-C', CAST(N'2023-11-17T23:51:48.777' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2367', N'W-AMUP-54459LX-A8C3-IN-MFF0', N'W-MF 544-59L STD UC INCOE AOP', CAST(N'2023-11-17T23:51:48.860' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2368', N'W-AMUP-54459LX-A8C3-IN-MFJ0', N'W-MF 544-59L STD UC INCOE UAE', CAST(N'2023-11-17T23:51:48.940' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2369', N'W-AMUP-54459LX-A8C3-IN-MFL0', N'W-MF 544-59L STD UC INCOE HYD', CAST(N'2023-11-17T23:51:49.020' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'237', N'W-CV02-D23RXXX-D02S-NL-00T0', N'CV D23R MF BLACK SIDE BA-IN', CAST(N'2023-11-17T23:48:36.633' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2370', N'W-AMUP-54459LX-A8C3-OH-MFA0', N'W-MF 544-59L STD UC OHAYO MF-A', CAST(N'2023-11-17T23:51:49.103' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2371', N'W-AMUP-54459LX-A8C3-QU-MFA0', N'W-MF 544-59L STD UC QUANTUM-A', CAST(N'2023-11-17T23:51:49.183' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2372', N'W-AMUP-54459LX-A8C3-QU-MFB0', N'W-MF 544-59L STD UC QUANTUM-B', CAST(N'2023-11-17T23:51:49.267' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2373', N'W-AMUP-54459LX-A8C3-QU-MFF0', N'W-MF 544-59L STD UC QUANTUM-F', CAST(N'2023-11-17T23:51:49.350' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2374', N'W-AMUP-54464RX-A8C3-IN-MFF0', N'W-MF 544-64R STD UC INCOE AOP', CAST(N'2023-11-17T23:51:49.430' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2375', N'W-AMUP-54464RX-A8C3-QU-MFA0', N'W-MF 544-64R STD UC QUANTUM-A', CAST(N'2023-11-17T23:51:49.510' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2376', N'W-AMUP-55055RX-A8C3-IN-MFB0', N'W-MF 550-55R STD UC INCOE MF-B', CAST(N'2023-11-17T23:51:49.593' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2377', N'W-AMUP-55059LX-A8C3-AP-MFA0', N'W-MF 550-59L STD UC ASPIRA MFA', CAST(N'2023-11-17T23:51:49.673' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2378', N'W-AMUP-55059LX-A8C3-IN-MFB0', N'W-MF 550-59L STD UC INCOE MF-B', CAST(N'2023-11-17T23:51:49.757' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2379', N'W-AMUP-55559LX-B0C3-AP-MFA0', N'W-MF 555-59L STD UC ASPIRA MFA', CAST(N'2023-11-17T23:51:49.840' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'238', N'W-CV02-D26LXXX-C02N-NL-DG00', N'CV D26L BLACK HD-BLACK', CAST(N'2023-11-17T23:48:36.720' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2380', N'W-AMUP-55559LX-B0C3-AS-HPB0', N'W-MF 555-59L STD UC ASAHI HP-B', CAST(N'2023-11-17T23:51:49.920' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2381', N'W-AMUP-65D23LX-B0P3-IN-MFA0', N'W-MF 65D23L STD UC INCOE MF-A', CAST(N'2023-11-17T23:51:50.003' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2382', N'W-AMUP-50B24LS-B1P3-BL-MFB0', N'W-MF 50B24LS STD UC N-BRAND -B', CAST(N'2023-11-17T23:51:50.083' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2383', N'W-AMUP-50B24LS-B1P3-IN-MFA0', N'W-MF 50B24LS STD UC INCOE MF-A', CAST(N'2023-11-17T23:51:50.167' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2384', N'W-AMUP-50B24LS-B1P3-IN-MFB0', N'W-MF 50B24LS STD UC INCOE MF-B', CAST(N'2023-11-17T23:51:50.250' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2385', N'W-AMUP-50B24LS-B1P3-IN-MFC0', N'W-MF 50B24LS STD UC INCOE MF-C', CAST(N'2023-11-17T23:51:50.330' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2386', N'W-AMUP-50B24LS-B1P3-IN-MFH0', N'W-MF 50B24LS STD UC INCOE VIE', CAST(N'2023-11-17T23:51:50.410' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2387', N'W-AMUP-50B24LS-B1P3-IN-MFJ0', N'W-MF 50B24LS STD UC INCOE UAE', CAST(N'2023-11-17T23:51:50.490' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2388', N'W-AMUP-50B24LS-B1P3-OH-MFA0', N'W-MF 50B24LS STD UC OHAYO MF-A', CAST(N'2023-11-17T23:51:50.573' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2389', N'W-AMUP-50B24LS-B1P3-OH-MFB0', N'W-MF 50B24LS STD UC OHAYO UAE', CAST(N'2023-11-17T23:51:50.653' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'239', N'W-CV02-D26LXXX-C03N-NL-DG00', N'CV D26L BLACK HD-ORANGE', CAST(N'2023-11-17T23:48:36.803' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2390', N'W-AMUP-50B24LX-B1P2-IN-MFB0', N'W-MF 50B24L C20 UC INCOE MF-B', CAST(N'2023-11-17T23:51:50.737' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2391', N'W-AMUP-50B24LX-B1P3-AP-MFA0', N'W-MF 50B24L STD UC ASPIRA MF-A', CAST(N'2023-11-17T23:51:50.820' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2392', N'W-AMUP-50B24LX-B1P3-AS-HPB0', N'W-MF 50B24L STD UC ASAHI HP-B', CAST(N'2023-11-17T23:51:50.900' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2393', N'W-AMUP-50B24LX-B1P3-AS-HPC0', N'W-MF 50B24L STD UC ASAHI HP-C', CAST(N'2023-11-17T23:51:50.983' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2394', N'W-AMUP-50B24LX-B1P3-BL-MFA0', N'W-MF 50B24L STD UC NON BRAND', CAST(N'2023-11-17T23:51:51.063' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2395', N'W-AMUP-95D31RX-B4P3-QU-MFC0', N'W-MF 95D31R STD UC QUANTUM-C', CAST(N'2023-11-17T23:51:51.147' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2396', N'W-AMUP-95E41LX-B5C4-BL-MFC0', N'W-MF 95RE41L SUB-1 UC N-BRA -C', CAST(N'2023-11-17T23:51:51.230' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2397', N'W-AMUP-95E41LX-B5C4-IN-MFB0', N'W-MF 95RE41L SUB-1 UC INC MF-B', CAST(N'2023-11-17T23:51:51.310' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2398', N'W-AMUP-95E41LX-B7C3-BL-MFC0', N'W-MF 95E41L STD UC N-BRAND -C', CAST(N'2023-11-17T23:51:51.387' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2399', N'W-AMUP-95E41LX-B7C3-QU-MFA0', N'W-MF 95E41L STD UC QUANTUM-A', CAST(N'2023-11-17T23:51:51.470' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'24', N'K-CV10-S105XXX-0MHN-NL-00M0', N'CV S105 BLACK (ABS)', CAST(N'2023-11-17T23:48:18.327' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'240', N'W-CV02-D26LXXX-C06N-NL-DG00', N'CV D26L BLACK HD-WHITE', CAST(N'2023-11-17T23:48:36.887' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2400', N'W-AMUP-95E41LX-B7C3-QU-MFD0', N'W-MF 95E41L STD UC QUANTUM-D', CAST(N'2023-11-17T23:51:51.553' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2401', N'W-AMUP-95E41RX-B5C4-BL-MFC0', N'W-MF 95RE41R SUB-1 UC N-BR - C', CAST(N'2023-11-17T23:51:51.633' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2402', N'W-AMUP-95E41RX-B5C4-IN-MFB0', N'W-MF 95RE41R SUB-1 UC INC MF-B', CAST(N'2023-11-17T23:51:51.710' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2403', N'W-AMUP-95E41RX-B5C4-OH-MFA0', N'W-MF 95RE41R SUB-1 UC OHA MF-A', CAST(N'2023-11-17T23:51:51.793' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2404', N'W-AMUP-95E41RX-B7C3-AM-PRA0', N'W-MF 95E41R STD UC AMK PRE', CAST(N'2023-11-17T23:51:51.877' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2405', N'W-AMUP-95E41RX-B7C3-IN-MFF0', N'W-MF 95E41R STD UC INCOE AOP', CAST(N'2023-11-17T23:51:51.960' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2406', N'W-AMUP-95E41RX-B7C3-OH-MFB0', N'W-MF 95E41R STD UC OHAYO UAE', CAST(N'2023-11-17T23:51:52.037' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2407', N'W-AMUP-95E41RX-B7C3-QU-MFA0', N'W-MF 95E41R STD UC QUANTUM-A', CAST(N'2023-11-17T23:51:52.120' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2408', N'W-AMUP-65D31RX-B2P3-QU-MFB0', N'W-MF 65D31R STD UC QUANTUM-B', CAST(N'2023-11-17T23:51:52.200' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2409', N'W-AMUP-75D23RX-B3P3-QU-MFB0', N'W-MF 75D23R STD UC QUANTUM-B', CAST(N'2023-11-17T23:51:52.283' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'241', N'W-CV02-D26LXXX-D02S-NL-00T0', N'CV D26L MF BLACK SIDE BA-IN', CAST(N'2023-11-17T23:48:36.970' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2410', N'W-AHUP-65D26RX-B2A3-AP-HYA0', N'WIP 65D26R STD UC ASPIRA HYB', CAST(N'2023-11-17T23:51:52.367' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2411', N'W-AHUP-32B20RX-A8A3-AP-HYA0', N'WIP 32B20R STD UC ASPIRA HYB', CAST(N'2023-11-17T23:51:52.453' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2412', N'W-AHUP-32B20LX-A8A3-AP-HYA0', N'WIP 32B20L STD UC ASPIRA HYB', CAST(N'2023-11-17T23:51:52.537' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2413', N'W-AHUP-36B20RX-B0A3-AP-HYA0', N'WIP 36B20R STD UC ASPIRA HYB', CAST(N'2023-11-17T23:51:52.620' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2414', N'W-AHUP-36B20LX-B0A3-AP-HYA0', N'WIP 36B20L STD UC ASPIRA HYB', CAST(N'2023-11-17T23:51:52.700' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2415', N'W-AHUP-46B24RX-B1A3-AP-HYA0', N'WIP 46B24R STD UC ASPIRA HYB', CAST(N'2023-11-17T23:51:52.780' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2416', N'W-AHUP-46B24LX-B1A3-AP-HYA0', N'WIP 46B24L STD UC ASPIRA HYB', CAST(N'2023-11-17T23:51:52.860' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2417', N'W-AHUP-46B24LS-B1A3-AP-HYA0', N'WIP 46B24LS STD UC ASPIRA HYB', CAST(N'2023-11-17T23:51:52.943' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2418', N'W-AMUP-57540LH-B7C3-IN-MFB0', N'W-MF 575-40L LH STD UC INC MFB', CAST(N'2023-11-17T23:51:53.023' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2419', N'W-AMUP-57540LH-B7C3-IN-MFE0', N'W-MF 575-40L LH STD UC INC PHI', CAST(N'2023-11-17T23:51:53.107' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'242', N'W-CV02-D26RXXX-C02N-NL-DG00', N'CV D26R BLACK HD-BLACK', CAST(N'2023-11-17T23:48:37.053' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2420', N'W-AMUP-57540LH-B7C3-LU-00A0', N'W-MF 575-40L LH STD UC LUCAS', CAST(N'2023-11-17T23:51:53.190' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2421', N'W-AMUP-57540LH-B7C3-OH-MFA0', N'W-MF 575-40L LH STD UC OHA MFA', CAST(N'2023-11-17T23:51:53.270' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2422', N'W-AMUP-57540LH-B7C3-QU-MFA0', N'W-MF 575-40L LH STD UC QUANT-A', CAST(N'2023-11-17T23:51:53.350' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2423', N'W-AMUP-58815LH-C1C3-IN-MFA0', N'W-MF 588-15L LH STD UC INC MFA', CAST(N'2023-11-17T23:51:53.430' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2424', N'W-AMUP-58815LH-C1C3-IN-MFB0', N'W-MF 588-15L LH STD UC INC MFB', CAST(N'2023-11-17T23:51:53.510' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2425', N'W-AMUP-58815LH-C1C3-OH-MFA0', N'W-MF 588-15L LH STD UC OHA MFA', CAST(N'2023-11-17T23:51:53.593' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2426', N'W-AMUP-58815LH-C1C3-QU-MFB0', N'W-MF 588-15L LH STD UC QUA MFB', CAST(N'2023-11-17T23:51:53.673' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2427', N'W-AVUP-46B24RX-B0A4-QU-MFA0', N'W-MF 46B24R SUB-1 UC QUANTUM-A', CAST(N'2023-11-17T23:51:53.753' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2428', N'W-AVUP-36B20LX-A7A5-OH-MFA0', N'W-MF 36B20L SUB-2 UC OHAYO MFA', CAST(N'2023-11-17T23:51:53.837' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2429', N'W-AVUP-36B20LX-A7A5-QU-MFA0', N'W-MF 36B20L SUB-2 UC QUANTUM-A', CAST(N'2023-11-17T23:51:53.917' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'243', N'W-CV02-D26RXXX-C03N-NL-DG00', N'CV D26R BLACK HD-ORANGE', CAST(N'2023-11-17T23:48:37.137' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2430', N'W-AVUP-36B20LX-A8A4-IN-MFB0', N'W-MF 36B20L SUB-1 UC INC MF-B', CAST(N'2023-11-17T23:51:54.000' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2431', N'W-AVUP-36B20LX-A8A4-QU-MFA0', N'W-MF 36B20L SUB-1 UC QUANTUM-A', CAST(N'2023-11-17T23:51:54.080' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2432', N'W-AVUP-36B20RS-A8A4-IN-MFB0', N'W-MF 36B20RS SUB-1 UC INC MF-B', CAST(N'2023-11-17T23:51:54.160' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2433', N'W-AVUP-36B20RX-A7A5-OH-MFA0', N'W-MF 36B20R SUB-2 UC OHAYO MFA', CAST(N'2023-11-17T23:51:54.243' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2434', N'W-AVUP-36B20RX-A7A5-QU-MFA0', N'W-MF 36B20R SUB-2 UC QUANTUM-A', CAST(N'2023-11-17T23:51:54.327' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2435', N'W-AVUP-36B20RX-A8A4-QU-MFA0', N'W-MF 36B20R SUB-1 UC QUANTUM-A', CAST(N'2023-11-17T23:51:54.410' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2436', N'W-AVUP-46B24LS-A9A5-OH-MFA0', N'W-MF 46B24LS SUB-2 UC OHA MF-2', CAST(N'2023-11-17T23:51:54.490' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2437', N'W-AVUP-46B24LS-A9A5-QU-MFA0', N'W-MF 46B24LS SUB-2 UC QUANTU-A', CAST(N'2023-11-17T23:51:54.573' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2438', N'W-AVUP-46B24LS-B0A4-AS-HPB0', N'W-MF 46B24LS SUB-1 UC ASA HP-B', CAST(N'2023-11-17T23:51:54.653' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2439', N'W-AVUP-46B24LS-B0A4-IN-MFB0', N'W-MF 46B24LS SUB-1 UC INC MFB', CAST(N'2023-11-17T23:51:54.733' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'244', N'W-CV02-D26RXXX-C06N-NL-DG00', N'CV D26R BLACK HD-WHITE', CAST(N'2023-11-17T23:48:37.220' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2440', N'W-AVUP-46B24LS-B0A4-QU-MFA0', N'W-MF 46B24LS SUB-1 UC QUANTU-A', CAST(N'2023-11-17T23:51:54.817' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2441', N'W-AVUP-46B24LX-B0A4-IN-MFB0', N'W-MF 46B24L SUB-1 UC INCOE MFB', CAST(N'2023-11-17T23:51:54.897' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2442', N'W-AVUP-46B24LX-B0A4-QU-MFA0', N'W-MF 46B24L SUB-1 UC QUANTUM-A', CAST(N'2023-11-17T23:51:54.980' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2443', N'W-AVUP-46B24RS-A9A5-OH-MFA0', N'W-MF 46B24RS SUB-2 UC OHA MF-A', CAST(N'2023-11-17T23:51:55.083' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2444', N'W-AVUP-46B24RS-A9A5-QU-MFA0', N'W-MF 46B24RS SUB-2 UC QUANTU-A', CAST(N'2023-11-17T23:51:55.537' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2445', N'W-AVUP-46B24RS-B0A4-AS-HPB0', N'W-MF 46B24RS SUB-1 UC ASA HP-B', CAST(N'2023-11-17T23:51:55.620' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2446', N'W-AVUP-46B24RS-B0A4-QU-MFA0', N'W-MF 46B24RS SUB-1 UC QUANTU-A', CAST(N'2023-11-17T23:51:55.700' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2447', N'W-AVUP-46B24RX-B0A4-AS-HPB0', N'W-MF 46B24R SUB-1 UC ASAHI HPB', CAST(N'2023-11-17T23:51:55.780' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2448', N'W-AVUP-46B24RX-B0A4-IN-MFB0', N'W-MF 46B24R SUB-1 UC INCOE MFB', CAST(N'2023-11-17T23:51:55.863' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2449', N'W-AVUP-65D26LX-B0A4-AS-HPB0', N'W-MF 65D26L SUB-1 UC ASAHI HPB', CAST(N'2023-11-17T23:51:55.943' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'245', N'W-CV02-D26RXXX-D02C-NL-00T0', N'CV D26R MF BLACK CENTER BA-IN', CAST(N'2023-11-17T23:48:37.303' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2450', N'W-AVUP-65D26LX-B0A4-IN-MFB0', N'W-MF 65D26L SUB-1 UC INCOE MFB', CAST(N'2023-11-17T23:51:56.023' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2451', N'W-AVUP-65D26LX-B0A4-OH-MFA0', N'W-MF 65D26L SUB-1 UC OHAYO MFA', CAST(N'2023-11-17T23:51:56.110' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2452', N'W-AVUP-65D26LX-B0A4-QU-MFA0', N'W-MF 65D26L SUB-1 UC QUANTUM-A', CAST(N'2023-11-17T23:51:56.193' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2453', N'W-AVUP-65D26RX-B0A4-AS-HPB0', N'W-MF 65D26R SUB-1 UC ASAHI HPB', CAST(N'2023-11-17T23:51:56.280' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2454', N'W-AVUP-65D26RX-B0A4-IN-MFB0', N'W-MF 65D26R SUB-1 UC INCOE MFB', CAST(N'2023-11-17T23:51:56.757' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2455', N'W-AVUP-65D26RX-B0A4-OH-MFA0', N'W-MF 65D26R SUB-1 UC OHAYO MFA', CAST(N'2023-11-17T23:51:56.843' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2456', N'W-AVUP-65D26RX-B0A4-QU-MFA0', N'W-MF 65D26R SUB-1 UC QUANTUM-A', CAST(N'2023-11-17T23:51:56.930' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2457', N'W-ACUP-65D26RX-B1C4-NI-00B0', N'WIP 65D26R SUB-1 UC NIKO 002', CAST(N'2023-11-17T23:51:57.033' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2458', N'W-ACUP-65D26LX-B1C4-NI-00B0', N'WIP 65D26L SUB-1 UC NIKO 002', CAST(N'2023-11-17T23:51:57.130' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2459', N'W-ACUP-36B20LX-A7C5-NI-00B0', N'WIP 36B20L SUB-2 UC NIKO 002', CAST(N'2023-11-17T23:51:57.230' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'246', N'W-CV02-D26RXXX-D02S-NL-00T0', N'CV D26R MF BLACK SIDE BA-IN', CAST(N'2023-11-17T23:48:37.387' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2460', N'W-ACUP-46B24RX-A9C5-NI-00B0', N'WIP 46B24R SUB-2 UC NIKO 002', CAST(N'2023-11-17T23:51:57.347' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2461', N'W-ACUP-46B24LX-A9C5-NI-00B0', N'WIP 46B24L SUB-2 UC NIKO 002', CAST(N'2023-11-17T23:51:57.447' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2462', N'W-AHUP-48D26RX-A8A3-AP-HYA0', N'WIP 48D26R STD UC ASPIRA HYB', CAST(N'2023-11-17T23:51:57.553' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2463', N'W-AHUP-55D26RX-B0A3-AP-HYA0', N'WIP 55D26R STD UC ASPIRA HYB', CAST(N'2023-11-17T23:51:57.663' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2464', N'W-AHUP-65D31RX-B2A3-AP-HYA0', N'WIP 65D31R STD UC ASPIRA HYB', CAST(N'2023-11-17T23:51:57.790' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2465', N'W-AHUP-75D31RX-B4A3-AP-HYA0', N'WIP 75D31R STD UC ASPIRA HYB', CAST(N'2023-11-17T23:51:57.907' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2466', N'W-AMUP-54519LH-B1C3-AP-MFA0', N'W-MF 545-19L LH STD UC ASP MFA', CAST(N'2023-11-17T23:51:58.007' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2467', N'W-AMUP-54519LH-B1C3-IN-MFA0', N'W-MF 545-19L LH STD UC INC MFA', CAST(N'2023-11-17T23:51:58.117' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2468', N'W-AMUP-54519LH-B1C3-IN-MFB0', N'W-MF 545-19L LH STD UC INC MFB', CAST(N'2023-11-17T23:51:58.207' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2469', N'W-AMUP-54519LH-B1C3-QU-MFB0', N'W-MF 545-19L LH STD UC QUA MFB', CAST(N'2023-11-17T23:51:58.297' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'247', N'W-CV02-D31LXXX-C02N-NL-DG00', N'CV D31L BLACK HD-BLACK', CAST(N'2023-11-17T23:48:37.470' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2470', N'W-AMUP-55563RH-B3C3-IN-MFF0', N'W-MF 555-63R LH STD UC INC AOP', CAST(N'2023-11-17T23:51:58.380' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2471', N'W-AMUP-55563RH-B3C3-OH-MFA0', N'W-MF 555-63R LH STD UC OHA MFA', CAST(N'2023-11-17T23:51:58.463' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2472', N'W-AMUP-55566LH-B3C3-IN-MFF0', N'W-MF 555-66L LH STD UC INC AOP', CAST(N'2023-11-17T23:51:58.550' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2473', N'W-AMUP-55566LH-B3C3-QU-MFD0', N'W-MF 555-66L LH STD UC QUANT-D', CAST(N'2023-11-17T23:51:58.633' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2474', N'W-AMUP-56205LH-B5C3-IN-MFE0', N'W-MF 562-05L LH STD UC INC PHI', CAST(N'2023-11-17T23:51:58.717' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2475', N'W-AMUP-56205LH-B5C3-OH-MFA0', N'W-MF 562-05L LH STD UC OHA MFA', CAST(N'2023-11-17T23:51:58.797' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2476', N'W-AMUP-56640LH-B5C3-AP-MFA0', N'W-MF 566-40L LH STD UC ASP MFA', CAST(N'2023-11-17T23:51:58.877' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2477', N'W-AMUP-56640LH-B5C3-IN-MFB0', N'W-MF 566-40L LH STD UC INC MFB', CAST(N'2023-11-17T23:51:58.960' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2478', N'W-AMUP-56640LH-B5C3-QU-MFA0', N'W-MF 566-40L LH STD UC QUANT-A', CAST(N'2023-11-17T23:51:59.043' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2479', N'W-AMUP-56640LH-B5C3-QU-MFD0', N'W-MF 566-40L LH STD UC QUANT-D', CAST(N'2023-11-17T23:51:59.130' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'248', N'W-CV02-D31LXXX-C03N-NL-DG00', N'CV D31L BLACK HD-ORANGE', CAST(N'2023-11-17T23:48:37.553' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2480', N'W-ACDP-48D26RX-A9C3-IN-PPAR', N'48D26R STD DC INC PRE-SAW REP', CAST(N'2023-11-17T23:51:59.227' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2481', N'W-ACUR-6D115XX-B7C3-IN-00A0', N'WIP 6D115 STD UC INCOE', CAST(N'2023-11-17T23:51:59.317' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2482', N'W-ACUR-6D135XX-B9C3-IN-00A0', N'WIP 6D135 STD UC INCOE', CAST(N'2023-11-17T23:51:59.403' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2483', N'W-AHUR-8D90XXX-B4C3-IN-00A0', N'WIP 8D90 STD UC HYB INCOE', CAST(N'2023-11-17T23:51:59.483' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2484', N'W-AHWR-8D90XXX-B4C3-IN-00A0', N'WIP 8D90 STD WC HYB INCOE', CAST(N'2023-11-17T23:51:59.587' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2485', N'W-AVUP-65D26RX-A9A3-QU-MFG0', N'W-MF 65D26R STD-V UC QUANTUM-G', CAST(N'2023-11-17T23:51:59.693' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2486', N'W-AMDF-SG100CT-B3C3-IN-00A0', N'WIP SG-100CT STD INCOE', CAST(N'2023-11-17T23:51:59.773' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2487', N'W-AMUP-36B20LX-B0P3-IN-MFK0', N'W-MF 36B20L STD UC INCOE DSP', CAST(N'2023-11-17T23:51:59.853' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2488', N'W-AMUP-46B24RX-B1P3-IN-MFK0', N'W-MF 46B24R STD UC INCOE DSP', CAST(N'2023-11-17T23:51:59.937' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2489', N'W-AMUP-4D170LX-D7C3-AM-HDA0', N'W-MF 4D170L STD UC AMK HDT', CAST(N'2023-11-17T23:52:00.030' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'249', N'W-CV02-D31LXXX-C06N-NL-DG00', N'CV D31L BLACK HD-WHITE', CAST(N'2023-11-17T23:48:37.633' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2490', N'W-AMUP-55D23LX-B0P3-IN-MFK0', N'W-MF 55D23L STD UC INCOE DSP', CAST(N'2023-11-17T23:52:00.117' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2491', N'W-AMUP-70D26LX-B0P3-AS-HPD0', N'W-MF 70D26L STD UC ASAHI HP-D', CAST(N'2023-11-17T23:52:00.197' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2492', N'W-AMUP-80D26LX-B3P3-IN-MFK0', N'W-MF 80D26L STD UC INCOE DSP', CAST(N'2023-11-17T23:52:00.280' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2493', N'W-AMUP-EFBM42L-B2P3-NI-MFA0', N'W-EFB M-42L STD UC NIKO MF-A', CAST(N'2023-11-17T23:52:00.367' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2494', N'W-AMUP-EFBQ85L-B3C3-NI-MFA0', N'W-EFB Q-85L STD UC NIKO MF-A', CAST(N'2023-11-17T23:52:00.447' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2495', N'W-AVUP-36B20LX-A8A3-OH-MFC0', N'W-MF 36B20L STD-V UC OHA MF-C', CAST(N'2023-11-17T23:52:00.530' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2496', N'W-AVUP-36B20LX-A8A3-QU-MFG0', N'W-MF 36B20L STD-V UC QUANTU-G', CAST(N'2023-11-17T23:52:00.610' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2497', N'W-AVUP-36B20RX-A8A3-QU-MFG0', N'W-MF 36B20R STD-V UC QUANTU-G', CAST(N'2023-11-17T23:52:00.697' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2498', N'W-AVUP-46B24RS-A9A3-OH-MFC0', N'W-MF 46B24RS STD UC OHA MF-C', CAST(N'2023-11-17T23:52:00.780' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2499', N'W-AVUP-46B24RS-A9A3-QU-MFG0', N'W-MF 46B24RS STD UC QUANTU-G', CAST(N'2023-11-17T23:52:00.860' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'25', N'K-CV10-S12XXXX-0MHN-NL-00M0', N'CV S12 BLACK (ABS)', CAST(N'2023-11-17T23:48:18.410' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'250', N'W-CV02-D31LXXX-D02S-NL-00T0', N'CV D31L MF BLACK SIDE BA-IN', CAST(N'2023-11-17T23:48:37.720' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2500', N'W-AVUP-65D26LX-A9A3-QU-MFG0', N'W-MF 65D26L STD-V UC QUANTUM-G', CAST(N'2023-11-17T23:52:00.940' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2501', N'W-GRID-CG80DXX-SBCS-02-0000', N'GRID CG80-DEEP CYCLE', CAST(N'2023-11-17T23:52:01.023' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2502', N'W-GRID-CG80XXX-SBCS-02-0000', N'GRID CG80', CAST(N'2023-11-17T23:52:01.110' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2503', N'W-GRID-CG82XXX-SBCS-02-0000', N'GRID CG82', CAST(N'2023-11-17T23:52:01.197' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2504', N'W-GRID-CG85DXX-SBCS-02-0000', N'GRID CG85-DEEP CYCLE', CAST(N'2023-11-17T23:52:01.277' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2505', N'W-GRID-CG85EXX-SBCC-02-0000', N'GRID CG85-E', CAST(N'2023-11-17T23:52:01.360' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2506', N'W-GRID-CG85XXX-SBCS-02-0000', N'GRID CG85', CAST(N'2023-11-17T23:52:01.443' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2507', N'W-GRID-CG87XXX-SBCS-02-0000', N'GRID CG87', CAST(N'2023-11-17T23:52:01.523' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2508', N'W-GRID-YT80XXX-CACS-01-0000', N'GRID YT80', CAST(N'2023-11-17T23:52:01.607' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2509', N'W-GRID-YU67XXX-CACS-02-0000', N'GRID YU67', CAST(N'2023-11-17T23:52:01.687' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'251', N'W-CV02-D31RDFX-C03N-NL-DG00', N'CV D31R-DF BLACK HD-ORANGE', CAST(N'2023-11-17T23:48:37.800' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2510', N'W-GRID-YU76XXX-CACS-02-0000', N'GRID YU76', CAST(N'2023-11-17T23:52:01.767' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2511', N'W-GRID-YW73XXX-CACS-06-0000', N'GRID YW73', CAST(N'2023-11-17T23:52:01.850' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2512', N'W-GRID-YW75XXX-CACS-06-0000', N'GRID YW75', CAST(N'2023-11-17T23:52:01.930' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2513', N'W-GRID-YW82XXX-CACS-06-0000', N'GRID YW82', CAST(N'2023-11-17T23:52:02.013' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2514', N'W-GRID-YG85PXX-CACS-02-0000', N'GRID YG85-POSITIVE', CAST(N'2023-11-17T23:52:02.100' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2515', N'W-GRID-YG87NXX-CACS-02-0000', N'GRID YG87-NEGATIVE', CAST(N'2023-11-17T23:52:02.187' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2516', N'W-GRID-YH87XXX-CACS-06-0000', N'GRID YH87', CAST(N'2023-11-17T23:52:02.267' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2517', N'W-GRID-YL80XXX-CACS-02-0000', N'GRID YL80', CAST(N'2023-11-17T23:52:02.353' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2518', N'W-GRID-YL84XXX-CACS-02-0000', N'GRID YL84', CAST(N'2023-11-17T23:52:02.437' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2519', N'W-GRID-YM84NXX-CACS-02-0000', N'GRID YM84-NEGATIVE', CAST(N'2023-11-17T23:52:02.523' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'252', N'W-CV02-D31RDFX-C06N-NL-DG00', N'CV D31R-DF BLACK HD-WHITE', CAST(N'2023-11-17T23:48:37.883' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2520', N'W-GRID-YM84PXX-CACS-02-0000', N'GRID YM84-POSITIVE', CAST(N'2023-11-17T23:52:02.607' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2521', N'W-GRID-YS67XXX-CACS-01-0000', N'GRID YS67', CAST(N'2023-11-17T23:52:02.690' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2522', N'W-GRID-YS76XXX-CACS-01-0000', N'GRID YS76', CAST(N'2023-11-17T23:52:02.773' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2523', N'W-GRID-YT71XXX-CACS-01-0000', N'GRID YT71', CAST(N'2023-11-17T23:52:02.857' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2524', N'W-GRID-CM84XXX-SBCS-02-0000', N'GRID CM84', CAST(N'2023-11-17T23:52:02.940' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2525', N'W-GRID-CM87XXX-SBCS-02-0000', N'GRID CM87', CAST(N'2023-11-17T23:52:03.020' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2526', N'W-GRID-CR82XXX-SBCS-02-0000', N'GRID CR82', CAST(N'2023-11-17T23:52:03.103' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2527', N'W-GRID-CR87XXX-SBCS-02-0000', N'GRID CR87', CAST(N'2023-11-17T23:52:03.187' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2528', N'W-GRID-DF72XXX-SBCS-01-0000', N'GRID DF72', CAST(N'2023-11-17T23:52:03.267' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2529', N'W-GRID-DF78XXX-SBCS-01-0000', N'GRID DF78', CAST(N'2023-11-17T23:52:03.350' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'253', N'W-CV02-D31RXXX-C02N-NL-DG00', N'CV D31R BLACK HD-BLACK', CAST(N'2023-11-17T23:48:37.970' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2530', N'W-GRID-M87XXXX-CACS-08-0000', N'GRID M87', CAST(N'2023-11-17T23:52:03.430' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2531', N'W-GRID-WG83PXX-CAPC-00-0000', N'GRID WG83-POSITIVE', CAST(N'2023-11-17T23:52:03.520' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2532', N'W-GRID-WG87NXX-CAPC-00-0000', N'GRID WG87-NEGATIVE', CAST(N'2023-11-17T23:52:03.600' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2533', N'W-GRID-WM84PXX-CAPS-00-0000', N'GRID WM84-POSITIVE', CAST(N'2023-11-17T23:52:03.687' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2534', N'W-GRID-WM85NXX-CAPS-00-0000', N'GRID WM85-NEGATIVE', CAST(N'2023-11-17T23:52:03.777' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2535', N'W-GRID-YA82XXX-CACS-02-0000', N'GRID YA82', CAST(N'2023-11-17T23:52:03.860' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2536', N'W-GRID-YA85XXX-CACS-02-0000', N'GRID YA85', CAST(N'2023-11-17T23:52:03.940' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2537', N'W-GRID-YC62XXX-CACS-02-0000', N'GRID YC62', CAST(N'2023-11-17T23:52:04.023' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2538', N'W-GRID-YC70XXX-CACS-02-0000', N'GRID YC70', CAST(N'2023-11-17T23:52:04.107' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2539', N'W-GRID-YD85XXX-CACS-02-0000', N'GRID YD85', CAST(N'2023-11-17T23:52:04.187' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'254', N'W-CV02-D31RXXX-C03N-NL-DG00', N'CV D31R BLACK HD-ORANGE', CAST(N'2023-11-17T23:48:38.050' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2540', N'W-GRID-YG80HDX-CACS-02-0000', N'GRID YG80-HD', CAST(N'2023-11-17T23:52:04.270' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2541', N'W-GRID-YG80XXX-CACS-02-0000', N'GRID YG80', CAST(N'2023-11-17T23:52:04.360' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2542', N'W-GRID-YG82HDX-CACS-02-0000', N'GRID YG82-HD', CAST(N'2023-11-17T23:52:04.440' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2543', N'W-GRID-YG82XXX-CACS-02-0000', N'GRID YG82', CAST(N'2023-11-17T23:52:04.523' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2544', N'W-GRID-YG85NXX-CACS-02-0000', N'GRID YG85-NEGATIVE', CAST(N'2023-11-17T23:52:04.603' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2545', N'W-GRID-YF78XXX-CACS-01-0000', N'GRID YF78', CAST(N'2023-11-17T23:52:04.687' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2546', N'K-KBDC-115F51X-RSC0-IN-PPA0', N'K-BOX 115F51 INCOE PRE POWER', CAST(N'2023-11-17T23:52:04.763' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2547', N'K-KBDC-115F51X-RSC0-IN-PPB0', N'K-BOX 115F51 HD INCOE PRE PWR', CAST(N'2023-11-17T23:52:04.847' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2548', N'K-KBDC-115F51X-RSC0-IN-XPB0', N'K-BOX 115F51 INCOE XP - SUD', CAST(N'2023-11-17T23:52:04.930' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2549', N'K-KBDC-115F51X-RSC0-IN-XPC0', N'K-BOX 115F51 INCOE XP - UAE', CAST(N'2023-11-17T23:52:05.013' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'255', N'W-CV02-D31RXXX-C06N-NL-DG00', N'CV D31R BLACK HD-WHITE', CAST(N'2023-11-17T23:48:38.137' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2550', N'K-KBDC-115F51X-RSC0-RA-00A0', N'K-BOX 115F51 RAPTOR', CAST(N'2023-11-17T23:52:05.097' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2551', N'K-KBDC-544XXXX-RSC0-IN-XPA0', N'K-BOX 544 INCOE XTRA POWER', CAST(N'2023-11-17T23:52:05.180' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2552', N'K-KBDC-544XXXX-RSC0-IN-XPC0', N'K-BOX 544 INCOE XP - UAE', CAST(N'2023-11-17T23:52:05.263' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2553', N'K-KBDC-544XXXX-RSC0-QU-00A0', N'K-BOX 544 QUANTUM', CAST(N'2023-11-17T23:52:05.350' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2554', N'K-KBDC-544XXXX-RSC0-QU-00B0', N'K-BOX 544 QUANTUM - MII', CAST(N'2023-11-17T23:52:05.437' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2555', N'K-KBDC-544XXXX-RSC0-QU-MFA0', N'K-BOX 544 QUANTUM MF', CAST(N'2023-11-17T23:52:05.527' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2556', N'K-KBDC-544XXXX-TTSB-AP-MFA0', N'K-BOX 544 ASPIRA MF', CAST(N'2023-11-17T23:52:05.607' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2557', N'K-KBDC-544XXXX-TTSB-IN-MFA0', N'K-BOX 544 INCOE MF', CAST(N'2023-11-17T23:52:05.690' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2558', N'K-KBDC-544XXXX-TTSB-IN-MFB0', N'K-BOX 544 INCOE MF - UAE', CAST(N'2023-11-17T23:52:05.773' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2559', N'K-KBDC-544XXXX-TTSB-IN-MFC0', N'K-BOX 544 INCOE MF - DOM', CAST(N'2023-11-17T23:52:05.860' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'256', N'W-CV02-D31RXXX-D02C-NL-00T0', N'CV D31R MF BLACK CENTER BA-IN', CAST(N'2023-11-17T23:48:38.220' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2560', N'K-KBDC-544XXXX-TTSB-OH-MFA0', N'K-BOX 544 OHAYO MF', CAST(N'2023-11-17T23:52:05.940' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2561', N'K-KBDC-544XXXX-TTSB-OH-MFB0', N'K-BOX 544 OHAYO MF - UAE', CAST(N'2023-11-17T23:52:06.023' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2562', N'K-KBDC-545XXXX-RSC0-AP-PRA0', N'K-BOX 545 ASPIRA PREMIUM', CAST(N'2023-11-17T23:52:06.107' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2563', N'K-KBDC-545XXXX-RSC0-IN-XPA0', N'K-BOX 545 INCOE XTRA POWER', CAST(N'2023-11-17T23:52:06.190' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2564', N'K-KBDC-545XXXX-RSC0-QU-00A0', N'K-BOX 545 QUANTUM', CAST(N'2023-11-17T23:52:06.270' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2565', N'K-KBDC-545XXXX-RSC0-QU-00B0', N'K-BOX 545 QUANTUM - MII', CAST(N'2023-11-17T23:52:06.353' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2566', N'K-KBDC-545XXXX-RSC0-QU-MFA0', N'K-BOX 545 QUANTUM MF', CAST(N'2023-11-17T23:52:06.433' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2567', N'K-KBDC-545XXXX-TTSB-IN-MFA0', N'K-BOX 545 INCOE MF', CAST(N'2023-11-17T23:52:06.523' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2568', N'K-KBDC-55559XX-RSC0-IN-PPA0', N'K-BOX 555-59 INCOE PRE POWER', CAST(N'2023-11-17T23:52:06.607' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2569', N'K-KBDC-55559XX-RSC0-IN-XPB0', N'K-BOX 555-59 INCOE XP - SUD', CAST(N'2023-11-17T23:52:06.687' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'257', N'W-CV02-D31RXXX-D02N-NL-00T0', N'CV D31R MF BLACK NO BA-IN', CAST(N'2023-11-17T23:48:38.303' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2570', N'K-KBDC-55565XX-RSC0-IN-PPA0', N'K-BOX 555-65 INCOE PRE POWER', CAST(N'2023-11-17T23:52:06.770' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2571', N'K-KBDC-55565XX-RSC0-IN-XPB0', N'K-BOX 555-65 INCOE XP - SUD', CAST(N'2023-11-17T23:52:06.850' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2572', N'K-KBDC-555XXXX-RSC0-AP-PRA0', N'K-BOX 555 ASPIRA PREMIUM', CAST(N'2023-11-17T23:52:06.933' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2573', N'K-KBDC-115E41R-RSC0-NI-PRA0', N'K-BOX 115E41R NIKO PREMIUM', CAST(N'2023-11-17T23:52:07.013' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2574', N'K-KBDC-555XXXX-RSC0-AS-HMA0', N'K-BOX 555 ASAHI HPE MF', CAST(N'2023-11-17T23:52:07.097' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2575', N'K-KBDC-555XXXX-RSC0-IN-XPA0', N'K-BOX 555 INCOE XTRA POWER', CAST(N'2023-11-17T23:52:07.177' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2576', N'K-KBDC-555XXXX-RSC0-IN-XPC0', N'K-BOX 555 INCOE XP - UAE', CAST(N'2023-11-17T23:52:07.257' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2577', N'K-KBDC-566XXXX-TTSB-OH-MFA0', N'K-BOX 566 OHAYO MF', CAST(N'2023-11-17T23:52:07.340' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2578', N'K-KBDC-566XXXX-TTSB-OH-MFB0', N'K-BOX 566 OHAYO MF - UAE', CAST(N'2023-11-17T23:52:07.420' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2579', N'K-KBDC-57412XX-RSC0-IN-XPB0', N'K-BOX 574-12 INCOE XP - SUD', CAST(N'2023-11-17T23:52:07.500' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'258', N'W-CV02-D31RXXX-D02S-NL-00T0', N'CV D31R MF BLACK SIDE BA-IN', CAST(N'2023-11-17T23:48:38.383' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2580', N'K-KBDC-57540XX-RSC0-LU-MFA0', N'K-BOX 575-40 LUCAS MF', CAST(N'2023-11-17T23:52:07.587' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2581', N'K-KBDC-58024XX-RSC0-IN-PPA0', N'K-BOX 580-24 INCOE PRE POWER', CAST(N'2023-11-17T23:52:07.670' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2582', N'K-KBDC-58827XX-RSC0-IN-XPB0', N'K-BOX 588-27 INCOE XP - SUD', CAST(N'2023-11-17T23:52:07.753' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2583', N'K-KBDC-588XXXX-RSC0-AP-PRA0', N'K-BOX 588 ASPIRA PREMIUM', CAST(N'2023-11-17T23:52:07.833' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2584', N'K-KBDC-588XXXX-RSC0-AS-HMA0', N'K-BOX 588 ASAHI HPE MF', CAST(N'2023-11-17T23:52:07.913' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2585', N'K-KBDC-588XXXX-RSC0-IN-XPA0', N'K-BOX 588 INCOE XTRA POWER', CAST(N'2023-11-17T23:52:08.000' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2586', N'K-KBDC-588XXXX-RSC0-IN-XPC0', N'K-BOX 588 INCOE XP - UAE', CAST(N'2023-11-17T23:52:08.093' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2587', N'K-KBDC-588XXXX-RSC0-QU-00A0', N'K-BOX 588 QUANTUM', CAST(N'2023-11-17T23:52:08.187' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2588', N'K-KBDC-588XXXX-RSC0-QU-00B0', N'K-BOX 588 QUANTUM - MII', CAST(N'2023-11-17T23:52:08.287' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2589', N'K-KBDC-588XXXX-RSC0-QU-MFA0', N'K-BOX 588 QUANTUM MF', CAST(N'2023-11-17T23:52:08.383' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'259', N'W-CV02-E41LXXX-0MHN-NL-DG00', N'CV E41L BLACK', CAST(N'2023-11-17T23:48:38.467' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2590', N'K-KBDC-588XXXX-TTSB-AP-MFA0', N'K-BOX 588 ASPIRA MF', CAST(N'2023-11-17T23:52:08.477' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2591', N'K-KBDC-588XXXX-TTSB-IN-MFA0', N'K-BOX 588 INCOE MF', CAST(N'2023-11-17T23:52:08.573' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2592', N'K-KBDC-588XXXX-TTSB-IN-MFB0', N'K-BOX 588 INCOE MF - UAE', CAST(N'2023-11-17T23:52:08.667' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2593', N'K-KBDC-34B20RX-RSC0-IN-GOA0', N'K-BOX 34B20R INCOE GOLD', CAST(N'2023-11-17T23:52:08.743' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2594', N'K-KBDC-36B20LS-RSC0-IN-GOA0', N'K-BOX 36B20LS INCOE GOLD', CAST(N'2023-11-17T23:52:08.827' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2595', N'K-KBDC-36B20LS-RSC0-IN-PPA0', N'K-BOX 36B20LS INCOE PRE POWER', CAST(N'2023-11-17T23:52:08.907' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2596', N'K-KBDC-36B20LX-RSC0-AP-PRB0', N'K-BOX 36B20L ASPIRA PRE - AOP', CAST(N'2023-11-17T23:52:08.987' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2597', N'K-KBDC-36B20LX-RSC0-IN-GOA0', N'K-BOX 36B20L INCOE GOLD', CAST(N'2023-11-17T23:52:09.070' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2598', N'K-KBDC-36B20LX-RSC0-IN-PPA0', N'K-BOX 36B20L INCOE PRE POWER', CAST(N'2023-11-17T23:52:09.153' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2599', N'K-KBDC-36B20LX-RSC0-IN-XPB0', N'K-BOX 36B20L INCOE XP - SUD', CAST(N'2023-11-17T23:52:09.233' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'26', N'K-CV10-S65XXXX-0MHN-NL-00M0', N'CV S65 BLACK (ABS)', CAST(N'2023-11-17T23:48:18.500' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'260', N'W-CV02-E41LXXX-0MHS-NL-00T0', N'CV E41L MF BLACK', CAST(N'2023-11-17T23:48:38.553' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2600', N'K-KBDC-555XXXX-RSC0-QU-00A0', N'K-BOX 555 QUANTUM', CAST(N'2023-11-17T23:52:09.317' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2601', N'K-KBDC-555XXXX-RSC0-QU-00B0', N'K-BOX 555 QUANTUM - MII', CAST(N'2023-11-17T23:52:09.403' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2602', N'K-KBDC-555XXXX-RSC0-QU-MFA0', N'K-BOX 555 QUANTUM MF', CAST(N'2023-11-17T23:52:09.483' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2603', N'K-KBDC-555XXXX-TTSB-AP-MFA0', N'K-BOX 555 ASPIRA MF', CAST(N'2023-11-17T23:52:09.570' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2604', N'K-KBDC-555XXXX-TTSB-IN-MFA0', N'K-BOX 555 INCOE MF', CAST(N'2023-11-17T23:52:09.653' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2605', N'K-KBDC-555XXXX-TTSB-IN-MFB0', N'K-BOX 555 INCOE MF - UAE', CAST(N'2023-11-17T23:52:09.733' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2606', N'K-KBDC-555XXXX-TTSB-IN-MFC0', N'K-BOX 555 INCOE MF - DOM', CAST(N'2023-11-17T23:52:09.817' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2607', N'K-KBDC-555XXXX-TTSB-OH-MFA0', N'K-BOX 555 OHAYO MF', CAST(N'2023-11-17T23:52:09.897' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2608', N'K-KBDC-555XXXX-TTSB-OH-MFB0', N'K-BOX 555 OHAYO MF - UAE', CAST(N'2023-11-17T23:52:09.980' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2609', N'K-KBDC-55D23LX-RSC0-AP-PRB0', N'K-BOX 55D23L ASPIRA PRE - AOP', CAST(N'2023-11-17T23:52:10.060' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'261', N'W-CV02-E41RDFX-0MHN-NL-DG00', N'CV E41R-DF BLACK', CAST(N'2023-11-17T23:48:38.637' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2610', N'K-KBDC-55D23LX-RSC0-IN-GOA0', N'K-BOX 55D23L INCOE GOLD', CAST(N'2023-11-17T23:52:10.143' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2611', N'K-KBDC-55D23LX-RSC0-IN-PPA0', N'K-BOX 55D23L INCOE PRE POWER', CAST(N'2023-11-17T23:52:10.227' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2612', N'K-KBDC-55D23RX-RSC0-IN-GOA0', N'K-BOX 55D23R INCOE GOLD', CAST(N'2023-11-17T23:52:10.310' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2613', N'K-KBDC-55D23RX-RSC0-IN-PPA0', N'K-BOX 55D23R INCOE PRE POWER', CAST(N'2023-11-17T23:52:10.397' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2614', N'K-KBDC-55D26LX-RSC0-IN-GOA0', N'K-BOX 55D26L INCOE GOLD', CAST(N'2023-11-17T23:52:10.480' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2615', N'K-KBDC-55D26LX-RSC0-IN-PPA0', N'K-BOX 55D26L INCOE PRE POWER', CAST(N'2023-11-17T23:52:10.567' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2616', N'K-KBDC-55D26LX-RSC0-RA-00A0', N'K-BOX 55D26L RAPTOR', CAST(N'2023-11-17T23:52:10.650' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2617', N'K-KBDC-55D26RX-RSC0-AP-PRB0', N'K-BOX 55D26R ASPIRA PRE - AOP', CAST(N'2023-11-17T23:52:10.733' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2618', N'K-KBDC-55D26RX-RSC0-IN-GOA0', N'K-BOX 55D26R INCOE GOLD', CAST(N'2023-11-17T23:52:10.820' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2619', N'K-KBDC-55D26RX-RSC0-IN-PPA0', N'K-BOX 55D26R INCOE PRE POWER', CAST(N'2023-11-17T23:52:10.903' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'262', N'W-CV01-75D31LX-C03N-NL-DG00', N'CV 75D31L BLUE HD-ORANGE', CAST(N'2023-11-17T23:48:38.720' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2620', N'K-KBDC-55D26RX-RSC0-RA-00A0', N'K-BOX 55D26R RAPTOR', CAST(N'2023-11-17T23:52:10.990' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2621', N'K-KBDC-56048XX-RSC0-RA-00A0', N'K-BOX 560-48 RAPTOR', CAST(N'2023-11-17T23:52:11.073' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2622', N'K-KBDC-56219XX-RSC0-LU-MFA0', N'K-BOX 562-19 LUCAS MF', CAST(N'2023-11-17T23:52:11.157' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2623', N'K-KBDC-56221XX-RSC0-LU-MFA0', N'K-BOX 562-21 LUCAS MF', CAST(N'2023-11-17T23:52:11.240' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2624', N'K-KBDC-45B20LX-RSC0-LU-MFA0', N'K-BOX 45B20L LUCAS MF', CAST(N'2023-11-17T23:52:11.327' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2625', N'K-KBDC-46B24LS-RSC0-AP-PRB0', N'K-BOX 46B24LS ASPIRA PRE - AOP', CAST(N'2023-11-17T23:52:11.420' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2626', N'K-KBDC-46B24LS-RSC0-IN-GOA0', N'K-BOX 46B24LS INCOE GOLD', CAST(N'2023-11-17T23:52:11.503' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2627', N'K-KBDC-46B24LS-RSC0-IN-PPA0', N'K-BOX 46B24LS INCOE PRE POWER', CAST(N'2023-11-17T23:52:11.587' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2628', N'K-KBDC-46B24LS-RSC0-IN-XPB0', N'K-BOX 46B24LS INCOE XP - SUD', CAST(N'2023-11-17T23:52:11.673' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2629', N'K-KBDC-46B24LS-TTSB-IN-MFC0', N'K-BOX 46B24LS INCOE MF - DOM', CAST(N'2023-11-17T23:52:11.757' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'263', N'W-CV01-75D31LX-C06N-NL-DG00', N'CV 75D31L BLUE HD-WHITE', CAST(N'2023-11-17T23:48:38.803' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2630', N'K-KBDC-46B24LX-RSC0-AP-PRB0', N'K-BOX 46B24L ASPIRA PRE - AOP', CAST(N'2023-11-17T23:52:11.840' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2631', N'K-KBDC-46B24LX-RSC0-IN-GOA0', N'K-BOX 46B24L INCOE GOLD', CAST(N'2023-11-17T23:52:11.927' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2632', N'K-KBDC-46B24LX-RSC0-IN-PPA0', N'K-BOX 46B24L INCOE PRE POWER', CAST(N'2023-11-17T23:52:12.010' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2633', N'K-KBDC-46B24LX-RSC0-RA-00A0', N'K-BOX 46B24L RAPTOR', CAST(N'2023-11-17T23:52:12.097' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2634', N'K-KBDC-46B24LX-TTSB-IN-MFC0', N'K-BOX 46B24L INCOE MF - DOM', CAST(N'2023-11-17T23:52:12.180' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2635', N'K-KBDC-46B24RS-RSC0-IN-GOA0', N'K-BOX 46B24RS INCOE GOLD', CAST(N'2023-11-17T23:52:12.260' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2636', N'K-KBDC-46B24RS-RSC0-IN-PPA0', N'K-BOX 46B24RS INCOE PRE POWER', CAST(N'2023-11-17T23:52:12.347' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2637', N'K-KBDC-46B24RX-RSC0-AP-PRB0', N'K-BOX 46B24R ASPIRA PRE - AOP', CAST(N'2023-11-17T23:52:12.430' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2638', N'K-KBDC-46B24RX-RSC0-IN-GOA0', N'K-BOX 46B24R INCOE GOLD', CAST(N'2023-11-17T23:52:12.513' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2639', N'K-KBDC-46B24RX-RSC0-IN-PPA0', N'K-BOX 46B24R INCOE PRE POWER', CAST(N'2023-11-17T23:52:12.597' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'264', N'W-CV01-75D31RX-C03N-NL-DG00', N'CV 75D31R BLUE HD-ORANGE', CAST(N'2023-11-17T23:48:38.883' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2640', N'K-KBDC-46B24RX-TTSB-IN-MFC0', N'K-BOX 46B24R INCOE MF - DOM', CAST(N'2023-11-17T23:52:12.680' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2641', N'K-KBDC-48D26LX-RSC0-IN-GOA0', N'K-BOX 48D26L INCOE GOLD', CAST(N'2023-11-17T23:52:12.770' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2642', N'K-KBDC-48D26LX-RSC0-IN-PPA0', N'K-BOX 48D26L INCOE PRE POWER', CAST(N'2023-11-17T23:52:12.853' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2643', N'K-KBDC-48D26LX-RSC0-QU-MFA0', N'K-BOX 48D26L QUANTUM MF', CAST(N'2023-11-17T23:52:12.933' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2644', N'K-KBDC-48D26RX-RSC0-AP-PRA0', N'K-BOX 48D26R ASPIRA PREMIUM', CAST(N'2023-11-17T23:52:13.013' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2645', N'K-KBDC-48D26RX-RSC0-AP-PRB0', N'K-BOX 48D26R ASPIRA PRE - AOP', CAST(N'2023-11-17T23:52:13.093' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2646', N'K-KBDC-G51XXXX-TTSB-IN-MFA0', N'K-BOX G51 INCOE MF', CAST(N'2023-11-17T23:52:13.180' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2647', N'K-KBDC-G51XXXX-TTSB-IN-MFB0', N'K-BOX G51 INCOE MF - UAE', CAST(N'2023-11-17T23:52:13.260' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2648', N'K-KBDC-G51XXXX-TTSB-OH-MFA0', N'K-BOX G51 OHAYO MF', CAST(N'2023-11-17T23:52:13.343' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2649', N'K-KBDC-G51XXXX-TTSB-QU-MFA0', N'K-BOX G51 QUANTUM MF', CAST(N'2023-11-17T23:52:13.427' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'265', N'W-CV01-75D31RX-C06N-NL-DG00', N'CV 75D31R BLUE HD-WHITE', CAST(N'2023-11-17T23:48:39.147' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2650', N'K-KBDC-GTZ5SXX-TTAB-AP-KIA0', N'K-BOX GTZ-5S ASPIRA KIT', CAST(N'2023-11-17T23:52:13.510' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2651', N'K-KBDC-GTZ5SXX-TTAB-QU-FPA0', N'K-BOX GTZ-5S QUANTUM FED KIT', CAST(N'2023-11-17T23:52:13.590' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2652', N'K-KBDC-GTZ6VXX-TTAB-AP-KIA0', N'K-BOX GTZ-6V ASPIRA KIT', CAST(N'2023-11-17T23:52:13.673' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2653', N'K-KBDC-H52XXXX-RSC0-AP-PRA0', N'K-BOX H52 ASPIRA PREMIUM', CAST(N'2023-11-17T23:52:13.757' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2654', N'K-KBDC-36B20LX-RSC0-RA-00A0', N'K-BOX 36B20L RAPTOR', CAST(N'2023-11-17T23:52:13.837' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2655', N'K-KBDC-36B20LX-TTSB-IN-MFC0', N'K-BOX 36B20L INCOE MF - DOM', CAST(N'2023-11-17T23:52:13.917' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2656', N'K-KBDC-36B20RX-RSC0-AP-PRB0', N'K-BOX 36B20R ASPIRA PRE - AOP', CAST(N'2023-11-17T23:52:14.000' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2657', N'K-KBDC-36B20RX-RSC0-IN-GOA0', N'K-BOX 36B20R INCOE GOLD', CAST(N'2023-11-17T23:52:14.087' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2658', N'K-KBDC-36B20RX-RSC0-IN-PPA0', N'K-BOX 36B20R INCOE PRE POWER', CAST(N'2023-11-17T23:52:14.167' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2659', N'K-KBDC-36B20RX-TTSB-IN-MFC0', N'K-BOX 36B20R INCOE MF - DOM', CAST(N'2023-11-17T23:52:14.250' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'266', N'W-CV01-95D31LX-C01N-NL-DG00', N'CV 95D31L BLUE HD-BLUE', CAST(N'2023-11-17T23:48:39.467' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2660', N'K-KBWK-555XXXX-RSC0-AS-MMA0', N'K-BOX 555 ASAHI MEN MF', CAST(N'2023-11-17T23:52:14.333' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2661', N'K-KBWK-555XXXX-RSC0-OH-HDA0', N'K-BOX 555 OHAYO', CAST(N'2023-11-17T23:52:14.420' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2662', N'K-KBWK-555XXXX-RSC0-UL-00A0', N'K-BOX 555 ULTRA-X', CAST(N'2023-11-17T23:52:14.510' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2663', N'K-KBWK-55D26RX-RSC0-TN-HDA0', N'K-BOX 55D26R TRAKNUS HDT', CAST(N'2023-11-17T23:52:14.590' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2664', N'K-KBWK-566XXXX-RSC0-AS-HPA0', N'K-BOX 566 ASAHI HPE', CAST(N'2023-11-17T23:52:14.677' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2665', N'K-KBDC-105D31L-RSC0-IN-XPB0', N'K-BOX 105D31L INCOE XP - SUD', CAST(N'2023-11-17T23:52:14.757' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2666', N'K-KBDC-105D31R-RSC0-IN-XPB0', N'K-BOX 105D31R INCOE XP - SUD', CAST(N'2023-11-17T23:52:14.837' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2667', N'K-KBDC-115E41R-RSC0-LU-MFA0', N'K-BOX 115E41R LUCAS MF', CAST(N'2023-11-17T23:52:14.920' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2668', N'K-KBDC-56638XX-RSC0-IN-XPB0', N'K-BOX 566-38 INCOE XP - SUD', CAST(N'2023-11-17T23:52:15.000' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2669', N'K-KBDC-56638XX-RSC0-LU-MFA0', N'K-BOX 566-38 LUCAS MF', CAST(N'2023-11-17T23:52:15.080' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'267', N'W-CV00-544LXXX-0MHN-NL-DG00', N'CV 544L NATURAL', CAST(N'2023-11-17T23:48:39.557' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2670', N'K-KBDC-566XXXX-RSC0-AS-HMA0', N'K-BOX 566 ASAHI HPE MF', CAST(N'2023-11-17T23:52:15.163' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2671', N'K-KBDC-566XXXX-RSC0-IN-XPA0', N'K-BOX 566 INCOE XTRA POWER', CAST(N'2023-11-17T23:52:15.247' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2672', N'K-KBDC-566XXXX-RSC0-IN-XPC0', N'K-BOX 566 INCOE XP - UAE', CAST(N'2023-11-17T23:52:15.327' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2673', N'K-KBDC-566XXXX-RSC0-QU-00A0', N'K-BOX 566 QUANTUM', CAST(N'2023-11-17T23:52:15.410' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2674', N'K-KBDC-190H52X-TTSB-IN-MFC0', N'K-BOX 190H52 INCOE MF - DOM', CAST(N'2023-11-17T23:52:15.493' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2675', N'K-KBDC-210H52X-RSC0-LU-MFA0', N'K-BOX 210H52 LUCAS MF', CAST(N'2023-11-17T23:52:15.577' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2676', N'K-KBDC-210H52X-RSC0-NI-PRA0', N'K-BOX 210H52 NIKO PREMIUM', CAST(N'2023-11-17T23:52:15.660' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2677', N'K-KBDC-32B20LS-RSC0-IN-GOA0', N'K-BOX 32B20LS INCOE GOLD', CAST(N'2023-11-17T23:52:15.743' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2678', N'K-KBDC-32B20LS-RSC0-IN-PPA0', N'K-BOX 32B20LS INCOE PRE POWER', CAST(N'2023-11-17T23:52:15.823' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2679', N'K-KBDC-32B20LS-RSC0-IN-XPB0', N'K-BOX 32B20LS INCOE XP - SUD', CAST(N'2023-11-17T23:52:15.903' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'268', N'W-CV00-544RXXX-0MHN-NL-DG00', N'CV 544R NATURAL', CAST(N'2023-11-17T23:48:39.640' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2680', N'K-KBDC-32B20LX-RSC0-AP-PRB0', N'K-BOX 32B20L ASPIRA PRE - AOP', CAST(N'2023-11-17T23:52:15.983' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2681', N'K-KBDC-48D26RX-RSC0-IN-GOA0', N'K-BOX 48D26R INCOE GOLD', CAST(N'2023-11-17T23:52:16.073' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2682', N'K-KBDC-48D26RX-RSC0-IN-PPA0', N'K-BOX 48D26R INCOE PRE POWER', CAST(N'2023-11-17T23:52:16.153' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2683', N'K-KBDC-48D26RX-RSC0-IN-XPB0', N'K-BOX 48D26R INCOE XP - SUD', CAST(N'2023-11-17T23:52:16.237' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2684', N'K-KBDC-48D26RX-RSC0-IN-XPC0', N'K-BOX 48D26R INCOE XP - UAE', CAST(N'2023-11-17T23:52:16.317' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2685', N'K-KBDC-50B24LX-RSC0-LU-MFA0', N'K-BOX 50B24L LUCAS MF', CAST(N'2023-11-17T23:52:16.400' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2686', N'K-KBDC-50B24RX-RSC0-LU-MFA0', N'K-BOX 50B24R LUCAS MF', CAST(N'2023-11-17T23:52:16.480' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2687', N'K-KBDC-544XXXX-RSC0-AS-HMA0', N'K-BOX 544 ASAHI HPE MF', CAST(N'2023-11-17T23:52:16.560' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2688', N'K-KBDC-115F51X-TTSB-IN-MFC0', N'K-BOX 115F51 INCOE MF - DOM', CAST(N'2023-11-17T23:52:16.643' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2689', N'K-KBDC-115F51X-TTSB-OH-MFB0', N'K-BOX 115F51 OHAYO MF - UAE', CAST(N'2023-11-17T23:52:16.727' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'269', N'W-CV00-545LXXX-0MHN-NL-DG00', N'CV 545L NATURAL', CAST(N'2023-11-17T23:48:39.723' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2690', N'K-KBDC-125D31L-RSC0-LU-MFA0', N'K-BOX 125D31L LUCAS MF', CAST(N'2023-11-17T23:52:16.803' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2691', N'K-KBDC-125D31R-RSC0-LU-MFA0', N'K-BOX 125D31R LUCAS MF', CAST(N'2023-11-17T23:52:16.887' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2692', N'K-KBDC-135F51X-RSC0-LU-MFA0', N'K-BOX 135F51 LUCAS MF', CAST(N'2023-11-17T23:52:16.967' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2693', N'K-KBDC-135F51X-RSC0-NI-PRA0', N'K-BOX 135F51 NIKO PREMIUM', CAST(N'2023-11-17T23:52:17.047' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2694', N'K-KBDC-145G51L-RSC0-IN-PPA0', N'K-BOX 145G51L INCOE PRE POWER', CAST(N'2023-11-17T23:52:17.130' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2695', N'K-KBDC-145G51X-RSC0-IN-PPA0', N'K-BOX 145G51 INCOE PRE POWER', CAST(N'2023-11-17T23:52:17.210' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2696', N'K-KBDC-145G51X-RSC0-IN-PPB0', N'K-BOX 145G51 HD INCOE PRE PWR', CAST(N'2023-11-17T23:52:17.290' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2697', N'K-KBDC-145G51X-RSC0-IN-XPB0', N'K-BOX 145G51 INCOE XP - SUD', CAST(N'2023-11-17T23:52:17.373' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2698', N'K-KBDC-145G51X-RSC0-IN-XPC0', N'K-BOX 145G51 INCOE XP - UAE', CAST(N'2023-11-17T23:52:17.457' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2699', N'K-KBDC-145G51X-RSC0-RA-00A0', N'K-BOX 145G51 RAPTOR', CAST(N'2023-11-17T23:52:17.537' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'27', N'K-CV10-S9XXXXX-0MHN-NL-00M0', N'CV S9 BLACK (ABS)', CAST(N'2023-11-17T23:48:18.583' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'270', N'W-CV00-545LXXX-0MHN-NL-DQ00', N'CV 545L NATURAL DG-QR', CAST(N'2023-11-17T23:48:39.810' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2700', N'K-KBDC-145G51X-TTSB-IN-MFC0', N'K-BOX 145G51 INCOE MF - DOM', CAST(N'2023-11-17T23:52:17.620' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2701', N'K-KBDC-145G51X-TTSB-OH-MFB0', N'K-BOX 145G51 OHAYO MF - UAE', CAST(N'2023-11-17T23:52:17.700' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2702', N'K-KBDC-170G51X-RSC0-LU-MFA0', N'K-BOX 170G51 LUCAS MF', CAST(N'2023-11-17T23:52:17.790' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2703', N'K-KBDC-170G51X-RSC0-NI-PRA0', N'K-BOX 170G51 NIKO PREMIUM', CAST(N'2023-11-17T23:52:17.870' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2704', N'K-KBDC-190H52X-RSC0-IN-PPA0', N'K-BOX 190H52 INCOE PRE POWER', CAST(N'2023-11-17T23:52:17.957' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2705', N'K-KBDC-190H52X-RSC0-IN-PPB0', N'K-BOX 190H52 HD INCOE PRE PWR', CAST(N'2023-11-17T23:52:18.040' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2706', N'K-KBDC-190H52X-RSC0-IN-XPB0', N'K-BOX 190H52 INCOE XP - SUD', CAST(N'2023-11-17T23:52:18.127' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2707', N'K-KBDC-190H52X-RSC0-IN-XPC0', N'K-BOX 190H52 INCOE XP - UAE', CAST(N'2023-11-17T23:52:18.210' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2708', N'K-KBDC-190H52X-RSC0-OH-MFB0', N'K-BOX 190H52 OHAYO MF - UAE', CAST(N'2023-11-17T23:52:18.290' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2709', N'K-KBDC-190H52X-RSC0-QU-MFA0', N'K-BOX 190H52 QUANTUM MF', CAST(N'2023-11-17T23:52:18.373' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'271', N'W-CV00-545RXXX-0MHN-NL-DG00', N'CV 545R NATURAL', CAST(N'2023-11-17T23:48:39.900' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2710', N'K-KBDC-190H52X-RSC0-RA-00A0', N'K-BOX 190H52 RAPTOR', CAST(N'2023-11-17T23:52:18.457' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2711', N'K-KBDC-566XXXX-RSC0-QU-00B0', N'K-BOX 566 QUANTUM - MII', CAST(N'2023-11-17T23:52:18.543' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2712', N'K-KBDC-566XXXX-RSC0-QU-MFA0', N'K-BOX 566 QUANTUM MF', CAST(N'2023-11-17T23:52:18.627' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2713', N'K-KBDC-566XXXX-TTSB-AP-MFA0', N'K-BOX 566 ASPIRA MF', CAST(N'2023-11-17T23:52:18.723' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2714', N'K-KBDC-566XXXX-TTSB-IN-MFA0', N'K-BOX 566 INCOE MF', CAST(N'2023-11-17T23:52:18.817' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2715', N'K-KBDC-566XXXX-TTSB-IN-MFB0', N'K-BOX 566 INCOE MF - UAE', CAST(N'2023-11-17T23:52:18.913' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2716', N'K-KBDC-566XXXX-TTSB-IN-MFC0', N'K-BOX 566 INCOE MF - DOM', CAST(N'2023-11-17T23:52:19.010' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2717', N'K-KBDC-32B20LX-RSC0-IN-GOA0', N'K-BOX 32B20L INCOE GOLD', CAST(N'2023-11-17T23:52:19.107' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2718', N'K-KBDC-32B20LX-RSC0-IN-PPA0', N'K-BOX 32B20L INCOE PRE POWER', CAST(N'2023-11-17T23:52:19.210' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2719', N'K-KBDC-32B20LX-RSC0-IN-XPB0', N'K-BOX 32B20L INCOE XP - SUD', CAST(N'2023-11-17T23:52:19.310' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'272', N'W-CV00-545RXXX-0MHN-NL-DQ00', N'CV 545R NATURAL DG-QR', CAST(N'2023-11-17T23:48:39.980' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2720', N'K-KBDC-32B20RS-RSC0-IN-GOA0', N'K-BOX 32B20RS INCOE GOLD', CAST(N'2023-11-17T23:52:19.400' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2721', N'K-KBDC-32B20RS-RSC0-IN-PPA0', N'K-BOX 32B20RS INCOE PRE POWER', CAST(N'2023-11-17T23:52:19.483' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2722', N'K-KBDC-32B20RX-RSC0-AP-PRB0', N'K-BOX 32B20R ASPIRA PRE - AOP', CAST(N'2023-11-17T23:52:19.567' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2723', N'K-KBDC-32B20RX-RSC0-IN-GOA0', N'K-BOX 32B20R INCOE GOLD', CAST(N'2023-11-17T23:52:19.650' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2724', N'K-KBDC-32B20RX-RSC0-IN-PPA0', N'K-BOX 32B20R INCOE PRE POWER', CAST(N'2023-11-17T23:52:19.733' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2725', N'K-KBDC-32B20RX-TTSB-IN-MFC0', N'K-BOX 32B20R INCOE MF - DOM', CAST(N'2023-11-17T23:52:19.813' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2726', N'K-KBDC-32C24LX-RSC0-IN-PPA0', N'K-BOX 32C24L INCOE PRE POWER', CAST(N'2023-11-17T23:52:19.900' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2727', N'K-KBDC-32C24RX-RSC0-IN-PPA0', N'K-BOX 32C24R INCOE PRE POWER', CAST(N'2023-11-17T23:52:19.980' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2728', N'K-KBDC-32C24RX-RSC0-IN-XPB0', N'K-BOX 32C24R INCOE XP - SUD', CAST(N'2023-11-17T23:52:20.067' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2729', N'K-KBDC-32C24RX-RSC0-IN-XPC0', N'K-BOX 32C24R INCOE XP - UAE', CAST(N'2023-11-17T23:52:20.150' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'273', N'W-CV01-105D31L-C03N-NL-DG00', N'CV 105D31L BLUE HD-ORANGE', CAST(N'2023-11-17T23:48:40.100' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2730', N'K-KBDC-34B20LX-RSC0-IN-GOA0', N'K-BOX 34B20L INCOE GOLD', CAST(N'2023-11-17T23:52:20.233' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2731', N'K-KBDC-Q85LXXX-RSC0-LU-EFA0', N'K-BOX Q-85L LUCAS ISS-EFB', CAST(N'2023-11-17T23:52:20.320' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2732', N'K-KBDC-Q85LXXX-TTSB-IN-SSA0', N'K-BOX Q-85L INCOE ISS-EFB', CAST(N'2023-11-17T23:52:20.620' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2733', N'K-KBKR-115D31R-RSC0-AP-MFB0', N'K-BOX 115D31R ASPIRA MF - AOP', CAST(N'2023-11-17T23:52:22.167' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2734', N'K-KBKR-544XXXX-RSC0-BL-00A0', N'K-BOX 544 BLANK', CAST(N'2023-11-17T23:52:23.677' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2735', N'K-KBKR-545XXXX-RSC0-BL-00A0', N'K-BOX 545 BLANK', CAST(N'2023-11-17T23:52:25.210' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2736', N'K-KBKR-555XXXX-RSC0-BL-00A0', N'K-BOX 555 BLANK', CAST(N'2023-11-17T23:52:25.780' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2737', N'K-KBWK-145G51X-RSC0-AM-PRA0', N'K-BOX 145G51 ALL MAKES PRE', CAST(N'2023-11-17T23:52:25.870' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2738', N'K-KBWK-145G51X-RSC0-TN-00A0', N'K-BOX 145G51 TRAKNUS', CAST(N'2023-11-17T23:52:25.967' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2739', N'K-KBWK-145G51X-RSC0-TN-DMA0', N'K-BOX 145G51 TRAKNUS HDT MF', CAST(N'2023-11-17T23:52:26.053' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'274', N'W-CV01-105D31L-C06N-NL-DG00', N'CV 105D31L BLUE HD-WHITE', CAST(N'2023-11-17T23:48:40.203' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2740', N'K-KBWK-145G51X-RSC0-TR-00A0', N'K-BOX 145G51 TRANSPORT', CAST(N'2023-11-17T23:52:26.143' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2741', N'K-KBWK-145G51X-TTSB-AM-PMA0', N'K-BOX 145G51 ALL MAKES PRE MF', CAST(N'2023-11-17T23:52:26.233' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2742', N'K-KBDC-D26XXXX-RSC0-QU-MFA0', N'K-BOX D26 QUANTUM MF', CAST(N'2023-11-17T23:52:26.323' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2743', N'K-KBDC-D26XXXX-TTSB-AP-MFA0', N'K-BOX D26 ASPIRA MF', CAST(N'2023-11-17T23:52:26.410' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2744', N'K-KBDC-D26XXXX-TTSB-IN-MFA0', N'K-BOX D26 INCOE MF', CAST(N'2023-11-17T23:52:26.503' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2745', N'K-KBDC-D26XXXX-TTSB-IN-MFB0', N'K-BOX D26 INCOE MF - UAE', CAST(N'2023-11-17T23:52:26.603' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2746', N'K-KBDC-D26XXXX-TTSB-IN-MFC0', N'K-BOX D26 INCOE MF - DOM', CAST(N'2023-11-17T23:52:26.690' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2747', N'K-KBDC-D26XXXX-TTSB-OH-MFA0', N'K-BOX D26 OHAYO MF', CAST(N'2023-11-17T23:52:26.777' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2748', N'K-KBDC-D26XXXX-TTSB-OH-MFB0', N'K-BOX D26 OHAYO MF - UAE', CAST(N'2023-11-17T23:52:26.860' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2749', N'K-KBDC-D31XXXX-RSC0-AP-PRA0', N'K-BOX D31 ASPIRA PREMIUM', CAST(N'2023-11-17T23:52:26.947' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'275', N'W-CV01-105D31R-C03N-NL-DG00', N'CV 105D31R BLUE HD-ORANGE', CAST(N'2023-11-17T23:48:40.290' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2750', N'K-KBDC-D31XXXX-RSC0-AP-PRB0', N'K-BOX D31 ASPIRA PRE - AOP', CAST(N'2023-11-17T23:52:27.040' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2751', N'K-KBDC-D31XXXX-RSC0-AS-HMA0', N'K-BOX D31 ASAHI HPE MF', CAST(N'2023-11-17T23:52:27.127' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2752', N'K-KBDC-D31XXXX-RSC0-IN-XPA0', N'K-BOX D31 INCOE XTRA POWER', CAST(N'2023-11-17T23:52:27.210' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2753', N'K-KBDC-D31XXXX-RSC0-IN-XPC0', N'K-BOX D31 INCOE XP - UAE', CAST(N'2023-11-17T23:52:27.297' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2754', N'K-KBDC-D31XXXX-RSC0-QU-00A0', N'K-BOX D31 QUANTUM', CAST(N'2023-11-17T23:52:27.387' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2755', N'K-KBDC-D31XXXX-RSC0-QU-00B0', N'K-BOX D31 QUANTUM - MII', CAST(N'2023-11-17T23:52:27.520' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2756', N'K-KBDC-D31XXXX-RSC0-QU-MFA0', N'K-BOX D31 QUANTUM MF', CAST(N'2023-11-17T23:52:27.650' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2757', N'K-KBDC-D31XXXX-TTSB-AP-MFA0', N'K-BOX D31 ASPIRA MF', CAST(N'2023-11-17T23:52:27.743' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2758', N'K-KBDC-D31XXXX-TTSB-IN-MFA0', N'K-BOX D31 INCOE MF', CAST(N'2023-11-17T23:52:27.837' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2759', N'K-KBDC-D31XXXX-TTSB-IN-MFB0', N'K-BOX D31 INCOE MF - UAE', CAST(N'2023-11-17T23:52:27.930' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'276', N'W-CV01-105D31R-C06N-NL-DG00', N'CV 105D31R BLUE HD-WHITE', CAST(N'2023-11-17T23:48:40.370' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2760', N'K-KBDC-D31XXXX-TTSB-IN-MFC0', N'K-BOX D31 INCOE MF - DOM', CAST(N'2023-11-17T23:52:28.013' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2761', N'K-KBDC-D31XXXX-TTSB-OH-MFA0', N'K-BOX D31 OHAYO MF', CAST(N'2023-11-17T23:52:28.097' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2762', N'K-KBDC-D31XXXX-TTSB-OH-MFB0', N'K-BOX D31 OHAYO MF - UAE', CAST(N'2023-11-17T23:52:28.180' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2763', N'K-KBDC-DINBXXX-RSC0-AP-PRA0', N'K-BOX DIN-B ASPIRA PREMIUM', CAST(N'2023-11-17T23:52:28.260' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2764', N'K-KBDC-DINBXXX-RSC0-IN-XPA0', N'K-BOX DIN-B INCOE XTRA POWER', CAST(N'2023-11-17T23:52:28.343' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2765', N'K-KBDC-DINCXXX-RSC0-IN-XPA0', N'K-BOX DIN-C INCOE XTRA POWER', CAST(N'2023-11-17T23:52:28.430' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2766', N'K-KBWK-180G51X-TTSB-AM-DMA0', N'K-BOX 180G51 ALL MAKES HDT MF', CAST(N'2023-11-17T23:52:28.517' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2767', N'K-KBWK-190H52X-RSC0-AM-HDA0', N'K-BOX 190H52 ALL MAKES HDT', CAST(N'2023-11-17T23:52:28.613' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2768', N'K-KBWK-190H52X-RSC0-AM-PRA0', N'K-BOX 190H52 ALL MAKES PRE', CAST(N'2023-11-17T23:52:28.693' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2769', N'K-KBWK-190H52X-RSC0-TN-00A0', N'K-BOX 190H52 TRAKNUS', CAST(N'2023-11-17T23:52:28.783' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'277', N'W-CV01-115F51X-0MHN-NL-DG00', N'CV 115F51 BLUE', CAST(N'2023-11-17T23:48:40.460' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2770', N'K-KBWK-190H52X-RSC0-TN-DMA0', N'K-BOX 190H52 TRAKNUS HDT MF', CAST(N'2023-11-17T23:52:28.873' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2771', N'K-KBWK-190H52X-RSC0-TR-00A0', N'K-BOX 190H52 TRANSPORT', CAST(N'2023-11-17T23:52:28.960' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2772', N'K-KBWK-190H52X-TTSB-AM-PMA0', N'K-BOX 190H52 ALL MAKES PRE MF', CAST(N'2023-11-17T23:52:29.040' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2773', N'K-KBWK-210H52X-TTSB-AM-DMA0', N'K-BOX 210H52 ALL MAKES HDT MF', CAST(N'2023-11-17T23:52:29.123' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2774', N'K-KBWK-48D26LX-RSC0-AS-MEA0', N'K-BOX 48D26L ASAHI MEN', CAST(N'2023-11-17T23:52:29.210' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2775', N'K-KBWK-48D26RX-RSC0-AM-PMA0', N'K-BOX 48D26R ALL MAKES PRE MF', CAST(N'2023-11-17T23:52:29.303' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2776', N'K-KBWK-48D26RX-RSC0-AM-PRA0', N'K-BOX 48D26R ALL MAKES PRE', CAST(N'2023-11-17T23:52:29.453' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2777', N'K-KBWK-48D26RX-RSC0-AS-MEA0', N'K-BOX 48D26R ASAHI MEN', CAST(N'2023-11-17T23:52:29.557' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2778', N'K-KBWK-48D26RX-RSC0-TN-HDA0', N'K-BOX 48D26R TRAKNUS HDT', CAST(N'2023-11-17T23:52:29.650' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2779', N'K-KBWK-48D26RX-RSC0-TN-MFA0', N'K-BOX 48D26R TRAKNUS MF', CAST(N'2023-11-17T23:52:29.843' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'278', N'W-CV01-130F51X-0MHN-NL-DG00', N'CV 130F51 BLUE', CAST(N'2023-11-17T23:48:40.543' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2780', N'K-KBWK-4D150RX-TTSB-AM-DMA0', N'K-BOX 4D150R ALL MAKES HDT MF', CAST(N'2023-11-17T23:52:29.990' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2781', N'K-KBWK-4D170RX-TTSB-AM-DMA0', N'K-BOX 4D170R ALL MAKES HDT MF', CAST(N'2023-11-17T23:52:30.103' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2782', N'K-KBWK-4D200RX-TTSB-AM-DMA0', N'K-BOX 4D200R ALL MAKES HDT MF', CAST(N'2023-11-17T23:52:30.210' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2783', N'K-KBWK-544XXXX-RSC0-AS-MMA0', N'K-BOX 544 ASAHI MEN MF', CAST(N'2023-11-17T23:52:30.290' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2784', N'K-KBWK-544XXXX-RSC0-OH-HDA0', N'K-BOX 544 OHAYO', CAST(N'2023-11-17T23:52:30.370' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2785', N'K-KBWK-544XXXX-RSC0-UL-00A0', N'K-BOX 544 ULTRA-X', CAST(N'2023-11-17T23:52:30.460' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2786', N'K-KBWK-545XXXX-RSC0-AS-HPA0', N'K-BOX 545 ASAHI HPE', CAST(N'2023-11-17T23:52:30.553' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2787', N'K-KBWK-545XXXX-RSC0-UL-00A0', N'K-BOX 545 ULTRA-X', CAST(N'2023-11-17T23:52:30.640' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2788', N'K-KBWK-E41XXXX-RSC0-AS-HPA0', N'K-BOX E41 ASAHI HPE', CAST(N'2023-11-17T23:52:30.720' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2789', N'K-KBWK-E41XXXX-RSC0-AS-MEA0', N'K-BOX E41 ASAHI MEN', CAST(N'2023-11-17T23:52:30.803' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'279', N'W-CV01-145G51L-0MHN-NL-DG00', N'CV 145G51L BLUE', CAST(N'2023-11-17T23:48:40.630' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2790', N'K-KBWK-E41XXXX-RSC0-OH-HDA0', N'K-BOX E41 OHAYO', CAST(N'2023-11-17T23:52:30.960' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2791', N'K-KBWK-E41XXXX-RSC0-UL-00A0', N'K-BOX E41 ULTRA-X', CAST(N'2023-11-17T23:52:31.060' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2792', N'K-KBWK-F51XXXX-RSC0-AS-HPA0', N'K-BOX F51 ASAHI HPE', CAST(N'2023-11-17T23:52:31.160' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2793', N'K-KBWK-F51XXXX-RSC0-AS-MEA0', N'K-BOX F51 ASAHI MEN', CAST(N'2023-11-17T23:52:31.243' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2794', N'K-KBWK-F51XXXX-RSC0-OH-HDA0', N'K-BOX F51 OHAYO', CAST(N'2023-11-17T23:52:31.323' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2795', N'K-KBWK-F51XXXX-RSC0-OH-HDB0', N'K-BOX F51 OHAYO - UAE', CAST(N'2023-11-17T23:52:31.407' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2796', N'K-KBWK-F51XXXX-RSC0-UL-00A0', N'K-BOX F51 ULTRA-X', CAST(N'2023-11-17T23:52:31.493' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2797', N'K-KBWK-G51XXXX-RSC0-AS-HPA0', N'K-BOX G51 ASAHI HPE', CAST(N'2023-11-17T23:52:31.577' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2798', N'K-KBWK-555XXXX-RSC0-AS-HPA0', N'K-BOX 555 ASAHI HPE', CAST(N'2023-11-17T23:52:31.660' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2799', N'K-KBWK-555XXXX-RSC0-AS-MEA0', N'K-BOX 555 ASAHI MEN', CAST(N'2023-11-17T23:52:31.743' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'28', N'K-SCCV-240X045-11VJ-AS-MFA0', N'STIC CVR ASAHI MF 240X45 MM', CAST(N'2023-11-17T23:48:18.670' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'280', N'W-CV01-145G51X-0MHN-NL-DG00', N'CV 145G51 BLUE', CAST(N'2023-11-17T23:48:40.713' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2800', N'K-KBDC-B20XXXX-TTSB-OH-MFB0', N'K-BOX B20 OHAYO MF- UAE', CAST(N'2023-11-17T23:52:31.827' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2801', N'K-KBDC-B24XXXX-RSC0-AP-PRA0', N'K-BOX B24 ASPIRA PREMIUM', CAST(N'2023-11-17T23:52:31.907' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2802', N'K-KBDC-B24XXXX-RSC0-AS-HMA0', N'K-BOX B24 ASAHI HPE MF', CAST(N'2023-11-17T23:52:32.000' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2803', N'K-KBDC-B24XXXX-RSC0-IN-XPA0', N'K-BOX B24 INCOE XTRA POWER', CAST(N'2023-11-17T23:52:32.080' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2804', N'K-KBDC-B24XXXX-RSC0-IN-XPC0', N'K-BOX B24 INCOE XP - UAE', CAST(N'2023-11-17T23:52:32.163' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2805', N'K-KBKR-566XXXX-RSC0-BL-00A0', N'K-BOX 566 BLANK', CAST(N'2023-11-17T23:52:32.300' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2806', N'K-KBWK-G51XXXX-RSC0-AS-MEA0', N'K-BOX G51 ASAHI MEN', CAST(N'2023-11-17T23:52:32.427' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2807', N'K-KBWK-G51XXXX-RSC0-OH-HDA0', N'K-BOX G51 OHAYO', CAST(N'2023-11-17T23:52:32.533' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2808', N'K-KBWK-G51XXXX-RSC0-OH-HDB0', N'K-BOX G51 OHAYO - UAE', CAST(N'2023-11-17T23:52:32.613' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2809', N'K-KBWK-G51XXXX-RSC0-UL-00A0', N'K-BOX G51 ULTRA-X', CAST(N'2023-11-17T23:52:32.693' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'281', N'W-CV01-155G51X-0MHN-NL-DG00', N'CV 155G51 BLUE', CAST(N'2023-11-17T23:48:40.807' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2810', N'K-KBWK-H52XXXX-RSC0-AS-HPA0', N'K-BOX H52 ASAHI HPE', CAST(N'2023-11-17T23:52:32.777' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2811', N'K-KBWK-H52XXXX-RSC0-AS-MEA0', N'K-BOX H52 ASAHI MEN', CAST(N'2023-11-17T23:52:32.857' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2812', N'K-KBWK-H52XXXX-RSC0-OH-HDA0', N'K-BOX H52 OHAYO', CAST(N'2023-11-17T23:52:32.943' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2813', N'K-KBWK-H52XXXX-RSC0-OH-HDB0', N'K-BOX H52 OHAYO - UAE', CAST(N'2023-11-17T23:52:33.030' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2814', N'K-KBWK-H52XXXX-RSC0-UL-00A0', N'K-BOX H52 ULTRA-X', CAST(N'2023-11-17T23:52:33.120' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2815', N'K-KBDC-B24XXXX-RSC0-QU-00A0', N'K-BOX B24 QUANTUM', CAST(N'2023-11-17T23:52:33.210' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2816', N'K-KBDC-B24XXXX-RSC0-QU-00B0', N'K-BOX B24 QUANTUM - MII', CAST(N'2023-11-17T23:52:33.303' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2817', N'K-KBDC-B24XXXX-RSC0-QU-MFA0', N'K-BOX B24 QUANTUM MF', CAST(N'2023-11-17T23:52:33.390' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2818', N'K-KBDC-B24XXXX-TTSB-AP-MFA0', N'K-BOX B24 ASPIRA MF', CAST(N'2023-11-17T23:52:33.480' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2819', N'K-KBDC-B24XXXX-TTSB-IN-MFA0', N'K-BOX B24 INCOE MF', CAST(N'2023-11-17T23:52:33.570' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'282', N'W-CV01-165G51X-0MHN-NL-DG00', N'CV 165G51 BLUE', CAST(N'2023-11-17T23:48:40.897' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2820', N'K-KBDC-B24XXXX-TTSB-IN-MFB0', N'K-BOX B24 INCOE MF - UAE', CAST(N'2023-11-17T23:52:33.657' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2821', N'K-KBDC-B24XXXX-TTSB-IN-MFC0', N'K-BOX B24 INCOE MF - DOM', CAST(N'2023-11-17T23:52:33.743' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2822', N'K-KBDC-B24XXXX-TTSB-OH-MFA0', N'K-BOX B24 OHAYO MF', CAST(N'2023-11-17T23:52:33.833' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2823', N'K-KBDC-B24XXXX-TTSB-OH-MFB0', N'K-BOX B24 OHAYO MF - UAE', CAST(N'2023-11-17T23:52:33.930' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2824', N'K-KBDC-D23XXXX-RSC0-AP-PRA0', N'K-BOX D23 ASPIRA PREMIUM', CAST(N'2023-11-17T23:52:34.013' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2825', N'K-KBDC-D23XXXX-RSC0-AS-HMA0', N'K-BOX D23 ASAHI HPE MF', CAST(N'2023-11-17T23:52:34.100' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2826', N'K-KBDC-D23XXXX-RSC0-IN-XPA0', N'K-BOX D23 INCOE XTRA POWER', CAST(N'2023-11-17T23:52:34.187' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2827', N'K-KBDC-D23XXXX-RSC0-IN-XPC0', N'K-BOX D23 INCOE XP - UAE', CAST(N'2023-11-17T23:52:34.273' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2828', N'K-KBDC-D23XXXX-RSC0-QU-00A0', N'K-BOX D23 QUANTUM', CAST(N'2023-11-17T23:52:34.357' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2829', N'K-KBDC-D23XXXX-RSC0-QU-00B0', N'K-BOX D23 QUANTUM - MII', CAST(N'2023-11-17T23:52:34.440' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'283', N'W-CV01-190H52X-0MHN-NL-DG00', N'CV 190H52 BLUE', CAST(N'2023-11-17T23:48:40.990' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2830', N'K-KBDC-D23XXXX-RSC0-QU-MFA0', N'K-BOX D23 QUANTUM MF', CAST(N'2023-11-17T23:52:34.523' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2831', N'K-KBDC-D23XXXX-TTSB-AP-MFA0', N'K-BOX D23 ASPIRA MF', CAST(N'2023-11-17T23:52:34.607' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2832', N'K-KBDC-D23XXXX-TTSB-IN-MFA0', N'K-BOX D23 INCOE MF', CAST(N'2023-11-17T23:52:34.687' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2833', N'K-KBDC-D23XXXX-TTSB-IN-MFB0', N'K-BOX D23 INCOE MF - UAE', CAST(N'2023-11-17T23:52:34.767' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2834', N'K-KBDC-D23XXXX-TTSB-IN-MFC0', N'K-BOX D23 INCOE MF - DOM', CAST(N'2023-11-17T23:52:34.850' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2835', N'K-KBDC-D23XXXX-TTSB-OH-MFA0', N'K-BOX D23 OHAYO MF', CAST(N'2023-11-17T23:52:34.930' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2836', N'K-KBDC-D23XXXX-TTSB-OH-MFB0', N'K-BOX D23 OHAYO MF - UAE', CAST(N'2023-11-17T23:52:35.010' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2837', N'K-KBDC-D26XXXX-RSC0-AP-PRA0', N'K-BOX D26 ASPIRA PREMIUM', CAST(N'2023-11-17T23:52:35.093' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2838', N'K-KBDC-D26XXXX-RSC0-AP-PRB0', N'K-BOX D26 ASPIRA PRE - AOP', CAST(N'2023-11-17T23:52:35.177' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2839', N'K-KBKR-N200XXX-RSC0-AP-PRC0', N'K-BOX N200 ASPIRA PRE - AOP', CAST(N'2023-11-17T23:52:35.260' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'284', N'W-CV01-210H52X-0MHN-NL-DG00', N'CV 210H52 BLUE', CAST(N'2023-11-17T23:48:41.080' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2840', N'K-KBKR-N200XXX-RSC0-AU-00A0', N'K-BOX N200 AURORA', CAST(N'2023-11-17T23:52:35.340' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2841', N'K-KBKR-N50LXXX-RSC0-AU-00A0', N'K-BOX N50L AURORA', CAST(N'2023-11-17T23:52:35.423' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2842', N'K-KBKR-N50XXXX-RSC0-AU-00A0', N'K-BOX N50 AURORA', CAST(N'2023-11-17T23:52:35.503' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2843', N'K-KBKR-N70LXXX-RSC0-AU-00A0', N'K-BOX N70L AURORA', CAST(N'2023-11-17T23:52:35.593' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2844', N'K-KBDC-E41XXXX-RSC0-QU-00B0', N'K-BOX E41 QUANTUM - MII', CAST(N'2023-11-17T23:52:35.690' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2845', N'K-KBDC-E41XXXX-TTSB-AP-MFA0', N'K-BOX E41 ASPIRA MF', CAST(N'2023-11-17T23:52:35.780' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2846', N'K-KBDC-E41XXXX-TTSB-IN-MFA0', N'K-BOX E41 INCOE MF', CAST(N'2023-11-17T23:52:35.867' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2847', N'K-KBDC-E41XXXX-TTSB-IN-MFB0', N'K-BOX E41 INCOE MF - UAE', CAST(N'2023-11-17T23:52:35.953' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2848', N'K-KBDC-E41XXXX-TTSB-OH-MFA0', N'K-BOX E41 OHAYO MF', CAST(N'2023-11-17T23:52:36.030' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2849', N'K-KBDC-E41XXXX-TTSB-QU-MFA0', N'K-BOX E41 QUANTUM MF', CAST(N'2023-11-17T23:52:36.113' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'285', N'W-CV01-34B20LX-B03N-IN-DG00', N'CV 34B20L BLUE HD2-ORANGE INC', CAST(N'2023-11-17T23:48:41.383' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2850', N'K-KBDC-F51XXXX-RSC0-AP-PRA0', N'K-BOX F51 ASPIRA PREMIUM', CAST(N'2023-11-17T23:52:36.193' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2851', N'K-KBDC-F51XXXX-RSC0-AS-HMA0', N'K-BOX F51 ASAHI HPE MF', CAST(N'2023-11-17T23:52:36.273' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2852', N'K-KBDC-F51XXXX-RSC0-IN-XPA0', N'K-BOX F51 INCOE XTRA POWER', CAST(N'2023-11-17T23:52:36.360' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2853', N'K-KBDC-F51XXXX-RSC0-IN-XPC0', N'K-BOX F51 INCOE XP - UAE', CAST(N'2023-11-17T23:52:36.450' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2854', N'K-KBDC-F51XXXX-RSC0-QU-00A0', N'K-BOX F51 QUANTUM', CAST(N'2023-11-17T23:52:36.540' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2855', N'K-KBDC-F51XXXX-RSC0-QU-00B0', N'K-BOX F51 QUANTUM - MII', CAST(N'2023-11-17T23:52:36.640' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2856', N'K-KBDC-F51XXXX-TTSB-AP-MFA0', N'K-BOX F51 ASPIRA MF', CAST(N'2023-11-17T23:52:36.730' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2857', N'K-KBKR-566XXXX-RSC0-BL-MFA0', N'K-BOX 566 BLANK MF', CAST(N'2023-11-17T23:52:36.817' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2858', N'K-KBKR-588XXXX-RSC0-BL-00A0', N'K-BOX 588 BLANK', CAST(N'2023-11-17T23:52:36.917' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2859', N'K-KBKR-6D115XX-RSC0-IN-DCA0', N'K-BOX 6D115 INCOE DEEP CYCLE', CAST(N'2023-11-17T23:52:37.023' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'286', N'W-CV01-55D23LX-D01N-NL-DG00', N'CV 55D23L BLUE HD-BLUE', CAST(N'2023-11-17T23:48:41.467' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2860', N'K-KBKR-6D135XX-RSC0-IN-DCA0', N'K-BOX 6D135 INCOE DEEP CYCLE', CAST(N'2023-11-17T23:52:37.110' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2861', N'K-KBDC-F51XXXX-TTSB-IN-MFA0', N'K-BOX F51 INCOE MF', CAST(N'2023-11-17T23:52:37.207' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2862', N'K-KBDC-F51XXXX-TTSB-IN-MFB0', N'K-BOX F51 INCOE MF - UAE', CAST(N'2023-11-17T23:52:37.307' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2863', N'K-KBDC-F51XXXX-TTSB-OH-MFA0', N'K-BOX F51 OHAYO MF', CAST(N'2023-11-17T23:52:37.400' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2864', N'K-KBDC-F51XXXX-TTSB-QU-MFA0', N'K-BOX F51 QUANTUM MF', CAST(N'2023-11-17T23:52:37.487' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2865', N'K-KBDC-G51XXXX-RSC0-AP-PRA0', N'K-BOX G51 ASPIRA PREMIUM', CAST(N'2023-11-17T23:52:37.583' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2866', N'K-KBDC-G51XXXX-RSC0-AS-HMA0', N'K-BOX G51 ASAHI HPE MF', CAST(N'2023-11-17T23:52:37.670' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2867', N'K-KBDC-G51XXXX-RSC0-IN-XPA0', N'K-BOX G51 INCOE XTRA POWER', CAST(N'2023-11-17T23:52:37.750' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2868', N'K-KBDC-G51XXXX-RSC0-IN-XPC0', N'K-BOX G51 INCOE XP - UAE', CAST(N'2023-11-17T23:52:37.833' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2869', N'K-KBDC-G51XXXX-RSC0-QU-00A0', N'K-BOX G51 QUANTUM', CAST(N'2023-11-17T23:52:37.913' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'287', N'W-CV01-55D23RX-D01N-NL-DG00', N'CV 55D23R BLUE HD-BLUE', CAST(N'2023-11-17T23:48:41.563' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2870', N'K-KBDC-G51XXXX-RSC0-QU-00B0', N'K-BOX G51 QUANTUM - MII', CAST(N'2023-11-17T23:52:38.010' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2871', N'K-KBDC-G51XXXX-TTSB-AP-MFA0', N'K-BOX G51 ASPIRA MF', CAST(N'2023-11-17T23:52:38.093' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2872', N'K-KBWK-75D31LX-RSC0-TN-MFA0', N'K-BOX 75D31L TRAKNUS MF', CAST(N'2023-11-17T23:52:38.177' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2873', N'K-KBWK-75D31RX-RSC0-AM-PMA0', N'K-BOX 75D31R ALL MAKES PRE MF', CAST(N'2023-11-17T23:52:38.260' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2874', N'K-KBWK-80D26RX-RSC0-AM-PMA0', N'K-BOX 80D26R ALL MAKES PRE MF', CAST(N'2023-11-17T23:52:38.347' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2875', N'K-KBWK-95D31LX-RSC0-AM-PRA0', N'K-BOX 95D31L ALL MAKES PRE', CAST(N'2023-11-17T23:52:38.433' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2876', N'K-KBWK-95D31RX-RSC0-AM-PMA0', N'K-BOX 95D31R ALL MAKES PRE MF', CAST(N'2023-11-17T23:52:38.520' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2877', N'K-KBKR-N70XXXX-RSC0-AU-00A0', N'K-BOX N70 AURORA', CAST(N'2023-11-17T23:52:38.607' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2878', N'K-KBKR-N70ZXXX-RSC0-IS-00A0', N'K-BOX N70Z ISUZU DOMESTIC', CAST(N'2023-11-17T23:52:38.690' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2879', N'K-KBKR-NS70XXX-RSC0-IS-00A0', N'K-BOX NS70 ISUZU DOMESTIC', CAST(N'2023-11-17T23:52:38.777' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'288', N'W-CV01-65D31LX-C01N-NL-DG00', N'CV 65D31L BLUE HD-BLUE', CAST(N'2023-11-17T23:48:41.643' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2880', N'K-KBKR-SA100XX-TTSB-AM-DCA0', N'K-BOX SA-100 ALL MAKES', CAST(N'2023-11-17T23:52:38.857' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2881', N'K-KBKR-SA100XX-TTSB-IN-DCA0', N'K-BOX SA-100 INCOE', CAST(N'2023-11-17T23:52:38.940' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2882', N'K-KBKR-SA105XX-RSC0-AM-DCA0', N'K-BOX SA-105 ALL MAKES', CAST(N'2023-11-17T23:52:39.017' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2883', N'K-KBKR-SA105XX-RSC0-IN-DCA0', N'K-BOX SA-105 INCOE', CAST(N'2023-11-17T23:52:39.097' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2884', N'K-KBKR-SA12XXX-TTAB-AM-DCA0', N'K-BOX SA-12 ALL MAKES', CAST(N'2023-11-17T23:52:39.177' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2885', N'K-KBKR-SA65XXX-TTSB-IN-DCA0', N'K-BOX SA-65 INCOE', CAST(N'2023-11-17T23:52:39.260' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2886', N'K-KBKR-SG100XX-TTSB-AM-DCA0', N'K-BOX SG-100 ALL MAKES', CAST(N'2023-11-17T23:52:39.343' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2887', N'K-KBKR-SG100XX-TTSB-IN-DCA0', N'K-BOX SG-100 INCOE', CAST(N'2023-11-17T23:52:39.430' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2888', N'K-KBKR-SG65XXX-TTSB-IN-DCA0', N'K-BOX SG-65 INCOE', CAST(N'2023-11-17T23:52:39.510' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2889', N'K-KBWK-105D31R-RSC0-AM-PRA0', N'K-BOX 105D31R ALL MAKES PRE', CAST(N'2023-11-17T23:52:39.590' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'289', N'W-CV01-65D31LX-C03N-NL-DG00', N'CV 65D31L BLUE HD-ORANGE', CAST(N'2023-11-17T23:48:41.843' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2890', N'K-KBKR-8D90XXX-RSC0-IN-DCA0', N'K-BOX 8D90 INCOE DEEP CYCLE', CAST(N'2023-11-17T23:52:39.673' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2891', N'K-KBKR-B20XXXX-RSC0-BL-00A0', N'K-BOX B20 BLANK', CAST(N'2023-11-17T23:52:39.757' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2892', N'K-KBKR-B20XXXX-RSC0-BL-MFA0', N'K-BOX B20 BLANK MF', CAST(N'2023-11-17T23:52:39.840' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2893', N'K-KBKR-B24XXXX-RSC0-BL-00A0', N'K-BOX B24 BLANK', CAST(N'2023-11-17T23:52:39.920' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2894', N'K-KBKR-D23XXXX-RSC0-BL-00A0', N'K-BOX D23 BLANK', CAST(N'2023-11-17T23:52:40.007' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2895', N'K-KBKR-D26XXXX-RSC0-BL-00A0', N'K-BOX D26 BLANK', CAST(N'2023-11-17T23:52:40.090' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2896', N'K-KBKR-D31XXXX-RSC0-BL-00A0', N'K-BOX D31 BLANK', CAST(N'2023-11-17T23:52:40.170' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2897', N'K-KBKR-E41XXXX-RSC0-BL-00A0', N'K-BOX E41 BLANK', CAST(N'2023-11-17T23:52:40.350' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2898', N'K-KBKR-E41XXXX-RSC0-BL-MFA0', N'K-BOX E41 BLANK MF', CAST(N'2023-11-17T23:52:40.447' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2899', N'K-KBKR-F51XXXX-RSC0-BL-00A0', N'K-BOX F51 BLANK', CAST(N'2023-11-17T23:52:40.547' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'29', N'K-SCCV-240X045-11VJ-IN-MFA0', N'STIC COVER INCOE MF 240X45 MM', CAST(N'2023-11-17T23:48:18.760' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'290', N'W-CV01-65D31LX-C06N-NL-DG00', N'CV 65D31L BLUE HD-WHITE', CAST(N'2023-11-17T23:48:41.923' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2900', N'K-KBKR-F51XXXX-RSC0-BL-MFA0', N'K-BOX F51 BLANK MF', CAST(N'2023-11-17T23:52:40.643' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2901', N'K-KBKR-G51XXXX-RSC0-BL-00A0', N'K-BOX G51 BLANK', CAST(N'2023-11-17T23:52:40.737' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2902', N'K-KBKR-G51XXXX-RSC0-BL-MFA0', N'K-BOX G51 BLANK MF', CAST(N'2023-11-17T23:52:40.830' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2903', N'K-KBKR-GOLFCAR-RSC0-BL-00A0', N'K-BOX GOLF CART NON BRAND', CAST(N'2023-11-17T23:52:40.923' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2904', N'K-KBKR-GTZ5SXX-TTAB-TR-KIA0', N'K-BOX GTZ-5S TRANSPORT KIT', CAST(N'2023-11-17T23:52:41.007' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2905', N'K-KBKR-GTZ6VXX-TTAB-TR-KIA0', N'K-BOX GTZ-6V TRANSPORT KIT', CAST(N'2023-11-17T23:52:41.090' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2906', N'K-KBKR-H52XXXX-RSC0-BL-00A0', N'K-BOX H52 BLANK', CAST(N'2023-11-17T23:52:41.170' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2907', N'K-KBKR-N100LXX-RSC0-AU-00A0', N'K-BOX N100L AURORA', CAST(N'2023-11-17T23:52:41.250' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2908', N'K-KBKR-N100XXX-RSC0-AP-PRC0', N'K-BOX N100 ASPIRA PRE - AOP', CAST(N'2023-11-17T23:52:41.330' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2909', N'K-KBKR-N100XXX-RSC0-AU-00A0', N'K-BOX N100 AURORA', CAST(N'2023-11-17T23:52:41.413' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'291', N'W-CV01-65D31RX-C01N-NL-DG00', N'CV 65D31R BLUE HD-BLUE', CAST(N'2023-11-17T23:48:42.003' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2910', N'K-KBKR-N120LXX-RSC0-AP-PRC0', N'K-BOX N120L ASPIRA PRE - AOP', CAST(N'2023-11-17T23:52:41.497' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2911', N'K-KBKR-N120XXX-RSC0-AP-PRC0', N'K-BOX N120 ASPIRA PRE - AOP', CAST(N'2023-11-17T23:52:41.580' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2912', N'K-KBWK-115D31R-RSC0-AM-DMA0', N'K-BOX 115D31R ALL MAKES HDT MF', CAST(N'2023-11-17T23:52:41.663' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2913', N'K-KBWK-115E41R-TTSB-AM-DMA0', N'K-BOX 115E41R ALL MAKES HDT MF', CAST(N'2023-11-17T23:52:41.747' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2914', N'K-KBWK-115F51X-RSC0-AM-HDA0', N'K-BOX 115F51 ALL MAKES HDT', CAST(N'2023-11-17T23:52:41.830' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2915', N'K-KBWK-115F51X-RSC0-AM-PRA0', N'K-BOX 115F51 ALL MAKES PRE', CAST(N'2023-11-17T23:52:41.913' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2916', N'K-KBWK-115F51X-RSC0-TN-00A0', N'K-BOX 115F51 TRAKNUS', CAST(N'2023-11-17T23:52:41.997' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2917', N'K-KBWK-115F51X-RSC0-TN-DMA0', N'K-BOX 115F51 TRAKNUS HDT MF', CAST(N'2023-11-17T23:52:42.080' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2918', N'K-KBWK-115F51X-RSC0-TR-00A0', N'K-BOX 115F51 TRANSPORT', CAST(N'2023-11-17T23:52:42.160' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2919', N'K-KBWK-115F51X-TTSB-AM-PMA0', N'K-BOX 115F51 ALL MAKES PRE MF', CAST(N'2023-11-17T23:52:42.243' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'292', N'W-CV01-65D31RX-C03N-NL-DG00', N'CV 65D31R BLUE HD-ORANGE', CAST(N'2023-11-17T23:48:42.103' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2920', N'K-KBWK-145F51X-TTSB-AM-DMA0', N'K-BOX 145F51 ALL MAKES HDT MF', CAST(N'2023-11-17T23:52:42.323' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2921', N'K-KBWK-145G51X-RSC0-AM-HDA0', N'K-BOX 145G51 ALL MAKES HDT', CAST(N'2023-11-17T23:52:42.413' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2922', N'K-KBDC-95D31RX-RSC0-IN-GOA0', N'K-BOX 95D31R INCOE GOLD', CAST(N'2023-11-17T23:52:42.493' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2923', N'K-KBDC-95D31RX-RSC0-IN-PPA0', N'K-BOX 95D31R INCOE PRE POWER', CAST(N'2023-11-17T23:52:42.577' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2924', N'K-KBDC-95D31RX-RSC0-IN-XPB0', N'K-BOX 95D31R INCOE XP - SUD', CAST(N'2023-11-17T23:52:42.660' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2925', N'K-KBDC-95D31RX-RSC0-RA-00A0', N'K-BOX 95D31R RAPTOR', CAST(N'2023-11-17T23:52:42.740' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2926', N'K-KBDC-95E41RX-RSC0-IN-PPA0', N'K-BOX 95E41R INCOE PRE POWER', CAST(N'2023-11-17T23:52:42.820' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2927', N'K-KBKR-N120XXX-RSC0-AU-00A0', N'K-BOX N120 AURORA', CAST(N'2023-11-17T23:52:42.903' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2928', N'K-KBKR-N150LXX-RSC0-AP-PRC0', N'K-BOX N150L ASPIRA PRE - AOP', CAST(N'2023-11-17T23:52:42.987' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2929', N'K-KBKR-N150XXX-RSC0-AP-PRC0', N'K-BOX N150 ASPIRA PRE - AOP', CAST(N'2023-11-17T23:52:43.067' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'293', N'W-CV01-95D31LX-C03N-NL-DG00', N'CV 95D31L BLUE HD-ORANGE', CAST(N'2023-11-17T23:48:42.180' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2930', N'K-KBKR-N150XXX-RSC0-AU-00A0', N'K-BOX N150 AURORA', CAST(N'2023-11-17T23:52:43.150' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2931', N'K-KBDC-67018XX-RSC0-QU-00B0', N'K-BOX 670-18 QUANTUM - MII', CAST(N'2023-11-17T23:52:43.233' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2932', N'K-KBDC-67018XX-TTSB-IN-MFA0', N'K-BOX 670-18 INCOE MF', CAST(N'2023-11-17T23:52:43.313' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2933', N'K-KBDC-67018XX-TTSB-OH-MFA0', N'K-BOX 670-18 OHAYO MF', CAST(N'2023-11-17T23:52:43.397' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2934', N'K-KBDC-70027XX-RSC0-AP-PRA0', N'K-BOX 700-27 ASPIRA PREMIUM', CAST(N'2023-11-17T23:52:43.480' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2935', N'K-KBWK-95D31RX-RSC0-AM-PRA0', N'K-BOX 95D31R ALL MAKES PRE', CAST(N'2023-11-17T23:52:43.560' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2936', N'K-KBWK-95E41RX-RSC0-AM-HDA0', N'K-BOX 95E41R ALL MAKES HDT', CAST(N'2023-11-17T23:52:43.640' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2937', N'K-KBWK-95E41RX-RSC0-AM-PRA0', N'K-BOX 95E41R ALL MAKES PRE', CAST(N'2023-11-17T23:52:43.723' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2938', N'K-KBWK-95E41RX-RSC0-TN-00A0', N'K-BOX 95E41R TRAKNUS', CAST(N'2023-11-17T23:52:43.803' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2939', N'K-KBWK-95E41RX-RSC0-TN-MFA0', N'K-BOX 95E41R TRAKNUS MF', CAST(N'2023-11-17T23:52:43.887' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'294', N'W-CV01-95D31LX-C06N-NL-DG00', N'CV 95D31L BLUE HD-WHITE', CAST(N'2023-11-17T23:48:42.263' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2940', N'K-KBWK-95E41RX-RSC0-TR-00A0', N'K-BOX 95E41R TRANSPORT', CAST(N'2023-11-17T23:52:43.970' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2941', N'K-KBWK-95E41RX-TTSB-AM-PMA0', N'K-BOX 95E41R ALL MAKES PRE MF', CAST(N'2023-11-17T23:52:44.050' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2942', N'K-KBWK-B20XXXX-RSC0-AS-HPA0', N'K-BOX B20 ASAHI HPE', CAST(N'2023-11-17T23:52:44.137' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2943', N'K-KBWK-B20XXXX-RSC0-AS-MEA0', N'K-BOX B20 ASAHI MEN', CAST(N'2023-11-17T23:52:44.217' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2944', N'K-KBWK-B20XXXX-RSC0-AS-MMA0', N'K-BOX B20 ASAHI MEN MF', CAST(N'2023-11-17T23:52:44.297' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2945', N'K-KBWK-B20XXXX-RSC0-OH-HDA0', N'K-BOX B20 OHAYO', CAST(N'2023-11-17T23:52:44.380' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2946', N'K-KBWK-B20XXXX-RSC0-UL-00A0', N'K-BOX B20 ULTRA-X', CAST(N'2023-11-17T23:52:44.467' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2947', N'K-KBWK-B24XXXX-RSC0-AS-HPA0', N'K-BOX B24 ASAHI HPE', CAST(N'2023-11-17T23:52:44.547' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2948', N'K-KBWK-B24XXXX-RSC0-AS-MEA0', N'K-BOX B24 ASAHI MEN', CAST(N'2023-11-17T23:52:44.627' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2949', N'K-KBWK-B24XXXX-RSC0-AS-MMA0', N'K-BOX B24 ASAHI MEN MF', CAST(N'2023-11-17T23:52:44.707' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'295', N'W-CV01-95D31RX-C01N-NL-DG00', N'CV 95D31R BLUE HD-BLUE', CAST(N'2023-11-17T23:48:42.347' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2950', N'K-KBWK-B24XXXX-RSC0-OH-HDA0', N'K-BOX B24 OHAYO', CAST(N'2023-11-17T23:52:44.790' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2951', N'K-KBWK-B24XXXX-RSC0-UL-00A0', N'K-BOX B24 ULTRA-X', CAST(N'2023-11-17T23:52:44.870' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2952', N'K-KBWK-D23XXXX-RSC0-AS-HPA0', N'K-BOX D23 ASAHI HPE', CAST(N'2023-11-17T23:52:44.953' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2953', N'K-KBWK-D23XXXX-RSC0-AS-MMA0', N'K-BOX D23 ASAHI MEN MF', CAST(N'2023-11-17T23:52:45.033' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2954', N'K-KBWK-D23XXXX-RSC0-OH-HDA0', N'K-BOX D23 OHAYO', CAST(N'2023-11-17T23:52:45.113' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2955', N'K-KBWK-D23XXXX-RSC0-UL-00A0', N'K-BOX D23 ULTRA-X', CAST(N'2023-11-17T23:52:45.197' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2956', N'K-KBWK-D26XXXX-RSC0-UL-00A0', N'K-BOX D26 ULTRA-X', CAST(N'2023-11-17T23:52:45.280' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2957', N'K-KBWK-D31XXXX-RSC0-AS-HPA0', N'K-BOX D31 ASAHI HPE', CAST(N'2023-11-17T23:52:45.360' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2958', N'K-KBWK-D31XXXX-RSC0-AS-MMA0', N'K-BOX D31 ASAHI MEN MF', CAST(N'2023-11-17T23:52:45.440' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2959', N'K-KBWK-D31XXXX-RSC0-OH-HDA0', N'K-BOX D31 OHAYO', CAST(N'2023-11-17T23:52:45.523' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'296', N'W-CV01-95D31RX-C03N-NL-DG00', N'CV 95D31R BLUE HD-ORANGE', CAST(N'2023-11-17T23:48:42.443' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2960', N'K-KBWK-D31XXXX-RSC0-UL-00A0', N'K-BOX D31 ULTRA-X', CAST(N'2023-11-17T23:52:45.603' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2961', N'K-KBDC-588XXXX-TTSB-IN-MFC0', N'K-BOX 588 INCOE MF - DOM', CAST(N'2023-11-17T23:52:45.687' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2962', N'K-KBDC-588XXXX-TTSB-OH-MFA0', N'K-BOX 588 OHAYO MF', CAST(N'2023-11-17T23:52:45.770' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2963', N'K-KBDC-588XXXX-TTSB-OH-MFB0', N'K-BOX 588 OHAYO MF - UAE', CAST(N'2023-11-17T23:52:45.850' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2964', N'K-KBDC-60038XX-RSC0-LU-MFA0', N'K-BOX 600-38 LUCAS MF', CAST(N'2023-11-17T23:52:45.930' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2965', N'K-KBWK-566XXXX-RSC0-AS-MMA0', N'K-BOX 566 ASAHI MEN MF', CAST(N'2023-11-17T23:52:46.017' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2966', N'K-KBWK-566XXXX-RSC0-OH-HDA0', N'K-BOX 566 OHAYO', CAST(N'2023-11-17T23:52:46.100' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2967', N'K-KBWK-566XXXX-RSC0-UL-00A0', N'K-BOX 566 ULTRA-X', CAST(N'2023-11-17T23:52:46.180' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2968', N'K-KBWK-588XXXX-RSC0-AS-HPA0', N'K-BOX 588 ASAHI HPE', CAST(N'2023-11-17T23:52:46.263' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2969', N'K-KBWK-588XXXX-RSC0-AS-MMA0', N'K-BOX 588 ASAHI MEN MF', CAST(N'2023-11-17T23:52:46.347' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'297', N'W-CV01-95D31RX-C06N-NL-DG00', N'CV 95D31R BLUE HD-WHITE', CAST(N'2023-11-17T23:48:42.520' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2970', N'K-KBWK-588XXXX-RSC0-OH-HDA0', N'K-BOX 588 OHAYO', CAST(N'2023-11-17T23:52:46.430' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2971', N'K-KBWK-588XXXX-RSC0-UL-00A0', N'K-BOX 588 ULTRA-X', CAST(N'2023-11-17T23:52:46.510' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2972', N'K-KBWK-65038XX-RSC0-OH-HDA0', N'K-BOX 650-38 OHAYO', CAST(N'2023-11-17T23:52:46.593' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2973', N'K-KBWK-65038XX-RSC0-UL-00A0', N'K-BOX 650-38 ULTRA-X', CAST(N'2023-11-17T23:52:46.677' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2974', N'K-KBWK-65D26RX-RSC0-TR-00A0', N'K-BOX 65D26R TRANSPORT', CAST(N'2023-11-17T23:52:46.757' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2975', N'K-KBWK-65D31LX-RSC0-AS-MEA0', N'K-BOX 65D31L ASAHI MEN', CAST(N'2023-11-17T23:52:46.840' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2976', N'K-KBWK-65D31RX-RSC0-AM-HDA0', N'K-BOX 65D31R ALL MAKES HDT', CAST(N'2023-11-17T23:52:46.920' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2977', N'K-KBWK-65D31RX-RSC0-AM-PMA0', N'K-BOX 65D31R ALL MAKES PRE MF', CAST(N'2023-11-17T23:52:47.000' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2978', N'K-KBWK-65D31RX-RSC0-AM-PRA0', N'K-BOX 65D31R ALL MAKES PRE', CAST(N'2023-11-17T23:52:47.083' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2979', N'K-KBWK-65D31RX-RSC0-AS-MEA0', N'K-BOX 65D31R ASAHI MEN', CAST(N'2023-11-17T23:52:47.167' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'298', N'W-CV01-95E41LX-0MHN-NL-DG00', N'CV 95E41L BLUE', CAST(N'2023-11-17T23:48:42.603' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2980', N'K-KBWK-65D31RX-RSC0-TN-00A0', N'K-BOX 65D31R TRAKNUS', CAST(N'2023-11-17T23:52:47.247' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2981', N'K-KBWK-65D31RX-RSC0-TN-DMA0', N'K-BOX 65D31R TRAKNUS HDT MF', CAST(N'2023-11-17T23:52:47.327' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2982', N'K-KBWK-65D31RX-RSC0-TN-MFA0', N'K-BOX 65D31R TRAKNUS MF', CAST(N'2023-11-17T23:52:47.410' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2983', N'K-KBWK-65D31RX-RSC0-TR-00A0', N'K-BOX 65D31R TRANSPORT', CAST(N'2023-11-17T23:52:47.490' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2984', N'K-KBWK-67018XX-RSC0-AS-HPA0', N'K-BOX 670-18 ASAHI HPE', CAST(N'2023-11-17T23:52:47.577' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2985', N'K-KBWK-67018XX-RSC0-OH-HDA0', N'K-BOX 670-18 OHAYO', CAST(N'2023-11-17T23:52:47.660' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2986', N'K-KBWK-70027XX-RSC0-OH-HDA0', N'K-BOX 700-27 OHAYO', CAST(N'2023-11-17T23:52:47.740' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2987', N'K-KBWK-72018XX-TTSB-AM-DMA0', N'K-BOX 720-18 ALL MAKES HDT MF', CAST(N'2023-11-17T23:52:47.823' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2988', N'K-KBWK-75D31LX-RSC0-AM-PMA0', N'K-BOX 75D31L ALL MAKES PRE MF', CAST(N'2023-11-17T23:52:47.903' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2989', N'K-KBWK-D26XXXX-RSC0-AS-HPA0', N'K-BOX D26 ASAHI HPE', CAST(N'2023-11-17T23:52:47.987' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'299', N'W-CV01-95E41RX-0MHN-NL-DG00', N'CV 95E41R BLUE', CAST(N'2023-11-17T23:48:42.713' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2990', N'K-KBWK-D26XXXX-RSC0-AS-MEA0', N'K-BOX D26 ASAHI MEN', CAST(N'2023-11-17T23:52:48.067' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2991', N'K-KBWK-D26XXXX-RSC0-AS-MMA0', N'K-BOX D26 ASAHI MEN MF', CAST(N'2023-11-17T23:52:48.150' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2992', N'K-KBWK-D26XXXX-RSC0-OH-HDA0', N'K-BOX D26 OHAYO', CAST(N'2023-11-17T23:52:48.230' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2993', N'K-KBDC-E41XXXX-RSC0-AP-PRA0', N'K-BOX E41 ASPIRA PREMIUM', CAST(N'2023-11-17T23:52:48.313' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2994', N'K-KBDC-E41XXXX-RSC0-AS-HMA0', N'K-BOX E41 ASAHI HPE MF', CAST(N'2023-11-17T23:52:48.397' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2995', N'K-KBDC-E41XXXX-RSC0-IN-XPA0', N'K-BOX E41 INCOE XTRA POWER', CAST(N'2023-11-17T23:52:48.477' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2996', N'K-KBDC-E41XXXX-RSC0-IN-XPC0', N'K-BOX E41 INCOE XP - UAE', CAST(N'2023-11-17T23:52:48.560' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2997', N'K-KBDC-E41XXXX-RSC0-QU-00A0', N'K-BOX E41 QUANTUM', CAST(N'2023-11-17T23:52:48.640' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2998', N'K-KBWK-75D31LX-RSC0-TN-HDA0', N'K-BOX 75D31L TRAKNUS HDT', CAST(N'2023-11-17T23:52:48.727' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'2999', N'K-KBDC-D26XXXX-RSC0-AS-HMA0', N'K-BOX D26 ASAHI HPE MF', CAST(N'2023-11-17T23:52:48.807' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3', N'K-TC09-S45XXXX-GLFS-NL-0000', N'TOP COVER S45 GREY', CAST(N'2023-11-17T23:48:16.520' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'30', N'K-SCCV-240X045-11VJ-OH-MFA0', N'STIC COVER OHAYO MF 240X45 MM', CAST(N'2023-11-17T23:48:18.843' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'300', N'W-CV01-B20LSXX-A01N-NL-DG00', N'CV B20LS BLUE HD1-BLUE', CAST(N'2023-11-17T23:48:42.950' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3000', N'K-KBDC-D26XXXX-RSC0-IN-XPA0', N'K-BOX D26 INCOE XTRA POWER', CAST(N'2023-11-17T23:52:48.887' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3001', N'K-KBDC-D26XXXX-RSC0-IN-XPC0', N'K-BOX D26 INCOE XP - UAE', CAST(N'2023-11-17T23:52:48.967' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3002', N'K-KBDC-D26XXXX-RSC0-QU-00A0', N'K-BOX D26 QUANTUM', CAST(N'2023-11-17T23:52:49.047' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3003', N'K-KBDC-D26XXXX-RSC0-QU-00B0', N'K-BOX D26 QUANTUM - MII', CAST(N'2023-11-17T23:52:49.130' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3004', N'K-KBDC-70027XX-RSC0-IN-PPA0', N'K-BOX 700-27 INCOE PRE POWER', CAST(N'2023-11-17T23:52:49.210' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3005', N'K-KBDC-95E41RX-RSC0-IN-PPB0', N'K-BOX 95E41R HD INCOE PRE PWR', CAST(N'2023-11-17T23:52:49.293' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3006', N'K-KBDC-95E41RX-RSC0-IN-XPB0', N'K-BOX 95E41R INCOE XP - SUD', CAST(N'2023-11-17T23:52:49.377' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3007', N'K-KBDC-95E41RX-RSC0-RA-00A0', N'K-BOX 95E41R RAPTOR', CAST(N'2023-11-17T23:52:49.460' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3008', N'K-KBDC-95E41RX-TTSB-IN-MFC0', N'K-BOX 95E41R INCOE MF - DOM', CAST(N'2023-11-17T23:52:49.543' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3009', N'K-KBDC-95E41RX-TTSB-OH-MFB0', N'K-BOX 95E41R OHAYO MF - UAE', CAST(N'2023-11-17T23:52:49.623' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'301', N'W-CV01-B20LSXX-B03N-NL-DQ00', N'CV B20LS BLUE HD2-ORANGE DG-QR', CAST(N'2023-11-17T23:48:43.143' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3010', N'K-KBDC-A19XXXX-RSC0-QU-MFA0', N'K-BOX A19 QUANTUM MF', CAST(N'2023-11-17T23:52:49.707' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3011', N'K-KBDC-A19XXXX-TTSB-AP-MFA0', N'K-BOX A19 ASPIRA MF', CAST(N'2023-11-17T23:52:49.790' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3012', N'K-KBDC-A19XXXX-TTSB-IN-MFA0', N'K-BOX A19 INCOE MF', CAST(N'2023-11-17T23:52:49.870' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3013', N'K-KBDC-A19XXXX-TTSB-IN-MFB0', N'K-BOX A19 INCOE MF - UAE', CAST(N'2023-11-17T23:52:49.950' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3014', N'K-KBDC-A19XXXX-TTSB-IN-MFC0', N'K-BOX A19 INCOE MF - DOM', CAST(N'2023-11-17T23:52:50.033' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3015', N'K-KBDC-A19XXXX-TTSB-OH-MFA0', N'K-BOX A19 OHAYO MF', CAST(N'2023-11-17T23:52:50.113' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3016', N'K-KBDC-A19XXXX-TTSB-OH-MFB0', N'K-BOX A19 OHAYO MF - UAE', CAST(N'2023-11-17T23:52:50.193' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3017', N'K-KBDC-B20XXXX-RSC0-AP-PRA0', N'K-BOX B20 ASPIRA PREMIUM', CAST(N'2023-11-17T23:52:50.277' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3018', N'K-KBDC-B20XXXX-RSC0-AS-HMA0', N'K-BOX B20 ASAHI HPE MF', CAST(N'2023-11-17T23:52:50.360' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3019', N'K-KBDC-B20XXXX-RSC0-IN-XPA0', N'K-BOX B20 INCOE XTRA POWER', CAST(N'2023-11-17T23:52:50.443' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'302', N'W-CV01-B20LXXX-A01N-NL-DG00', N'CV B20L BLUE HD1-BLUE', CAST(N'2023-11-17T23:48:43.230' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3020', N'K-KBDC-70027XX-RSC0-QU-00B0', N'K-BOX 700-27 QUANTUM - MII', CAST(N'2023-11-17T23:52:50.523' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3021', N'K-KBDC-72018XX-TTSB-IN-MFA0', N'K-BOX 720-18 INCOE MF', CAST(N'2023-11-17T23:52:50.607' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3022', N'K-KBDC-72018XX-TTSB-OH-MFA0', N'K-BOX 720-18 OHAYO MF', CAST(N'2023-11-17T23:52:50.687' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3023', N'K-KBDC-75D31LX-RSC0-IN-GOA0', N'K-BOX 75D31L INCOE GOLD', CAST(N'2023-11-17T23:52:50.770' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3024', N'K-KBDC-75D31LX-RSC0-IN-PPA0', N'K-BOX 75D31L INCOE PRE POWER', CAST(N'2023-11-17T23:52:50.850' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3025', N'K-KBDC-75D31LX-RSC0-RA-00A0', N'K-BOX 75D31L RAPTOR', CAST(N'2023-11-17T23:52:50.937' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3026', N'K-KBDC-75D31RX-RSC0-AP-PRB0', N'K-BOX 75D31R ASPIRA PRE - AOP', CAST(N'2023-11-17T23:52:51.030' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3027', N'K-KBDC-75D31RX-RSC0-IN-GOA0', N'K-BOX 75D31R INCOE GOLD', CAST(N'2023-11-17T23:52:51.123' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3028', N'K-KBDC-75D31RX-RSC0-IN-PPA0', N'K-BOX 75D31R INCOE PRE POWER', CAST(N'2023-11-17T23:52:51.220' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3029', N'K-KBDC-75D31RX-RSC0-RA-00A0', N'K-BOX 75D31R RAPTOR', CAST(N'2023-11-17T23:52:51.313' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'303', N'W-CV01-B20LXXX-B03N-NL-DQ00', N'CV B20L BLUE HD2-ORANGE DG-QR', CAST(N'2023-11-17T23:48:43.307' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3030', N'K-KBDC-80D23LX-RSC0-LU-MFA0', N'K-BOX 80D23L LUCAS MF', CAST(N'2023-11-17T23:52:51.413' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3031', N'K-KBDC-80D26LX-RSC0-IN-GOA0', N'K-BOX 80D26L INCOE GOLD', CAST(N'2023-11-17T23:52:51.507' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3032', N'K-KBDC-80D26LX-RSC0-IN-PPA0', N'K-BOX 80D26L INCOE PRE POWER', CAST(N'2023-11-17T23:52:51.597' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3033', N'K-KBDC-80D26LX-RSC0-IN-XPB0', N'K-BOX 80D26L INCOE XP - SUD', CAST(N'2023-11-17T23:52:51.677' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3034', N'K-KBDC-80D26RX-RSC0-IN-GOA0', N'K-BOX 80D26R INCOE GOLD', CAST(N'2023-11-17T23:52:51.760' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3035', N'K-KBDC-80D26RX-RSC0-IN-PPA0', N'K-BOX 80D26R INCOE PRE POWER', CAST(N'2023-11-17T23:52:51.840' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3036', N'K-KBDC-80D31RX-TTSB-IN-MFA0', N'K-BOX 80D31R INCOE MF', CAST(N'2023-11-17T23:52:51.923' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3037', N'K-KBDC-80D31RX-TTSB-IN-MFB0', N'K-BOX 80D31R INCOE MF - UAE', CAST(N'2023-11-17T23:52:52.010' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3038', N'K-KBDC-95D26LX-RSC0-LU-MFA0', N'K-BOX 95D26L LUCAS MF', CAST(N'2023-11-17T23:52:52.093' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3039', N'K-KBDC-95D26RX-RSC0-LU-MFA0', N'K-BOX 95D26R LUCAS MF', CAST(N'2023-11-17T23:52:52.177' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'304', N'W-CV01-B20RSXX-A01N-NL-DG00', N'CV B20RS BLUE HD1-BLUE', CAST(N'2023-11-17T23:48:43.390' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3040', N'K-KBDC-95D31LX-RSC0-IN-GOA0', N'K-BOX 95D31L INCOE GOLD', CAST(N'2023-11-17T23:52:52.273' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3041', N'K-KBDC-95D31LX-RSC0-IN-PPA0', N'K-BOX 95D31L INCOE PRE POWER', CAST(N'2023-11-17T23:52:52.360' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3042', N'K-KBDC-95D31LX-RSC0-IN-XPB0', N'K-BOX 95D31L INCOE XP - SUD', CAST(N'2023-11-17T23:52:52.440' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3043', N'K-KBDC-95D31LX-RSC0-RA-00A0', N'K-BOX 95D31L RAPTOR', CAST(N'2023-11-17T23:52:52.523' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3044', N'K-KBDC-B20XXXX-RSC0-IN-XPC0', N'K-BOX B20 INCOE XP - UAE', CAST(N'2023-11-17T23:52:52.603' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3045', N'K-KBDC-B20XXXX-RSC0-QU-00A0', N'K-BOX B20 QUANTUM', CAST(N'2023-11-17T23:52:52.687' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3046', N'K-KBDC-B20XXXX-RSC0-QU-00B0', N'K-BOX B20 QUANTUM - MII', CAST(N'2023-11-17T23:52:52.767' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3047', N'K-KBDC-B20XXXX-RSC0-QU-MFA0', N'K-BOX B20 QUANTUM MF', CAST(N'2023-11-17T23:52:52.850' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3048', N'K-KBDC-B20XXXX-TTSB-AP-MFA0', N'K-BOX B20 ASPIRA MF', CAST(N'2023-11-17T23:52:52.930' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3049', N'K-KBDC-B20XXXX-TTSB-IN-MFA0', N'K-BOX B20 INCOE MF', CAST(N'2023-11-17T23:52:53.010' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'305', N'W-CV01-B20RSXX-B03N-NL-DQ00', N'CV B20RS BLUE HD2-ORANGE DG-QR', CAST(N'2023-11-17T23:48:43.473' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3050', N'K-KBDC-B20XXXX-TTSB-IN-MFB0', N'K-BOX B20 INCOE MF - UAE', CAST(N'2023-11-17T23:52:53.093' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3051', N'K-KBDC-B20XXXX-TTSB-IN-MFC0', N'K-BOX B20 INCOE MF - DOM', CAST(N'2023-11-17T23:52:53.173' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3052', N'K-KBDC-B20XXXX-TTSB-OH-MFA0', N'K-BOX B20 OHAYO MF', CAST(N'2023-11-17T23:52:53.253' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3053', N'K-KBDC-H52XXXX-RSC0-AS-HMA0', N'K-BOX H52 ASAHI HPE MF', CAST(N'2023-11-17T23:52:53.337' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3054', N'K-KBDC-H52XXXX-RSC0-IN-XPA0', N'K-BOX H52 INCOE XTRA POWER', CAST(N'2023-11-17T23:52:53.420' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3055', N'K-KBDC-H52XXXX-RSC0-IN-XPC0', N'K-BOX H52 INCOE XP - UAE', CAST(N'2023-11-17T23:52:53.500' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3056', N'K-KBDC-H52XXXX-RSC0-OH-MFA0', N'K-BOX H52 OHAYO MF', CAST(N'2023-11-17T23:52:53.583' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3057', N'K-KBDC-H52XXXX-RSC0-QU-00A0', N'K-BOX H52 QUANTUM', CAST(N'2023-11-17T23:52:53.667' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3058', N'K-KBDC-H52XXXX-RSC0-QU-00B0', N'K-BOX H52 QUANTUM - MII', CAST(N'2023-11-17T23:52:53.750' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3059', N'K-KBDC-H52XXXX-TTSB-AP-MFA0', N'K-BOX H52 ASPIRA MF', CAST(N'2023-11-17T23:52:53.830' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'306', N'W-CV01-B20RXXX-A01N-NL-DG00', N'CV B20R BLUE HD1-BLUE', CAST(N'2023-11-17T23:48:43.560' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3060', N'K-KBDC-H52XXXX-TTSB-IN-MFA0', N'K-BOX H52 INCOE MF', CAST(N'2023-11-17T23:52:53.927' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3061', N'K-KBDC-H52XXXX-TTSB-IN-MFB0', N'K-BOX H52 INCOE MF - UAE', CAST(N'2023-11-17T23:52:54.030' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3062', N'K-KBDC-M42LXXX-RSC0-LU-EFA0', N'K-BOX M-42L LUCAS ISS-EFB', CAST(N'2023-11-17T23:52:54.123' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3063', N'K-KBDC-M42LXXX-TTSB-IN-SSA0', N'K-BOX M-42L INCOE ISS-EFB', CAST(N'2023-11-17T23:52:54.220' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3064', N'K-KBDC-N100TXX-RSC0-IN-PPA0', N'K-BOX N100T INCOE PRE POWER', CAST(N'2023-11-17T23:52:54.310' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3065', N'K-KBDC-N150TXX-RSC0-IN-PPA0', N'K-BOX N150T INCOE PRE POWER', CAST(N'2023-11-17T23:52:54.397' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3066', N'K-KBDC-N200TXX-RSC0-IN-PPA0', N'K-BOX N200T INCOE PRE POWER', CAST(N'2023-11-17T23:52:54.497' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3067', N'K-KBDC-N70TXXX-RSC0-IN-PPA0', N'K-BOX N70T INCOE PRE POWER', CAST(N'2023-11-17T23:52:54.590' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3068', N'K-KBDC-60D26RX-TTSB-IN-MFB0', N'K-BOX 60D26R INCOE MF - UAE', CAST(N'2023-11-17T23:52:54.687' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3069', N'K-KBDC-65038XX-RSC0-IN-PPA0', N'K-BOX 650-38 INCOE PRE POWER', CAST(N'2023-11-17T23:52:54.773' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'307', N'W-CV01-B20RXXX-B03N-NL-DQ00', N'CV B20R BLUE HD2-ORANGE DG-QR', CAST(N'2023-11-17T23:48:43.640' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3070', N'K-KBDC-65038XX-TTSB-IN-MFA0', N'K-BOX 650-38 INCOE MF', CAST(N'2023-11-17T23:52:54.880' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3071', N'K-KBDC-65D26LX-RSC0-IN-GOA0', N'K-BOX 65D26L INCOE GOLD', CAST(N'2023-11-17T23:52:54.960' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3072', N'K-KBDC-65D26LX-RSC0-IN-PPA0', N'K-BOX 65D26L INCOE PRE POWER', CAST(N'2023-11-17T23:52:55.057' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3073', N'K-KBDC-65D26LX-RSC0-RA-00A0', N'K-BOX 65D26L RAPTOR', CAST(N'2023-11-17T23:52:55.153' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3074', N'K-KBDC-65D26RX-RSC0-AP-PRB0', N'K-BOX 65D26R ASPIRA PRE - AOP', CAST(N'2023-11-17T23:52:55.250' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3075', N'K-KBDC-65D26RX-RSC0-IN-GOA0', N'K-BOX 65D26R INCOE GOLD', CAST(N'2023-11-17T23:52:55.387' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3076', N'K-KBDC-65D26RX-RSC0-IN-PPA0', N'K-BOX 65D26R INCOE PRE POWER', CAST(N'2023-11-17T23:52:55.533' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3077', N'K-KBDC-65D26RX-RSC0-RA-00A0', N'K-BOX 65D26R RAPTOR', CAST(N'2023-11-17T23:52:55.630' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3078', N'K-KBDC-65D31LX-RSC0-IN-GOA0', N'K-BOX 65D31L INCOE GOLD', CAST(N'2023-11-17T23:52:55.723' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3079', N'K-KBDC-65D31LX-RSC0-IN-PPA0', N'K-BOX 65D31L INCOE PRE POWER', CAST(N'2023-11-17T23:52:55.817' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'308', N'W-CV01-B24LSXX-A01N-NL-DG00', N'CV B24LS BLUE HD1-BLUE', CAST(N'2023-11-17T23:48:43.720' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3080', N'K-KBDC-65D31LX-RSC0-IN-XPB0', N'K-BOX 65D31L INCOE XP - SUD', CAST(N'2023-11-17T23:52:55.917' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3081', N'K-KBDC-65D31LX-RSC0-QU-MFA0', N'K-BOX 65D31L QUANTUM MF', CAST(N'2023-11-17T23:52:56.013' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3082', N'K-KBDC-65D31LX-RSC0-RA-00A0', N'K-BOX 65D31L RAPTOR', CAST(N'2023-11-17T23:52:56.107' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3083', N'K-KBDC-65D31RX-RSC0-AP-PRA0', N'K-BOX 65D31R ASPIRA PREMIUM', CAST(N'2023-11-17T23:52:56.193' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3084', N'K-KBDC-65D31RX-RSC0-AP-PRB0', N'K-BOX 65D31R ASPIRA PRE - AOP', CAST(N'2023-11-17T23:52:56.283' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3085', N'K-KBDC-65D31RX-RSC0-IN-GOA0', N'K-BOX 65D31R INCOE GOLD', CAST(N'2023-11-17T23:52:56.363' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3086', N'K-KBDC-65D31RX-RSC0-IN-PPA0', N'K-BOX 65D31R INCOE PRE POWER', CAST(N'2023-11-17T23:52:56.447' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3087', N'K-KBDC-65D31RX-RSC0-IN-PPB0', N'K-BOX 65D31R HD INCOE PRE PWR', CAST(N'2023-11-17T23:52:56.533' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3088', N'K-KBDC-65D31RX-RSC0-IN-XPB0', N'K-BOX 65D31R INCOE XP - SUD', CAST(N'2023-11-17T23:52:56.623' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3089', N'K-KBDC-65D31RX-RSC0-IN-XPC0', N'K-BOX 65D31R INCOE XP - UAE', CAST(N'2023-11-17T23:52:56.703' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'309', N'W-CV01-B24LSXX-B03N-NL-DQ00', N'CV B24LS BLUE HD2-ORANGE DG-QR', CAST(N'2023-11-17T23:48:43.797' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3090', N'K-KBDC-65D31RX-RSC0-RA-00A0', N'K-BOX 65D31R RAPTOR', CAST(N'2023-11-17T23:52:56.787' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3091', N'K-KBDC-67018XX-RSC0-IN-PPA0', N'K-BOX 670-18 INCOE PRE POWER', CAST(N'2023-11-17T23:52:56.870' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3092', N'K-KBDC-67018XX-RSC0-QU-00A0', N'K-BOX 670-18 QUANTUM', CAST(N'2023-11-17T23:52:56.950' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3093', N'K-KBDC-555XXXX-RSC0-IN-MFD0', N'K-BOX 555 INCOE MF - KSA', CAST(N'2023-11-17T23:52:57.030' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3094', N'K-KBDC-566XXXX-RSC0-IN-MFD0', N'K-BOX 566 INCOE MF - KSA', CAST(N'2023-11-17T23:52:57.113' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3095', N'K-KBDC-M42RXXX-TTSB-IN-SSA0', N'K-BOX M-42R INCOE ISS-EFB', CAST(N'2023-11-17T23:52:57.193' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3096', N'K-KBDC-NS40ZLX-RSC0-UL-MFA0', N'K-BOX NS40ZL UTRA MF', CAST(N'2023-11-17T23:52:57.280' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3097', N'K-KBDC-NS60SXX-RSC0-UL-MFA0', N'K-BOX NS60S UTRA MF', CAST(N'2023-11-17T23:52:57.363' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3098', N'K-KBDC-NS60LSX-RSC0-UL-MFA0', N'K-BOX NS60LS UTRA MF', CAST(N'2023-11-17T23:52:57.447' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3099', N'K-KBDC-NS70XXX-RSC0-UL-MFA0', N'K-BOX NS70 UTRA MF', CAST(N'2023-11-17T23:52:57.537' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'31', N'K-SCCV-240X045-15V0-IN-MFB0', N'STC CVR INCOE MF UAE 240X45 MM', CAST(N'2023-11-17T23:48:18.930' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'310', N'W-CV01-B24LXXX-A01N-NL-DG00', N'CV B24L BLUE HD1-BLUE', CAST(N'2023-11-17T23:48:43.877' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3100', N'K-KBDC-NS70LXX-RSC0-UL-MFA0', N'K-BOX NS70L UTRA MF', CAST(N'2023-11-17T23:52:57.623' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3101', N'K-KBDC-48D26RX-RSC0-AP-HYA0', N'K-BOX 48D26R ASPIRA HYBRID', CAST(N'2023-11-17T23:52:57.710' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3102', N'K-KBDC-55D26RX-RSC0-AP-HYA0', N'K-BOX 55D26R ASPIRA HYBRID', CAST(N'2023-11-17T23:52:57.790' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3103', N'K-KBDC-65D31RX-RSC0-AP-HYA0', N'K-BOX 65D31R ASPIRA HYBRID', CAST(N'2023-11-17T23:52:57.873' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3104', N'K-KBDC-75D31RX-RSC0-AP-HYA0', N'K-BOX 75D31R ASPIRA HYBRID', CAST(N'2023-11-17T23:52:57.960' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3105', N'K-KBDC-32B20LX-RSC0-AP-HYA0', N'K-BOX 32B20L ASPIRA HYBRID', CAST(N'2023-11-17T23:52:58.040' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3106', N'K-KBDC-32B20RX-RSC0-AP-HYA0', N'K-BOX 32B20R ASPIRA HYBRID', CAST(N'2023-11-17T23:52:58.120' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3107', N'K-KBDC-36B20LX-RSC0-AP-HYA0', N'K-BOX 36B20L ASPIRA HYBRID', CAST(N'2023-11-17T23:52:58.203' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3108', N'K-KBDC-36B20RX-RSC0-AP-HYA0', N'K-BOX 36B20R ASPIRA HYBRID', CAST(N'2023-11-17T23:52:58.283' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3109', N'K-KBDC-M42LXXX-RSC0-NI-PRA0', N'K-BOX M42L NIKO PREMIUM', CAST(N'2023-11-17T23:52:58.367' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'311', N'W-CV01-B24LXXX-B03N-NL-DQ00', N'CV B24L BLUE HD2-ORANGE DG-QR', CAST(N'2023-11-17T23:48:43.957' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3110', N'K-KBDC-M42RXXX-RSC0-NI-PRA0', N'K-BOX M42R NIKO PREMIUM', CAST(N'2023-11-17T23:52:58.453' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3111', N'K-KBDC-Q85LXXX-RSC0-NI-PRA0', N'K-BOX Q85L NIKO PREMIUM', CAST(N'2023-11-17T23:52:58.533' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3112', N'K-KBWK-165G51L-RSC0-AM-HDA0', N'K-BOX 165G51L ALL MAKES HDT', CAST(N'2023-11-17T23:52:58.617' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3113', N'K-KBDC-115F51X-TTSB-OH-MFZ0', N'K-BOX 115F51 OHAYO MF', CAST(N'2023-11-17T23:52:58.697' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3114', N'K-KBDC-145G51X-RSC0-AS-HMZ0', N'K-BOX 145G51 ASAHI HPE MF', CAST(N'2023-11-17T23:52:58.780' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3115', N'K-KBDC-145G51X-TTSB-OH-MFZ0', N'K-BOX 145G51 OHAYO MF', CAST(N'2023-11-17T23:52:58.863' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3116', N'K-KBDC-555XXXX-RSC0-IN-XPV0', N'K-BOX 555 INCOE XP - V', CAST(N'2023-11-17T23:52:58.943' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3117', N'K-KBDC-566XXXX-RSC0-IN-XPV0', N'K-BOX 566 INCOE XP - V', CAST(N'2023-11-17T23:52:59.023' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3118', N'K-KBDC-566XXXX-TTSB-IN-MFY0', N'K-BOX 566 INCOE MF - Y', CAST(N'2023-11-17T23:52:59.107' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3119', N'K-KBDC-70027XX-RSC0-IN-XPW0', N'K-BOX 700-27 INCOE XP - W', CAST(N'2023-11-17T23:52:59.187' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'312', N'W-CV01-B24RSXX-A01N-NL-DG00', N'CV B24RS BLUE HD1-BLUE', CAST(N'2023-11-17T23:48:44.037' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3120', N'K-KBDC-95E41LX-RSC0-AS-HMZ0', N'K-BOX 95E41L ASAHI HPE MF', CAST(N'2023-11-17T23:52:59.270' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3121', N'K-KBDC-95E41LX-TTSB-OH-MFZ0', N'K-BOX 95E41L OHAYO MF', CAST(N'2023-11-17T23:52:59.350' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3122', N'K-KBDC-95E41RX-TTSB-OH-MFZ0', N'K-BOX 95E41R OHAYO MF', CAST(N'2023-11-17T23:52:59.433' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3123', N'K-KBDC-B20XXXX-RSC0-IN-XPV0', N'K-BOX B20 INCOE XP - V', CAST(N'2023-11-17T23:52:59.517' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3124', N'K-KBDC-D26XXXX-RSC0-IN-XPY0', N'K-BOX D26 INCOE XP - Y', CAST(N'2023-11-17T23:52:59.600' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3125', N'K-KBDC-H52XXXX-TTSB-OH-MFY0', N'K-BOX H52 OHAYO MF - Y', CAST(N'2023-11-17T23:52:59.683' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3126', N'K-KBDC-N100XXX-RSC0-NI-00Z0', N'K-BOX N100 NIKO', CAST(N'2023-11-17T23:52:59.807' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3127', N'K-KBDC-N120XXX-RSC0-NI-00Z0', N'K-BOX N120 NIKO', CAST(N'2023-11-17T23:52:59.887' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3128', N'K-KBDC-N200XXX-RSC0-NI-00Z0', N'K-BOX N200 NIKO', CAST(N'2023-11-17T23:52:59.970' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3129', N'K-KBDC-NS40ZLX-RSC0-NI-00Z0', N'K-BOX NS40ZL NIKO', CAST(N'2023-11-17T23:53:00.077' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'313', N'W-CV01-B24RSXX-B03N-NL-DQ00', N'CV B24RS BLUE HD2-ORANGE DG-QR', CAST(N'2023-11-17T23:48:44.120' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3130', N'K-KBDC-NS60LXX-RSC0-NI-00Z0', N'K-BOX NS60L NIKO', CAST(N'2023-11-17T23:53:00.157' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3131', N'K-KBDC-NS60SXX-RSC0-NI-00Z0', N'K-BOX NS60S NIKO', CAST(N'2023-11-17T23:53:00.240' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3132', N'K-KBDC-NS60XXX-RSC0-NI-00Z0', N'K-BOX NS60 NIKO', CAST(N'2023-11-17T23:53:00.320' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3133', N'K-KBDC-NS70LXX-RSC0-NI-00Z0', N'K-BOX NS70L NIKO', CAST(N'2023-11-17T23:53:00.403' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3134', N'K-KBDC-NS70XXX-RSC0-NI-00Z0', N'K-BOX NS70 NIKO', CAST(N'2023-11-17T23:53:00.487' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3135', N'K-KBDC-NX1207L-RSC0-NI-00Z0', N'K-BOX NX120-7L NIKO', CAST(N'2023-11-17T23:53:00.570' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3136', N'K-KBDC-46B24LS-RSC0-AP-HYA0', N'K-BOX 46B24LS ASPIRA HYBRID', CAST(N'2023-11-17T23:53:00.650' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3137', N'K-KBDC-46B24LX-RSC0-AP-HYA0', N'K-BOX 46B24L ASPIRA HYBRID', CAST(N'2023-11-17T23:53:00.897' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3138', N'K-KBDC-46B24RX-RSC0-AP-HYA0', N'K-BOX 46B24R ASPIRA HYBRID', CAST(N'2023-11-17T23:53:01.243' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3139', N'K-KBDC-65D26RX-RSC0-AP-HYA0', N'K-BOX 65D26R ASPIRA HYBRID', CAST(N'2023-11-17T23:53:01.327' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'314', N'W-CV01-B24RXXX-A01N-NL-DG00', N'CV B24R BLUE HD1-BLUE', CAST(N'2023-11-17T23:48:44.203' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3140', N'K-KBDC-NX1207X-RSC0-NI-00Z0', N'K-BOX NX120-7 NIKO', CAST(N'2023-11-17T23:53:01.413' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3141', N'K-KBWK-D26XXXX-RSC0-AS-MEZ0', N'K-BOX D26 ASAHI MEN', CAST(N'2023-11-17T23:53:01.503' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3142', N'K-KBWK-E41XXXX-RSC0-AS-MEZ0', N'K-BOX E41 ASAHI MEN', CAST(N'2023-11-17T23:53:01.600' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3143', N'K-SCTY-50B24RS-00TC-MF-V07A', N'STIC TYPE MF 50B24RS V07', CAST(N'2023-11-17T23:53:01.693' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3144', N'K-SCTY-50B24RS-00TC-MF-V08A', N'STIC TYPE MF 50B24RS V08', CAST(N'2023-11-17T23:53:01.797' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3145', N'K-SCTY-50B24RX-00TC-MF-V07A', N'STIC TYPE MF 50B24R V07', CAST(N'2023-11-17T23:53:01.900' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3146', N'K-SCTY-50B24RX-00TC-MF-V08A', N'STIC TYPE MF 50B24R V08', CAST(N'2023-11-17T23:53:02.007' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3147', N'K-SCTY-54459XX-00CA-CV-V03A', N'STIC TYPE CONV 544-59 V03', CAST(N'2023-11-17T23:53:02.110' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3148', N'K-SCTY-54459XX-00CA-MF-V03A', N'STIC TYPE MF 544-59 V03', CAST(N'2023-11-17T23:53:02.213' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3149', N'K-SCTY-54459XX-00TB-CV-V06A', N'STIC TYPE CONV 544-59 V06', CAST(N'2023-11-17T23:53:02.310' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'315', N'W-CV01-B24RXXX-B03N-NL-DQ00', N'CV B24R BLUE HD2-ORANGE DG-QR', CAST(N'2023-11-17T23:48:44.287' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3150', N'K-SCTY-54459XX-00TB-MF-V05A', N'STIC TYPE MF 544-59 V05', CAST(N'2023-11-17T23:53:02.397' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3151', N'K-SCTY-N50ZLXX-00CA-MF-V02A', N'STIC TYPE MF N50ZL V02', CAST(N'2023-11-17T23:53:02.480' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3152', N'K-SCTY-N50ZXXX-00CA-MF-V02A', N'STIC TYPE MF N50Z V02', CAST(N'2023-11-17T23:53:02.570' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3153', N'K-SCTY-N70LXXX-00CA-MF-V02A', N'STIC TYPE MF N70L V02', CAST(N'2023-11-17T23:53:02.657' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3154', N'K-SCTY-N70TXXX-00CA-DC-V02A', N'STIC TYPE N70T V02', CAST(N'2023-11-17T23:53:02.740' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3155', N'K-SCTY-N70XXXX-0000-CV-IAPC', N'STIC TYPE CONV N70 IAPC', CAST(N'2023-11-17T23:53:02.820' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3156', N'K-SCTY-N70XXXX-00CA-MF-V02A', N'STIC TYPE MF N70 V02', CAST(N'2023-11-17T23:53:02.903' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3157', N'K-SCTY-N70ZDXX-00CA-MF-V03A', N'STIC TYPE MF N70Z-D V03', CAST(N'2023-11-17T23:53:02.990' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3158', N'K-SCTY-N70ZDXX-00TB-MF-V05A', N'STIC TYPE MF N70Z-D V05', CAST(N'2023-11-17T23:53:03.073' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3159', N'K-SCTY-N70ZDXX-00TC-MF-V06A', N'STIC TYPE MF N70Z-D V06', CAST(N'2023-11-17T23:53:03.160' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'316', N'K-SCCV-180X045-11V0-BL-MFB0', N'STC CVR NO BRAND MF-B 180X45 M', CAST(N'2023-11-17T23:48:44.373' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3160', N'K-SCTY-N70ZLXX-00CA-MF-V02A', N'STIC TYPE MF N70ZL V02', CAST(N'2023-11-17T23:53:03.243' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3161', N'K-SCTY-N70ZXXX-0000-CV-IADG', N'STICKER TYPE N70Z ISUZU DOM', CAST(N'2023-11-17T23:53:03.340' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3162', N'K-SCTY-N70ZXXX-00CA-MF-V02A', N'STIC TYPE MF N70Z V02', CAST(N'2023-11-17T23:53:03.440' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3163', N'K-SCTY-NS40LXX-00CA-MF-V02A', N'STIC TYPE MF NS40L V02', CAST(N'2023-11-17T23:53:03.537' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3164', N'K-SCTY-NS40XXX-00CA-MF-V02A', N'STIC TYPE MF NS40 V02', CAST(N'2023-11-17T23:53:03.637' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3165', N'K-SCTY-NS40ZLX-0000-CV-IAPC', N'STIC TYPE CONV NS40ZL IAPC', CAST(N'2023-11-17T23:53:03.727' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3166', N'K-SCTY-NS40ZLX-00CA-MF-V02A', N'STIC TYPE MF NS40ZL V02', CAST(N'2023-11-17T23:53:03.813' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3167', N'K-SCTY-NS40ZSX-00CA-MF-V02A', N'STIC TYPE MF NS40ZS V02', CAST(N'2023-11-17T23:53:03.907' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3168', N'K-SCTY-NS40ZXX-00CA-MF-V02A', N'STIC TYPE MF NS40Z V02', CAST(N'2023-11-17T23:53:04.027' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3169', N'K-SCTY-NS50LXX-00CA-MF-V03A', N'STIC TYPE MF NS50L V03', CAST(N'2023-11-17T23:53:04.130' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'317', N'K-TC10-S20XXXX-USFS-NL-0000', N'TOP COVER S20 BLACK', CAST(N'2023-11-17T23:48:44.470' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3170', N'K-SCTY-48D26LX-00CA-MF-V03A', N'STIC TYPE MF 48D26L V03', CAST(N'2023-11-17T23:53:04.223' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3171', N'K-SCTY-48D26LX-00TB-CV-V06A', N'STIC TYPE CONV 48D26L V06', CAST(N'2023-11-17T23:53:04.317' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3172', N'K-SCTY-48D26LX-00TC-CV-V07A', N'STIC TYPE CONV 48D26L V07', CAST(N'2023-11-17T23:53:04.403' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3173', N'K-SCTY-48D26LX-07CA-CV-V02A', N'STIC TYPE CONV 48D26L 07 V02', CAST(N'2023-11-17T23:53:04.490' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3174', N'K-SCTY-48D26RX-00CA-CV-V01A', N'STIC TYPE CONV 48D26R V01', CAST(N'2023-11-17T23:53:04.587' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3175', N'K-SCTY-48D26RX-00CA-CV-V03A', N'STIC TYPE CONV 48D26R V03', CAST(N'2023-11-17T23:53:04.670' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3176', N'K-SCTY-48D26RX-00CA-CV-V04A', N'STIC TYPE CONV 48D26R V04', CAST(N'2023-11-17T23:53:04.753' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3177', N'K-SCTY-48D26RX-00CA-MF-V01A', N'STIC TYPE MF 48D26R V01', CAST(N'2023-11-17T23:53:04.850' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3178', N'K-SCTY-48D26RX-00CA-MF-V03A', N'STIC TYPE MF 48D26R V03', CAST(N'2023-11-17T23:53:05.010' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3179', N'K-SCTY-48D26RX-00TB-CV-V06A', N'STIC TYPE CONV 48D26R V06', CAST(N'2023-11-17T23:53:05.100' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'318', N'K-CV06-566LXXX-J06N-NL-0000', N'CV 566L WHITE HD-WHITE', CAST(N'2023-11-17T23:48:44.577' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3180', N'K-SCTY-48D26RX-00TC-CV-V07A', N'STIC TYPE CONV 48D26R V07', CAST(N'2023-11-17T23:53:05.187' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3181', N'K-SCTY-48D26RX-07CA-CV-V02A', N'STIC TYPE CONV 48D26R 07 V02', CAST(N'2023-11-17T23:53:05.273' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3182', N'K-SCTY-50B24LS-00TC-MF-V07A', N'STIC TYPE MF 50B24LS V07', CAST(N'2023-11-17T23:53:05.367' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3183', N'K-SCTY-50B24LS-00TC-MF-V08A', N'STIC TYPE MF 50B24LS V08', CAST(N'2023-11-17T23:53:05.453' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3184', N'K-SCTY-50B24LX-00TC-MF-V07A', N'STIC TYPE MF 50B24L V07', CAST(N'2023-11-17T23:53:05.550' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3185', N'K-SCTY-50B24LX-00TC-MF-V08A', N'STIC TYPE MF 50B24L V08', CAST(N'2023-11-17T23:53:05.640' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3186', N'K-SCTY-NS60LSX-00CA-MF-V02A', N'STIC TYPE MF NS60LS V02', CAST(N'2023-11-17T23:53:05.727' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3187', N'K-SCTY-NS60LXX-00CA-MF-V02A', N'STIC TYPE MF NS60L V02', CAST(N'2023-11-17T23:53:05.810' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3188', N'K-SCTY-NS60SXX-00CA-MF-V02A', N'STIC TYPE MF NS60S V02', CAST(N'2023-11-17T23:53:05.903' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3189', N'K-SCTY-NS60XXX-00CA-MF-V02A', N'STIC TYPE MF NS60 V02', CAST(N'2023-11-17T23:53:05.990' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'319', N'K-CV06-588LXXX-K06N-NL-0000', N'CV 588L WHITE HD-WHITE', CAST(N'2023-11-17T23:48:44.670' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3190', N'K-SCTY-NS70LXX-00CA-MF-V02A', N'STIC TYPE MF NS70L V02', CAST(N'2023-11-17T23:53:06.130' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3191', N'K-SCTY-80D26LX-00CA-CV-V05A', N'STIC TYPE CONV 80D26L V05 ASPR', CAST(N'2023-11-17T23:53:06.410' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3192', N'K-SCTY-80D26LX-00CA-MF-V01A', N'STIC TYPE MF 80D26L V01', CAST(N'2023-11-17T23:53:06.500' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3193', N'K-SCTY-80D26LX-00CA-MF-V03A', N'STIC TYPE MF 80D26L V03', CAST(N'2023-11-17T23:53:06.580' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3194', N'K-SCTY-80D26LX-00CC-MF-V04A', N'STIC TYPE MF 80D26L V04', CAST(N'2023-11-17T23:53:06.683' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3195', N'K-SCTY-80D26LX-00TB-CV-V06A', N'STIC TYPE CONV 80D26L V06', CAST(N'2023-11-17T23:53:06.770' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3196', N'K-SCTY-80D26LX-00TC-CV-V07A', N'STIC TYPE CONV 80D26L V07', CAST(N'2023-11-17T23:53:06.857' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3197', N'K-SCTY-80D26LX-12CA-CV-V02A', N'STIC TYPE CONV 80D26L 12 V02', CAST(N'2023-11-17T23:53:06.947' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3198', N'K-SCTY-80D26RX-00CA-CV-V01A', N'STIC TYPE CONV 80D26R V01', CAST(N'2023-11-17T23:53:07.033' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3199', N'K-SCTY-32C24RX-00CA-MF-V03A', N'STIC TYPE MF 32C24R V03', CAST(N'2023-11-17T23:53:07.120' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'32', N'K-SCCV-252X050-11V0-AP-MFA0', N'STIC CVR ASPIRA MF 252X50 MM', CAST(N'2023-11-17T23:48:19.020' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'320', N'K-CV06-DINBLXX-0MHN-NL-0000', N'CV DIN-B-L WHITE', CAST(N'2023-11-17T23:48:44.767' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3200', N'K-SCTY-32C24RX-00TB-CV-V06A', N'STIC TYPE CONV 32C24R V06', CAST(N'2023-11-17T23:53:07.203' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3201', N'K-SCTY-32C24RX-00TC-CV-V07A', N'STIC TYPE CONV 32C24R V07', CAST(N'2023-11-17T23:53:07.293' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3202', N'K-SCTY-32C24RX-09CA-CV-V02A', N'STIC TYPE CONV 32C24R 09 V02', CAST(N'2023-11-17T23:53:07.380' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3203', N'K-SCTY-36B20LS-00CA-CV-V01A', N'STIC TYPE CONV 36B20LS V01', CAST(N'2023-11-17T23:53:07.470' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3204', N'K-SCTY-36B20LS-00CA-CV-V03A', N'STIC TYPE CONV 36B20LS V03', CAST(N'2023-11-17T23:53:07.557' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3205', N'K-SCTY-36B20LS-00CA-MF-V03A', N'STIC TYPE MF 36B20LS V03', CAST(N'2023-11-17T23:53:07.650' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3206', N'K-SCTY-36B20LS-00TB-CV-V06A', N'STIC TYPE CONV 36B20LS V06', CAST(N'2023-11-17T23:53:07.737' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3207', N'K-SCTY-36B20LS-00TC-CV-V07A', N'STIC TYPE CONV 36B20LS V07', CAST(N'2023-11-17T23:53:07.823' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3208', N'K-SCTY-36B20LS-09CA-CV-V02A', N'STIC TYPE CONV 36B20LS 09 V02', CAST(N'2023-11-17T23:53:07.923' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3209', N'K-SCTY-36B20LX-00CA-CV-V01A', N'STIC TYPE CONV 36B20L V01', CAST(N'2023-11-17T23:53:08.027' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'321', N'K-CV06-DINCLXX-0MHN-NL-0000', N'CV DIN-C-L WHITE', CAST(N'2023-11-17T23:48:44.870' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3210', N'K-SCTY-36B20LX-00CA-CV-V03A', N'STIC TYPE CONV 36B20L V03', CAST(N'2023-11-17T23:53:08.110' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3211', N'K-SCTY-36B20LX-00CA-CV-V04A', N'STIC TYPE CONV 36B20L V04', CAST(N'2023-11-17T23:53:08.193' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3212', N'K-SCTY-36B20LX-00CA-MF-V01A', N'STIC TYPE MF 36B20L V01', CAST(N'2023-11-17T23:53:08.293' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3213', N'K-SCTY-N200TXX-00CA-DC-V02A', N'STIC TYPE N200T V02', CAST(N'2023-11-17T23:53:08.387' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3214', N'K-SCTY-N200XXX-0000-CV-IAPC', N'STIC TYPE CONV N200 IAPC', CAST(N'2023-11-17T23:53:08.487' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3215', N'K-SCTY-N200XXX-00CA-MF-V02A', N'STIC TYPE MF N200 V02', CAST(N'2023-11-17T23:53:08.583' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3216', N'K-SCTY-N40LXXX-0000-CV-IAPC', N'STIC TYPE CONV N40L IAPC', CAST(N'2023-11-17T23:53:08.670' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3217', N'K-SCTY-N40LXXX-00CA-MF-V02A', N'STIC TYPE MF N40L V02', CAST(N'2023-11-17T23:53:08.760' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3218', N'K-SCTY-N40XXXX-00CA-MF-V02A', N'STIC TYPE MF N40 V02', CAST(N'2023-11-17T23:53:08.843' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3219', N'K-SCTY-N50LXXX-0000-CV-IAPC', N'STIC TYPE CONV N50L IAPC', CAST(N'2023-11-17T23:53:08.927' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'322', N'K-CV11-GC8VXXX-0MHN-NL-0000', N'CV GC-8V GREEN (SPINCAP)', CAST(N'2023-11-17T23:48:44.960' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3220', N'K-SCTY-N50LXXX-00CA-MF-V02A', N'STIC TYPE MF N50L V02', CAST(N'2023-11-17T23:53:09.007' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3221', N'K-SCTY-55D26LX-00TB-CV-V06A', N'STIC TYPE CONV 55D26L V06', CAST(N'2023-11-17T23:53:09.087' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3222', N'K-SCTY-55D26LX-00TC-CV-V07A', N'STIC TYPE CONV 55D26L V07', CAST(N'2023-11-17T23:53:09.183' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3223', N'K-SCTY-55D26LX-09CA-CV-V02A', N'STIC TYPE CONV 55D26L 09 V02', CAST(N'2023-11-17T23:53:09.263' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3224', N'K-SCTY-55D26RX-00CA-CV-V01A', N'STIC TYPE CONV 55D26R V01', CAST(N'2023-11-17T23:53:09.343' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3225', N'K-SCTY-65D26RX-11CA-CV-V02A', N'STIC TYPE CONV 65D26R 11 V02', CAST(N'2023-11-17T23:53:09.427' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3226', N'K-SCTY-32B20LX-00CA-CV-V04A', N'STIC TYPE CONV 32B20L V04', CAST(N'2023-11-17T23:53:09.510' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3227', N'K-SCTY-32B20LX-00CA-MF-V03A', N'STIC TYPE MF 32B20L V03', CAST(N'2023-11-17T23:53:09.597' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3228', N'K-SCTY-32B20LX-00TB-CV-V06A', N'STIC TYPE CONV 32B20L V06', CAST(N'2023-11-17T23:53:09.680' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3229', N'K-SCTY-32B20LX-00TC-CV-V07A', N'STIC TYPE CONV 32B20L V07', CAST(N'2023-11-17T23:53:09.767' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'323', N'K-SCCV-180X045-11V0-NI-PRA0', N'STIC COVER NIKO PRE 180X45 MM', CAST(N'2023-11-17T23:48:45.043' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3230', N'K-SCTY-32B20LX-07CA-CV-V02A', N'STIC TYPE CONV 32B20L 07 V02', CAST(N'2023-11-17T23:53:09.847' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3231', N'K-SCTY-32B20RS-00CA-CV-V01A', N'STIC TYPE CONV 32B20RS V01', CAST(N'2023-11-17T23:53:09.927' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3232', N'K-SCTY-32B20RS-00CA-CV-V03A', N'STIC TYPE CONV 32B20RS V03', CAST(N'2023-11-17T23:53:10.007' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3233', N'K-SCTY-32B20RS-00CA-MF-V03A', N'STIC TYPE MF 32B20RS V03', CAST(N'2023-11-17T23:53:10.090' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3234', N'K-SCTY-32B20RS-00TB-CV-V06A', N'STIC TYPE CONV 32B20RS V06', CAST(N'2023-11-17T23:53:10.170' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3235', N'K-SCTY-32B20RS-00TC-CV-V07A', N'STIC TYPE CONV 32B20RS V07', CAST(N'2023-11-17T23:53:10.250' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3236', N'K-SCTY-32B20RS-07CA-CV-V02A', N'STIC TYPE CONV 32B20RS 07 V02', CAST(N'2023-11-17T23:53:10.330' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3237', N'K-SCTY-32B20RX-00CA-CV-V01A', N'STIC TYPE CONV 32B20R V01', CAST(N'2023-11-17T23:53:10.410' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3238', N'K-SCTY-32B20RX-00CA-CV-V03A', N'STIC TYPE CONV 32B20R V03', CAST(N'2023-11-17T23:53:10.493' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3239', N'K-SCTY-32B20RX-00CA-CV-V04A', N'STIC TYPE CONV 32B20R V04', CAST(N'2023-11-17T23:53:10.573' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'324', N'K-SCCV-220X045-11V0-NI-PRA0', N'STIC COVER NIKO PRE 220X45 MM', CAST(N'2023-11-17T23:48:45.123' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3240', N'K-SCTY-32B20RX-00CA-MF-V03A', N'STIC TYPE MF 32B20R V03', CAST(N'2023-11-17T23:53:10.653' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3241', N'K-SCTY-32B20RX-00TB-CV-V06A', N'STIC TYPE CONV 32B20R V06', CAST(N'2023-11-17T23:53:10.737' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3242', N'K-SCTY-32B20RX-00TC-CV-V07A', N'STIC TYPE CONV 32B20R V07', CAST(N'2023-11-17T23:53:10.817' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3243', N'K-SCTY-32B20RX-07CA-CV-V02A', N'STIC TYPE CONV 32B20R 07 V02', CAST(N'2023-11-17T23:53:10.897' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3244', N'K-SCTY-32C24LX-00CA-CV-V01A', N'STIC TYPE CONV 32C24L V01', CAST(N'2023-11-17T23:53:10.980' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3245', N'K-SCTY-32C24LX-00CA-CV-V03A', N'STIC TYPE CONV 32C24L V03', CAST(N'2023-11-17T23:53:11.073' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3246', N'K-SCTY-32C24LX-00CA-CV-V04A', N'STIC TYPE CONV 32C24L V04', CAST(N'2023-11-17T23:53:11.170' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3247', N'K-SCTY-32C24LX-00CA-MF-V03A', N'STIC TYPE MF 32C24L V03', CAST(N'2023-11-17T23:53:11.267' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3248', N'K-SCTY-32C24LX-00TB-CV-V06A', N'STIC TYPE CONV 32C24L V06', CAST(N'2023-11-17T23:53:11.350' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3249', N'K-SCTY-36B20LX-00CA-MF-V03A', N'STIC TYPE MF 36B20L V03', CAST(N'2023-11-17T23:53:11.433' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'325', N'W-CV01-32B20LS-A01N-NL-DG00', N'CV 32B20LS BLUE HD1-BLUE', CAST(N'2023-11-17T23:48:45.203' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3250', N'K-SCTY-36B20LX-00TB-CV-V06A', N'STIC TYPE CONV 36B20L V06', CAST(N'2023-11-17T23:53:11.527' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3251', N'K-SCTY-36B20LX-00TC-CV-V07A', N'STIC TYPE CONV 36B20L V07', CAST(N'2023-11-17T23:53:11.610' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3252', N'K-SCTY-36B20LX-09CA-CV-V02A', N'STIC TYPE CONV 36B20L 09 V02', CAST(N'2023-11-17T23:53:11.697' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3253', N'K-SCTY-36B20RS-00CA-CV-V01A', N'STIC TYPE CONV 36B20RS V01', CAST(N'2023-11-17T23:53:11.777' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3254', N'K-SCTY-36B20RS-00CA-CV-V03A', N'STIC TYPE CONV 36B20RS V03', CAST(N'2023-11-17T23:53:11.857' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3255', N'K-SCTY-36B20RS-00CA-MF-V03A', N'STIC TYPE MF 36B20RS V03', CAST(N'2023-11-17T23:53:11.940' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3256', N'K-SCTY-36B20RS-00TB-CV-V06A', N'STIC TYPE CONV 36B20RS V06', CAST(N'2023-11-17T23:53:12.020' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3257', N'K-SCTY-36B20RS-00TC-CV-V07A', N'STIC TYPE CONV 36B20RS V07', CAST(N'2023-11-17T23:53:12.100' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3258', N'K-SCTY-65D31LX-00CA-CV-V01A', N'STIC TYPE CONV 65D31L V01', CAST(N'2023-11-17T23:53:12.180' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3259', N'K-SCTY-65D31LX-00CA-CV-V03A', N'STIC TYPE CONV 65D31L V03', CAST(N'2023-11-17T23:53:12.260' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'326', N'W-CV01-32B20LX-A01N-NL-DG00', N'CV 32B20L BLUE HD1-BLUE', CAST(N'2023-11-17T23:48:45.283' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3260', N'K-SCTY-65D31LX-00CA-CV-V04A', N'STIC TYPE CONV 65D31L V04', CAST(N'2023-11-17T23:53:12.343' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3261', N'K-SCTY-65D31LX-00CA-MF-V01A', N'STIC TYPE MF 65D31L V01', CAST(N'2023-11-17T23:53:12.440' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3262', N'K-SCTY-180G51X-00CA-MF-V03A', N'STIC TYPE MF 180G51 HD V03', CAST(N'2023-11-17T23:53:12.530' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3263', N'K-SCTY-190H52X-00CA-CV-V01A', N'STIC TYPE CONV 190H52 V01', CAST(N'2023-11-17T23:53:12.623' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3264', N'K-SCTY-190H52X-00CA-CV-V03A', N'STIC TYPE CONV 190H52 V03', CAST(N'2023-11-17T23:53:12.723' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3265', N'K-SCTY-190H52X-00CA-CV-V04A', N'STIC TYPE CONV 190H52 V04', CAST(N'2023-11-17T23:53:12.820' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3266', N'K-SCTY-190H52X-00CA-MF-V03A', N'STIC TYPE MF 190H52 V03', CAST(N'2023-11-17T23:53:12.917' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3267', N'K-SCTY-190H52X-00TB-CV-V06A', N'STIC TYPE CONV 190H52 V06', CAST(N'2023-11-17T23:53:13.007' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3268', N'K-SCTY-190H52X-00TC-CV-V07A', N'STIC TYPE CONV 190H52 V07', CAST(N'2023-11-17T23:53:13.090' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3269', N'K-SCTY-190H52X-29CA-CV-V02A', N'STIC TYPE CONV 190H52 29 V02', CAST(N'2023-11-17T23:53:13.167' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'327', N'W-CV01-32B20LX-B03N-NL-DG00', N'CV 32B20L BLUE HD2-ORANGE', CAST(N'2023-11-17T23:48:45.363' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3270', N'K-SCTY-190H52X-31CA-CV-V02A', N'STIC TYPE CONV 190H52 31 V02', CAST(N'2023-11-17T23:53:13.243' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3271', N'K-SCTY-190H52X-33CA-CV-V02A', N'STIC TYPE CONV 190H52 33 V02', CAST(N'2023-11-17T23:53:13.323' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3272', N'K-SCTY-190H52X-35CA-CV-V02A', N'STIC TYPE CONV 190H52 35 V02', CAST(N'2023-11-17T23:53:13.407' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3273', N'K-SCTY-210H52X-00CA-MF-V03A', N'STIC TYPE MF 210H52 HD V03', CAST(N'2023-11-17T23:53:13.487' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3274', N'K-SCTY-210H52X-00TC-MF-V07A', N'STIC TYPE MF 210H52 V07', CAST(N'2023-11-17T23:53:13.567' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3275', N'K-SCTY-210H52X-00TC-MF-V08A', N'STIC TYPE MF 210H52 V08', CAST(N'2023-11-17T23:53:13.647' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3276', N'K-SCTY-26A19LX-00CA-MF-V03A', N'STIC TYPE MF 26A19L V03', CAST(N'2023-11-17T23:53:13.727' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3277', N'K-SCTY-26A19LX-00TB-MF-V05A', N'STIC TYPE MF 26A19L V05', CAST(N'2023-11-17T23:53:13.807' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3278', N'K-SCTY-26A19LX-00TC-MF-V06A', N'STIC TYPE MF 26A19L V06', CAST(N'2023-11-17T23:53:13.890' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3279', N'K-SCTY-26A19RX-00CA-MF-V03A', N'STIC TYPE MF 26A19R V03', CAST(N'2023-11-17T23:53:13.970' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'328', N'W-CV01-32B20RX-A01N-NL-DG00', N'CV 32B20R BLUE HD1-BLUE', CAST(N'2023-11-17T23:48:45.443' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3280', N'K-SCTY-26A19RX-00TB-MF-V05A', N'STIC TYPE MF 26A19R V05', CAST(N'2023-11-17T23:53:14.050' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3281', N'K-SCTY-26A19RX-00TC-MF-V06A', N'STIC TYPE MF 26A19R V06', CAST(N'2023-11-17T23:53:14.130' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3282', N'K-SCTY-32B20LS-00CA-CV-V01A', N'STIC TYPE CONV 32B20LS V01', CAST(N'2023-11-17T23:53:14.203' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3283', N'K-SCTY-32B20LS-00CA-CV-V03A', N'STIC TYPE CONV 32B20LS V03', CAST(N'2023-11-17T23:53:14.283' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3284', N'K-SCTY-32B20LS-00CA-MF-V03A', N'STIC TYPE MF 32B20LS V03', CAST(N'2023-11-17T23:53:14.363' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3285', N'K-SCTY-100D31L-00TC-MF-V07A', N'STIC TYPE MF 100D31L V07', CAST(N'2023-11-17T23:53:14.443' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3286', N'K-SCTY-56638XX-00CA-MF-V03A', N'STIC TYPE MF 566-38 V03', CAST(N'2023-11-17T23:53:14.523' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3287', N'K-SCTY-56638XX-00TB-CV-V06A', N'STIC TYPE CONV 566-38 V06', CAST(N'2023-11-17T23:53:14.607' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3288', N'K-SCTY-56638XX-00TB-MF-V05A', N'STIC TYPE MF 566-38 V05', CAST(N'2023-11-17T23:53:14.687' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3289', N'K-SCTY-56638XX-00TC-CV-V07A', N'STIC TYPE CONV 566-38 V07', CAST(N'2023-11-17T23:53:14.767' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'329', N'W-CV01-32C24LX-A01N-NL-DG00', N'CV 32C24L BLUE HD1-BLUE', CAST(N'2023-11-17T23:48:45.523' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3290', N'K-SCTY-56638XX-00TC-MF-V06A', N'STIC TYPE MF 566-38 V06', CAST(N'2023-11-17T23:53:14.847' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3291', N'K-SCTY-56640LH-00CA-MF-V03A', N'STIC TYPE MF 566-40 LH V03', CAST(N'2023-11-17T23:53:14.930' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3292', N'K-SCTY-56640LH-00TB-MF-V05A', N'STIC TYPE MF 566-40 LH V05', CAST(N'2023-11-17T23:53:15.010' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3293', N'K-SCTY-56640LH-00TC-MF-V06A', N'STIC TYPE MF 566-40 LH V06', CAST(N'2023-11-17T23:53:15.090' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3294', N'K-SCTY-57229XX-00CA-MF-V03A', N'STIC TYPE MF 572-29 V03', CAST(N'2023-11-17T23:53:15.180' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3295', N'K-SCTY-57229XX-00TB-MF-V05A', N'STIC TYPE MF 572-29 V05', CAST(N'2023-11-17T23:53:15.260' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3296', N'K-SCTY-55D26RX-00CA-CV-V03A', N'STIC TYPE CONV 55D26R V03', CAST(N'2023-11-17T23:53:15.340' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3297', N'K-SCTY-55D26RX-00CA-MF-V03A', N'STIC TYPE MF 55D26R V03', CAST(N'2023-11-17T23:53:15.420' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3298', N'K-SCTY-55D26RX-00TB-CV-V06A', N'STIC TYPE CONV 55D26R V06', CAST(N'2023-11-17T23:53:15.500' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3299', N'K-SCTY-57229XX-00TC-MF-V06A', N'STIC TYPE MF 572-29 V06', CAST(N'2023-11-17T23:53:15.583' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'33', N'K-SCCV-252X050-11V0-BL-MFA0', N'STC CVR NO BRAND MF 252X50 MM', CAST(N'2023-11-17T23:48:19.110' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'330', N'W-CV01-32C24RX-A01N-NL-DG00', N'CV 32C24R BLUE HD1-BLUE', CAST(N'2023-11-17T23:48:45.603' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3300', N'K-SCTY-57233XX-00CA-MF-V03A', N'STIC TYPE MF 572-33 V03', CAST(N'2023-11-17T23:53:15.663' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3301', N'K-SCTY-57233XX-00TB-MF-V05A', N'STIC TYPE MF 572-33 V05', CAST(N'2023-11-17T23:53:15.743' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3302', N'K-SCTY-57233XX-00TC-MF-V06A', N'STIC TYPE MF 572-33 V06', CAST(N'2023-11-17T23:53:15.823' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3303', N'K-SCTY-57412XX-00CA-CV-V01A', N'STIC TYPE CONV 574-12 V01', CAST(N'2023-11-17T23:53:15.903' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3304', N'K-SCTY-57412XX-00CA-MF-V03A', N'STIC TYPE MF 574-12 V03', CAST(N'2023-11-17T23:53:15.983' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3305', N'K-SCTY-57412XX-00TB-CV-V06A', N'STIC TYPE CONV 574-12 V06', CAST(N'2023-11-17T23:53:16.067' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3306', N'K-SCTY-57412XX-00TB-MF-V05A', N'STIC TYPE MF 574-12 V05', CAST(N'2023-11-17T23:53:16.147' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3307', N'K-SCTY-57412XX-00TC-CV-V07A', N'STIC TYPE CONV 574-12 V07', CAST(N'2023-11-17T23:53:16.227' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3308', N'K-SCTY-57412XX-00TC-MF-V06A', N'STIC TYPE MF 574-12 V06', CAST(N'2023-11-17T23:53:16.317' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3309', N'K-SCTY-57413XX-00CA-MF-V03A', N'STIC TYPE MF 574-13 V03', CAST(N'2023-11-17T23:53:16.407' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'331', N'W-CV01-32C24RX-B03N-NL-DG00', N'CV 32C24R BLUE HD2-ORANGE', CAST(N'2023-11-17T23:48:45.683' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3310', N'K-SCTY-57413XX-00TB-MF-V05A', N'STIC TYPE MF 574-13 V05', CAST(N'2023-11-17T23:53:16.500' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3311', N'K-SCTY-57413XX-00TC-MF-V06A', N'STIC TYPE MF 574-13 V06', CAST(N'2023-11-17T23:53:16.593' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3312', N'K-SCTY-57519XX-00CA-MF-V03A', N'STIC TYPE MF 575-19 V03', CAST(N'2023-11-17T23:53:16.680' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3313', N'K-SCTY-57519XX-00TB-CV-V06A', N'STIC TYPE CONV 575-19 V06', CAST(N'2023-11-17T23:53:16.767' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3314', N'K-SCTY-95D26RX-00TC-MF-V07A', N'STIC TYPE MF 95D26R V07', CAST(N'2023-11-17T23:53:16.853' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3315', N'K-SCTY-95D26RX-00TC-MF-V08A', N'STIC TYPE MF 95D26R V08', CAST(N'2023-11-17T23:53:16.937' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3316', N'K-SCTY-95D31LX-00CA-CV-V01A', N'STIC TYPE CONV 95D31L V01', CAST(N'2023-11-17T23:53:17.020' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3317', N'K-SCTY-95D31LX-00CA-CV-V03A', N'STIC TYPE CONV 95D31L V03', CAST(N'2023-11-17T23:53:17.100' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3318', N'K-SCTY-115E41R-00CA-MF-V03A', N'STIC TYPE MF 115E41R HD V03', CAST(N'2023-11-17T23:53:17.180' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3319', N'K-SCTY-65D26RX-00TC-CV-V07A', N'STIC TYPE CONV 65D26R V07', CAST(N'2023-11-17T23:53:17.260' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'332', N'W-CV01-36B20LX-A01N-NL-DG00', N'CV 36B20L BLUE HD1-BLUE', CAST(N'2023-11-17T23:48:45.763' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3320', N'K-SCTY-N50XXXX-0000-CV-IAPC', N'STIC TYPE CONV N50 IAPC', CAST(N'2023-11-17T23:53:17.340' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3321', N'K-SCTY-N50XXXX-00CA-MF-V02A', N'STIC TYPE MF N50 V02', CAST(N'2023-11-17T23:53:17.423' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3322', N'K-SCTY-N50ZDXX-00CA-MF-V03A', N'STIC TYPE MF N50Z-D V03', CAST(N'2023-11-17T23:53:17.503' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3323', N'K-SCTY-N50ZDXX-00TB-MF-V05A', N'STIC TYPE MF N50Z-D V05', CAST(N'2023-11-17T23:53:17.583' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3324', N'K-SCTY-N50ZDXX-00TC-MF-V06A', N'STIC TYPE MF N50Z-D V06', CAST(N'2023-11-17T23:53:17.667' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3325', N'K-SCTY-55D26RX-00TC-CV-V07A', N'STIC TYPE CONV 55D26R V07', CAST(N'2023-11-17T23:53:17.747' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3326', N'K-SCTY-55D26RX-09CA-CV-V02A', N'STIC TYPE CONV 55D26R 09 V02', CAST(N'2023-11-17T23:53:17.827' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3327', N'K-SCTY-56073XX-00CA-MF-V03A', N'STIC TYPE MF 560-73 V03', CAST(N'2023-11-17T23:53:17.907' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3328', N'K-SCTY-56073XX-00TB-MF-V05A', N'STIC TYPE MF 560-73 V05', CAST(N'2023-11-17T23:53:17.987' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3329', N'K-SCTY-56073XX-00TC-MF-V06A', N'STIC TYPE MF 560-73 V06', CAST(N'2023-11-17T23:53:18.070' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'333', N'W-CV01-36B20RX-A01N-NL-DG00', N'CV 36B20R BLUE HD1-BLUE', CAST(N'2023-11-17T23:48:45.840' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3330', N'K-SCTY-56205LH-00CA-MF-V03A', N'STIC TYPE MF 562-05 LH V03', CAST(N'2023-11-17T23:53:18.150' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3331', N'K-SCTY-56205LH-00TB-MF-V05A', N'STIC TYPE MF 562-05 LH V05', CAST(N'2023-11-17T23:53:18.230' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3332', N'K-SCTY-56205LH-00TC-MF-V06A', N'STIC TYPE MF 562-05 LH V06', CAST(N'2023-11-17T23:53:18.310' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3333', N'K-SCTY-56219L2-00CA-MF-V03A', N'STIC TYPE MF 562-19 LN2 V03', CAST(N'2023-11-17T23:53:18.390' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3334', N'K-SCTY-56219XX-00CA-MF-V03A', N'STIC TYPE MF 562-19 V03', CAST(N'2023-11-17T23:53:18.480' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3335', N'K-SCTY-56219XX-00TB-CV-V06A', N'STIC TYPE CONV 562-19 V06', CAST(N'2023-11-17T23:53:18.577' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3336', N'K-SCTY-56219XX-00TB-MF-V05A', N'STIC TYPE MF 562-19 V05', CAST(N'2023-11-17T23:53:18.670' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3337', N'K-SCTY-56219XX-00TC-CV-V07A', N'STIC TYPE CONV 562-19 V07', CAST(N'2023-11-17T23:53:18.767' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3338', N'K-SCTY-56219XX-00TC-MF-V06A', N'STIC TYPE MF 562-19 V06', CAST(N'2023-11-17T23:53:18.867' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3339', N'K-SCTY-56221XX-00CA-MF-V03A', N'STIC TYPE MF 562-21 V03', CAST(N'2023-11-17T23:53:19.077' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'334', N'W-CV01-46B24LS-B03N-NL-DG00', N'CV 46B24LS BLUE HD2-ORANGE', CAST(N'2023-11-17T23:48:45.923' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3340', N'K-SCTY-56221XX-00TB-CV-V06A', N'STIC TYPE CONV 562-21 V06', CAST(N'2023-11-17T23:53:19.167' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3341', N'K-SCTY-56221XX-00TB-MF-V05A', N'STIC TYPE MF 562-21 V05', CAST(N'2023-11-17T23:53:19.257' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3342', N'K-SCTY-56221XX-00TC-CV-V07A', N'STIC TYPE CONV 562-21 V07', CAST(N'2023-11-17T23:53:19.343' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3343', N'K-SCTY-56221XX-00TC-MF-V06A', N'STIC TYPE MF 562-21 V06', CAST(N'2023-11-17T23:53:19.433' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3344', N'K-SCTY-56633XX-00CA-MF-V03A', N'STIC TYPE MF 566-33 V03', CAST(N'2023-11-17T23:53:19.523' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3345', N'K-SCTY-56633XX-00TB-CV-V06A', N'STIC TYPE CONV 566-33 V06', CAST(N'2023-11-17T23:53:19.610' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3346', N'K-SCTY-56633XX-00TB-MF-V05A', N'STIC TYPE MF 566-33 V05', CAST(N'2023-11-17T23:53:19.700' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3347', N'K-SCTY-65D31LX-00CA-MF-V03A', N'STIC TYPE MF 65D31L V03', CAST(N'2023-11-17T23:53:19.787' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3348', N'K-SCTY-65D31LX-00TB-CV-V06A', N'STIC TYPE CONV 65D31L V06', CAST(N'2023-11-17T23:53:19.870' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3349', N'K-SCTY-65D31LX-00TC-CV-V07A', N'STIC TYPE CONV 65D31L V07', CAST(N'2023-11-17T23:53:19.957' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'335', N'W-CV01-46B24LX-A01N-NL-DG00', N'CV 46B24L BLUE HD1-BLUE', CAST(N'2023-11-17T23:48:46.003' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3350', N'K-SCTY-65D31LX-11CA-CV-V02A', N'STIC TYPE CONV 65D31L 11 V02', CAST(N'2023-11-17T23:53:20.053' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3351', N'K-SCTY-115E41R-00TC-MF-V07A', N'STIC TYPE MF 115E41R V07', CAST(N'2023-11-17T23:53:20.307' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3352', N'K-SCTY-115E41R-00TC-MF-V08A', N'STIC TYPE MF 115E41R V08', CAST(N'2023-11-17T23:53:20.397' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3353', N'K-SCTY-115F51X-00CA-CV-V01A', N'STIC TYPE CONV 115F51 V01', CAST(N'2023-11-17T23:53:20.483' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3354', N'K-SCTY-115F51X-00CA-CV-V03A', N'STIC TYPE CONV 115F51 V03', CAST(N'2023-11-17T23:53:20.570' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3355', N'K-SCTY-115F51X-00CA-CV-V04A', N'STIC TYPE CONV 115F51 V04', CAST(N'2023-11-17T23:53:20.660' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3356', N'K-SCTY-65D31RX-00CA-CV-V01A', N'STIC TYPE CONV 65D31R V01', CAST(N'2023-11-17T23:53:20.747' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3357', N'K-SCTY-65D31RX-00CA-CV-V03A', N'STIC TYPE CONV 65D31R V03', CAST(N'2023-11-17T23:53:20.843' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3358', N'K-SCTY-65D31RX-00CA-CV-V04A', N'STIC TYPE CONV 65D31R V04', CAST(N'2023-11-17T23:53:20.940' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3359', N'K-SCTY-65D31RX-00CA-MF-V01A', N'STIC TYPE MF 65D31R V01', CAST(N'2023-11-17T23:53:21.073' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'336', N'W-CV01-46B24RX-A01N-NL-DG00', N'CV 46B24R BLUE HD1-BLUE', CAST(N'2023-11-17T23:48:46.087' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3360', N'K-SCTY-65D31RX-00CA-MF-V03A', N'STIC TYPE MF 65D31R V03', CAST(N'2023-11-17T23:53:21.167' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3361', N'K-SCTY-65D31RX-00TB-CV-V06A', N'STIC TYPE CONV 65D31R V06', CAST(N'2023-11-17T23:53:21.260' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3362', N'K-SCTY-65D31RX-00TC-CV-V07A', N'STIC TYPE CONV 65D31R V07', CAST(N'2023-11-17T23:53:21.353' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3363', N'K-SCTY-65D31RX-11CA-CV-V02A', N'STIC TYPE CONV 65D31R 11 V02', CAST(N'2023-11-17T23:53:21.447' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3364', N'K-SCTY-67018XX-0000-CV-IAPC', N'STIC TYPE CONV 670-18 IAPC', CAST(N'2023-11-17T23:53:21.590' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3365', N'K-SCTY-67018XX-00CA-MF-V03A', N'STIC TYPE MF 670-18 V03', CAST(N'2023-11-17T23:53:21.690' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3366', N'K-SCTY-67018XX-00TB-CV-V06A', N'STIC TYPE CONV 670-18 V06', CAST(N'2023-11-17T23:53:21.773' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3367', N'K-SCTY-67018XX-00TC-CV-V07A', N'STIC TYPE CONV 670-18 V07', CAST(N'2023-11-17T23:53:21.860' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3368', N'K-SCTY-6D115XX-00CA-DC-V01A', N'STIC TYPE DEEP CYCLE 6D115 V01', CAST(N'2023-11-17T23:53:21.947' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3369', N'K-SCTY-6D135XX-00CA-DC-V01A', N'STIC TYPE DEEP CYCLE 6D135 V01', CAST(N'2023-11-17T23:53:22.030' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'337', N'W-CV01-48D26LX-C01N-NL-DG00', N'CV 48D26L BLUE HD-BLUE', CAST(N'2023-11-17T23:48:46.163' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3370', N'K-SCTY-70027XX-00TB-CV-V06A', N'STIC TYPE CONV 700-27 V06', CAST(N'2023-11-17T23:53:22.110' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3371', N'K-SCTY-70027XX-00TC-CV-V07A', N'STIC TYPE CONV 700-27 V07', CAST(N'2023-11-17T23:53:22.190' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3372', N'K-SCTY-70D26LX-00TC-MF-V07A', N'STIC TYPE MF 70D26L V07', CAST(N'2023-11-17T23:53:22.300' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3373', N'K-SCTY-70D26LX-00TC-MF-V08A', N'STIC TYPE MF 70D26L V08', CAST(N'2023-11-17T23:53:22.390' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3374', N'K-SCTY-70D26RX-00TC-MF-V07A', N'STIC TYPE MF 70D26R V07', CAST(N'2023-11-17T23:53:22.477' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3375', N'K-SCTY-70D26RX-00TC-MF-V08A', N'STIC TYPE MF 70D26R V08', CAST(N'2023-11-17T23:53:22.570' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3376', N'K-SCTY-NS70XXX-0000-CV-IADG', N'STICKER TYPE NS70 ISUZU DOM', CAST(N'2023-11-17T23:53:22.657' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3377', N'K-SCTY-NS70XXX-00CA-MF-V02A', N'STIC TYPE MF NS70 V02', CAST(N'2023-11-17T23:53:22.737' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3378', N'K-SCTY-NX1207L-00CA-MF-V02A', N'STIC TYPE MF NX120-7L V02', CAST(N'2023-11-17T23:53:22.820' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3379', N'K-SCTY-NX1207X-00CA-MF-V02A', N'STIC TYPE MF NX120-7 V02', CAST(N'2023-11-17T23:53:22.923' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'338', N'W-CV01-48D26LX-C03N-NL-DG00', N'CV 48D26L BLUE HD-ORANGE', CAST(N'2023-11-17T23:48:46.243' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3380', N'K-SCTY-Q85LXXX-00CA-EF-V01A', N'STIC TYPE EFB Q-85L V01', CAST(N'2023-11-17T23:53:23.040' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3381', N'K-SCTY-100D31L-00TC-MF-V08A', N'STIC TYPE MF 100D31L V08', CAST(N'2023-11-17T23:53:23.173' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3382', N'K-SCTY-100D31R-00TC-MF-V07A', N'STIC TYPE MF 100D31R V07', CAST(N'2023-11-17T23:53:23.290' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3383', N'K-SCTY-100D31R-00TC-MF-V08A', N'STIC TYPE MF 100D31R V08', CAST(N'2023-11-17T23:53:23.387' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3384', N'K-SCTY-105D31L-00CA-CV-V01A', N'STIC TYPE CONV 105D31L V01', CAST(N'2023-11-17T23:53:23.510' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3385', N'K-SCTY-105D31L-00CA-CV-V03A', N'STIC TYPE CONV 105D31L V03', CAST(N'2023-11-17T23:53:23.630' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3386', N'K-SCTY-105D31L-00CA-MF-V01A', N'STIC TYPE MF 105D31L V01', CAST(N'2023-11-17T23:53:23.750' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3387', N'K-SCTY-105D31L-00CA-MF-V03A', N'STIC TYPE MF 105D31L V03', CAST(N'2023-11-17T23:53:23.843' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3388', N'K-SCTY-105D31L-00TB-CV-V06A', N'STIC TYPE CONV 105D31L V06', CAST(N'2023-11-17T23:53:23.927' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3389', N'K-SCTY-105D31L-00TC-CV-V07A', N'STIC TYPE CONV 105D31L V07', CAST(N'2023-11-17T23:53:24.010' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'339', N'W-CV01-55D26LX-C03N-NL-DG00', N'CV 55D26L BLUE HD-ORANGE', CAST(N'2023-11-17T23:48:46.323' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3390', N'K-SCTY-105D31R-00CA-CV-V01A', N'STIC TYPE CONV 105D31R V01', CAST(N'2023-11-17T23:53:24.093' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3391', N'K-SCTY-105D31R-00CA-CV-V03A', N'STIC TYPE CONV 105D31R V03', CAST(N'2023-11-17T23:53:24.173' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3392', N'K-SCTY-105D31R-00CA-CV-V04A', N'STIC TYPE CONV 105D31R V04', CAST(N'2023-11-17T23:53:24.487' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3393', N'K-SCTY-105D31R-00CA-MF-V01A', N'STIC TYPE MF 105D31L V01', CAST(N'2023-11-17T23:53:24.577' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3394', N'K-SCTY-36B20RS-09CA-CV-V02A', N'STIC TYPE CONV 36B20RS 09 V02', CAST(N'2023-11-17T23:53:24.663' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3395', N'K-SCTY-36B20RX-00CA-CV-V01A', N'STIC TYPE CONV 36B20R V01', CAST(N'2023-11-17T23:53:24.750' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3396', N'K-SCTY-36B20RX-00CA-CV-V03A', N'STIC TYPE CONV 36B20R V03', CAST(N'2023-11-17T23:53:24.830' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3397', N'K-SCTY-36B20RX-00CA-CV-V04A', N'STIC TYPE CONV 36B20R V04', CAST(N'2023-11-17T23:53:24.957' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3398', N'K-SCTY-36B20RX-00CA-MF-V03A', N'STIC TYPE MF 36B20R V03', CAST(N'2023-11-17T23:53:25.040' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3399', N'K-SCTY-105D31R-00CA-MF-V03A', N'STIC TYPE MF 105D31R V03', CAST(N'2023-11-17T23:53:25.127' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'34', N'K-SCCV-252X050-11V0-IN-MFD0', N'STC CVR INCOE MF KSA 252X50 MM', CAST(N'2023-11-17T23:48:19.197' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'340', N'W-CV01-55D26LX-C06N-NL-DG00', N'CV 55D26L BLUE HD-WHITE', CAST(N'2023-11-17T23:48:46.403' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3400', N'K-SCTY-105D31R-00TB-CV-V06A', N'STIC TYPE CONV 105D31R V06', CAST(N'2023-11-17T23:53:25.217' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3401', N'K-SCTY-105D31R-00TC-CV-V07A', N'STIC TYPE CONV 105D31R V07', CAST(N'2023-11-17T23:53:25.317' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3402', N'K-SCTY-110D31L-00TC-MF-V07A', N'STIC TYPE MF 110D31L V07', CAST(N'2023-11-17T23:53:25.403' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3403', N'K-SCTY-110D31L-00TC-MF-V08A', N'STIC TYPE MF 110D31L V08', CAST(N'2023-11-17T23:53:25.483' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3404', N'K-SCTY-110D31R-00TC-MF-V07A', N'STIC TYPE MF 110D31R V07', CAST(N'2023-11-17T23:53:25.573' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3405', N'K-SCTY-110D31R-00TC-MF-V08A', N'STIC TYPE MF 110D31R V08', CAST(N'2023-11-17T23:53:25.657' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3406', N'K-SCTY-115D31L-00CA-MF-V03A', N'STIC TYPE MF 115D31L V03', CAST(N'2023-11-17T23:53:25.743' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3407', N'K-SCTY-115D31R-00CA-MF-V03A', N'STIC TYPE MF 115D31R V03', CAST(N'2023-11-17T23:53:25.830' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3408', N'K-SCTY-115E41L-00TC-MF-V07A', N'STIC TYPE MF 115E41L V07', CAST(N'2023-11-17T23:53:25.923' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3409', N'K-SCTY-115E41L-00TC-MF-V08A', N'STIC TYPE MF 115E41L V08', CAST(N'2023-11-17T23:53:26.020' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'341', N'W-CV01-55D26RX-C06N-NL-DG00', N'CV 55D26R BLUE HD-WHITE', CAST(N'2023-11-17T23:48:46.483' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3410', N'K-SCTY-55565XX-00CA-MF-V03A', N'STIC TYPE MF 555-65 V03', CAST(N'2023-11-17T23:53:26.870' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3411', N'K-SCTY-55565XX-00TB-CV-V06A', N'STIC TYPE CONV 555-65 V06', CAST(N'2023-11-17T23:53:27.893' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3412', N'K-SCTY-55565XX-00TB-MF-V05A', N'STIC TYPE MF 555-65 V05', CAST(N'2023-11-17T23:53:28.897' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3413', N'K-SCTY-55565XX-00TC-CV-V07A', N'STIC TYPE CONV 555-65 V07', CAST(N'2023-11-17T23:53:30.343' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3414', N'K-SCTY-95D31LX-00CA-CV-V04A', N'STIC TYPE CONV 95D31L V04', CAST(N'2023-11-17T23:53:31.380' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3415', N'K-SCTY-95D31LX-00CA-CV-V05A', N'STIC TYPE CONV 95D31L V05 ASPR', CAST(N'2023-11-17T23:53:31.490' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3416', N'K-SCTY-95D31LX-00CA-MF-V03A', N'STIC TYPE MF 95D31L V03', CAST(N'2023-11-17T23:53:31.577' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3417', N'K-SCTY-95D31LX-00CC-MF-V04A', N'STIC TYPE MF 95D31L V04', CAST(N'2023-11-17T23:53:31.663' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3418', N'K-SCTY-95D31LX-00TB-CV-V06A', N'STIC TYPE CONV 95D31L V06', CAST(N'2023-11-17T23:53:31.747' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3419', N'K-SCTY-95D31LX-00TC-CV-V07A', N'STIC TYPE CONV 95D31L V07', CAST(N'2023-11-17T23:53:31.830' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'342', N'W-CV01-65D26LX-C03N-NL-DG00', N'CV 65D26L BLUE HD-ORANGE', CAST(N'2023-11-17T23:48:46.563' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3420', N'K-SCTY-95D31LX-00TC-MF-V07A', N'STIC TYPE MF 95D31L V07', CAST(N'2023-11-17T23:53:31.910' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3421', N'K-SCTY-95D31LX-00TC-MF-V08A', N'STIC TYPE MF 95D31L V08', CAST(N'2023-11-17T23:53:32.003' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3422', N'K-SCTY-95D31LX-13CA-CV-V02A', N'STIC TYPE CONV 95D31L 13 V02', CAST(N'2023-11-17T23:53:32.103' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3423', N'K-SCTY-95D31LX-15CA-CV-V02A', N'STIC TYPE CONV 95D31L 15 V02', CAST(N'2023-11-17T23:53:32.193' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3424', N'K-SCTY-95D31RX-00CA-CV-V01A', N'STIC TYPE CONV 95D31R V01', CAST(N'2023-11-17T23:53:32.310' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3425', N'K-SCTY-95D31RX-00CA-CV-V03A', N'STIC TYPE CONV 95D31R V03', CAST(N'2023-11-17T23:53:32.453' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3426', N'K-SCTY-95D31RX-00CA-CV-V04A', N'STIC TYPE CONV 95D31R V04', CAST(N'2023-11-17T23:53:32.543' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3427', N'K-SCTY-60038XX-00CA-MF-V03A', N'STIC TYPE MF 600-38 V03', CAST(N'2023-11-17T23:53:32.640' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3428', N'K-SCTY-54459XX-00TC-CV-V07A', N'STIC TYPE CONV 544-59 V07', CAST(N'2023-11-17T23:53:32.723' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3429', N'K-SCTY-54459XX-00TC-MF-V06A', N'STIC TYPE MF 544-59 V06', CAST(N'2023-11-17T23:53:32.803' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'343', N'W-CV01-65D26LX-C06N-NL-DG00', N'CV 65D26L BLUE HD-WHITE', CAST(N'2023-11-17T23:48:46.643' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3430', N'K-SCTY-54464XX-00CA-MF-V03A', N'STIC TYPE MF 544-64 V03', CAST(N'2023-11-17T23:53:32.887' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3431', N'K-SCTY-54464XX-00TB-CV-V06A', N'STIC TYPE CONV 544-64 V06', CAST(N'2023-11-17T23:53:32.990' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3432', N'K-SCTY-54464XX-00TB-MF-V05A', N'STIC TYPE MF 544-64 V05', CAST(N'2023-11-17T23:53:33.087' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3433', N'K-SCTY-95D31RX-00CA-MF-V03A', N'STIC TYPE MF 95D31R V03', CAST(N'2023-11-17T23:53:33.167' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3434', N'K-SCTY-95D31RX-00CC-MF-V04A', N'STIC TYPE MF 95D31R V04', CAST(N'2023-11-17T23:53:33.250' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3435', N'K-SCTY-95D31RX-00TB-CV-V06A', N'STIC TYPE CONV 95D31R V06', CAST(N'2023-11-17T23:53:33.330' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3436', N'K-SCTY-95D31RX-00TC-CV-V07A', N'STIC TYPE CONV 95D31R V07', CAST(N'2023-11-17T23:53:33.420' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3437', N'K-SCTY-95D31RX-00TC-MF-V07A', N'STIC TYPE MF 95D31R V07', CAST(N'2023-11-17T23:53:33.507' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3438', N'K-SCTY-95D31RX-00TC-MF-V08A', N'STIC TYPE MF 95D31R V08', CAST(N'2023-11-17T23:53:33.590' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3439', N'K-SCTY-95D31RX-13CA-CV-V02A', N'STIC TYPE CONV 95D31R 13 V02', CAST(N'2023-11-17T23:53:33.683' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'344', N'W-CV01-65D26RX-C03N-NL-DG00', N'CV 65D26R BLUE HD-ORANGE', CAST(N'2023-11-17T23:48:46.723' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3440', N'K-SCTY-95D31RX-15CA-CV-V02A', N'STIC TYPE CONV 95D31R 15 V02', CAST(N'2023-11-17T23:53:33.773' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3441', N'K-SCTY-95E41LX-00CA-CV-V01A', N'STIC TYPE CONV 95E41L V01', CAST(N'2023-11-17T23:53:33.920' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3442', N'K-SCTY-95E41LX-00CA-CV-V03A', N'STIC TYPE CONV 95E41L V03', CAST(N'2023-11-17T23:53:34.177' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3443', N'K-SCTY-95E41LX-00CA-MF-V03A', N'STIC TYPE MF 95E41L V03', CAST(N'2023-11-17T23:53:34.320' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3444', N'K-SCTY-60038XX-00TB-CV-V06A', N'STIC TYPE CONV 600-38 V06', CAST(N'2023-11-17T23:53:34.433' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3445', N'K-SCTY-60038XX-00TB-MF-V05A', N'STIC TYPE MF 600-38 V05', CAST(N'2023-11-17T23:53:34.570' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3446', N'K-SCTY-60038XX-00TC-CV-V07A', N'STIC TYPE CONV 600-38 V07', CAST(N'2023-11-17T23:53:34.737' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3447', N'K-SCTY-60038XX-00TC-MF-V06A', N'STIC TYPE MF 600-38 V06', CAST(N'2023-11-17T23:53:34.873' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3448', N'K-SCTY-115F51X-00CA-MF-V03A', N'STIC TYPE MF 115F51 V03', CAST(N'2023-11-17T23:53:35.010' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3449', N'K-SCTY-115F51X-00TB-CV-V06A', N'STIC TYPE CONV 115F51 V06', CAST(N'2023-11-17T23:53:35.110' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'345', N'W-CV01-75D31LX-C03N-NL-DQ00', N'CV 75D31L BLUE HD-ORANGE', CAST(N'2023-11-17T23:48:46.803' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3450', N'K-SCTY-115F51X-00TC-CV-V07A', N'STIC TYPE CONV 115F51 V07', CAST(N'2023-11-17T23:53:35.207' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3451', N'K-SCTY-115F51X-19CA-CV-V02A', N'STIC TYPE CONV 115F51 19 V02', CAST(N'2023-11-17T23:53:35.303' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3452', N'K-SCTY-125D31L-00TC-MF-V07A', N'STIC TYPE MF 125D31L V07', CAST(N'2023-11-17T23:53:35.410' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3453', N'K-SCTY-125D31L-00TC-MF-V08A', N'STIC TYPE MF 125D31L V08', CAST(N'2023-11-17T23:53:35.540' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3454', N'K-SCTY-125D31R-00TC-MF-V07A', N'STIC TYPE MF 125D31R V07', CAST(N'2023-11-17T23:53:35.653' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3455', N'K-SCTY-125D31R-00TC-MF-V08A', N'STIC TYPE MF 125D31R V08', CAST(N'2023-11-17T23:53:35.750' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3456', N'K-SCTY-135F51X-00TC-MF-V07A', N'STIC TYPE MF 135F51 V07', CAST(N'2023-11-17T23:53:35.837' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3457', N'K-SCTY-135F51X-00TC-MF-V08A', N'STIC TYPE MF 135F51 V08', CAST(N'2023-11-17T23:53:35.930' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3458', N'K-SCTY-145F51X-00CA-MF-V03A', N'STIC TYPE MF 145F51 HD V03', CAST(N'2023-11-17T23:53:36.027' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3459', N'K-SCTY-145G51L-00CA-CV-V01A', N'STIC TYPE CONV 145G51L V01', CAST(N'2023-11-17T23:53:36.113' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'346', N'W-CV01-75D31RX-C03N-NL-DQ00', N'CV 75D31R BLUE HD-ORANGE', CAST(N'2023-11-17T23:48:46.883' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3460', N'K-SCTY-145G51L-00TB-CV-V06A', N'STIC TYPE CONV 145G51L V06', CAST(N'2023-11-17T23:53:36.193' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3461', N'K-SCTY-145G51L-00TC-CV-V07A', N'STIC TYPE CONV 145G51L V07', CAST(N'2023-11-17T23:53:36.277' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3462', N'K-SCTY-145G51X-00CA-CV-V01A', N'STIC TYPE CONV 145G51 V01', CAST(N'2023-11-17T23:53:36.360' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3463', N'K-SCTY-145G51X-00CA-CV-V03A', N'STIC TYPE CONV 145G51 V03', CAST(N'2023-11-17T23:53:36.437' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3464', N'K-SCTY-145G51X-00CA-CV-V04A', N'STIC TYPE CONV 145G51 V04', CAST(N'2023-11-17T23:53:36.547' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3465', N'K-SCTY-145G51X-00CA-MF-V03A', N'STIC TYPE MF 145G51 V03', CAST(N'2023-11-17T23:53:36.630' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3466', N'K-SCTY-145G51X-00TB-CV-V06A', N'STIC TYPE CONV 145G51 V06', CAST(N'2023-11-17T23:53:36.720' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3467', N'K-SCTY-145G51X-00TC-CV-V07A', N'STIC TYPE CONV 145G51 V07', CAST(N'2023-11-17T23:53:36.810' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3468', N'K-SCTY-145G51X-23CA-CV-V02A', N'STIC TYPE CONV 145G51 23 V02', CAST(N'2023-11-17T23:53:36.907' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3469', N'K-SCTY-145G51X-25CA-CV-V02A', N'STIC TYPE CONV 145G51 25 V02', CAST(N'2023-11-17T23:53:37.030' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'347', N'W-CV01-80D26LX-C03N-NL-DG00', N'CV 80D26L BLUE HD-ORANGE', CAST(N'2023-11-17T23:48:46.960' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3470', N'K-SCTY-95D26LX-00TC-MF-V08A', N'STIC TYPE MF 95D26L V08', CAST(N'2023-11-17T23:53:37.120' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3471', N'K-SCTY-72018XX-00CA-MF-V03A', N'STIC TYPE MF 720-18 V03', CAST(N'2023-11-17T23:53:37.213' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3472', N'K-SCTY-75D23LX-00CA-MF-V03A', N'STIC TYPE MF 75D23L V03', CAST(N'2023-11-17T23:53:37.300' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3473', N'K-SCTY-75D23RX-00CA-MF-V03A', N'STIC TYPE MF 75D23R V03', CAST(N'2023-11-17T23:53:37.397' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3474', N'K-SCTY-75D26LX-00TC-MF-V07A', N'STIC TYPE MF 75D26L V07', CAST(N'2023-11-17T23:53:37.480' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3475', N'K-SCTY-75D26LX-00TC-MF-V08A', N'STIC TYPE MF 75D26L V08', CAST(N'2023-11-17T23:53:37.560' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3476', N'K-SCTY-36B20RX-00TB-CV-V06A', N'STIC TYPE CONV 36B20R V06', CAST(N'2023-11-17T23:53:37.643' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3477', N'K-SCTY-36B20RX-00TC-CV-V07A', N'STIC TYPE CONV 36B20R V07', CAST(N'2023-11-17T23:53:37.723' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3478', N'K-SCTY-36B20RX-09CA-CV-V02A', N'STIC TYPE CONV 36B20R 09 V02', CAST(N'2023-11-17T23:53:37.807' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3479', N'K-SCTY-40B20LS-00TC-MF-V07A', N'STIC TYPE MF 40B20LS V07', CAST(N'2023-11-17T23:53:37.887' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'348', N'W-CV01-80D26LX-C06N-NL-DG00', N'CV 80D26L BLUE HD-WHITE', CAST(N'2023-11-17T23:48:47.040' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3480', N'K-SCTY-40B20LS-00TC-MF-V08A', N'STIC TYPE MF 40B20LS V08', CAST(N'2023-11-17T23:53:37.967' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3481', N'K-SCTY-40B20LX-00TC-MF-V07A', N'STIC TYPE MF 40B20L V07', CAST(N'2023-11-17T23:53:38.050' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3482', N'K-SCTY-40B20LX-00TC-MF-V08A', N'STIC TYPE MF 40B20L V08', CAST(N'2023-11-17T23:53:38.130' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3483', N'K-SCTY-40B20RS-00TC-MF-V07A', N'STIC TYPE MF 40B20RS V07', CAST(N'2023-11-17T23:53:38.217' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3484', N'K-SCTY-40B20RS-00TC-MF-V08A', N'STIC TYPE MF 40B20RS V08', CAST(N'2023-11-17T23:53:38.297' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3485', N'K-SCTY-40B20RX-00TC-MF-V07A', N'STIC TYPE MF 40B20R V07', CAST(N'2023-11-17T23:53:38.380' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3486', N'K-SCTY-40B20RX-00TC-MF-V08A', N'STIC TYPE MF 40B20R V08', CAST(N'2023-11-17T23:53:38.463' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3487', N'K-SCTY-45B20LS-00TC-MF-V07A', N'STIC TYPE MF 45B20LS V07', CAST(N'2023-11-17T23:53:38.547' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3488', N'K-SCTY-45B20LS-00TC-MF-V08A', N'STIC TYPE MF 45B20LS V08', CAST(N'2023-11-17T23:53:38.627' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3489', N'K-SCTY-45B20LX-00TC-MF-V07A', N'STIC TYPE MF 45B20L V07', CAST(N'2023-11-17T23:53:38.710' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'349', N'W-CV01-80D26RX-C03N-NL-DG00', N'CV 80D26R BLUE HD-ORANGE', CAST(N'2023-11-17T23:48:47.120' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3490', N'K-SCTY-45B20LX-00TC-MF-V08A', N'STIC TYPE MF 45B20L V08', CAST(N'2023-11-17T23:53:38.790' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3491', N'K-SCTY-45B20RS-00TC-MF-V07A', N'STIC TYPE MF 45B20RS V07', CAST(N'2023-11-17T23:53:38.873' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3492', N'K-SCTY-45B20RS-00TC-MF-V08A', N'STIC TYPE MF 45B20RS V08', CAST(N'2023-11-17T23:53:38.957' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3493', N'K-SCTY-45B20RX-00TC-MF-V07A', N'STIC TYPE MF 45B20R V07', CAST(N'2023-11-17T23:53:39.047' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3494', N'K-SCTY-45B20RX-00TC-MF-V08A', N'STIC TYPE MF 45B20R V08', CAST(N'2023-11-17T23:53:39.140' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3495', N'K-SCTY-45C24LX-00TC-MF-V07A', N'STIC TYPE MF 45C24L V07', CAST(N'2023-11-17T23:53:39.230' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3496', N'K-SCTY-45C24LX-00TC-MF-V08A', N'STIC TYPE MF 45C24L V08', CAST(N'2023-11-17T23:53:39.317' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3497', N'K-SCTY-45C24RX-00TC-MF-V07A', N'STIC TYPE MF 45C24R V07', CAST(N'2023-11-17T23:53:39.407' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3498', N'K-SCTY-45C24RX-00TC-MF-V08A', N'STIC TYPE MF 45C24R V08', CAST(N'2023-11-17T23:53:39.500' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3499', N'K-SCTY-46B24LS-00CA-CV-V01A', N'STIC TYPE CONV 46B24LS V01', CAST(N'2023-11-17T23:53:39.590' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'35', N'K-SCCV-252X050-11V0-LU-PRA0', N'STC COVER LUCAS PRE 252X50 MM', CAST(N'2023-11-17T23:48:19.283' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'350', N'W-CV01-34B20LX-B03N-NL-DG00', N'CV 34B20L BLUE HD2-ORANGE', CAST(N'2023-11-17T23:48:47.200' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3500', N'K-SCTY-55565XX-00TC-MF-V06A', N'STIC TYPE MF 555-65 V06', CAST(N'2023-11-17T23:53:39.677' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3501', N'K-SCTY-55566LH-00CA-MF-V03A', N'STIC TYPE MF 555-66 LH V03', CAST(N'2023-11-17T23:53:39.763' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3502', N'K-SCTY-55566LH-00TB-MF-V05A', N'STIC TYPE MF 555-66 LH V05', CAST(N'2023-11-17T23:53:39.857' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3503', N'K-SCTY-55566LH-00TC-MF-V06A', N'STIC TYPE MF 555-66 LH V06', CAST(N'2023-11-17T23:53:39.947' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3504', N'K-SCTY-55D23LX-0000-CV-IAPC', N'STIC TYPE CONV 55D23L IAPC', CAST(N'2023-11-17T23:53:40.037' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3505', N'K-SCTY-55D23LX-00CA-CV-V01A', N'STIC TYPE CONV 55D23L V01', CAST(N'2023-11-17T23:53:40.127' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3506', N'K-SCTY-55D23LX-00CA-CV-V03A', N'STIC TYPE CONV 55D23L V03', CAST(N'2023-11-17T23:53:40.263' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3507', N'K-SCTY-55D23LX-00CA-CV-V04A', N'STIC TYPE CONV 55D23L V04', CAST(N'2023-11-17T23:53:40.353' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3508', N'K-SCTY-55D23LX-00CA-MF-V01A', N'STIC TYPE MF 55D23L V01', CAST(N'2023-11-17T23:53:40.437' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3509', N'K-SCTY-55D23LX-00CA-MF-V03A', N'STIC TYPE MF 55D23L V03', CAST(N'2023-11-17T23:53:40.530' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'351', N'W-CV02-544LXXX-D02C-NL-00M0', N'CV 544L MF BLACK FS', CAST(N'2023-11-17T23:48:47.280' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3510', N'K-SCTY-55D23LX-00TB-CV-V06A', N'STIC TYPE CONV 55D23L V06', CAST(N'2023-11-17T23:53:40.643' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3511', N'K-SCTY-55D23LX-00TC-CV-V07A', N'STIC TYPE CONV 55D23L V07', CAST(N'2023-11-17T23:53:40.847' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3512', N'K-SCTY-55D23LX-09CA-CV-V02A', N'STIC TYPE CONV 55D23L 09 V02', CAST(N'2023-11-17T23:53:40.943' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3513', N'K-SCTY-54464XX-00TC-CV-V07A', N'STIC TYPE CONV 544-64 V07', CAST(N'2023-11-17T23:53:41.037' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3514', N'K-SCTY-54464XX-00TC-MF-V06A', N'STIC TYPE MF 544-64 V06', CAST(N'2023-11-17T23:53:41.130' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3515', N'K-SCTY-54519LH-00CA-MF-V03A', N'STIC TYPE MF 545-19 LH V03', CAST(N'2023-11-17T23:53:41.217' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3516', N'K-SCTY-54519LH-00TB-MF-V05A', N'STIC TYPE MF 545-19 LH V05', CAST(N'2023-11-17T23:53:41.300' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3517', N'K-SCTY-54519LH-00TC-MF-V06A', N'STIC TYPE MF 545-19 LH V06', CAST(N'2023-11-17T23:53:41.390' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3518', N'K-SCTY-54519XX-00CA-CV-V01A', N'STIC TYPE CONV 545-19 V01', CAST(N'2023-11-17T23:53:41.473' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3519', N'K-SCTY-54519XX-00TB-CV-V06A', N'STIC TYPE CONV 545-19 V06', CAST(N'2023-11-17T23:53:41.607' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'352', N'W-CV02-555LXXX-D02C-NL-00M0', N'CV 555L MF BLACK FS', CAST(N'2023-11-17T23:48:47.360' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3520', N'K-SCTY-54519XX-00TC-CV-V07A', N'STIC TYPE CONV 545-19 V07', CAST(N'2023-11-17T23:53:41.733' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3521', N'K-SCTY-55055XX-00TB-MF-V05A', N'STIC TYPE MF 550-55 V05', CAST(N'2023-11-17T23:53:41.820' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3522', N'K-SCTY-55055XX-00TC-MF-V06A', N'STIC TYPE MF 550-55 V06', CAST(N'2023-11-17T23:53:41.903' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3523', N'K-SCTY-55059XX-00TB-MF-V05A', N'STIC TYPE MF 550-59 V05', CAST(N'2023-11-17T23:53:41.993' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3524', N'K-SCTY-55059XX-00TC-MF-V06A', N'STIC TYPE MF 550-59 V06', CAST(N'2023-11-17T23:53:42.077' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3525', N'K-SCTY-55559XX-00CA-CV-V01A', N'STIC TYPE CONV 555-59 V01', CAST(N'2023-11-17T23:53:42.170' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3526', N'K-SCTY-55559XX-00CA-CV-V03A', N'STIC TYPE CONV 555-59 V03', CAST(N'2023-11-17T23:53:42.260' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3527', N'K-SCTY-55559XX-00CA-MF-V03A', N'STIC TYPE MF 555-59 V03', CAST(N'2023-11-17T23:53:42.350' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3528', N'K-SCTY-55559XX-00TB-CV-V06A', N'STIC TYPE CONV 555-59 V06', CAST(N'2023-11-17T23:53:42.440' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3529', N'K-SCTY-55559XX-00TB-MF-V05A', N'STIC TYPE MF 555-59 V05', CAST(N'2023-11-17T23:53:42.530' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'353', N'W-CV02-555RXXX-D02C-NL-00M0', N'CV 555R MF BLACK FS', CAST(N'2023-11-17T23:48:47.440' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3530', N'K-SCTY-55559XX-00TC-CV-V07A', N'STIC TYPE CONV 555-59 V07', CAST(N'2023-11-17T23:53:42.617' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3531', N'K-SCTY-55559XX-00TC-MF-V06A', N'STIC TYPE MF 555-59 V06', CAST(N'2023-11-17T23:53:42.700' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3532', N'K-SCTY-55D23RX-0000-CV-IAPC', N'STIC TYPE CONV 55D23R IAPC', CAST(N'2023-11-17T23:53:42.780' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3533', N'K-SCTY-55D23RX-00CA-CV-V01A', N'STIC TYPE CONV 55D23R V01', CAST(N'2023-11-17T23:53:42.863' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3534', N'K-SCTY-55D23RX-00CA-CV-V03A', N'STIC TYPE CONV 55D23R V03', CAST(N'2023-11-17T23:53:42.943' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3535', N'K-SCTY-55D23RX-00CA-CV-V04A', N'STIC TYPE CONV 55D23R V04', CAST(N'2023-11-17T23:53:43.027' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3536', N'K-SCTY-55D23RX-00CA-MF-V01A', N'STIC TYPE MF 55D23R V01', CAST(N'2023-11-17T23:53:43.110' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3537', N'K-SCTY-55D23RX-00CA-MF-V03A', N'STIC TYPE MF 55D23R V03', CAST(N'2023-11-17T23:53:43.223' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3538', N'K-SCTY-55D23RX-00TB-CV-V06A', N'STIC TYPE CONV 55D23R V06', CAST(N'2023-11-17T23:53:43.463' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3539', N'K-SCTY-55D23RX-00TC-CV-V07A', N'STIC TYPE CONV 55D23R V07', CAST(N'2023-11-17T23:53:43.630' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'354', N'W-CV02-566LXXX-D02C-NL-00M0', N'CV 566L MF BLACK FS', CAST(N'2023-11-17T23:48:47.520' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3540', N'K-SCTY-55D23RX-09CA-CV-V02A', N'STIC TYPE CONV 55D23R 09 V02', CAST(N'2023-11-17T23:53:43.713' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3541', N'K-SCTY-55D26LX-00CA-CV-V01A', N'STIC TYPE CONV 55D26L V01', CAST(N'2023-11-17T23:53:43.797' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3542', N'K-SCTY-55D26LX-00CA-CV-V03A', N'STIC TYPE CONV 55D26L V03', CAST(N'2023-11-17T23:53:43.913' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3543', N'K-SCTY-55D26LX-00CA-MF-V03A', N'STIC TYPE MF 55D26L V03', CAST(N'2023-11-17T23:53:44.000' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3544', N'K-SCTY-46B24RX-11CA-CV-V02A', N'STIC TYPE CONV 46B24R 11 V02', CAST(N'2023-11-17T23:53:44.083' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3545', N'K-SCTY-48D26LX-00CA-CV-V01A', N'STIC TYPE CONV 48D26L V01', CAST(N'2023-11-17T23:53:44.170' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3546', N'K-SCTY-48D26LX-00CA-CV-V03A', N'STIC TYPE CONV 48D26L V03', CAST(N'2023-11-17T23:53:44.253' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3547', N'K-SCTY-48D26LX-00CA-CV-V04A', N'STIC TYPE CONV 48D26L V04', CAST(N'2023-11-17T23:53:44.337' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3548', N'K-SCTY-48D26LX-00CA-MF-V01A', N'STIC TYPE MF 48D26L V01', CAST(N'2023-11-17T23:53:44.417' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3549', N'K-SCTY-60D26LX-00TC-MF-V07A', N'STIC TYPE MF 60D26L V07', CAST(N'2023-11-17T23:53:44.503' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'355', N'W-CV02-588LXXX-D02C-NL-00M0', N'CV 588L MF BLACK FS', CAST(N'2023-11-17T23:48:47.600' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3550', N'K-SCTY-60D26LX-00TC-MF-V08A', N'STIC TYPE MF 60D26L V08', CAST(N'2023-11-17T23:53:44.587' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3551', N'K-SCTY-60D26RX-00TC-MF-V07A', N'STIC TYPE MF 60D26R V07', CAST(N'2023-11-17T23:53:44.670' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3552', N'K-SCTY-60D26RX-00TC-MF-V08A', N'STIC TYPE MF 60D26R V08', CAST(N'2023-11-17T23:53:44.753' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3553', N'K-SCTY-65038XX-00CA-CV-V01A', N'STIC TYPE CONV 650-38 V01', CAST(N'2023-11-17T23:53:44.837' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3554', N'K-SCTY-65038XX-00TB-CV-V06A', N'STIC TYPE CONV 650-38 V06', CAST(N'2023-11-17T23:53:44.917' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3555', N'K-SCTY-65038XX-00TC-CV-V07A', N'STIC TYPE CONV 650-38 V07', CAST(N'2023-11-17T23:53:45.010' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3556', N'K-SCTY-65D23LX-00TC-MF-V07A', N'STIC TYPE MF 65D23L V07', CAST(N'2023-11-17T23:53:45.107' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3557', N'K-SCTY-65D23LX-00TC-MF-V08A', N'STIC TYPE MF 65D23L V08', CAST(N'2023-11-17T23:53:45.203' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3558', N'K-SCTY-65D23RX-00TC-MF-V07A', N'STIC TYPE MF 65D23R V07', CAST(N'2023-11-17T23:53:45.297' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3559', N'K-SCTY-65D23RX-00TC-MF-V08A', N'STIC TYPE MF 65D23R V08', CAST(N'2023-11-17T23:53:45.390' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'356', N'W-CV02-588RXXX-D02C-NL-00M0', N'CV 588R MF BLACK FS', CAST(N'2023-11-17T23:48:47.680' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3560', N'K-SCTY-65D26LX-00CA-CV-V01A', N'STIC TYPE CONV 65D26L V01', CAST(N'2023-11-17T23:53:45.503' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3561', N'K-SCTY-65D26LX-00CA-CV-V03A', N'STIC TYPE CONV 65D26L V03', CAST(N'2023-11-17T23:53:45.600' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3562', N'K-SCTY-65D26LX-00CA-CV-V04A', N'STIC TYPE CONV 65D26L V04', CAST(N'2023-11-17T23:53:45.687' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3563', N'K-SCTY-65D26LX-00CA-MF-V03A', N'STIC TYPE MF 65D26L V03', CAST(N'2023-11-17T23:53:45.770' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3564', N'K-SCTY-65D26LX-00TB-CV-V06A', N'STIC TYPE CONV 65D26L V06', CAST(N'2023-11-17T23:53:45.850' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3565', N'K-SCTY-65D26LX-00TC-CV-V07A', N'STIC TYPE CONV 65D26L V07', CAST(N'2023-11-17T23:53:45.933' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3566', N'K-SCTY-65D26LX-11CA-CV-V02A', N'STIC TYPE CONV 65D26L 11 V02', CAST(N'2023-11-17T23:53:46.017' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3567', N'K-SCTY-65D26RX-00CA-CV-V01A', N'STIC TYPE CONV 65D26R V01', CAST(N'2023-11-17T23:53:46.097' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3568', N'K-SCTY-65D26RX-00CA-CV-V03A', N'STIC TYPE CONV 65D26R V03', CAST(N'2023-11-17T23:53:46.180' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3569', N'K-SCTY-65D26RX-00CA-CV-V04A', N'STIC TYPE CONV 65D26R V04', CAST(N'2023-11-17T23:53:46.260' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'357', N'W-CV02-B20LSXX-D02C-NL-00M0', N'CV B20LS MF BLACK FS', CAST(N'2023-11-17T23:48:47.763' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3570', N'K-SCTY-65D26RX-00CA-MF-V03A', N'STIC TYPE MF 65D26R V03', CAST(N'2023-11-17T23:53:46.343' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3571', N'K-SCTY-65D26RX-00TB-CV-V06A', N'STIC TYPE CONV 65D26R V06', CAST(N'2023-11-17T23:53:46.423' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3572', N'K-SCTY-55563LH-00CA-MF-V03A', N'STIC TYPE MF 555-63 LH V03', CAST(N'2023-11-17T23:53:46.503' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3573', N'K-SCTY-55563LH-00TB-MF-V05A', N'STIC TYPE MF 555-63 LH V05', CAST(N'2023-11-17T23:53:46.587' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3574', N'K-SCTY-55563LH-00TC-MF-V06A', N'STIC TYPE MF 555-63 LH V06', CAST(N'2023-11-17T23:53:46.673' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3575', N'K-SCTY-55565XX-00CA-CV-V01A', N'STIC TYPE CONV 555-65 V01', CAST(N'2023-11-17T23:53:46.763' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3576', N'K-SCTY-55565XX-00CA-CV-V03A', N'STIC TYPE CONV 555-65 V03', CAST(N'2023-11-17T23:53:46.853' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3577', N'K-SCTY-46B24LS-00CA-CV-V03A', N'STIC TYPE CONV 46B24LS V03', CAST(N'2023-11-17T23:53:46.950' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3578', N'K-SCTY-46B24LS-00CA-CV-V04A', N'STIC TYPE CONV 46B24LS V04', CAST(N'2023-11-17T23:53:47.043' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3579', N'K-SCTY-46B24LS-00CA-MF-V01A', N'STIC TYPE MF 46B24LS V01', CAST(N'2023-11-17T23:53:47.133' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'358', N'W-CV02-B20LXXX-D02C-NL-00M0', N'CV B20L MF BLACK FS', CAST(N'2023-11-17T23:48:47.860' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3580', N'K-SCTY-46B24LS-00CA-MF-V03A', N'STIC TYPE MF 46B24LS V03', CAST(N'2023-11-17T23:53:47.227' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3581', N'K-SCTY-46B24LS-00TB-CV-V06A', N'STIC TYPE CONV 46B24LS V06', CAST(N'2023-11-17T23:53:47.320' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3582', N'K-SCTY-57520XX-00TC-MF-V06A', N'STIC TYPE MF 575-20 V06', CAST(N'2023-11-17T23:53:47.423' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3583', N'K-SCTY-57540LH-00CA-MF-V03A', N'STIC TYPE MF 575-40 LH V03', CAST(N'2023-11-17T23:53:47.517' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3584', N'K-SCTY-57540LH-00TB-MF-V05A', N'STIC TYPE MF 575-40 LH V05', CAST(N'2023-11-17T23:53:47.607' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3585', N'K-SCTY-57540LH-00TC-MF-V06A', N'STIC TYPE MF 575-40 LH V06', CAST(N'2023-11-17T23:53:47.690' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3586', N'K-SCTY-58024L3-00CA-MF-V03A', N'STIC TYPE MF 580-24 LN3 V03', CAST(N'2023-11-17T23:53:47.780' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3587', N'K-SCTY-58024XX-00CA-CV-V01A', N'STIC TYPE CONV 580-24 V01', CAST(N'2023-11-17T23:53:47.883' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3588', N'K-SCTY-58024XX-00CA-MF-V03A', N'STIC TYPE MF 580-24 V03', CAST(N'2023-11-17T23:53:47.980' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3589', N'K-SCTY-58024XX-00TB-CV-V06A', N'STIC TYPE CONV 580-24 V06', CAST(N'2023-11-17T23:53:48.073' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'359', N'W-CV02-B20RXXX-D02C-NL-00M0', N'CV B20R MF BLACK FS', CAST(N'2023-11-17T23:48:47.950' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3590', N'K-SCTY-58024XX-00TB-MF-V05A', N'STIC TYPE MF 580-24 V05', CAST(N'2023-11-17T23:53:48.163' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3591', N'K-SCTY-58024XX-00TC-CV-V07A', N'STIC TYPE CONV 580-24 V07', CAST(N'2023-11-17T23:53:48.250' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3592', N'K-SCTY-58024XX-00TC-MF-V06A', N'STIC TYPE MF 580-24 V06', CAST(N'2023-11-17T23:53:48.343' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3593', N'K-SCTY-58815LH-00CA-MF-V03A', N'STIC TYPE MF 588-15 LH V03', CAST(N'2023-11-17T23:53:48.450' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3594', N'K-SCTY-58815LH-00TB-MF-V05A', N'STIC TYPE MF 588-15 LH V05', CAST(N'2023-11-17T23:53:48.537' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3595', N'K-SCTY-58815LH-00TC-MF-V06A', N'STIC TYPE MF 588-15 LH V06', CAST(N'2023-11-17T23:53:48.617' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3596', N'K-SCTY-58821XX-00CA-MF-V03A', N'STIC TYPE MF 588-21 V03', CAST(N'2023-11-17T23:53:48.697' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3597', N'K-SCTY-58821XX-00TB-MF-V05A', N'STIC TYPE MF 588-21 V05', CAST(N'2023-11-17T23:53:48.780' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3598', N'K-SCTY-58821XX-00TC-MF-V06A', N'STIC TYPE MF 588-21 V06', CAST(N'2023-11-17T23:53:48.860' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3599', N'K-SCTY-58827XX-00CA-CV-V01A', N'STIC TYPE CONV 588-27 V01', CAST(N'2023-11-17T23:53:48.940' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'36', N'K-SCCV-252X050-11V0-OH-MFB0', N'STC CVR OHAYO MF UAE 252X50 MM', CAST(N'2023-11-17T23:48:19.367' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'360', N'W-CV02-B24LSXX-D02C-NL-00M0', N'CV B24LS MF BLACK FS', CAST(N'2023-11-17T23:48:48.033' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3600', N'K-SCTY-58827XX-00CA-CV-V03A', N'STIC TYPE CONV 588-27 V03', CAST(N'2023-11-17T23:53:49.023' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3601', N'K-SCTY-58827XX-00CA-MF-V03A', N'STIC TYPE MF 588-27 V03', CAST(N'2023-11-17T23:53:49.110' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3602', N'K-SCTY-58827XX-00TB-CV-V06A', N'STIC TYPE CONV 588-27 V06', CAST(N'2023-11-17T23:53:49.207' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3603', N'K-SCTY-58827XX-00TB-MF-V05A', N'STIC TYPE MF 588-27 V05', CAST(N'2023-11-17T23:53:49.303' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3604', N'K-SCTY-58827XX-00TC-CV-V07A', N'STIC TYPE CONV 588-27 V07', CAST(N'2023-11-17T23:53:49.393' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3605', N'K-SCTY-58827XX-00TC-MF-V06A', N'STIC TYPE MF 588-27 V06', CAST(N'2023-11-17T23:53:49.487' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3606', N'K-SCTY-95E41LX-00TB-CV-V06A', N'STIC TYPE CONV 95E41L V06', CAST(N'2023-11-17T23:53:49.587' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3607', N'K-SCTY-95E41LX-00TC-CV-V07A', N'STIC TYPE CONV 95E41L V07', CAST(N'2023-11-17T23:53:49.683' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3608', N'K-SCTY-95E41LX-15CA-CV-V02A', N'STIC TYPE CONV 95E41L 15 V02', CAST(N'2023-11-17T23:53:49.773' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3609', N'K-SCTY-95E41RX-00CA-CV-V01A', N'STIC TYPE CONV 95E41R V01', CAST(N'2023-11-17T23:53:49.853' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'361', N'W-CV02-B24LXXX-D02C-NL-00M0', N'CV B24L MF BLACK FS', CAST(N'2023-11-17T23:48:48.113' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3610', N'K-SCTY-95E41RX-00CA-CV-V03A', N'STIC TYPE CONV 95E41R V03', CAST(N'2023-11-17T23:53:49.940' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3611', N'K-SCTY-80D26RX-00CA-CV-V03A', N'STIC TYPE CONV 80D26R V03', CAST(N'2023-11-17T23:53:50.023' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3612', N'K-SCTY-80D26RX-00CA-CV-V04A', N'STIC TYPE CONV 80D26R V04', CAST(N'2023-11-17T23:53:50.107' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3613', N'K-SCTY-80D26RX-00CA-MF-V01A', N'STIC TYPE MF 80D26R V01', CAST(N'2023-11-17T23:53:50.207' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3614', N'K-SCTY-80D26RX-00CA-MF-V03A', N'STIC TYPE MF 80D26R V03', CAST(N'2023-11-17T23:53:50.290' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3615', N'K-SCTY-80D26RX-00CC-MF-V04A', N'STIC TYPE MF 80D26R V04', CAST(N'2023-11-17T23:53:50.390' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3616', N'K-SCTY-80D26RX-00TB-CV-V06A', N'STIC TYPE CONV 80D26R V06', CAST(N'2023-11-17T23:53:50.497' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3617', N'K-SCTY-80D26RX-00TC-CV-V07A', N'STIC TYPE CONV 80D26R V07', CAST(N'2023-11-17T23:53:50.593' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3618', N'K-SCTY-80D26RX-12CA-CV-V02A', N'STIC TYPE CONV 80D26R 12 V02', CAST(N'2023-11-17T23:53:50.680' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3619', N'K-SCTY-80D31LX-00TC-MF-V07A', N'STIC TYPE MF 80D31L V07', CAST(N'2023-11-17T23:53:50.777' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'362', N'W-CV02-B24RSXX-D02C-NL-00M0', N'CV B24RS MF BLACK FS', CAST(N'2023-11-17T23:48:48.190' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3620', N'K-SCTY-80D31LX-00TC-MF-V08A', N'STIC TYPE MF 80D31L V08', CAST(N'2023-11-17T23:53:50.860' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3621', N'K-SCTY-80D31RX-00TC-MF-V07A', N'STIC TYPE MF 80D31R V07', CAST(N'2023-11-17T23:53:50.940' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3622', N'K-SCTY-80D31RX-00TC-MF-V08A', N'STIC TYPE MF 80D31R V08', CAST(N'2023-11-17T23:53:51.023' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3623', N'K-SCTY-32C24LX-00TC-CV-V07A', N'STIC TYPE CONV 32C24L V07', CAST(N'2023-11-17T23:53:51.123' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3624', N'K-SCTY-32C24LX-09CA-CV-V02A', N'STIC TYPE CONV 32C24L 09 V02', CAST(N'2023-11-17T23:53:51.203' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3625', N'K-SCTY-32C24RX-00CA-CV-V01A', N'STIC TYPE CONV 32C24R V01', CAST(N'2023-11-17T23:53:51.287' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3626', N'K-SCTY-32C24RX-00CA-CV-V03A', N'STIC TYPE CONV 32C24R V03', CAST(N'2023-11-17T23:53:51.367' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3627', N'K-SCTY-32C24RX-00CA-CV-V04A', N'STIC TYPE CONV 32C24R V04', CAST(N'2023-11-17T23:53:51.457' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3628', N'K-SCTY-85D31RX-00CA-MF-V03A', N'STIC TYPE MF 85D31R HD V03', CAST(N'2023-11-17T23:53:51.543' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3629', N'K-SCTY-8D90XXX-00CA-DC-V01A', N'STIC TYPE DEEP CYCLE 8D90 V01', CAST(N'2023-11-17T23:53:51.630' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'363', N'W-CV02-B24RXXX-D02C-NL-00M0', N'CV B24R MF BLACK FS', CAST(N'2023-11-17T23:48:48.270' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3630', N'K-SCTY-90D26LX-00CA-MF-V03A', N'STIC TYPE MF 90D26L V03', CAST(N'2023-11-17T23:53:51.720' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3631', N'K-SCTY-90D26LX-00TC-MF-V07A', N'STIC TYPE MF 90D26L V07', CAST(N'2023-11-17T23:53:51.800' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3632', N'K-SCTY-90D26LX-00TC-MF-V08A', N'STIC TYPE MF 90D26L V08', CAST(N'2023-11-17T23:53:51.887' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3633', N'K-SCTY-90D26RX-00CA-MF-V03A', N'STIC TYPE MF 90D26R V03', CAST(N'2023-11-17T23:53:51.967' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3634', N'K-SCTY-90D26RX-00TC-MF-V07A', N'STIC TYPE MF 90D26R V07', CAST(N'2023-11-17T23:53:52.050' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3635', N'K-SCTY-90D26RX-00TC-MF-V08A', N'STIC TYPE MF 90D26R V08', CAST(N'2023-11-17T23:53:52.133' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3636', N'K-SCTY-95D26LX-00TC-MF-V07A', N'STIC TYPE MF 95D26L V07', CAST(N'2023-11-17T23:53:52.213' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3637', N'K-SCTY-32B20LS-00TB-CV-V06A', N'STIC TYPE CONV 32B20LS V06', CAST(N'2023-11-17T23:53:52.297' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3638', N'K-SCTY-32B20LS-00TC-CV-V07A', N'STIC TYPE CONV 32B20LS V07', CAST(N'2023-11-17T23:53:52.380' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3639', N'K-SCTY-32B20LS-07CA-CV-V02A', N'STIC TYPE CONV 32B20LS 07 V02', CAST(N'2023-11-17T23:53:52.460' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'364', N'W-CV02-D23LXXX-D02C-NL-00M0', N'CV D23L MF BLACK FS', CAST(N'2023-11-17T23:48:48.353' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3640', N'K-SCTY-32B20LX-00CA-CV-V01A', N'STIC TYPE CONV 32B20L V01', CAST(N'2023-11-17T23:53:52.547' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3641', N'K-SCTY-32B20LX-00CA-CV-V03A', N'STIC TYPE CONV 32B20L V03', CAST(N'2023-11-17T23:53:52.627' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3642', N'K-SCTY-46B24LS-00TC-CV-V07A', N'STIC TYPE CONV 46B24LS V07', CAST(N'2023-11-17T23:53:52.710' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3643', N'K-SCTY-46B24LS-09CA-CV-V02A', N'STIC TYPE CONV 46B24LS 09 V02', CAST(N'2023-11-17T23:53:52.793' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3644', N'K-SCTY-46B24LS-11CA-CV-V02A', N'STIC TYPE CONV 46B24LS 11 V02', CAST(N'2023-11-17T23:53:52.877' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3645', N'K-SCTY-46B24LX-00CA-CV-V01A', N'STIC TYPE CONV 46B24L V01', CAST(N'2023-11-17T23:53:52.957' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3646', N'K-SCTY-46B24LX-00CA-CV-V03A', N'STIC TYPE CONV 46B24L V03', CAST(N'2023-11-17T23:53:53.040' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3647', N'K-SCTY-46B24LX-00CA-CV-V04A', N'STIC TYPE CONV 46B24L V04', CAST(N'2023-11-17T23:53:53.120' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3648', N'K-SCTY-46B24LX-00CA-MF-V03A', N'STIC TYPE MF 46B24L V03', CAST(N'2023-11-17T23:53:53.203' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3649', N'K-SCTY-46B24LX-00TB-CV-V06A', N'STIC TYPE CONV 46B24L V06', CAST(N'2023-11-17T23:53:53.287' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'365', N'W-CV02-D23LXXX-D02S-NL-00M0', N'CV D23L MF BLACK FS SIDE BA-IN', CAST(N'2023-11-17T23:48:48.433' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3650', N'K-SCTY-46B24LX-00TC-CV-V07A', N'STIC TYPE CONV 46B24L V07', CAST(N'2023-11-17T23:53:53.367' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3651', N'K-SCTY-46B24LX-11CA-CV-V02A', N'STIC TYPE CONV 46B24L 11 V02', CAST(N'2023-11-17T23:53:53.450' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3652', N'K-SCTY-46B24RS-00CA-CV-V01A', N'STIC TYPE CONV 46B24RS V01', CAST(N'2023-11-17T23:53:53.533' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3653', N'K-SCTY-46B24RS-00CA-CV-V03A', N'STIC TYPE CONV 46B24RS V03', CAST(N'2023-11-17T23:53:53.617' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3654', N'K-SCTY-46B24RS-00CA-MF-V03A', N'STIC TYPE MF 46B24RS V03', CAST(N'2023-11-17T23:53:53.697' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3655', N'K-SCTY-46B24RS-00TB-CV-V06A', N'STIC TYPE CONV 46B24RS V06', CAST(N'2023-11-17T23:53:53.780' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3656', N'K-SCTY-46B24RS-00TC-CV-V07A', N'STIC TYPE CONV 46B24RS V07', CAST(N'2023-11-17T23:53:53.860' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3657', N'K-SCTY-46B24RS-09CA-CV-V02A', N'STIC TYPE CONV 46B24RS 09 V02', CAST(N'2023-11-17T23:53:53.947' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3658', N'K-SCTY-165G51X-00CA-CV-V01A', N'STIC TYPE CONV 165G51 V01', CAST(N'2023-11-17T23:53:54.030' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3659', N'K-SCTY-165G51X-00TB-CV-V06A', N'STIC TYPE CONV 165G51 V06', CAST(N'2023-11-17T23:53:54.113' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'366', N'W-CV02-D23RXXX-D02C-NL-00M0', N'CV D23R MF BLACK FS', CAST(N'2023-11-17T23:48:48.517' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3660', N'K-SCTY-165G51X-00TC-CV-V07A', N'STIC TYPE CONV 165G51 V07', CAST(N'2023-11-17T23:53:54.193' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3661', N'K-SCTY-170G51X-00TC-MF-V07A', N'STIC TYPE MF 170G51 V07', CAST(N'2023-11-17T23:53:54.277' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3662', N'K-SCTY-170G51X-00TC-MF-V08A', N'STIC TYPE MF 170G51 V08', CAST(N'2023-11-17T23:53:54.360' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3663', N'K-SCTY-56633XX-00TC-CV-V07A', N'STIC TYPE CONV 566-33 V07', CAST(N'2023-11-17T23:53:54.440' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3664', N'K-SCTY-56633XX-00TC-MF-V06A', N'STIC TYPE MF 566-33 V06', CAST(N'2023-11-17T23:53:54.527' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3665', N'K-SCTY-56638XX-00CA-CV-V01A', N'STIC TYPE CONV 566-38 V01', CAST(N'2023-11-17T23:53:54.607' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3666', N'K-SCTY-56638XX-00CA-CV-V03A', N'STIC TYPE CONV 566-38 V03', CAST(N'2023-11-17T23:53:54.687' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3667', N'K-SCTY-57519XX-00TB-MF-V05A', N'STIC TYPE MF 575-19 V05', CAST(N'2023-11-17T23:53:54.770' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3668', N'K-SCTY-57519XX-00TC-CV-V07A', N'STIC TYPE CONV 575-19 V07', CAST(N'2023-11-17T23:53:54.850' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3669', N'K-SCTY-57519XX-00TC-MF-V06A', N'STIC TYPE MF 575-19 V06', CAST(N'2023-11-17T23:53:54.930' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'367', N'W-CV02-D23RXXX-D02S-NL-00M0', N'CV D23R MF BLACK FS SIDE BA-IN', CAST(N'2023-11-17T23:48:48.597' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3670', N'K-SCTY-57520XX-00CA-MF-V03A', N'STIC TYPE MF 575-20 V03', CAST(N'2023-11-17T23:53:55.013' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3671', N'K-SCTY-57520XX-00TB-MF-V05A', N'STIC TYPE MF 575-20 V05', CAST(N'2023-11-17T23:53:55.377' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3672', N'K-SCTY-46B24RS-11CA-CV-V02A', N'STIC TYPE CONV 46B24RS 11 V02', CAST(N'2023-11-17T23:53:55.473' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3673', N'K-SCTY-46B24RX-00CA-CV-V01A', N'STIC TYPE CONV 46B24R V01', CAST(N'2023-11-17T23:53:55.567' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3674', N'K-SCTY-46B24RX-00CA-CV-V03A', N'STIC TYPE CONV 46B24R V03', CAST(N'2023-11-17T23:53:55.663' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3675', N'K-SCTY-46B24RX-00CA-CV-V04A', N'STIC TYPE CONV 46B24R V04', CAST(N'2023-11-17T23:53:55.767' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3676', N'K-SCTY-46B24RX-00CA-MF-V03A', N'STIC TYPE MF 46B24R V03', CAST(N'2023-11-17T23:53:55.883' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3677', N'K-SCTY-46B24RX-00TB-CV-V06A', N'STIC TYPE CONV 46B24R V06', CAST(N'2023-11-17T23:53:56.100' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3678', N'K-SCTY-46B24RX-00TC-CV-V07A', N'STIC TYPE CONV 46B24R V07', CAST(N'2023-11-17T23:53:56.223' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3679', N'K-SCTY-95E41RX-00CA-CV-V04A', N'STIC TYPE CONV 95E41R V04', CAST(N'2023-11-17T23:53:56.320' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'368', N'W-CV02-D26LXXX-D02S-NL-00M0', N'CV D26L MF BLACK FS SIDE BA-IN', CAST(N'2023-11-17T23:48:48.677' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3680', N'K-SCTY-95E41RX-00CA-MF-V03A', N'STIC TYPE MF 95E41R V03', CAST(N'2023-11-17T23:53:56.440' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3681', N'K-SCTY-95E41RX-00TB-CV-V06A', N'STIC TYPE CONV 95E41R V06', CAST(N'2023-11-17T23:53:56.553' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3682', N'K-SCTY-95E41RX-00TC-CV-V07A', N'STIC TYPE CONV 95E41R V07', CAST(N'2023-11-17T23:53:56.650' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3683', N'K-SCTY-95E41RX-15CA-CV-V02A', N'STIC TYPE CONV 95E41R 15 V02', CAST(N'2023-11-17T23:53:56.740' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3684', N'K-SCTY-M42LXXX-00CA-EF-V01A', N'STIC TYPE EFB M-42L V01', CAST(N'2023-11-17T23:53:56.830' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3685', N'K-SCTY-M42RXXX-00CA-EF-V01A', N'STIC TYPE EFB M-42R V01', CAST(N'2023-11-17T23:53:56.930' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3686', N'K-SCTY-N100LXX-00CA-MF-V02A', N'STIC TYPE MF N100L V02', CAST(N'2023-11-17T23:53:57.020' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3687', N'K-SCTY-N100TXX-00CA-DC-V02A', N'STIC TYPE N100T V02', CAST(N'2023-11-17T23:53:57.120' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3688', N'K-SCTY-N100XXX-0000-CV-IAPC', N'STIC TYPE CONV N100 IAPC', CAST(N'2023-11-17T23:53:57.227' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3689', N'K-SCTY-N100XXX-00CA-MF-V02A', N'STIC TYPE MF N100 V02', CAST(N'2023-11-17T23:53:57.333' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'369', N'W-CV02-D31LXXX-D02C-NL-00M0', N'CV D31L MF BLACK FS', CAST(N'2023-11-17T23:48:48.757' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3690', N'K-SCTY-N120XXX-0000-CV-IAPC', N'STIC TYPE CONV N120 IAPC', CAST(N'2023-11-17T23:53:57.427' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3691', N'K-SCTY-N120XXX-00CA-MF-V02A', N'STIC TYPE MF N120 V02', CAST(N'2023-11-17T23:53:57.510' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3692', N'K-SCTY-N150TXX-00CA-DC-V02A', N'STIC TYPE N150T V02', CAST(N'2023-11-17T23:53:57.610' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3693', N'K-SCTY-N150XXX-0000-CV-IAPC', N'STIC TYPE CONV N150 IAPC', CAST(N'2023-11-17T23:53:57.697' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3694', N'K-SCTY-N150XXX-00CA-MF-V02A', N'STIC TYPE MF N150 V02', CAST(N'2023-11-17T23:53:57.780' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3695', N'K-SCTY-75D26RX-00TC-MF-V07A', N'STIC TYPE MF 75D26R V07', CAST(N'2023-11-17T23:53:57.863' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3696', N'K-SCTY-75D26RX-00TC-MF-V08A', N'STIC TYPE MF 75D26R V08', CAST(N'2023-11-17T23:53:57.960' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3697', N'K-SCTY-75D31LX-00CA-CV-V01A', N'STIC TYPE CONV 75D31L V01', CAST(N'2023-11-17T23:53:58.043' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3698', N'K-SCTY-75D31LX-00CA-CV-V03A', N'STIC TYPE CONV 75D31L V03', CAST(N'2023-11-17T23:53:58.147' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3699', N'K-SCTY-75D31LX-00CA-CV-V04A', N'STIC TYPE CONV 75D31L V04', CAST(N'2023-11-17T23:53:58.230' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'37', N'K-SCCV-252X050-11V0-QU-MFA0', N'STC CVR QUANTUM MF 252X50 MM', CAST(N'2023-11-17T23:48:19.450' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'370', N'W-CV02-D31LXXX-D02C-NL-00T0', N'CV D31L MF BLACK', CAST(N'2023-11-17T23:48:48.837' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3700', N'K-SCTY-75D31LX-00CA-MF-V03A', N'STIC TYPE MF 75D31L V03', CAST(N'2023-11-17T23:53:58.320' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3701', N'K-SCTY-75D31LX-00TB-CV-V06A', N'STIC TYPE CONV 75D31L V06', CAST(N'2023-11-17T23:53:58.410' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3702', N'K-SCTY-75D31LX-00TC-CV-V07A', N'STIC TYPE CONV 75D31L V07', CAST(N'2023-11-17T23:53:58.490' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3703', N'K-SCTY-75D31LX-13CA-CV-V02A', N'STIC TYPE CONV 75D31L 13 V02', CAST(N'2023-11-17T23:53:58.573' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3704', N'K-SCTY-75D31RX-00CA-CV-V01A', N'STIC TYPE CONV 75D31R V01', CAST(N'2023-11-17T23:53:58.657' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3705', N'K-SCTY-75D31RX-00CA-CV-V03A', N'STIC TYPE CONV 75D31R V03', CAST(N'2023-11-17T23:53:58.753' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3706', N'K-SCTY-75D31RX-00CA-CV-V04A', N'STIC TYPE CONV 75D31R V04', CAST(N'2023-11-17T23:53:58.840' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3707', N'K-SCTY-75D31RX-00CA-MF-V03A', N'STIC TYPE MF 75D31R V03', CAST(N'2023-11-17T23:53:58.940' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3708', N'K-SCTY-75D31RX-00TB-CV-V06A', N'STIC TYPE CONV 75D31R V06', CAST(N'2023-11-17T23:53:59.017' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3709', N'K-SCTY-75D31RX-00TC-CV-V07A', N'STIC TYPE CONV 75D31R V07', CAST(N'2023-11-17T23:53:59.130' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'371', N'W-CV02-D31LXXX-D02S-NL-00M0', N'CV D31L MF BLACK FS SIDE BA-IN', CAST(N'2023-11-17T23:48:48.913' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3710', N'K-SCTY-75D31RX-13CA-CV-V02A', N'STIC TYPE CONV 75D31R 13 V02', CAST(N'2023-11-17T23:53:59.380' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3711', N'K-SCTY-80D23LX-00TC-MF-V07A', N'STIC TYPE MF 80D23L V07', CAST(N'2023-11-17T23:53:59.580' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3712', N'K-SCTY-80D23LX-00TC-MF-V08A', N'STIC TYPE MF 80D23L V08', CAST(N'2023-11-17T23:53:59.843' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3713', N'K-SCTY-80D23RX-00TC-MF-V07A', N'STIC TYPE MF 80D23R V07', CAST(N'2023-11-17T23:54:00.050' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3714', N'K-SCTY-80D23RX-00TC-MF-V08A', N'STIC TYPE MF 80D23R V08', CAST(N'2023-11-17T23:54:00.150' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3715', N'K-SCTY-80D26LX-0000-CV-IAPC', N'STIC TYPE CONV 80D26L IAPC', CAST(N'2023-11-17T23:54:00.240' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3716', N'K-SCTY-80D26LX-00CA-CV-V01A', N'STIC TYPE CONV 80D26L V01', CAST(N'2023-11-17T23:54:00.353' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3717', N'K-SCTY-80D26LX-00CA-CV-V03A', N'STIC TYPE CONV 80D26L V03', CAST(N'2023-11-17T23:54:00.460' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3718', N'K-SCTY-80D26LX-00CA-CV-V04A', N'STIC TYPE CONV 80D26L V04', CAST(N'2023-11-17T23:54:00.573' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3719', N'K-SCTY-145G51X-00TB-MF-V05A', N'STIC TYPE MF 145G51 V05', CAST(N'2023-11-17T23:54:00.660' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'372', N'W-CV02-D31RXXX-D02C-NL-00M0', N'CV D31R MF BLACK FS', CAST(N'2023-11-17T23:48:48.993' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3720', N'K-SCTY-32B20LX-00TB-MF-V05A', N'STIC TYPE MF 32B20L V05', CAST(N'2023-11-17T23:54:00.780' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3721', N'K-SCTY-32B20LX-00TC-MF-V06A', N'STIC TYPE MF 32B20L V06', CAST(N'2023-11-17T23:54:00.877' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3722', N'K-SCTY-32B20RX-00TB-MF-V05A', N'STIC TYPE MF 32B20R V05', CAST(N'2023-11-17T23:54:00.987' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3723', N'K-SCTY-32C24RX-00TB-MF-V05A', N'STIC TYPE MF 32C24R V05', CAST(N'2023-11-17T23:54:01.087' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3724', N'K-SCTY-32C24RX-00TC-MF-V06A', N'STIC TYPE MF 32C24R V06', CAST(N'2023-11-17T23:54:01.170' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3725', N'K-SCTY-34B20LX-00CA-HY-V01A', N'STIC TYPE HY 34B20L V01', CAST(N'2023-11-17T23:54:01.253' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3726', N'K-SCTY-36B20LX-00CA-HY-V01A', N'STIC TYPE HY 36B20L V01', CAST(N'2023-11-17T23:54:01.337' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3727', N'K-SCTY-36B20LX-00TB-MF-V05A', N'STIC TYPE MF 36B20L V05', CAST(N'2023-11-17T23:54:01.417' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3728', N'K-SCTY-36B20LX-00TC-MF-V06A', N'STIC TYPE MF 36B20L V06', CAST(N'2023-11-17T23:54:01.497' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3729', N'K-SCTY-36B20RX-00CA-HY-V01A', N'STIC TYPE HY 36B20R V01', CAST(N'2023-11-17T23:54:01.577' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'373', N'W-CV02-D31RXXX-D02N-NL-00M0', N'CV D31R MF BLACK FS NO BA-IN', CAST(N'2023-11-17T23:48:49.077' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3730', N'K-SCTY-46B24LS-00CA-HY-V01A', N'STIC TYPE HY 46B24LS V01', CAST(N'2023-11-17T23:54:01.660' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3731', N'K-SCTY-46B24LX-00CA-HY-V01A', N'STIC TYPE HY 46B24L V01', CAST(N'2023-11-17T23:54:01.740' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3732', N'K-SCTY-46B24LX-00TC-MF-V06A', N'STIC TYPE MF 46B24L V06', CAST(N'2023-11-17T23:54:01.820' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3733', N'K-SCTY-48D26LX-00TB-MF-V05A', N'STIC TYPE MF 48D26L V05', CAST(N'2023-11-17T23:54:01.903' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3734', N'K-SCTY-115F51X-00CA-CV-V08A', N'STIC TYPE CONV 115F51 V08', CAST(N'2023-11-17T23:54:01.983' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3735', N'K-SCTY-32B20RX-00CA-CV-V08A', N'STIC TYPE CONV 32B20R V08', CAST(N'2023-11-17T23:54:02.070' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3736', N'K-SCTY-32C24RX-00CA-CV-V08A', N'STIC TYPE CONV 32C24R V08', CAST(N'2023-11-17T23:54:02.153' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3737', N'K-SCTY-55D26RX-00CA-CV-V08A', N'STIC TYPE CONV 55D26R V08', CAST(N'2023-11-17T23:54:02.287' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3738', N'K-SCTY-65D31LX-00CA-CV-V08A', N'STIC TYPE CONV 65D31L V08', CAST(N'2023-11-17T23:54:02.387' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3739', N'K-SCTY-65D31RX-00CA-CV-V08A', N'STIC TYPE CONV 65D31R V08', CAST(N'2023-11-17T23:54:02.473' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'374', N'W-CV02-D31RXXX-D02S-NL-00M0', N'CV D31R MF BLACK FS SIDE BA-IN', CAST(N'2023-11-17T23:48:49.160' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3740', N'K-SCTY-95E41RX-00CA-CV-V08A', N'STIC TYPE CONV 95E41R V08', CAST(N'2023-11-17T23:54:02.583' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3741', N'K-SCTY-48D26LX-00TC-MF-V06A', N'STIC TYPE MF 48D26L V06', CAST(N'2023-11-17T23:54:02.740' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3742', N'K-SCTY-48D26RX-00CA-HY-V01A', N'STIC TYPE HY 48D26R V01', CAST(N'2023-11-17T23:54:02.853' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3743', N'K-SCTY-55D23RX-00TB-MF-V05A', N'STIC TYPE MF 55D23R V05', CAST(N'2023-11-17T23:54:02.947' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3744', N'K-SCTY-55D23RX-00TC-MF-V06A', N'STIC TYPE MF 55D23R V06', CAST(N'2023-11-17T23:54:03.040' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3745', N'K-SCTY-55D26RX-00CA-HY-V01A', N'STIC TYPE HY 55D26R V01', CAST(N'2023-11-17T23:54:03.133' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3746', N'K-SCTY-65D26LX-00CA-HY-V01A', N'STIC TYPE HY 65D26L V01', CAST(N'2023-11-17T23:54:03.240' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3747', N'K-SCTY-65D26LX-00TB-MF-V05A', N'STIC TYPE MF 65D26L V05', CAST(N'2023-11-17T23:54:03.337' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3748', N'K-SCTY-65D26RX-00CA-HY-V01A', N'STIC TYPE HY 65D26R V01', CAST(N'2023-11-17T23:54:03.433' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3749', N'K-SCTY-65D26RX-00TB-MF-V05A', N'STIC TYPE MF 65D26R V05', CAST(N'2023-11-17T23:54:03.530' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'375', N'W-TC02-544XXXX-HSFS-NL-0000', N'TOP COVER 544 BLACK FS', CAST(N'2023-11-17T23:48:49.240' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3750', N'K-SCTY-65D31LX-00CA-HY-V01A', N'STIC TYPE HY 65D31L V01', CAST(N'2023-11-17T23:54:03.620' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3751', N'K-SCTY-65D31LX-00TC-MF-V06A', N'STIC TYPE MF 65D31L V06', CAST(N'2023-11-17T23:54:03.877' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3752', N'K-SCTY-65D31RX-00CA-HY-V01A', N'STIC TYPE HY 65D31R V01', CAST(N'2023-11-17T23:54:04.003' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3753', N'K-SCTY-115D31L-00CA-MF-V02A', N'STIC TYPE MF 115D31L V02', CAST(N'2023-11-17T23:54:04.127' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3754', N'K-SCTY-115D31R-00CA-MF-V02A', N'STIC TYPE MF 115D31R V02', CAST(N'2023-11-17T23:54:04.240' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3755', N'K-SCTY-65D31RX-00TC-MF-V06A', N'STIC TYPE MF 65D31R V06', CAST(N'2023-11-17T23:54:04.357' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3756', N'K-SCTY-75D31LX-00CA-HY-V01A', N'STIC TYPE HY 75D31L V01', CAST(N'2023-11-17T23:54:04.470' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3757', N'K-SCTY-75D31RX-00CA-HY-V01A', N'STIC TYPE HY 75D31R V01', CAST(N'2023-11-17T23:54:04.573' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3758', N'K-SCTY-80D26LX-00TC-MF-V06A', N'STIC TYPE MF 80D26L V06', CAST(N'2023-11-17T23:54:04.660' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3759', N'K-SCTY-80D26RX-00TB-MF-V05A', N'STIC TYPE MF 80D26R V05', CAST(N'2023-11-17T23:54:04.747' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'376', N'W-TC02-555XXXX-HSFS-NL-0000', N'TOP COVER 555 BLACK FS', CAST(N'2023-11-17T23:48:49.320' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3760', N'K-SCTY-90D26LX-00TC-MF-V06A', N'STIC TYPE MF 90D26L V06', CAST(N'2023-11-17T23:54:04.890' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3761', N'K-SCTY-90D26RX-00TC-MF-V06A', N'STIC TYPE MF 90D26R V06', CAST(N'2023-11-17T23:54:05.013' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3762', N'K-SCTY-95D26LX-00TB-MF-V05A', N'STIC TYPE MF 90D26L V05', CAST(N'2023-11-17T23:54:05.110' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3763', N'K-SCTY-95D31LX-00CA-HY-V01A', N'STIC TYPE HY 95D31L V01', CAST(N'2023-11-17T23:54:05.207' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3764', N'K-SCTY-95E41LX-00TC-MF-V06A', N'STIC TYPE MF 95E41L V06', CAST(N'2023-11-17T23:54:05.297' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3765', N'K-SCTY-95E41RX-00TB-MF-V05A', N'STIC TYPE MF 95E41R V05', CAST(N'2023-11-17T23:54:05.393' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3766', N'K-SCTY-95E41RX-00TC-MF-V06A', N'STIC TYPE MF 95E41R V06', CAST(N'2023-11-17T23:54:05.487' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3767', N'K-SCTY-FG12100-00CA-DC-V02A', N'STIC TYPE DC FGB-12 100 V02', CAST(N'2023-11-17T23:54:05.583' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3768', N'K-SCTY-115D31R-00TC-MF-V06A', N'STIC TYPE MF 115D31R V06', CAST(N'2023-11-17T23:54:05.690' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3769', N'K-SCTY-115F51X-00TB-MF-V05A', N'STIC TYPE MF 115F51 V05', CAST(N'2023-11-17T23:54:05.787' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'377', N'W-TC02-566XXXX-HSFS-NL-0000', N'TOP COVER 566 BLACK FS', CAST(N'2023-11-17T23:48:49.400' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3770', N'K-SCTY-115F51X-00TC-MF-V06A', N'STIC TYPE MF 115F51 V06', CAST(N'2023-11-17T23:54:05.877' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3771', N'K-VP02-18A171X-SEFS-NL-VP00', N'VENT PLUG 18A-171 BLACK', CAST(N'2023-11-17T23:54:05.963' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3772', N'K-VP02-20238XX-SEFS-NL-VP00', N'VENT PLUG VRLA 20-23.8 BLACK', CAST(N'2023-11-17T23:54:06.043' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3773', N'K-VP02-GC6VXXX-SEFS-NL-SC00', N'SPIN CAP GC-6V BLACK', CAST(N'2023-11-17T23:54:06.130' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3774', N'K-VP02-GC8VXXX-SEFS-NL-SC00', N'SPIN CAP GC-8V BLACK', CAST(N'2023-11-17T23:54:06.213' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3775', N'K-VP02-PW1AXXX-SEFS-NL-VP00', N'VENT PLUG PW-1A BLACK', CAST(N'2023-11-17T23:54:06.297' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3776', N'K-VP07-G15A51X-22B2-NL-VP00', N'VENT PLUG G18-A51 RED', CAST(N'2023-11-17T23:54:06.380' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3777', N'K-VP07-PW1XXXX-0000-NL-VP00', N'VENT PLUG PW-1 RED', CAST(N'2023-11-17T23:54:06.463' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3778', N'K-VP07-PW5XXXX-0000-NL-VP00', N'VENT PLUG PW-5 RED', CAST(N'2023-11-17T23:54:06.550' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3779', N'W-VP02-TZ5SKIT-0000-NL-VC00', N'VENT CAP TZ-5S KIT', CAST(N'2023-11-17T23:54:06.643' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'378', N'W-TC02-588XXXX-HSFS-NL-0000', N'TOP COVER 588 BLACK FS', CAST(N'2023-11-17T23:48:49.480' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3780', N'W-VP03-1822XXX-22B1-NL-VPR0', N'VENT PLUG 18-22 ORANGE', CAST(N'2023-11-17T23:54:06.740' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3781', N'W-VP03-G18A50X-22B2-NL-VPR0', N'VENT PLUG G18-A50 ORANGE', CAST(N'2023-11-17T23:54:06.833' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3782', N'W-VP03-G30A9XX-35D3-NL-VPR0', N'VENT PLUG G30-A9 ORANGE', CAST(N'2023-11-17T23:54:06.930' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3783', N'W-VP05-1822XXX-22B1-NL-VPR0', N'VENT PLUG 18-22 YELLOW', CAST(N'2023-11-17T23:54:07.030' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3784', N'W-VP05-G18A50X-22B2-NL-VPR0', N'VENT PLUG G18-A50 YELLOW', CAST(N'2023-11-17T23:54:07.130' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3785', N'W-VP05-G30A9XX-35D3-NL-VPR0', N'VENT PLUG G30-A9 YELLOW', CAST(N'2023-11-17T23:54:07.237' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3786', N'K-VP03-G22A14X-18A3-NL-VPR0', N'VENT PLUG G22-A14 ORANGE', CAST(N'2023-11-17T23:54:07.320' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3787', N'W-VP02-1622XXX-22B1-NL-VP0T', N'VENT PLUG 16-22 BLACK', CAST(N'2023-11-17T23:54:07.423' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3788', N'W-VP02-1822XXX-22B1-NL-VPR0', N'VENT PLUG 18-22 BLACK', CAST(N'2023-11-17T23:54:07.897' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3789', N'W-VP02-G18A50X-22B2-NL-VPR0', N'VENT PLUG G18-A50 BLACK', CAST(N'2023-11-17T23:54:07.977' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'379', N'K-SCCV-170X045-11V0-OH-MFB0', N'STIC CVR OHAYO MF UAE 170X45 M', CAST(N'2023-11-17T23:48:49.563' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3790', N'W-VP02-G30A9XX-35D3-NL-VPR0', N'VENT PLUG G30-A9 BLACK', CAST(N'2023-11-17T23:54:08.067' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3791', N'K-SCCO-72018XX-11VE-OH-MFA0', N'STC CON 72018 OHA MF 200X100MM', CAST(N'2023-11-17T23:54:08.157' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3792', N'K-SCCO-150X075-11VE-OH-MFA0', N'STC CONT OHAYO MF 150X75 MM -E', CAST(N'2023-11-17T23:54:08.247' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3793', N'K-SCCO-150X075-11VE-QU-MFA0', N'STC CONT QUANTUM MF 150X75 MM', CAST(N'2023-11-17T23:54:08.330' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3794', N'K-SCCO-150X075-11VE-QU-MFB0', N'STC CONT QUA MII MF 150X75 MM', CAST(N'2023-11-17T23:54:08.420' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3795', N'K-SCCO-150X085-11VE-QU-MFA0', N'STC CONT QUANTUM MF 150X85 MM', CAST(N'2023-11-17T23:54:08.503' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3796', N'K-SCCO-150X085-11VE-QU-MFB0', N'STC CONT QUA MII MF 150X85 MM', CAST(N'2023-11-17T23:54:08.590' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3797', N'K-SCCO-150X090-11S0-BO-DCA0', N'STIC CONT BOND DEEP 150X90 MM', CAST(N'2023-11-17T23:54:08.680' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3798', N'K-SCCO-150X090-11S0-BO-PRA0', N'STIC CONT BOND PRE 150X90 MM', CAST(N'2023-11-17T23:54:08.763' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3799', N'K-SCCO-150X090-11S0-BO-PRB0', N'STC CONT BOND PRE CV 150X90 MM', CAST(N'2023-11-17T23:54:08.850' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'38', N'K-SCCV-252X050-11VE-AS-MFA0', N'STIC CVR ASAHI MF 252X50 MM', CAST(N'2023-11-17T23:48:19.537' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'380', N'K-SCCV-218X050-11VE-IN-MFZ0', N'STIC CVR INCOE MF 218X50 MM', CAST(N'2023-11-17T23:48:49.643' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3800', N'K-SCCO-150X090-11V0-LU-EFA0', N'STC CONT LUCAS EFB 150X90 MM', CAST(N'2023-11-17T23:54:08.937' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3801', N'K-SCCO-150X090-11V0-LU-PRA0', N'STC CONT LUCAS PRE 150X90 MM', CAST(N'2023-11-17T23:54:09.023' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3802', N'K-SCCO-150X090-11VE-AS-MFA0', N'STC CONT ASAHI MF 150X90 MM -E', CAST(N'2023-11-17T23:54:09.107' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3803', N'K-SCCO-150X090-11VE-OH-MFA0', N'STC CONT OHAYO MF 150X90 MM -E', CAST(N'2023-11-17T23:54:09.193' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3804', N'K-SCCO-150X090-11VJ-AS-EFA0', N'STIC CONT ASAHI EFB 150X90 MM', CAST(N'2023-11-17T23:54:09.277' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3805', N'K-SCCO-150X090-11VJ-AS-MFA0', N'STC CONT ASAHI MF 150X90 MM -J', CAST(N'2023-11-17T23:54:09.360' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3806', N'K-SCCO-150X090-11VJ-IN-EFA0', N'STIC CONT INCOE EFB 150X90 MM', CAST(N'2023-11-17T23:54:09.447' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3807', N'K-SCCO-150X090-11VJ-OH-EFA0', N'STIC CONT OHAYO EFB 150X90 MM', CAST(N'2023-11-17T23:54:09.530' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3808', N'K-SCCO-150X090-11VJ-OH-MFA0', N'STC CONT OHAYO MF 150X90 MM -J', CAST(N'2023-11-17T23:54:09.620' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3809', N'K-SCCO-150X090-11VJ-QU-EFA0', N'STC CONT QUANTUM EFB 150X90 MM', CAST(N'2023-11-17T23:54:09.707' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'381', N'K-SCCV-220X045-11VJ-IN-MFZ0', N'STIC CVR INCOE MF 220X45 MM', CAST(N'2023-11-17T23:48:49.727' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3810', N'K-SCCO-150X090-11VJ-QU-MFA0', N'STC CONT QUANTUM MF 150X90 MM', CAST(N'2023-11-17T23:54:09.790' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3811', N'K-SCCO-150X090-11VJ-QU-MFB0', N'STC CONT QUA MII MF 150X90 MM', CAST(N'2023-11-17T23:54:09.877' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3812', N'K-SCCO-170X100-11V0-BO-INA0', N'STIC CONT BOND IND 170X100 MM', CAST(N'2023-11-17T23:54:09.960' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3813', N'K-SCCO-200X100-11V0-LU-PRA0', N'STC CONT LUCAS PRE 200X100 MM', CAST(N'2023-11-17T23:54:10.047' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3814', N'K-SCCO-200X100-11V0-NI-PRA0', N'STIC CONT NIKO PREM 200X100 MM', CAST(N'2023-11-17T23:54:10.130' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3815', N'K-SCCO-200X100-11VJ-AS-MFA0', N'STIC CONT ASAHI MF 200X100 MM', CAST(N'2023-11-17T23:54:10.217' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3816', N'K-SCCO-124X074-11VJ-QU-MFA0', N'STC CONT QUANTUM MF 124X74 MM', CAST(N'2023-11-17T23:54:10.303' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3817', N'K-SCCO-150X075-11V0-LU-PRA0', N'STC CONT LUCAS PRE 150X75 MM', CAST(N'2023-11-17T23:54:10.387' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3818', N'K-SCCO-200X100-11VJ-OH-MFA0', N'STIC CONT OHAYO MF 200X100 MM', CAST(N'2023-11-17T23:54:10.467' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3819', N'K-SCCO-200X100-11VJ-QU-MFA0', N'STC CONT QUANTUM MF 200X100 MM', CAST(N'2023-11-17T23:54:10.553' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'382', N'K-SCCV-252X050-11VE-IN-MFZ0', N'STIC CVR INCOE MF 252X50 MM', CAST(N'2023-11-17T23:48:49.807' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3820', N'K-SCCO-200X100-11VJ-QU-MFB0', N'STC CONT QUA MII MF 200X100 MM', CAST(N'2023-11-17T23:54:10.637' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3821', N'K-SCCO-67018XX-11VE-OH-MFA0', N'STC CON 67018 OHA MF 200X100MM', CAST(N'2023-11-17T23:54:10.717' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3822', N'K-SCCO-150X075-11VE-AS-MFA0', N'STIC CONT ASAHI MF 150X75 MM', CAST(N'2023-11-17T23:54:10.803' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3823', N'K-SCCO-150X090-11V0-NI-PRA0', N'STIC CONT NIKO PREM 150X90 MM', CAST(N'2023-11-17T23:54:10.887' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3824', N'K-SCCO-150X075-11VE-IN-MFZ0', N'STC CON INCOE MF 150X75MM - E', CAST(N'2023-11-17T23:54:10.970' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3825', N'K-SCCO-150X090-11VJ-IN-MFZ0', N'STC CON INCOE MF 150X90MM - J', CAST(N'2023-11-17T23:54:11.053' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3826', N'K-SCCO-200X100-11VJ-IN-MFZ0', N'STC CON INCOE MF 200X100MM - J', CAST(N'2023-11-17T23:54:11.137' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3827', N'K-TERM-0818TX3-PBSB-00-0000', N'TERM M-8 IDP-18 TX-3 PBSB', CAST(N'2023-11-17T23:54:11.223' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3828', N'K-HAND-365RO40-BLAC-01-0000', N'HANDLE ROPE 365 BLACK', CAST(N'2023-11-17T23:54:11.310' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3829', N'K-HAND-365RO40-ORAN-01-0000', N'HANDLE ROPE 365 ORANGE', CAST(N'2023-11-17T23:54:11.397' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'383', N'Z-TC02-544XXXX-HSFS-NL-0000', N'Fee TOP COVER 544 BLACK FS', CAST(N'2023-11-17T23:48:49.887' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3830', N'K-HAND-365RO40-YELL-01-0000', N'HANDLE ROPE 365 YELLOW', CAST(N'2023-11-17T23:54:11.477' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3831', N'K-HAND-450RO40-BLAC-00-0000', N'ROPE 450 BLACK', CAST(N'2023-11-17T23:54:11.560' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3832', N'K-HAND-450RO40-GREY-00-0000', N'ROPE 450 GREY', CAST(N'2023-11-17T23:54:11.647' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3833', N'K-HAND-E41DROP-BLAC-00-0000', N'HANDLE E41 DROP BLACK', CAST(N'2023-11-17T23:54:11.733' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3834', N'K-HAND-E41DROP-BLUE-00-0000', N'HANDLE E41 DROP BLUE', CAST(N'2023-11-17T23:54:11.817' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3835', N'K-HAND-S105XXX-BLAC-00-0000', N'HANDLE S-105 BLACK', CAST(N'2023-11-17T23:54:11.903' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3836', N'K-HAND-S105XXX-GREY-00-0000', N'HANDLE S-105 GREY', CAST(N'2023-11-17T23:54:11.993' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3837', N'K-MBKR-D26XXXX-RSC0-BL-00A0', N'M-BOX D26 BLANK', CAST(N'2023-11-17T23:54:12.080' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3838', N'K-MBKR-D31XXXX-RSC0-BL-00A0', N'M-BOX D31 BLANK', CAST(N'2023-11-17T23:54:12.170' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3839', N'K-MBKR-GTZ5SXX-RSC0-AP-KIA0', N'M-BOX GTZ-5S ASPIRA KIT', CAST(N'2023-11-17T23:54:12.250' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'384', N'Z-TC02-555XXXX-HSFS-NL-0000', N'Fee TOP COVER 555 BLACK FS', CAST(N'2023-11-17T23:48:49.967' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3840', N'K-MBKR-GTZ5SXX-RSC0-QU-FPA0', N'M-BOX GTZ-5S QUANTUM FED KIT', CAST(N'2023-11-17T23:54:12.330' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3841', N'K-MBKR-GTZ5SXX-RSC0-TR-KIA0', N'M-BOX GTZ-5S TRANSPORT KIT', CAST(N'2023-11-17T23:54:12.417' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3842', N'K-MBKR-GTZ6VXX-RSC0-AP-KIA0', N'M-BOX GTZ-6V ASPIRA KIT', CAST(N'2023-11-17T23:54:12.497' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3843', N'K-MBKR-GTZ6VXX-RSC0-TR-KIA0', N'M-BOX GTZ-6V TRANSPORT KIT', CAST(N'2023-11-17T23:54:12.580' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3844', N'K-TERM-6020TX1-PBSB-00-0000', N'TERM IDN-6 IDP-20 TX-1 PBSB', CAST(N'2023-11-17T23:54:12.667' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3845', N'K-TERM-6518TX1-PBSB-00-0000', N'TERM IDN-6,5 IDP-18 TX-1 PBSB', CAST(N'2023-11-17T23:54:12.757' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3846', N'K-TERM-7478TX2-PBSB-00-0000', N'TERM 7.4x7.8 MM TX-2 PBSB', CAST(N'2023-11-17T23:54:12.840' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3847', N'K-TERM-7516TX1-PBCA-00-0000', N'TERM IDN-7,5 IDP-16 TX-1 PBCA', CAST(N'2023-11-17T23:54:12.920' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3848', N'K-TERM-7615TX1-PBCA-00-0000', N'TERM IDN-7,6 IDP-15 TX-1 PBCA', CAST(N'2023-11-17T23:54:13.003' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3849', N'K-MBKR-SA9XXXX-CSSC-AM-00A0', N'M-BOX SA-9 ALL MAKES', CAST(N'2023-11-17T23:54:13.087' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'385', N'Z-TC02-566XXXX-HSFS-NL-0000', N'Fee TOP COVER 566 BLACK FS', CAST(N'2023-11-17T23:48:50.047' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3850', N'K-MBKR-SG100CT-HSC0-BL-00A0', N'M-BOX SG-100CT BLANK', CAST(N'2023-11-17T23:54:13.170' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3851', N'K-PALL-E1AXXXX-WOOD-HT-0000', N'PALLET E1A - HT', CAST(N'2023-11-17T23:54:13.250' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3852', N'K-PALL-E2AXXXX-WOOD-00-0000', N'PALLET E2A (DOMESTIC)', CAST(N'2023-11-17T23:54:13.330' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3853', N'K-PALL-E2AXXXX-WOOD-HT-0000', N'PALLET E2A - HT', CAST(N'2023-11-17T23:54:13.413' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3854', N'K-MBKR-B20XXXX-RSC0-BL-00A0', N'M-BOX B20 BLANK', CAST(N'2023-11-17T23:54:13.500' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3855', N'K-MBKR-B24XXXX-RSC0-BL-00A0', N'M-BOX B24 BLANK', CAST(N'2023-11-17T23:54:13.587' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3856', N'K-MBKR-D23XXXX-RSC0-BL-00A0', N'M-BOX D23 BLANK', CAST(N'2023-11-17T23:54:13.667' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3857', N'M-MOAL-CAXXXXX-0060-00-0700', N'MOAL CA 0,6% - 7 KG', CAST(N'2023-11-17T23:54:13.750' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3858', N'M-MOAL-CAXXXXX-0100-00-0700', N'MOAL CA 1% - 7 KG', CAST(N'2023-11-17T23:54:13.833' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3859', N'M-MOAL-CAXXXXX-0100-00-2600', N'MOAL CA 1% - 26 KG', CAST(N'2023-11-17T23:54:13.917' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'386', N'Z-TC02-588XXXX-HSFS-NL-0000', N'Fee TOP COVER 588 BLACK FS', CAST(N'2023-11-17T23:48:50.127' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3860', N'K-TERM-9018TX1-PBSB-00-0000', N'TERM IDN-9 IDP-18 TX-1 PBSB', CAST(N'2023-11-17T23:54:13.997' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3861', N'K-TERM-2055TX4-PBCA-00-0000', N'TERM 20x55 MM TX-4 PBCA', CAST(N'2023-11-17T23:54:14.080' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3862', N'K-TERM-0896TX5-PBCA-ZN-0000', N'TERM M-8 IDP-9,6 TX-5 PBSB-ZN', CAST(N'2023-11-17T23:54:14.160' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3863', N'K-TERM-2033TX4-PBCA-00-0000', N'TERM 20x33 MM TX-4 PBCA', CAST(N'2023-11-17T23:54:14.240' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3864', N'K-RUVA-1000750-60BL-50-EPDM', N'VALVE OD-10 ID-7,5 T-6 MM', CAST(N'2023-11-17T23:54:14.320' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3865', N'K-RUVA-1000760-60BL-50-EPDM', N'VALVE OD-10 ID-7,6 T-6 MM', CAST(N'2023-11-17T23:54:14.403' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3866', N'K-RUVA-1020780-60BL-50-EPDM', N'VALVE OD-10,2 ID-7,8 T-6 MM', CAST(N'2023-11-17T23:54:14.483' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3867', N'K-RUVA-1331120-76BL-50-EPDM', N'VALVE OD-13,3 ID-11,2 T-7,6 MM', CAST(N'2023-11-17T23:54:14.570' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3868', N'K-RUVA-1331150-80BL-50-EPDM', N'VALVE OD-13,3 ID-11,5 T-8 MM', CAST(N'2023-11-17T23:54:14.650' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3869', N'W-TERM-0896TX5-PBCA-ZN-0000', N'TERM M-8 IDP-9,6 TX-5 PBSB-ZN', CAST(N'2023-11-17T23:54:14.733' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'387', N'Z-TC02-A19XXXX-COFS-NL-0000', N'Fee TOP COVER A19 BLACK', CAST(N'2023-11-17T23:48:50.207' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3870', N'W-TERM-2033TX4-PBCA-00-0000', N'TERM 20x33 MM TX-4 PBCA', CAST(N'2023-11-17T23:54:14.817' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3871', N'K-TERM-4363TX6-CUZN-00-0000', N'TERM OD-4.3 L-6.3 TX-6 CUZN', CAST(N'2023-11-17T23:54:14.903' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3872', N'K-TERM-5264TX6-CUZN-00-0000', N'TERM OD-5.2 L-6.4 TX-6 CUZN', CAST(N'2023-11-17T23:54:14.990' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3873', N'K-TERM-7478TX2-PBSB-00-INA0', N'TERM 7.4x7.8 MM TX-2 PBSB-INA', CAST(N'2023-11-17T23:54:15.077' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3874', N'K-HAND-D23STRA-BLAC-00-0000', N'HANDLE D23 STRAP BLACK', CAST(N'2023-11-17T23:54:15.160' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3875', N'K-TERM-0817TX7-SS00-00-3040', N'TERM M-8 H-176 TX-7 SS', CAST(N'2023-11-17T23:54:15.243' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3876', N'K-HAND-450RO40-BLAC-01-0000', N'HANDLE ROPE 450 BLACK', CAST(N'2023-11-17T23:54:15.330' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'3877', N'W-TERE-7476TX2-PBSB-00-0000', N'TERMINAL RESIN 74x76 MM', CAST(N'2023-11-17T23:54:15.410' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'388', N'Z-TC02-B20XXXX-HSFS-NL-0000', N'Fee TOP COVER B20 BLACK FS', CAST(N'2023-11-17T23:48:50.283' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'389', N'Z-TC02-B24XXXX-HSFS-NL-0000', N'Fee TOP COVER B24 BLACK FS', CAST(N'2023-11-17T23:48:50.367' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'39', N'K-SCCV-252X050-11VE-IN-MFA0', N'STIC COVER INCOE MF 252X50 MM', CAST(N'2023-11-17T23:48:19.620' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'390', N'Z-TC02-D23XXXX-HSFS-NL-0000', N'Fee TOP COVER D23 BLACK FS', CAST(N'2023-11-17T23:48:50.447' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'391', N'Z-TC02-D26XXXX-HSFS-NL-0000', N'Fee TOP COVER D26 BLACK FS', CAST(N'2023-11-17T23:48:50.527' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'392', N'Z-TC02-D31XXXX-HSFS-NL-0000', N'Fee TOP COVER D31 BLACK FS', CAST(N'2023-11-17T23:48:50.607' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'393', N'W-TC02-B20XXXX-HSFS-NL-0000', N'TOP COVER B20 BLACK FS', CAST(N'2023-11-17T23:48:50.687' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'394', N'W-TC02-B24XXXX-HSFS-NL-0000', N'TOP COVER B24 BLACK FS', CAST(N'2023-11-17T23:48:50.767' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'395', N'W-TC02-D23XXXX-HSFS-NL-0000', N'TOP COVER D23 BLACK FS', CAST(N'2023-11-17T23:48:50.847' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'396', N'W-TC02-D26XXXX-HSFS-NL-0000', N'TOP COVER D26 BLACK FS', CAST(N'2023-11-17T23:48:50.927' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'397', N'W-TC02-D31XXXX-HSFS-NL-0000', N'TOP COVER D31 BLACK FS', CAST(N'2023-11-17T23:48:51.013' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'398', N'K-SCCV-605XXXX-11S0-BO-MFA0', N'STIC SCR 605 BOND MF', CAST(N'2023-11-17T23:48:51.093' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'399', N'K-SCCV-606XXXX-11S0-BO-MFA0', N'STIC SCR 606 BOND MF', CAST(N'2023-11-17T23:48:51.173' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'4', N'K-TC09-S65XXXX-GLFS-NL-0000', N'TOP COVER S65 GREY', CAST(N'2023-11-17T23:48:16.607' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'40', N'K-SCCV-252X050-11VE-OH-MFA0', N'STIC COVER OHAYO MF 252X50 MM', CAST(N'2023-11-17T23:48:19.703' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'400', N'K-SCCV-265X070-11VJ-IN-MFZ0', N'STIC CVR INCOE MF 265X70 MM', CAST(N'2023-11-17T23:48:51.253' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'401', N'K-SCCV-327X050-11VE-IN-MFZ0', N'STIC CVR INCOE MF 327X50 MM', CAST(N'2023-11-17T23:48:51.333' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'402', N'K-SCCV-345X050-11VJ-IN-MFZ0', N'STIC CVR INCOE MF 345X50 MM', CAST(N'2023-11-17T23:48:51.413' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'403', N'K-SCCV-GC2225X-11V0-BO-INZ0', N'STICKER GC2-225 BOND INDUSTRI', CAST(N'2023-11-17T23:48:51.493' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'404', N'W-CV01-115F51X-I02N-NL-DG00', N'CV 115F51 BLUE RH-BLACK', CAST(N'2023-11-17T23:48:51.573' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'405', N'W-CV01-115F51X-I03N-NL-DG00', N'CV 115F51 BLUE RH-ORANGE', CAST(N'2023-11-17T23:48:51.653' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'406', N'W-CV01-115F51X-I05N-NL-DG00', N'CV 115F51 BLUE RH-YELLOW', CAST(N'2023-11-17T23:48:51.730' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'407', N'W-CV01-130F51X-I03N-NL-DG00', N'CV 130F51 BLUE RH-ORANGE', CAST(N'2023-11-17T23:48:51.810' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'408', N'W-CV01-F51XXXX-I03N-NL-DQ00', N'CV F51 BLUE RH-ORANGE DG-QR', CAST(N'2023-11-17T23:48:51.890' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'409', N'W-CV02-4DLXXXX-0MHN-AM-00T0', N'CV 4D-L MF BLACK ALL MAKES', CAST(N'2023-11-17T23:48:51.970' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'41', N'K-SCCV-188X050-11VE-IN-MFA0', N'STIC COVER INCOE MF 188X50 MM', CAST(N'2023-11-17T23:48:19.787' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'410', N'W-CV02-F51LXXX-I03N-NL-DG00', N'CV F51L BLACK RH-ORANGE', CAST(N'2023-11-17T23:48:52.047' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'411', N'W-CV02-F51XXXX-I02N-NL-DG00', N'CV F51 BLACK RH-BLACK', CAST(N'2023-11-17T23:48:52.127' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'412', N'W-CV02-F51XXXX-I03N-NL-DG00', N'CV F51 BLACK RH-ORANGE', CAST(N'2023-11-17T23:48:52.207' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'413', N'W-CV02-F51XXXX-I05N-NL-DG00', N'CV F51 BLACK RH-YELLOW', CAST(N'2023-11-17T23:48:52.287' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'414', N'W-CV01-F51XXXX-I02N-NL-DG00', N'CV F51 BLUE RH-BLACK', CAST(N'2023-11-17T23:48:52.367' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'415', N'W-CV01-F51XXXX-I03N-NL-DG00', N'CV F51 BLUE RH-ORANGE', CAST(N'2023-11-17T23:48:52.447' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'416', N'W-CV01-F51XXXX-I05N-NL-DG00', N'CV F51 BLUE RH-YELLOW', CAST(N'2023-11-17T23:48:52.527' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'417', N'F-LEST-2450075-NE00-00-0000', N'LEAD STRIP 245 X 0,75 NEGATIVE', CAST(N'2023-11-17T23:48:52.607' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'418', N'F-LEST-2450100-PO00-00-0000', N'LEAD STRIP 245 X 1,00 POSITIVE', CAST(N'2023-11-17T23:48:52.687' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'419', N'K-CONN-C15G2LX-PBSB-00-0000', N'CONNECTOR C15-G2L', CAST(N'2023-11-17T23:48:52.767' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'42', N'K-SCCV-188X050-11VE-OH-MFA0', N'STIC COVER OHAYO MF 188X50 MM', CAST(N'2023-11-17T23:48:19.867' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'420', N'K-CONN-C15G2RX-PBSB-00-0000', N'CONNECTOR C15-G2R', CAST(N'2023-11-17T23:48:52.850' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'421', N'K-CONN-C15G2XX-PBSB-00-0000', N'CONNECTOR C15-G2', CAST(N'2023-11-17T23:48:52.930' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'422', N'K-CONN-C200LXX-PBCA-00-0000', N'CONNECTOR C200L', CAST(N'2023-11-17T23:48:53.007' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'423', N'K-CONN-C200NXX-PBCA-00-0000', N'CONNECTOR C200 NEG', CAST(N'2023-11-17T23:48:53.090' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'424', N'K-CONN-C200PXX-PBCA-00-0000', N'CONNECTOR C200 POS', CAST(N'2023-11-17T23:48:53.170' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'425', N'K-CONN-C200RXX-PBCA-00-0000', N'CONNECTOR C200R', CAST(N'2023-11-17T23:48:53.250' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'426', N'K-CONN-C45LXXX-PBCA-00-0000', N'CONNECTOR C45L', CAST(N'2023-11-17T23:48:53.330' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'427', N'K-CONN-C45RXXX-PBCA-00-0000', N'CONNECTOR C45R', CAST(N'2023-11-17T23:48:53.410' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'428', N'K-CONN-C4ALXXX-PBSB-00-0000', N'CONNECTOR C4-AL', CAST(N'2023-11-17T23:48:53.490' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'429', N'K-CONN-C4ARXXX-PBSB-00-0000', N'CONNECTOR C4-AR', CAST(N'2023-11-17T23:48:53.570' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'43', N'K-SCCV-188X050-15V0-IN-MFB0', N'STC CVR INCOE MF UAE 188X50 MM', CAST(N'2023-11-17T23:48:19.960' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'430', N'K-CONN-C5BLXXX-PBSB-00-0000', N'CONNECTOR C5-BL', CAST(N'2023-11-17T23:48:53.650' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'431', N'K-CONN-C5BRXXX-PBSB-00-0000', N'CONNECTOR C5-BR', CAST(N'2023-11-17T23:48:53.730' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'432', N'K-CONN-C5CLXXX-PBSB-00-0000', N'CONNECTOR C5-CL', CAST(N'2023-11-17T23:48:53.810' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'433', N'K-CONN-C5CRXXX-PBSB-00-0000', N'CONNECTOR C5-CR', CAST(N'2023-11-17T23:48:53.893' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'434', N'K-CONN-C5DLXXX-PBSB-00-0000', N'CONNECTOR C5-DL', CAST(N'2023-11-17T23:48:53.973' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'435', N'K-CONN-C5DRXXX-PBSB-00-0000', N'CONNECTOR C5-DR', CAST(N'2023-11-17T23:48:54.050' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'436', N'K-CONN-C65LXXX-PBCA-00-0000', N'CONNECTOR C65L', CAST(N'2023-11-17T23:48:54.133' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'437', N'K-CONN-C65RXXX-PBCA-00-0000', N'CONNECTOR C65R', CAST(N'2023-11-17T23:48:54.213' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'438', N'K-CONN-C8LXXXX-PBSB-00-0000', N'CONNECTOR C8L', CAST(N'2023-11-17T23:48:54.293' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'439', N'K-CONN-C8RXXXX-PBSB-00-0000', N'CONNECTOR C8R', CAST(N'2023-11-17T23:48:54.373' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'44', N'K-SCCV-190H52X-11V0-TN-HDA0', N'STIC CVR MF 190H52 TRAKNUS HD', CAST(N'2023-11-17T23:48:20.043' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'440', N'K-BUSH-B17A7XX-PBSB-00-0000', N'BUSHING B17-A7', CAST(N'2023-11-17T23:48:54.457' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'441', N'K-BUSH-B18A10X-PBSB-00-0000', N'BUSHING B18-A10', CAST(N'2023-11-17T23:48:54.537' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'442', N'K-BUSH-B71A24X-PBSB-00-0000', N'BUSHING B71-A24', CAST(N'2023-11-17T23:48:54.617' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'443', N'K-BUSH-B7A13SX-PBSB-00-0000', N'BUSHING B7-A13S', CAST(N'2023-11-17T23:48:54.700' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'444', N'K-BUSH-B7A13XX-PBSB-00-0000', N'BUSHING B7-A13', CAST(N'2023-11-17T23:48:54.780' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'445', N'K-BUSH-B7A13XX-PBSB-OE-0000', N'BUSHING B7-A13 OE', CAST(N'2023-11-17T23:48:54.857' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'446', N'K-BUSH-B7A14SX-PBSB-00-0000', N'BUSHING B7-A14S', CAST(N'2023-11-17T23:48:54.940' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'447', N'K-BUSH-B7A14XX-PBSB-00-0000', N'BUSHING B7-A14', CAST(N'2023-11-17T23:48:55.020' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'448', N'K-BUSH-B10A14X-PBSB-00-0000', N'BUSHING B10-A14', CAST(N'2023-11-17T23:48:55.120' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'449', N'K-BUSH-B10A15T-PBSB-00-0000', N'BUSHING B10-A15T', CAST(N'2023-11-17T23:48:55.217' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'45', N'K-SCCV-218X050-11V0-AP-MFA0', N'STIC CVR ASPIRA MF 218X50 MM', CAST(N'2023-11-17T23:48:20.127' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'450', N'K-BUSH-B7A14XX-PBSB-OE-0000', N'BUSHING B7-A14 OE', CAST(N'2023-11-17T23:48:55.303' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'451', N'K-CONN-C100LXX-PBCA-00-0000', N'CONNECTOR C100L', CAST(N'2023-11-17T23:48:55.400' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'452', N'K-CONN-C100RXX-PBCA-00-0000', N'CONNECTOR C100R', CAST(N'2023-11-17T23:48:55.493' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'453', N'K-CONN-C105LXX-PBCA-00-0000', N'CONNECTOR C105L', CAST(N'2023-11-17T23:48:55.590' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'454', N'K-CONN-C105NXX-PBCA-00-0000', N'CONNECTOR C105 NEG', CAST(N'2023-11-17T23:48:55.680' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'455', N'K-CONN-C105PXX-PBCA-00-0000', N'CONNECTOR C105 POS', CAST(N'2023-11-17T23:48:55.763' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'456', N'K-CONN-C105RXX-PBCA-00-0000', N'CONNECTOR C105R', CAST(N'2023-11-17T23:48:55.843' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'457', N'K-CONN-C1227LX-PBSB-00-0000', N'CONNECTOR C12-27L', CAST(N'2023-11-17T23:48:55.923' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'458', N'K-CONN-C1227RX-PBSB-00-0000', N'CONNECTOR C12-27R', CAST(N'2023-11-17T23:48:56.003' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'459', N'K-CONN-C12G2LX-PBSB-00-0000', N'CONNECTOR C12-G2L', CAST(N'2023-11-17T23:48:56.083' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'46', N'K-SCCV-345X050-15V0-IN-MFB0', N'STC CVR INCOE MF UAE 345X50 MM', CAST(N'2023-11-17T23:48:20.210' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'460', N'K-CONN-C12G2RX-PBSB-00-0000', N'CONNECTOR C12-G2R', CAST(N'2023-11-17T23:48:56.163' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'461', N'K-CONN-C12G2XX-PBSB-00-0000', N'CONNECTOR C12-G2', CAST(N'2023-11-17T23:48:56.243' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'462', N'K-CONN-C12G3LX-PBCA-00-0000', N'CONNECTOR C12-G3L', CAST(N'2023-11-17T23:48:56.320' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'463', N'K-CONN-C12G3RX-PBCA-00-0000', N'CONNECTOR C12-G3R', CAST(N'2023-11-17T23:48:56.400' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'464', N'K-CONN-C12G3XX-PBCA-00-0000', N'CONNECTOR C12-G3', CAST(N'2023-11-17T23:48:56.483' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'465', N'K-CONN-C15BLXX-PBSB-00-0000', N'CONNECTOR C15-BL', CAST(N'2023-11-17T23:48:56.603' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'466', N'K-CONN-C15BNXX-PBSB-00-0000', N'CONNECTOR C15-B NEG', CAST(N'2023-11-17T23:48:56.683' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'467', N'K-CONN-C15BPXX-PBSB-00-0000', N'CONNECTOR C15-B POS', CAST(N'2023-11-17T23:48:56.763' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'468', N'K-CONN-C15BRXX-PBSB-00-0000', N'CONNECTOR C15-BR', CAST(N'2023-11-17T23:48:56.843' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'469', N'K-CONN-C15CLXX-PBSB-00-0000', N'CONNECTOR C15-CL', CAST(N'2023-11-17T23:48:56.923' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'47', N'K-SCCV-48D26RX-11V0-TN-MFA0', N'STIC CVR MF 48D26R TRAKNUS', CAST(N'2023-11-17T23:48:20.293' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'470', N'K-CONN-C15CNXX-PBSB-00-0000', N'CONNECTOR C15-C NEG', CAST(N'2023-11-17T23:48:57.003' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'471', N'K-BUSH-B10A15X-PBSB-00-0000', N'BUSHING B10-A15', CAST(N'2023-11-17T23:48:57.083' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'472', N'K-BUSH-B10A16X-PBSB-00-0000', N'BUSHING B10-A16', CAST(N'2023-11-17T23:48:57.167' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'473', N'K-BUSH-B10A17X-PBSB-00-0000', N'BUSHING B10-A17', CAST(N'2023-11-17T23:48:57.247' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'474', N'K-BUSH-B10A18X-PBSB-00-0000', N'BUSHING B10-A18', CAST(N'2023-11-17T23:48:57.327' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'475', N'K-BUSH-B10A19T-PBSB-00-0000', N'BUSHING B10-A19T', CAST(N'2023-11-17T23:48:57.407' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'476', N'K-BUSH-B10A19X-PBSB-00-0000', N'BUSHING B10-A19', CAST(N'2023-11-17T23:48:57.487' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'477', N'K-BUSH-B12A19X-PBSB-00-0000', N'BUSHING B12-A19', CAST(N'2023-11-17T23:48:57.567' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'478', N'K-CONN-C15CPXX-PBSB-00-0000', N'CONNECTOR C15-C POS', CAST(N'2023-11-17T23:48:57.647' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'479', N'K-CONN-C15CRXX-PBSB-00-0000', N'CONNECTOR C15-CR', CAST(N'2023-11-17T23:48:57.727' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'48', N'K-SCCV-65D31RX-11V0-TN-HDA0', N'STIC CVR MF 65D31R TRAKNUS HD', CAST(N'2023-11-17T23:48:20.377' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'480', N'M-LEAL-PBSBXXX-0275-00-2600', N'PBSB 2,75% (CB3)', CAST(N'2023-11-17T23:48:57.803' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'481', N'M-LEAL-PBSBXXX-0275-00-26M0', N'PBSB 2,75% (CB3) MLR', CAST(N'2023-11-17T23:48:57.883' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'482', N'M-LEAL-PBSBXXX-0280-00-2600', N'PBSB 2,8% (HR)', CAST(N'2023-11-17T23:48:57.963' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'483', N'M-LEAL-PBSBXXX-0460-00-2600', N'PBSB 4,6%', CAST(N'2023-11-17T23:48:58.043' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'484', N'M-LEAL-PBSNXXX-0210-00-0700', N'PBSN 2,1% - 7 KG', CAST(N'2023-11-17T23:48:58.127' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'485', N'M-LEAL-PBSNXXX-0210-00-2600', N'PBSN 2,1%', CAST(N'2023-11-17T23:48:58.203' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'486', N'K-POLE-P0936XX-PBCA-00-0000', N'POLE P09-36', CAST(N'2023-11-17T23:48:58.287' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'487', N'K-POLE-P0968XX-PBSB-00-0000', N'POLE P09-68', CAST(N'2023-11-17T23:48:58.370' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'488', N'K-POLE-P0973XX-PBSB-00-0000', N'POLE P09-73', CAST(N'2023-11-17T23:48:58.450' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'489', N'K-POLE-P100XXX-PBCA-00-0000', N'POLE P100', CAST(N'2023-11-17T23:48:58.527' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'49', N'K-SCCV-75D31LX-11V0-TN-MFA0', N'STIC CVR MF 75D31L TRAKNUS', CAST(N'2023-11-17T23:48:20.460' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'490', N'K-POLE-P10556X-PBSB-00-0000', N'POLE P10,5-56', CAST(N'2023-11-17T23:48:58.607' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'491', N'K-POLE-P1057XX-PBSB-00-0000', N'POLE P10-57', CAST(N'2023-11-17T23:48:58.687' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'492', N'K-POLE-P1059XX-PBSB-00-0000', N'POLE P10-59', CAST(N'2023-11-17T23:48:58.763' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'493', N'K-LESC-PBCASNX-0007-10-0500', N'L-STICK PBCA 0,07% SN 1% (CH2)', CAST(N'2023-11-17T23:48:58.847' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'494', N'M-PULE-9997XXX-0000-00-2600', N'PURE LEAD 99,97%', CAST(N'2023-11-17T23:48:58.923' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'495', N'M-PULE-9999XXX-0000-00-2600', N'PURE LEAD 99,99%', CAST(N'2023-11-17T23:48:59.000' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'496', N'M-LEAL-PBAGSNX-0300-00-2600', N'PBAG - SN 3%', CAST(N'2023-11-17T23:48:59.083' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'497', N'M-LEAL-PBCASNX-0007-03-2600', N'PBCA 0,07% SN 0,3% (CH4)', CAST(N'2023-11-17T23:48:59.167' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'498', N'M-LEAL-PBCASNX-0007-03-26M0', N'PBCA 0,07% SN 0,3% (CH4) MLR', CAST(N'2023-11-17T23:48:59.247' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'499', N'M-LEAL-PBCASNX-0007-10-2600', N'PBCA 0,07% SN 1% (CH2)', CAST(N'2023-11-17T23:48:59.327' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'5', N'K-TC10-S100XXX-GLFS-NL-0000', N'TOP COVER S100 BLACK', CAST(N'2023-11-17T23:48:16.703' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'50', N'K-SCCV-95E41RX-11V0-TN-MFA0', N'STIC CVR MF 95E41R TRAKNUS', CAST(N'2023-11-17T23:48:20.547' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'500', N'M-LEAL-PBCASNX-0007-10-26M0', N'PBCA 0,07% SN 1% (CH2) MLR', CAST(N'2023-11-17T23:48:59.407' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'501', N'M-LEAL-PBCASNX-0008-14-2600', N'PBCA 0,08% SN 1,4% (CH5)', CAST(N'2023-11-17T23:48:59.527' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'502', N'M-LEAL-PBCASNX-0008-14-26M0', N'PBCA 0,08% SN 1,4% (CH5) MLR', CAST(N'2023-11-17T23:48:59.607' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'503', N'M-LEAL-PBCAXXX-0100-00-0700', N'PBCA 1% (CA1)', CAST(N'2023-11-17T23:48:59.687' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'504', N'M-LEAL-PBCAXXX-0100-00-07M0', N'PBCA 1% (CA1) MLR', CAST(N'2023-11-17T23:48:59.767' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'505', N'M-LEAL-PBSBXXX-0170-00-2600', N'PBSB 1,7%', CAST(N'2023-11-17T23:48:59.893' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'506', N'M-LEAL-PBSBXXX-0170-00-26M0', N'PBSB 1,7% MLR', CAST(N'2023-11-17T23:48:59.980' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'507', N'K-POLE-P1064XX-PBSB-00-0000', N'POLE P10-64', CAST(N'2023-11-17T23:49:00.067' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'508', N'K-POLE-P1090XX-PBSB-00-0000', N'POLE P10-90', CAST(N'2023-11-17T23:49:00.147' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'509', N'K-POLE-P10TXXX-PBSB-00-0000', N'POLE P10-T', CAST(N'2023-11-17T23:49:00.230' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'51', N'K-SCCV-218X050-11V0-BL-MFA0', N'STC CVR NO BRAND MF 218X50 MM', CAST(N'2023-11-17T23:48:20.630' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'510', N'K-POLE-P12G2XX-PBSB-00-0000', N'POLE P12-G2', CAST(N'2023-11-17T23:49:00.330' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'511', N'K-POLE-P14569X-PBSB-00-0000', N'POLE P14,5-69', CAST(N'2023-11-17T23:49:00.410' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'512', N'K-POLE-P145975-PBSB-00-0000', N'POLE P14,5-97,5', CAST(N'2023-11-17T23:49:00.490' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'513', N'K-POLE-P1683XX-PBSB-00-0000', N'POLE P16-83', CAST(N'2023-11-17T23:49:00.573' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'514', N'K-POLE-P18865X-PBSB-00-0000', N'POLE P18-86,5', CAST(N'2023-11-17T23:49:00.650' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'515', N'K-POLE-P1896XX-PBSB-00-0000', N'POLE P18-96,5', CAST(N'2023-11-17T23:49:00.730' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'516', N'K-POLE-P45XXXX-PBCA-00-0000', N'POLE P45', CAST(N'2023-11-17T23:49:00.813' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'517', N'K-POLE-P6560XX-PBSB-00-0000', N'POLE P65-60', CAST(N'2023-11-17T23:49:00.897' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'518', N'K-POLE-P65XXXX-PBCA-00-0000', N'POLE P65', CAST(N'2023-11-17T23:49:00.983' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'519', N'K-POLE-P7IXXXX-PBSB-00-0000', N'POLE P7-I', CAST(N'2023-11-17T23:49:01.063' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'52', N'K-SCCV-218X050-11V0-IN-MFD0', N'STC CVR INCOE MF KSA 218X50 MM', CAST(N'2023-11-17T23:48:20.760' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'520', N'K-POLE-P7T935X-PBSB-00-0000', N'POLE P7-T 93,5', CAST(N'2023-11-17T23:49:01.143' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'521', N'K-POLE-P7TXXXX-PBSB-00-0000', N'POLE P7-T', CAST(N'2023-11-17T23:49:01.223' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'522', N'K-POLE-P8G2XXX-PBSB-00-0000', N'POLE P8-G2', CAST(N'2023-11-17T23:49:01.303' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'523', N'K-POLE-P8XXXXX-PBSB-00-0000', N'POLE P8', CAST(N'2023-11-17T23:49:01.387' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'524', N'K-POLE-P9582XX-PBSB-00-0000', N'POLE P95-82', CAST(N'2023-11-17T23:49:01.470' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'525', N'W-LEPO-9997XXX-0000-00-0000', N'LEAD POWDER 99,97%', CAST(N'2023-11-17T23:49:01.550' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'526', N'W-LEPO-9999XXX-0000-00-0000', N'LEAD POWDER 99,99%', CAST(N'2023-11-17T23:49:01.630' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'527', N'W-LESC-PBSBXXX-0275-00-0700', N'LEAD STICK PBSB 2,75% (CB3)', CAST(N'2023-11-17T23:49:01.710' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'528', N'W-LESR-B20XXXX-10SC-SB-0280', N'L-STRAP B20 10P SL COS (HR)', CAST(N'2023-11-17T23:49:01.793' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'529', N'W-LESR-D26XXXX-000C-SB-0275', N'L-STRAP D26 COS (CB3)', CAST(N'2023-11-17T23:49:01.870' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'53', N'K-SCCV-218X050-11V0-LU-PRA0', N'STC COVER LUCAS PRE 218X50 MM', CAST(N'2023-11-17T23:48:20.843' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'530', N'W-LESR-D26XXXX-11SC-SB-0275', N'L-STRAP D26 11P CL COS (CB3)', CAST(N'2023-11-17T23:49:01.953' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'531', N'W-LESR-D31XXXX-13SC-SB-0275', N'L-STRAP D31 13P SL COS (CB3)', CAST(N'2023-11-17T23:49:02.037' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'532', N'W-LESR-TERMINA-000S-SB-0275', N'L-STRAP TERMINAL BIG TYPE(CB3)', CAST(N'2023-11-17T23:49:02.117' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'533', N'W-LESR-A19MFXX-00SS-SB-0275', N'L-STRAP A19 MF SL SAW (CB3)', CAST(N'2023-11-17T23:49:02.197' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'534', N'W-LESR-B20XXXX-00SC-SB-0275', N'L-STRAP B20 SL COS (CB3)', CAST(N'2023-11-17T23:49:02.280' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'535', N'W-LESR-B20XXXX-00SC-SB-0280', N'L-STRAP B20 SL COS (HR)', CAST(N'2023-11-17T23:49:02.360' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'536', N'W-LESR-B24XXXX-00SC-SB-0275', N'L-STRAP B24 SL COS (CB3)', CAST(N'2023-11-17T23:49:02.440' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'537', N'W-LESR-B24XXXX-00SC-SB-0280', N'L-STRAP B24 SL COS (HR)', CAST(N'2023-11-17T23:49:02.520' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'538', N'W-LESR-D23MFXX-00CC-SB-0275', N'L-STRAP D23 MF CL COS (CB3)', CAST(N'2023-11-17T23:49:02.600' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'539', N'W-LESR-D23XXXX-00SS-SB-0275', N'L-STRAP D23 SL SAW (CB3)', CAST(N'2023-11-17T23:49:02.680' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'54', N'K-SCCV-218X050-11V0-OH-MFB0', N'STC CVR OHAYO MF UAE 218X50 MM', CAST(N'2023-11-17T23:48:20.927' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'540', N'W-LESR-D26MFXX-00CC-SB-0275', N'L-STRAP D26 MF CL COS (CB3)', CAST(N'2023-11-17T23:49:02.760' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'541', N'W-LESR-D26MFXX-00SS-SB-0275', N'L-STRAP D26 MF SL SAW (CB3)', CAST(N'2023-11-17T23:49:02.840' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'542', N'W-LESR-D26XXXX-00SS-SB-0275', N'L-STRAP D26 SL SAW (CB3)', CAST(N'2023-11-17T23:49:02.920' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'543', N'W-LESR-D31MFXX-00CC-SB-0275', N'L-STRAP D31 MF CL COS (CB3)', CAST(N'2023-11-17T23:49:02.997' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'544', N'W-LESR-D31MFXX-00SS-SB-0275', N'L-STRAP D31 MF SL SAW (CB3)', CAST(N'2023-11-17T23:49:03.077' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'545', N'W-LESR-D31XXXX-00SC-SB-0275', N'L-STRAP D31 SL COS (CB3)', CAST(N'2023-11-17T23:49:03.157' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'546', N'W-LESR-D31XXXX-00SS-SB-0275', N'L-STRAP D31 SL SAW (CB3)', CAST(N'2023-11-17T23:49:03.237' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'547', N'W-LESR-DINBXXX-00SS-SB-0275', N'L-STRAP DIN-B SL SAW (CB3)', CAST(N'2023-11-17T23:49:03.810' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'548', N'W-LESR-DINCXXX-00SS-SB-0275', N'L-STRAP DIN-C SL SAW (CB3)', CAST(N'2023-11-17T23:49:04.863' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'549', N'W-LESR-E41MFXX-00SC-SB-0275', N'L-STRAP E41 MF SL COS (CB3)', CAST(N'2023-11-17T23:49:06.013' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'55', N'K-SCCV-218X050-11V0-QU-MFA0', N'STC CVR QUANTUM MF 218X50 MM', CAST(N'2023-11-17T23:48:21.010' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'550', N'W-LESR-E41MFXX-00SS-SB-0275', N'L-STRAP E41 MF SL SAW (CB3)', CAST(N'2023-11-17T23:49:07.237' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'551', N'W-LESR-E41XXXX-00SC-SB-0275', N'L-STRAP E41 SL COS (CB3)', CAST(N'2023-11-17T23:49:08.230' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'552', N'W-LESR-E41XXXX-00SS-SB-0275', N'L-STRAP E41 SL SAW (CB3)', CAST(N'2023-11-17T23:49:08.560' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'553', N'W-LESR-F51MFXX-00SC-SB-0275', N'L-STRAP F51 MF SL COS (CB3)', CAST(N'2023-11-17T23:49:08.647' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'554', N'W-LESR-F51MFXX-00SS-SB-0275', N'L-STRAP F51 MF SL SAW (CB3)', CAST(N'2023-11-17T23:49:08.730' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'555', N'W-LESR-F51XXXX-00SC-SB-0275', N'L-STRAP F51 SL COS (CB3)', CAST(N'2023-11-17T23:49:08.813' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'556', N'W-CONN-C12G3LX-PBCA-00-000A', N'CONNECTOR C12-G3L ANNEALING', CAST(N'2023-11-17T23:49:08.890' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'557', N'W-CONN-C12G3RX-PBCA-00-000A', N'CONNECTOR C12-G3R ANNEALING', CAST(N'2023-11-17T23:49:08.973' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'558', N'W-CONN-C12G3XX-PBCA-00-000A', N'CONNECTOR C12-G3 ANNEALING', CAST(N'2023-11-17T23:49:09.053' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'559', N'W-CONN-C5ELXXX-PBSB-00-0000', N'CONNECTOR C5-EL', CAST(N'2023-11-17T23:49:09.147' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'56', N'K-SCCV-218X050-11VE-AS-MFA0', N'STIC CVR ASAHI MF 218X50 MM', CAST(N'2023-11-17T23:48:21.093' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'560', N'W-CONN-C5ERXXX-PBSB-00-0000', N'CONNECTOR C5-ER', CAST(N'2023-11-17T23:49:09.230' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'561', N'W-CONN-C5FLXXX-PBSB-00-0000', N'CONNECTOR C5-FL', CAST(N'2023-11-17T23:49:09.313' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'562', N'W-CONN-C5FRXXX-PBSB-00-0000', N'CONNECTOR C5-FR', CAST(N'2023-11-17T23:49:09.393' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'563', N'W-LESR-588XXXX-00SS-SB-0275', N'L-STRAP 588 SL SAW (CB3)', CAST(N'2023-11-17T23:49:09.473' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'564', N'M-LEAL-PBSBXXX-0170-00-2601', N'PBSB 1,7% IMPURITIES-N (CC1.7)', CAST(N'2023-11-17T23:49:09.557' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'565', N'M-LEAL-PBSBXXX-0275-00-2601', N'PBSB 2,75% Sn 0,12% (HR2)', CAST(N'2023-11-17T23:49:09.640' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'566', N'W-LESR-544XXXX-00SS-SB-0275', N'L-STRAP 544 SL SAW (CB3)', CAST(N'2023-11-17T23:49:09.727' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'567', N'W-POLE-P4MXXXX-PBSB-00-0000', N'POLE P4M', CAST(N'2023-11-17T23:49:09.810' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'568', N'W-LESR-G51MFXX-00SC-SB-0275', N'L-STRAP G51 MF SL COS (CB3)', CAST(N'2023-11-17T23:49:09.890' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'569', N'W-LESR-G51MFXX-00SS-SB-0275', N'L-STRAP G51 MF SL SAW (CB3)', CAST(N'2023-11-17T23:49:09.973' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'57', N'K-SCCV-218X050-11VE-IN-MFA0', N'STIC COVER INCOE MF 218X50 MM', CAST(N'2023-11-17T23:48:21.173' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'570', N'W-LESR-G51XXXX-00SC-SB-0275', N'L-STRAP G51 SL COS (CB3)', CAST(N'2023-11-17T23:49:10.053' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'571', N'W-LESR-G51XXXX-00SS-SB-0275', N'L-STRAP G51 SL SAW (CB3)', CAST(N'2023-11-17T23:49:10.133' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'572', N'W-LESR-GC6VXXX-00SS-SB-0275', N'L-STRAP GC 6V SL SAW (CB3)', CAST(N'2023-11-17T23:49:10.217' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'573', N'W-LESR-GC8VXXX-00SS-SB-0275', N'L-STRAP GC 8V SL SAW (CB3)', CAST(N'2023-11-17T23:49:10.297' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'574', N'W-LESR-545XXXX-00SS-SB-0275', N'L-STRAP 545 SL SAW (CB3)', CAST(N'2023-11-17T23:49:10.377' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'575', N'W-LESR-555XXXX-00SS-SB-0275', N'L-STRAP 555 SL SAW (CB3)', CAST(N'2023-11-17T23:49:10.457' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'576', N'W-LESR-566XXXX-00SS-SB-0275', N'L-STRAP 566 SL SAW (CB3)', CAST(N'2023-11-17T23:49:10.540' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'577', N'K-POLE-P4MXXXX-PBSB-00-0000', N'POLE P4M', CAST(N'2023-11-17T23:49:10.620' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'578', N'K-CONN-C4BLXXX-PBSB-00-0000', N'CONNECTOR C4BL', CAST(N'2023-11-17T23:49:10.703' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'579', N'K-CONN-C4BRXXX-PBSB-00-0000', N'CONNECTOR C4BR', CAST(N'2023-11-17T23:49:10.790' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'58', N'K-SCCV-218X050-11VE-OH-MFA0', N'STIC COVER OHAYO MF 218X50 MM', CAST(N'2023-11-17T23:48:21.260' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'580', N'W-CONN-C1227LX-PBSB-00-000A', N'CONNECTOR C12-27L ANNEALING', CAST(N'2023-11-17T23:49:10.867' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'581', N'W-CONN-C1227RX-PBSB-00-000A', N'CONNECTOR C12-27R ANNEALING', CAST(N'2023-11-17T23:49:10.950' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'582', N'K-CONN-C12G4LX-PBCA-00-0000', N'CONNECTOR C12-G4L', CAST(N'2023-11-17T23:49:11.030' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'583', N'K-CONN-C12G4RX-PBCA-00-0000', N'CONNECTOR C12-G4R', CAST(N'2023-11-17T23:49:11.117' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'584', N'K-CONN-C12G4XX-PBCA-00-0000', N'CONNECTOR C12-G4', CAST(N'2023-11-17T23:49:11.193' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'585', N'W-LEAL-PBSBXXX-0170-00-26R0', N'PBSB 1,7% RECYCLE SCRAP NFU', CAST(N'2023-11-17T23:49:11.280' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'586', N'W-LESR-H52MFXX-00CC-SB-0275', N'L-STRAP H52 MF CL COS (CB3)', CAST(N'2023-11-17T23:49:11.360' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'587', N'W-LESR-H52MFXX-00SS-SB-0275', N'L-STRAP H52 MF SL SAW (CB3)', CAST(N'2023-11-17T23:49:11.440' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'588', N'W-LESR-H52XXXX-00SS-SB-0275', N'L-STRAP H52 SL SAW (CB3)', CAST(N'2023-11-17T23:49:11.520' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'589', N'W-LESR-N03XXXX-00SS-SB-0275', N'L-STRAP N03 SL SAW (CB3)', CAST(N'2023-11-17T23:49:11.603' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'59', N'K-SCCV-218X050-15V0-IN-MFB0', N'STC CVR INCOE MF UAE 218X50 MM', CAST(N'2023-11-17T23:48:21.343' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'590', N'W-LESR-N12DXXX-00SS-SB-0275', N'L-STRAP N12D SL SAW (CB3)', CAST(N'2023-11-17T23:49:11.683' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'591', N'W-LESR-NS50MFX-00SS-SB-0275', N'L-STRAP NS50 MF SL SAW (CB3)', CAST(N'2023-11-17T23:49:11.767' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'592', N'W-LESR-S100CTX-00SS-CA-0007', N'L-STRAP S-100CT SL SAW (CH2)', CAST(N'2023-11-17T23:49:11.850' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'593', N'W-LESR-S100XXX-00SS-CA-0007', N'L-STRAP S-100 SL SAW (CH2)', CAST(N'2023-11-17T23:49:11.930' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'594', N'W-LESR-S105XXX-00SS-CA-0007', N'L-STRAP S-105 SL SAW (CH2)', CAST(N'2023-11-17T23:49:12.010' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'595', N'W-LESR-S12XXXX-00SC-CA-0100', N'L-STRAP S-12 SL COS (CA1)', CAST(N'2023-11-17T23:49:12.100' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'596', N'W-LESR-S200XXX-00SS-CA-0007', N'L-STRAP S-200 SL SAW (CH2)', CAST(N'2023-11-17T23:49:12.183' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'597', N'W-LESR-S45XXXX-00SS-CA-0007', N'L-STRAP S-45 SL SAW (CH2)', CAST(N'2023-11-17T23:49:12.267' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'598', N'W-LESR-S65XXXX-00SS-CA-0007', N'L-STRAP S-65 SL SAW (CH2)', CAST(N'2023-11-17T23:49:12.347' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'599', N'W-LESR-S9XXXXX-00SC-CA-0100', N'L-STRAP S-9 SL COS (CA1)', CAST(N'2023-11-17T23:49:12.427' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'6', N'K-TC10-S105XXX-GLFS-NL-0000', N'TOP COVER S105 BLACK', CAST(N'2023-11-17T23:48:16.790' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'60', N'K-SCCV-220X045-11V0-AP-EFA0', N'STIC CVR ASPIRA EFB 220X45 MM', CAST(N'2023-11-17T23:48:21.427' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'600', N'W-LESR-TZ5SXXX-00SC-CA-0100', N'L-STRAP TZ-5S SL COS (CA1)', CAST(N'2023-11-17T23:49:12.507' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'601', N'W-LESR-F51XXXX-00SS-SB-0275', N'L-STRAP F51 SL SAW (CB3)', CAST(N'2023-11-17T23:49:12.590' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'602', N'K-CT00-588XXXX-0532-BA-0000', N'CONTAINER 588 LC-532 NATURAL', CAST(N'2023-11-17T23:49:12.670' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'603', N'K-CT00-DINBXXX-0973-NH-0000', N'CONTAINER DIN-B LC-973 NATURAL', CAST(N'2023-11-17T23:49:12.753' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'604', N'K-CT00-DINCXXX-1235-NH-0000', N'CONTAINER DIN-C LC-1235 NAT', CAST(N'2023-11-17T23:49:12.833' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'605', N'K-CT02-555LHXX-0347-BA-0000', N'CONTAINER 555LH LC-347 BLACK', CAST(N'2023-11-17T23:49:12.913' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'606', N'K-CT02-566LHXX-0406-BA-0000', N'CONTAINER 566LH LC-406 BLACK', CAST(N'2023-11-17T23:49:12.993' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'607', N'K-CT02-588LHXX-0522-BA-0000', N'CONTAINER 588LH LC-522 BLACK', CAST(N'2023-11-17T23:49:13.073' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'608', N'K-CT02-DINBXXX-0973-NH-0000', N'CONTAINER DIN-B LC-973 BLACK', CAST(N'2023-11-17T23:49:13.150' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'609', N'K-CT02-DINCXXX-1235-NH-0000', N'CONTAINER DIN-C LC-1235 BLACK', CAST(N'2023-11-17T23:49:13.233' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'61', N'K-SCCV-220X045-11V0-AP-MFA0', N'STIC CVR ASPIRA MF 220X45 MM', CAST(N'2023-11-17T23:48:21.510' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'610', N'K-CT02-GC6VXXX-0795-NH-0000', N'CONTAINER GC-6V LC-795 BLACK', CAST(N'2023-11-17T23:49:13.320' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'611', N'K-CT02-GC8VXXX-0590-NH-0000', N'CONTAINER GC-8V LC-590 BLACK', CAST(N'2023-11-17T23:49:13.400' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'612', N'K-CT09-S100XXX-0503-NH-0000', N'CONTAINER S100 LC-503 GREY', CAST(N'2023-11-17T23:49:13.480' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'613', N'K-CT09-S105XXX-0467-NH-0000', N'CONTAINER S105 LC-467 BLACK', CAST(N'2023-11-17T23:49:13.560' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'614', N'K-CT09-S200XXX-1561-NH-0000', N'CONTAINER S200 LC-1561 GREY', CAST(N'2023-11-17T23:49:13.640' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'615', N'K-CT09-S45XXXX-0292-NH-0000', N'CONTAINER S45 LC-292 GREY', CAST(N'2023-11-17T23:49:13.720' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'616', N'K-CT09-S65XXXX-0520-NH-0000', N'CONTAINER S65 LC-520 GREY', CAST(N'2023-11-17T23:49:13.800' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'617', N'K-CT10-S100XXX-0503-NH-0000', N'CONTAINER S100 LC-503 BLACK', CAST(N'2023-11-17T23:49:13.880' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'618', N'K-CT10-S105XXX-0467-NH-0000', N'CONTAINER S105 LC-467 BLACK', CAST(N'2023-11-17T23:49:13.963' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'619', N'K-CT10-S12XXXX-0474-NH-0000', N'CONTAINER S12 LC-424 BLACK', CAST(N'2023-11-17T23:49:14.040' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'62', N'K-SCCV-220X045-11V0-BL-MFA0', N'STC CVR NO BRAND MF 220X45 MM', CAST(N'2023-11-17T23:48:21.597' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'620', N'K-CT10-S65XXXX-0520-NH-0000', N'CONTAINER S65 LC-520 BLACK', CAST(N'2023-11-17T23:49:14.123' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'621', N'K-CT10-S9XXXXX-0470-NH-0000', N'CONTAINER S9 LC-470 BLACK', CAST(N'2023-11-17T23:49:14.207' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'622', N'W-CP02-555XXXX-0344-AP-MFA0', N'CP 555 LC-344 BLA ASPIRA MF', CAST(N'2023-11-17T23:49:14.287' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'623', N'W-CP02-555XXXX-0344-AS-MEA0', N'CP 555 LC-344 BLA ASAHI MEN MF', CAST(N'2023-11-17T23:49:14.363' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'624', N'W-CP02-555XXXX-0344-IN-MFA0', N'CP 555 LC-344 BLA INCOE MF', CAST(N'2023-11-17T23:49:14.443' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'625', N'W-CP02-555XXXX-0344-IN-MFB0', N'CP 555 LC-344 BLA INCOE MF UAE', CAST(N'2023-11-17T23:49:14.527' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'626', N'W-CP02-555XXXX-0344-IN-MFC0', N'CP 555 LC-344 BLA INCOE MF AOP', CAST(N'2023-11-17T23:49:14.607' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'627', N'W-CP02-555XXXX-0344-IN-MFD0', N'CP 555 LC-344 BLA INCOE MF KSA', CAST(N'2023-11-17T23:49:14.687' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'628', N'W-CP02-566XXXX-0403-AP-MFA0', N'CP 566 LC-403 BLA ASPIRA MF', CAST(N'2023-11-17T23:49:14.767' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'629', N'W-CP02-566XXXX-0403-AS-MEA0', N'CP 566 LC-403 BLA ASAHI MEN MF', CAST(N'2023-11-17T23:49:14.860' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'63', N'K-SCCV-220X045-11V0-IN-EFA0', N'STIC COVER INCOE EFB 220X45 MM', CAST(N'2023-11-17T23:48:21.683' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'630', N'W-CP02-566XXXX-0403-IN-MFA0', N'CP 566 LC-403 BLA INCOE MF', CAST(N'2023-11-17T23:49:14.943' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'631', N'W-CP02-566XXXX-0403-IN-MFB0', N'CP 566 LC-403 BLA INCOE MF UAE', CAST(N'2023-11-17T23:49:15.023' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'632', N'W-CP02-566XXXX-0403-IN-MFC0', N'CP 566 LC-403 BLA INCOE MF AOP', CAST(N'2023-11-17T23:49:15.100' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'633', N'W-CP02-566XXXX-0403-IN-MFD0', N'CP 566 LC-403 BLA INCOE MF KSA', CAST(N'2023-11-17T23:49:15.180' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'634', N'W-CP02-588XXXX-0529-AP-MFA0', N'CP 588 LC-529 BLA ASPIRA MF', CAST(N'2023-11-17T23:49:15.260' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'635', N'W-CP02-588XXXX-0529-AS-MEA0', N'CP 588 LC-529 BLA ASAHI MEN MF', CAST(N'2023-11-17T23:49:15.340' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'636', N'W-CP02-588XXXX-0529-IN-MFA0', N'CP 588 LC-529 BLA INCOE MF', CAST(N'2023-11-17T23:49:15.423' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'637', N'W-CP02-588XXXX-0529-IN-MFB0', N'CP 588 LC-529 BLA INCOE MF UAE', CAST(N'2023-11-17T23:49:15.503' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'638', N'W-CP02-588XXXX-0529-IN-MFC0', N'CP 588 LC-529 BLA INCOE MF AOP', CAST(N'2023-11-17T23:49:15.583' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'639', N'W-CP02-65D31RX-0329-AM-PRA0', N'CP 65D31R LC-329 BLA AM PRE MF', CAST(N'2023-11-17T23:49:15.663' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'64', N'K-SCCV-220X045-11V0-LU-EFA0', N'STC COVER LUCAS EFB 220X45 MM', CAST(N'2023-11-17T23:48:21.763' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'640', N'W-CP02-65D31RX-0386-TN-HDA0', N'CP 65D31R LC386 BLA TRAKNUS HD', CAST(N'2023-11-17T23:49:15.747' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'641', N'W-CP02-75D31LX-0329-AM-PRA0', N'CP 75D31L LC-329 BLA AM PRE MF', CAST(N'2023-11-17T23:49:15.823' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'642', N'W-CP02-75D31RX-0329-AM-PRA0', N'CP 75D31R LC-329 BLA AM PRE MF', CAST(N'2023-11-17T23:49:15.907' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'643', N'W-CP02-75D31RX-0329-TN-MFA0', N'CP 75D31R LC329 BLA TRAKNUS MF', CAST(N'2023-11-17T23:49:15.987' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'644', N'W-CP02-80D26RX-0329-AM-PRA0', N'CP 80D26R LC-329 BLA AM PRE MF', CAST(N'2023-11-17T23:49:16.077' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'645', N'W-CP02-95D31RX-0433-AM-PRA0', N'CP 95D31R LC-433 BLA AM PRE MF', CAST(N'2023-11-17T23:49:16.157' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'646', N'W-CP02-95E41RX-0563-AM-PRA0', N'CP 95E41R LC-563 BLA AM PRE MF', CAST(N'2023-11-17T23:49:16.233' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'647', N'W-CP02-95E41RX-0563-TN-MFA0', N'CP 95E41R LC563 BLA TRAKNUS MF', CAST(N'2023-11-17T23:49:16.317' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'648', N'W-CP02-A19XXXX-0242-IN-MFA0', N'CP A19 LC-242 BLA INCOE MF', CAST(N'2023-11-17T23:49:16.397' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'649', N'W-CP02-A19XXXX-0242-IN-MFB0', N'CP A19 LC-242 BLA INCOE MF UAE', CAST(N'2023-11-17T23:49:16.480' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'65', N'K-SCCV-220X045-11V0-LU-PRA0', N'STC COVER LUCAS PRE 220X45 MM', CAST(N'2023-11-17T23:48:21.850' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'650', N'W-CP00-D26XXXX-0313-IN-XPB0', N'CP D26 LC-313 NAT INCOE XP SUD', CAST(N'2023-11-17T23:49:16.567' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'651', N'W-CP00-D26XXXX-0313-IN-XPC0', N'CP D26 LC-313 NAT INCOE XP UAE', CAST(N'2023-11-17T23:49:16.660' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'652', N'W-CP00-D26XXXX-0313-IN-XPD0', N'CP D26 LC-313 NAT INCOE XP MLY', CAST(N'2023-11-17T23:49:16.753' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'653', N'W-CP00-D26XXXX-0313-OH-HDB0', N'CP D26 LC-313 NAT OHAYO HD MII', CAST(N'2023-11-17T23:49:16.850' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'654', N'W-CP00-D26XXXX-0313-QU-00A0', N'CP D26 LC-313 NAT QUANTUM', CAST(N'2023-11-17T23:49:16.940' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'655', N'W-CP00-D26XXXX-0313-QU-00B0', N'CP D26 LC-313 NAT QUANTUM MII', CAST(N'2023-11-17T23:49:17.037' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'656', N'W-CP00-D26XXXX-0313-RA-00A0', N'CP D26 LC-313 NAT RAPTOR', CAST(N'2023-11-17T23:49:17.130' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'657', N'W-CP00-D26XXXX-0313-UL-00A0', N'CP D26 LC-313 NAT ULTRA-X', CAST(N'2023-11-17T23:49:17.223' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'658', N'W-CP00-D26XXXX-0313-UL-00B0', N'CP D26 LC-313 NAT ULTRA-X MII', CAST(N'2023-11-17T23:49:17.303' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'659', N'W-CP00-D26XXXX-0375-AP-PRB0', N'CP D26 LC375 NAT ASPIRA PRE AO', CAST(N'2023-11-17T23:49:17.383' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'66', N'K-SCCV-220X045-11V0-OH-MFB0', N'STC CVR OHAYO MF UAE 220X45 MM', CAST(N'2023-11-17T23:48:21.933' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'660', N'W-CP00-D26XXXX-0375-AS-HPA0', N'CP D26 LC-375 NAT ASAHI H-PERF', CAST(N'2023-11-17T23:49:17.463' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'661', N'W-CP00-D26XXXX-0375-AS-HPB0', N'CP D26 LC-375 NAT ASAHI HP MII', CAST(N'2023-11-17T23:49:17.547' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'662', N'W-CP00-D26XXXX-0375-AS-MEA0', N'CP D26 LC-375 NAT ASAHI MEANS', CAST(N'2023-11-17T23:49:17.627' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'663', N'W-CP00-D26XXXX-0375-BL-00A0', N'CP D26 LC-375 NAT NO BRAND', CAST(N'2023-11-17T23:49:17.710' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'664', N'W-CP00-D26XXXX-0375-IN-GOB0', N'CP D26 LC-375 NAT INCOE GOLD', CAST(N'2023-11-17T23:49:17.790' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'665', N'W-CP00-D26XXXX-0375-IN-PPA0', N'CP D26 LC-375 NAT INCOE PPO', CAST(N'2023-11-17T23:49:17.870' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'666', N'W-CP00-D26XXXX-0375-IN-XPA0', N'CP D26 LC-375 NAT INCOE XP', CAST(N'2023-11-17T23:49:17.950' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'667', N'W-CP00-D26XXXX-0375-IN-XPB0', N'CP D26 LC-375 NAT INCOE XP SUD', CAST(N'2023-11-17T23:49:18.030' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'668', N'W-CP00-D26XXXX-0375-IN-XPC0', N'CP D26 LC-375 NAT INCOE XP UAE', CAST(N'2023-11-17T23:49:18.110' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'669', N'W-CP00-D26XXXX-0375-IN-XPD0', N'CP D26 LC-375 NAT INCOE XP MLY', CAST(N'2023-11-17T23:49:18.190' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'67', N'K-SCCV-220X045-11V0-QU-EFA0', N'STC CVR QUANTUM EFB 220X45 MM', CAST(N'2023-11-17T23:48:22.017' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'670', N'W-CP00-D26XXXX-0375-OH-HDB0', N'CP D26 LC-375 NAT OHAYO HD MII', CAST(N'2023-11-17T23:49:18.270' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'671', N'W-CP00-D26XXXX-0375-QU-00A0', N'CP D26 LC-375 NAT QUANTUM', CAST(N'2023-11-17T23:49:18.357' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'672', N'W-CP00-D26XXXX-0375-QU-00B0', N'CP D26 LC-375 NAT QUANTUM MII', CAST(N'2023-11-17T23:49:18.437' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'673', N'W-CP00-D26XXXX-0375-RA-00A0', N'CP D26 LC-375 NAT RAPTOR', CAST(N'2023-11-17T23:49:18.517' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'674', N'W-CP00-D26XXXX-0375-UL-00A0', N'CP D26 LC-375 NAT ULTRA-X', CAST(N'2023-11-17T23:49:18.600' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'675', N'W-CP00-D26XXXX-0375-UL-00B0', N'CP D26 LC-375 NAT ULTRA-X MII', CAST(N'2023-11-17T23:49:18.680' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'676', N'W-CP00-D31XXXX-0283-AM-PRA0', N'CP D31 LC-283 NAT AM PRMEIUM', CAST(N'2023-11-17T23:49:18.763' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'677', N'W-CP00-F51XXXX-0672-AP-PRC0', N'CP F51 LC672 NAT ASPIRA PRE AO', CAST(N'2023-11-17T23:49:18.840' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'678', N'W-CP00-F51XXXX-0672-AS-HPA0', N'CP F51 LC-672 NAT ASAHI H-PERF', CAST(N'2023-11-17T23:49:18.920' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'679', N'W-CP00-F51XXXX-0672-AS-HPB0', N'CP F51 LC-672 NAT ASAHI HP MII', CAST(N'2023-11-17T23:49:19.000' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'68', N'K-SCCV-220X045-11V0-QU-MFA0', N'STC CVR QUANTUM MF 220X45 MM', CAST(N'2023-11-17T23:48:22.100' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'680', N'W-CP00-F51XXXX-0672-AS-MEA0', N'CP F51 LC-672 NAT ASAHI MEANS', CAST(N'2023-11-17T23:49:19.090' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'681', N'W-CP00-F51XXXX-0672-AU-00A0', N'CP F51 LC-672 NAT AURORA', CAST(N'2023-11-17T23:49:19.170' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'682', N'W-CP00-F51XXXX-0672-IN-PPA0', N'CP F51 LC-672 NAT INCOE PPO', CAST(N'2023-11-17T23:49:19.253' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'683', N'W-CP00-F51XXXX-0672-IN-XPA0', N'CP F51 LC-672 NAT INCOE XP', CAST(N'2023-11-17T23:49:19.333' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'684', N'W-CP00-F51XXXX-0672-IN-XPB0', N'CP F51 LC-672 NAT INCOE XP SUD', CAST(N'2023-11-17T23:49:19.413' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'685', N'W-CP00-F51XXXX-0672-IN-XPC0', N'CP F51 LC-672 NAT INCOE XP UAE', CAST(N'2023-11-17T23:49:19.493' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'686', N'W-CP00-F51XXXX-0672-IN-XPD0', N'CP F51 LC-672 NAT INCOE XP MLY', CAST(N'2023-11-17T23:49:19.577' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'687', N'W-CP00-F51XXXX-0672-OH-HDB0', N'CP F51 LC-672 NAT OHAYO HD MII', CAST(N'2023-11-17T23:49:19.657' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'688', N'W-CP00-F51XXXX-0672-QU-00A0', N'CP F51 LC-672 NAT QUANTUM', CAST(N'2023-11-17T23:49:19.737' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'689', N'W-CP00-F51XXXX-0672-QU-00B0', N'CP F51 LC-672 NAT QUANTUM MII', CAST(N'2023-11-17T23:49:19.820' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'69', N'K-SCCV-220X045-11VJ-AS-EFA0', N'STIC CVR ASAHI EFB 220X45 MM', CAST(N'2023-11-17T23:48:22.183' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'690', N'W-CP00-F51XXXX-0672-RA-00A0', N'CP F51 LC-672 NAT RAPTOR', CAST(N'2023-11-17T23:49:19.900' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'691', N'W-CP00-F51XXXX-0672-UL-00A0', N'CP F51 LC-672 NAT ULTRA-X', CAST(N'2023-11-17T23:49:19.980' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'692', N'W-CP00-F51XXXX-0672-UL-00B0', N'CP F51 LC-672 NAT ULTRA-X MII', CAST(N'2023-11-17T23:49:20.060' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'693', N'W-CP00-G51XXXX-0880-AM-HDA0', N'CP G51 LC-880 NAT AM H-DUTY', CAST(N'2023-11-17T23:49:20.143' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'694', N'W-CP00-G51XXXX-0880-AM-PRA0', N'CP G51 LC-880 NAT AM PREMIUM', CAST(N'2023-11-17T23:49:20.220' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'695', N'W-CP00-G51XXXX-0880-AP-DCA0', N'CP G51 LC-880 NAT ASPIRA DEEP', CAST(N'2023-11-17T23:49:20.303' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'696', N'W-CP00-G51XXXX-0880-AP-PRA0', N'CP G51 LC-880 NAT ASPIRA PRE', CAST(N'2023-11-17T23:49:20.383' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'697', N'W-CP00-G51XXXX-0880-AP-PRC0', N'CP G51 LC880 NAT ASPIRA PRE AO', CAST(N'2023-11-17T23:49:20.463' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'698', N'W-CP00-G51XXXX-0880-AS-HPA0', N'CP G51 LC-880 NAT ASAHI H-PERF', CAST(N'2023-11-17T23:49:20.543' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'699', N'W-CP00-G51XXXX-0880-AS-HPB0', N'CP G51 LC-880 NAT ASAHI HP MII', CAST(N'2023-11-17T23:49:20.623' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'7', N'K-TC10-S12XXXX-GLFS-NL-0000', N'TOP COVER S12 BLACK', CAST(N'2023-11-17T23:48:16.873' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'70', N'K-SCCV-220X045-11VJ-AS-MFA0', N'STIC CVR ASAHI MF 220X45 MM', CAST(N'2023-11-17T23:48:22.267' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'700', N'W-CP00-G51XXXX-0880-AS-MCA0', N'CP G51 LC880 NAT ASAHI ME DEEP', CAST(N'2023-11-17T23:49:20.743' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'701', N'W-CP00-G51XXXX-0880-AS-MEA0', N'CP G51 LC-880 NAT ASAHI MEANS', CAST(N'2023-11-17T23:49:20.823' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'702', N'W-CP00-F51XXXX-0672-AP-PRA0', N'CP F51 LC-672 NAT ASPIRA PRE', CAST(N'2023-11-17T23:49:20.903' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'703', N'W-CP00-G51XXXX-0880-AU-00A0', N'CP G51 LC-880 NAT AURORA', CAST(N'2023-11-17T23:49:20.983' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'704', N'W-CP00-G51XXXX-0880-IN-PPA0', N'CP G51 LC-880 NAT INCOE PPO', CAST(N'2023-11-17T23:49:21.060' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'705', N'W-CP00-B20XXXX-0203-AP-PRA0', N'CP B20 LC-203 NAT ASPIRA PRE', CAST(N'2023-11-17T23:49:21.143' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'706', N'W-CP02-D26XXXX-0252-AP-MFA0', N'CP D26 LC-252 BLA ASPIRA MF', CAST(N'2023-11-17T23:49:21.223' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'707', N'W-CP02-D26XXXX-0252-AS-MEA0', N'CP D26 LC-252 BLA ASAHI MEN MF', CAST(N'2023-11-17T23:49:21.303' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'708', N'W-CP02-D26XXXX-0252-IN-MFA0', N'CP D26 LC-252 BLA INCOE MF', CAST(N'2023-11-17T23:49:21.387' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'709', N'W-CP02-D26XXXX-0252-IN-MFB0', N'CP D26 LC-252 BLA INCOE MF UAE', CAST(N'2023-11-17T23:49:21.467' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'71', N'K-SCCV-220X045-11VJ-IN-MFA0', N'STIC COVER INCOE MF 220X45 MM', CAST(N'2023-11-17T23:48:22.350' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'710', N'W-CP02-D26XXXX-0252-IN-MFC0', N'CP D26 LC-252 BLA INCOE MF AOP', CAST(N'2023-11-17T23:49:21.550' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'711', N'W-CP02-D26XXXX-0310-AP-MFA0', N'CP D26 LC-310 BLA ASPIRA MF', CAST(N'2023-11-17T23:49:21.627' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'712', N'W-CP02-D26XXXX-0310-IN-MFA0', N'CP D26 LC-310 BLA INCOE MF', CAST(N'2023-11-17T23:49:21.707' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'713', N'W-CP02-D26XXXX-0310-IN-MFB0', N'CP D26 LC-310 BLA INCOE MF UAE', CAST(N'2023-11-17T23:49:21.790' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'714', N'W-CP02-D26XXXX-0310-IN-MFC0', N'CP D26 LC-310 BLA INCOE MF AOP', CAST(N'2023-11-17T23:49:21.870' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'715', N'W-CP02-D26XXXX-0329-AP-MFA0', N'CP D26 LC-329 BLA ASPIRA MF', CAST(N'2023-11-17T23:49:21.950' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'716', N'W-CP02-D26XXXX-0329-AS-MEA0', N'CP D26 LC-329 BLA ASAHI MEN MF', CAST(N'2023-11-17T23:49:22.030' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'717', N'W-CP02-D26XXXX-0329-IN-MFA0', N'CP D26 LC-329 BLA INCOE MF', CAST(N'2023-11-17T23:49:22.107' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'718', N'W-CP02-D26XXXX-0329-IN-MFB0', N'CP D26 LC-329 BLA INCOE MF UAE', CAST(N'2023-11-17T23:49:22.190' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'719', N'W-CP02-D26XXXX-0329-IN-MFC0', N'CP D26 LC-329 BLA INCOE MF AOP', CAST(N'2023-11-17T23:49:22.270' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'72', N'K-SCCV-220X045-11VJ-OH-EFA0', N'STIC COVER OHAYO EFB 220X45 MM', CAST(N'2023-11-17T23:48:22.433' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'720', N'W-CP02-D26XXXX-0375-AP-MFA0', N'CP D26 LC-375 BLA ASPIRA MF', CAST(N'2023-11-17T23:49:22.350' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'721', N'W-CP02-D26XXXX-0375-IN-MDA0', N'CP D26 LC375 BLA INCOE MF DEEP', CAST(N'2023-11-17T23:49:22.430' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'722', N'W-CP02-D26XXXX-0375-IN-MDC0', N'CP D26 375 BLA INC MF DEEP AOP', CAST(N'2023-11-17T23:49:22.513' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'723', N'W-CP02-D26XXXX-0375-IN-MFA0', N'CP D26 LC-375 BLA INCOE MF', CAST(N'2023-11-17T23:49:22.593' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'724', N'W-CP02-D26XXXX-0375-IN-MFB0', N'CP D26 LC-375 BLA INCOE MF UAE', CAST(N'2023-11-17T23:49:22.673' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'725', N'W-CP02-D26XXXX-0375-IN-MFC0', N'CP D26 LC-375 BLA INCOE MF AOP', CAST(N'2023-11-17T23:49:22.753' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'726', N'W-CP02-D31XXXX-0329-AP-MFA0', N'CP D31 LC-329 BLA ASPIRA MF', CAST(N'2023-11-17T23:49:22.833' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'727', N'W-CP02-D31XXXX-0329-AS-MEA0', N'CP D31 LC-329 BLA ASAHI MEN MF', CAST(N'2023-11-17T23:49:22.917' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'728', N'W-CP02-D31XXXX-0329-IN-MFA0', N'CP D31 LC-329 BLA INCOE MF', CAST(N'2023-11-17T23:49:22.997' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'729', N'W-CT00-E41XXXX-0562-NH-0000', N'CONTAINER E41 LC-562 NATURAL', CAST(N'2023-11-17T23:49:23.083' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'73', N'K-SCCV-220X045-11VJ-OH-MFA0', N'STIC COVER OHAYO MF 220X45 MM', CAST(N'2023-11-17T23:48:22.520' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'730', N'W-CT00-F51XXXX-0672-NH-0000', N'CONTAINER F51 LC-672 NATURAL', CAST(N'2023-11-17T23:49:23.163' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'731', N'W-CP00-B20XXXX-0203-AS-HPA0', N'CP B20 LC-203 NAT ASAHI H-PERF', CAST(N'2023-11-17T23:49:23.243' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'732', N'W-CP00-B20XXXX-0203-AS-HPB0', N'CP B20 LC-203 NAT ASAHI HP MII', CAST(N'2023-11-17T23:49:23.323' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'733', N'W-CP00-B20XXXX-0203-BL-00A0', N'CP B20 LC-203 NAT NO BRAND', CAST(N'2023-11-17T23:49:23.407' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'734', N'W-CP00-B20XXXX-0203-IN-GOB0', N'CP B20 LC-203 NAT INCOE GOLD', CAST(N'2023-11-17T23:49:23.487' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'735', N'W-CP00-B20XXXX-0203-IN-PPA0', N'CP B20 LC-203 NAT INCOE PPO', CAST(N'2023-11-17T23:49:23.567' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'736', N'W-CP02-D31XXXX-0329-IN-MFB0', N'CP D31 LC-329 BLA INCOE MF UAE', CAST(N'2023-11-17T23:49:23.650' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'737', N'W-CP02-D31XXXX-0329-IN-MFC0', N'CP D31 LC-329 BLA INCOE MF AOP', CAST(N'2023-11-17T23:49:23.730' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'738', N'W-CP02-D31XXXX-0386-AP-MFA0', N'CP D31 LC-386 BLA ASPIRA MF', CAST(N'2023-11-17T23:49:23.807' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'739', N'W-CP02-D31XXXX-0386-AS-MEA0', N'CP D31 LC-386 BLA ASAHI MEN MF', CAST(N'2023-11-17T23:49:23.890' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'74', N'K-SCCV-265X070-15V0-IN-MFB0', N'STC CVR INCOE MF UAE 265X70 MM', CAST(N'2023-11-17T23:48:22.603' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'740', N'W-CP02-A19XXXX-0242-IN-MFC0', N'CP A19 LC-242 BLA INCOE MF AOP', CAST(N'2023-11-17T23:49:23.970' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'741', N'W-CP02-B20XXXX-0273-AP-EFA0', N'CP B20 LC-273 BLA ASPIRA EFB', CAST(N'2023-11-17T23:49:24.047' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'742', N'W-CP02-B20XXXX-0273-AP-MFA0', N'CP B20 LC-273 BLA ASPIRA MF', CAST(N'2023-11-17T23:49:24.130' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'743', N'W-CP02-B20XXXX-0273-AS-MEA0', N'CP B20 LC-273 BLA ASAHI MEN MF', CAST(N'2023-11-17T23:49:24.213' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'744', N'W-CP02-B20XXXX-0273-IN-MFA0', N'CP B20 LC-273 BLA INCOE MF', CAST(N'2023-11-17T23:49:24.290' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'745', N'W-CP02-B20XXXX-0273-IN-MFB0', N'CP B20 LC-273 BLA INCOE MF UAE', CAST(N'2023-11-17T23:49:24.373' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'746', N'W-CP00-D23XXXX-0315-AP-PRA0', N'CP D23 LC-315 NAT ASPIRA PRE', CAST(N'2023-11-17T23:49:24.457' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'747', N'W-CP02-D31XXXX-0386-IN-MFB0', N'CP D31 LC-386 BLA INCOE MF UAE', CAST(N'2023-11-17T23:49:24.537' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'748', N'W-CP02-D31XXXX-0386-IN-MFC0', N'CP D31 LC-386 BLA INCOE MF AOP', CAST(N'2023-11-17T23:49:24.613' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'749', N'W-CP02-D31XXXX-0433-AP-MFA0', N'CP D31 LC-433 BLA ASPIRA MF', CAST(N'2023-11-17T23:49:24.693' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'75', N'K-SCCV-GC2225X-11V0-BO-INA0', N'STICKER GC2-225 BOND INDUSTRI', CAST(N'2023-11-17T23:48:22.687' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'750', N'W-CP02-D31XXXX-0433-AP-MFB0', N'CP D31 LC433 BLA ASPIRA MF AOP', CAST(N'2023-11-17T23:49:24.777' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'751', N'W-CP02-D31XXXX-0433-IN-MDA0', N'CP D31 LC433 BLA INCOE MF DEEP', CAST(N'2023-11-17T23:49:24.857' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'752', N'W-CP02-D31XXXX-0433-IN-MDC0', N'CP D31 433 BLA INC MF DEEP AOP', CAST(N'2023-11-17T23:49:24.937' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'753', N'W-CP02-D31XXXX-0433-IN-MFA0', N'CP D31 LC-433 BLA INCOE MF', CAST(N'2023-11-17T23:49:25.017' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'754', N'W-CP02-D31XXXX-0433-IN-MFB0', N'CP D31 LC-433 BLA INCOE MF UAE', CAST(N'2023-11-17T23:49:25.097' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'755', N'W-CP02-D31XXXX-0433-IN-MFC0', N'CP D31 LC-433 BLA INCOE MF AOP', CAST(N'2023-11-17T23:49:25.177' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'756', N'W-CP02-E41XXXX-0563-AP-MFA0', N'CP E41 LC-563 BLA ASPIRA MF', CAST(N'2023-11-17T23:49:25.257' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'757', N'W-CP02-E41XXXX-0563-IN-MFA0', N'CP E41 LC-563 BLA INCOE MF', CAST(N'2023-11-17T23:49:25.337' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'758', N'W-CP02-E41XXXX-0563-IN-MFB0', N'CP E41 LC-563 BLA INCOE MF UAE', CAST(N'2023-11-17T23:49:25.417' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'759', N'W-CP02-E41XXXX-0563-IN-MFC0', N'CP E41 LC-563 BLA INCOE MF AOP', CAST(N'2023-11-17T23:49:25.497' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'76', N'K-SCCV-GC8170X-11V0-BO-INA0', N'STICKER GC8-170 BOND INDUSTRI', CAST(N'2023-11-17T23:48:22.770' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'760', N'W-CP02-F51XXXX-0672-AP-MFA0', N'CP F51 LC-672 BLA ASPIRA MF', CAST(N'2023-11-17T23:49:25.577' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'761', N'W-CP02-F51XXXX-0672-IN-HDC0', N'CP F51 LC672 BLA INC MF HD AOP', CAST(N'2023-11-17T23:49:25.660' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'762', N'W-CP02-F51XXXX-0672-IN-MFA0', N'CP F51 LC-672 BLA INCOE MF', CAST(N'2023-11-17T23:49:25.740' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'763', N'W-CP02-F51XXXX-0672-IN-MFB0', N'CP F51 LC-672 BLA INCOE MF UAE', CAST(N'2023-11-17T23:49:25.823' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'764', N'W-CP02-F51XXXX-0672-IN-MFC0', N'CP F51 LC-672 BLA INCOE MF AOP', CAST(N'2023-11-17T23:49:25.903' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'765', N'W-CP02-G51XXXX-0880-AP-MFA0', N'CP G51 LC-880 BLA ASPIRA MF', CAST(N'2023-11-17T23:49:25.983' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'766', N'W-CP02-G51XXXX-0880-IN-HDC0', N'CP G51 LC880 BLA INC MF HD AOP', CAST(N'2023-11-17T23:49:26.063' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'767', N'W-CP02-G51XXXX-0880-IN-MFA0', N'CP G51 LC-880 BLA INCOE MF', CAST(N'2023-11-17T23:49:26.143' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'768', N'W-CP02-G51XXXX-0880-IN-MFB0', N'CP G51 LC-880 BLA INCOE MF UAE', CAST(N'2023-11-17T23:49:26.223' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'769', N'W-CP02-G51XXXX-0880-IN-MFC0', N'CP G51 LC-880 BLA INCOE MF AOP', CAST(N'2023-11-17T23:49:26.303' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'77', N'K-SCCV-270X045-11V0-AP-MFA0', N'STIC CVR ASPIRA MF 270X45 MM', CAST(N'2023-11-17T23:48:22.863' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'770', N'W-CP02-H52XXXX-1060-AP-MFA0', N'CP H52 LC-1060 BLA ASPIRA MF', CAST(N'2023-11-17T23:49:26.383' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'771', N'W-CP02-H52XXXX-1060-IN-HDC0', N'CP H52 L1060 BLA INC MF HD AOP', CAST(N'2023-11-17T23:49:26.467' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'772', N'W-CP02-H52XXXX-1060-IN-MFA0', N'CP H52 LC-1060 BLA INCOE MF', CAST(N'2023-11-17T23:49:26.557' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'773', N'W-CP02-H52XXXX-1060-IN-MFB0', N'CP H52 LC1060 BLA INCOE MF UAE', CAST(N'2023-11-17T23:49:26.633' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'774', N'W-CP02-TZ6VXXX-0165-TR-00A0', N'CP TZ-6V LC-165 BLA TRANSPORT', CAST(N'2023-11-17T23:49:26.713' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'775', N'W-CP08-SG100CT-0771-AM-00A0', N'CP SG100-CT LC-771 GREY AMK', CAST(N'2023-11-17T23:49:26.793' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'776', N'W-CP08-SG100CT-0771-AP-00A0', N'CP SG100-CT LC-771 GREY ASPIRA', CAST(N'2023-11-17T23:49:26.877' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'777', N'W-CT00-544XXXX-0278-BA-0000', N'CONTAINER 544 LC-278 NATURAL', CAST(N'2023-11-17T23:49:26.957' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'778', N'W-CT00-545XXXX-0338-BA-0000', N'CONTAINER 545 LC-338 NATURAL', CAST(N'2023-11-17T23:49:27.037' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'779', N'W-CT00-555XXXX-0338-BA-0000', N'CONTAINER 555 LC-338 NATURAL', CAST(N'2023-11-17T23:49:27.117' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'78', N'K-SCCV-270X045-11V0-BL-MFA0', N'STC CVR NO BRAND MF 270X45 MM', CAST(N'2023-11-17T23:48:22.960' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'780', N'W-CT00-566XXXX-0403-BA-0000', N'CONTAINER 566 LC-403 NATURAL', CAST(N'2023-11-17T23:49:27.197' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'781', N'W-CT00-B20XXXX-0203-NH-0000', N'CONTAINER B20 LC-203 NATURAL', CAST(N'2023-11-17T23:49:27.287' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'782', N'W-CT00-B20XXXX-0220-NH-0000', N'CONTAINER B20 LC-220 NATURAL', CAST(N'2023-11-17T23:49:27.383' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'783', N'W-CT00-B20XXXX-0273-NH-0000', N'CONTAINER B20 LC-273 NATURAL', CAST(N'2023-11-17T23:49:27.477' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'784', N'W-CT00-B24XXXX-0232-NH-0000', N'CONTAINER B24 LC-232 NATURAL', CAST(N'2023-11-17T23:49:27.570' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'785', N'W-CT00-B24XXXX-0331-NH-0000', N'CONTAINER B24 LC-331 NATURAL', CAST(N'2023-11-17T23:49:27.663' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'786', N'W-CT00-D23XXXX-0315-BL-0000', N'CONTAINER D23 LC-315 NATURAL', CAST(N'2023-11-17T23:49:27.760' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'787', N'W-CT00-D26XXXX-0209-NH-0000', N'CONTAINER D26 LC-209 NATURAL', CAST(N'2023-11-17T23:49:27.853' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'788', N'W-CT00-D26XXXX-0257-NH-0000', N'CONTAINER D26 LC-257 NATURAL', CAST(N'2023-11-17T23:49:27.940' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'789', N'W-CT00-D26XXXX-0313-NH-0000', N'CONTAINER D26 LC-313 NATURAL', CAST(N'2023-11-17T23:49:28.020' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'79', N'K-SCCV-270X045-11V0-LU-PRA0', N'STC COVER LUCAS PRE 270X45 MM', CAST(N'2023-11-17T23:48:23.060' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'790', N'W-CT00-D26XXXX-0375-NH-0000', N'CONTAINER D26 LC-375 NATURAL', CAST(N'2023-11-17T23:49:28.107' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'791', N'W-CT00-D31XXXX-0283-NH-0000', N'CONTAINER D31 LC-283 NATURAL', CAST(N'2023-11-17T23:49:28.187' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'792', N'W-CT00-D31XXXX-0300-NH-0000', N'CONTAINER D31 LC-300 NATURAL', CAST(N'2023-11-17T23:49:28.270' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'793', N'W-CT00-D31XXXX-0375-NH-0000', N'CONTAINER D31 LC-375 NATURAL', CAST(N'2023-11-17T23:49:28.350' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'794', N'W-CT00-D31XXXX-0421-NH-0000', N'CONTAINER D31 LC-421 NATURAL', CAST(N'2023-11-17T23:49:28.430' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'795', N'W-CT00-E41XXXX-0393-NH-0000', N'CONTAINER E41 LC-393 NATURAL', CAST(N'2023-11-17T23:49:28.513' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'796', N'W-CP02-B20XXXX-0273-IN-MFC0', N'CP B20 LC-273 BLA INCOE MF AOP', CAST(N'2023-11-17T23:49:28.593' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'797', N'W-CP02-B24XXXX-0232-AP-MFA0', N'CP B24 LC-232 BLA ASPIRA MF', CAST(N'2023-11-17T23:49:28.673' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'798', N'W-CP02-B24XXXX-0232-AS-MEA0', N'CP B24 LC-232 BLA ASAHI MEN MF', CAST(N'2023-11-17T23:49:28.757' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'799', N'W-CP02-B24XXXX-0232-IN-MFA0', N'CP B24 LC-232 BLA INCOE MF', CAST(N'2023-11-17T23:49:28.837' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'8', N'K-TC10-S65XXXX-GLFS-NL-0000', N'TOP COVER S65 BLACK', CAST(N'2023-11-17T23:48:16.967' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'80', N'K-SCCV-270X045-11V0-OH-MFB0', N'STC CVR OHAYO MF UAE 270X45 MM', CAST(N'2023-11-17T23:48:23.160' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'800', N'W-CP02-B24XXXX-0232-IN-MFB0', N'CP B24 LC-232 BLA INCOE MF UAE', CAST(N'2023-11-17T23:49:28.917' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'801', N'W-CP02-B24XXXX-0232-IN-MFC0', N'CP B24 LC-232 BLA INCOE MF AOP', CAST(N'2023-11-17T23:49:28.997' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'802', N'W-CP02-B24XXXX-0331-AP-MFA0', N'CP B24 LC-331 BLA ASPIRA MF', CAST(N'2023-11-17T23:49:29.077' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'803', N'W-CP02-B24XXXX-0331-AS-MEA0', N'CP B24 LC-331 BLA ASAHI MEN MF', CAST(N'2023-11-17T23:49:29.160' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'804', N'W-CP02-B24XXXX-0331-IN-MFA0', N'CP B24 LC-331 BLA INCOE MF', CAST(N'2023-11-17T23:49:29.243' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'805', N'W-CP02-B24XXXX-0331-IN-MFB0', N'CP B24 LC-331 BLA INCOE MF UAE', CAST(N'2023-11-17T23:49:29.323' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'806', N'W-CP02-B24XXXX-0331-IN-MFC0', N'CP B24 LC-331 BLA INCOE MF AOP', CAST(N'2023-11-17T23:49:29.403' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'807', N'W-CP02-D23XXXX-0315-AP-EFA0', N'CP D23 LC-315 BLA ASPIRA EFB', CAST(N'2023-11-17T23:49:29.487' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'808', N'W-CP02-D23XXXX-0315-AP-MFA0', N'CP D23 LC-315 BLA ASPIRA MF', CAST(N'2023-11-17T23:49:29.567' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'809', N'W-CP02-D23XXXX-0315-AS-MEA0', N'CP D23 LC-315 BLA ASAHI MEN MF', CAST(N'2023-11-17T23:49:29.647' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'81', N'K-SCCV-115F51X-11V0-TN-HDA0', N'STIC CVR MF 115F51 TRAKNUS HD', CAST(N'2023-11-17T23:48:23.263' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'810', N'W-CP02-D23XXXX-0315-IN-MFA0', N'CP D23 LC-315 BLA INCOE MF', CAST(N'2023-11-17T23:49:29.727' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'811', N'W-CP02-D23XXXX-0315-IN-MFB0', N'CP D23 LC-315 BLA INCOE MF UAE', CAST(N'2023-11-17T23:49:29.807' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'812', N'W-CP02-D23XXXX-0315-IN-MFC0', N'CP D23 LC-315 BLA INCOE MF AOP', CAST(N'2023-11-17T23:49:29.890' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'813', N'W-CP02-D26XXXX-0210-AP-MFA0', N'CP D26 LC-210 BLA ASPIRA MF', CAST(N'2023-11-17T23:49:29.970' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'814', N'W-CP02-D26XXXX-0210-AS-MEA0', N'CP D26 LC-210 BLA ASAHI MEN MF', CAST(N'2023-11-17T23:49:30.053' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'815', N'W-CP02-D26XXXX-0210-IN-MFA0', N'CP D26 LC-210 BLA INCOE MF', CAST(N'2023-11-17T23:49:30.133' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'816', N'W-CP02-D26XXXX-0210-IN-MFB0', N'CP D26 LC-210 BLA INCOE MF UAE', CAST(N'2023-11-17T23:49:30.217' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'817', N'W-CP02-D26XXXX-0210-IN-MFC0', N'CP D26 LC-210 BLA INCOE MF AOP', CAST(N'2023-11-17T23:49:30.297' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'818', N'W-CP00-D31XXXX-0300-IN-XPD0', N'CP D31 LC-300 NAT INCOE XP MLY', CAST(N'2023-11-17T23:49:30.377' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'819', N'W-CP00-D31XXXX-0300-OH-HDB0', N'CP D31 LC-300 NAT OHAYO HD MII', CAST(N'2023-11-17T23:49:30.457' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'82', N'K-SCCV-145G51X-11V0-TN-HDA0', N'STIC CVR MF 145H51 TRAKNUS HD', CAST(N'2023-11-17T23:48:23.363' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'820', N'W-CP00-D31XXXX-0300-QU-00A0', N'CP D31 LC-300 NAT QUANTUM', CAST(N'2023-11-17T23:49:30.540' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'821', N'W-CP00-D31XXXX-0300-QU-00B0', N'CP D31 LC-300 NAT QUANTUM MII', CAST(N'2023-11-17T23:49:30.620' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'822', N'W-CP00-D31XXXX-0300-UL-00A0', N'CP D31 LC-300 NAT ULTRA-X', CAST(N'2023-11-17T23:49:30.703' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'823', N'W-CP00-D31XXXX-0300-UL-00B0', N'CP D31 LC-300 NAT ULTRA-X MII', CAST(N'2023-11-17T23:49:30.783' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'824', N'W-CP00-555XXXX-0338-QU-00B0', N'CP 555 LC-338 NAT QUANTUM MII', CAST(N'2023-11-17T23:49:30.863' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'825', N'W-CP00-555XXXX-0338-RA-00A0', N'CP 555 LC-338 NAT RAPTOR', CAST(N'2023-11-17T23:49:30.943' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'826', N'W-CP00-555XXXX-0338-UL-00A0', N'CP 555 LC-338 NAT ULTRA-X', CAST(N'2023-11-17T23:49:31.023' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'827', N'W-CP00-555XXXX-0338-UL-00B0', N'CP 555 LC-338 NAT ULTRA-X MII', CAST(N'2023-11-17T23:49:31.103' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'828', N'W-CP00-55D26RX-0313-TN-HDA0', N'CP 55D26R LC313 NAT TRAKNUS HD', CAST(N'2023-11-17T23:49:31.183' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'829', N'W-CP00-566XXXX-0403-AS-HPA0', N'CP 566 LC-403 NAT ASAHI H-PERF', CAST(N'2023-11-17T23:49:31.267' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'83', N'K-SCCV-170X045-11V0-BL-MFA0', N'STC CVR NO BRAND MF 170X45 MM', CAST(N'2023-11-17T23:48:23.463' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'830', N'W-CP00-566XXXX-0403-AS-HPB0', N'CP 566 LC-403 NAT ASAHI HP MII', CAST(N'2023-11-17T23:49:31.350' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'831', N'W-CP00-566XXXX-0403-IN-PPA0', N'CP 566 LC-403 NAT INCOE PPO', CAST(N'2023-11-17T23:49:31.430' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'832', N'W-CP00-566XXXX-0403-IN-XPA0', N'CP 566 LC-403 NAT INCOE XP', CAST(N'2023-11-17T23:49:31.510' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'833', N'W-CP00-566XXXX-0403-IN-XPB0', N'CP 566 LC-403 NAT INCOE XP SUD', CAST(N'2023-11-17T23:49:31.590' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'834', N'W-CP00-566XXXX-0403-IN-XPC0', N'CP 566 LC-403 NAT INCOE XP UAE', CAST(N'2023-11-17T23:49:31.670' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'835', N'W-CP00-566XXXX-0403-IN-XPD0', N'CP 566 LC-403 NAT INCOE XP MLY', CAST(N'2023-11-17T23:49:31.750' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'836', N'W-CP00-566XXXX-0403-OH-HDB0', N'CP 566 LC-403 NAT OHAYO HD MII', CAST(N'2023-11-17T23:49:31.833' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'837', N'W-CP00-566XXXX-0403-QU-00A0', N'CP 566 LC-403 NAT QUANTUM', CAST(N'2023-11-17T23:49:31.913' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'838', N'W-CP00-566XXXX-0403-QU-00B0', N'CP 566 LC-403 NAT QUANTUM MII', CAST(N'2023-11-17T23:49:31.993' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'839', N'W-CP00-566XXXX-0403-UL-00A0', N'CP 566 LC-403 NAT ULTRA-X', CAST(N'2023-11-17T23:49:32.073' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'84', N'K-SCCV-170X045-11V0-QU-MFA0', N'STC CVR QUANTUM MF 170X45 MM', CAST(N'2023-11-17T23:48:23.557' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'840', N'W-CP00-566XXXX-0403-UL-00B0', N'CP 566 LC-403 NAT ULTRA-X MII', CAST(N'2023-11-17T23:49:32.163' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'841', N'W-CP00-65D26RX-0313-TR-00A0', N'CP 65D26R LC-313 NAT TRANSPORT', CAST(N'2023-11-17T23:49:32.243' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'842', N'W-CP00-65D31RX-0283-TN-00A0', N'CP 65D31R LC-283 NAT TRAKNUS', CAST(N'2023-11-17T23:49:32.327' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'843', N'W-CP00-65D31RX-0283-TR-00A0', N'CP 65D31R LC-283 NAT TRANSPORT', CAST(N'2023-11-17T23:49:32.403' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'844', N'W-CP00-65D31RX-0300-TN-00A0', N'CP 65D31R LC-300 NAT TRAKNUS', CAST(N'2023-11-17T23:49:32.487' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'845', N'W-CP00-65D31RX-0300-TR-00A0', N'CP 65D31R LC-300 NAT TRANSPORT', CAST(N'2023-11-17T23:49:32.567' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'846', N'W-CP00-75D31LX-0421-TN-HDA0', N'CP 75D31L LC421 NAT TRAKNUS HD', CAST(N'2023-11-17T23:49:32.650' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'847', N'W-CP00-95E41RX-0393-TN-00A0', N'CP 95E41R LC-393 NAT TRAKNUS', CAST(N'2023-11-17T23:49:32.730' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'848', N'W-CP00-95E41RX-0393-TR-00A0', N'CP 95E41R LC-393 NAT TRANSPORT', CAST(N'2023-11-17T23:49:32.810' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'849', N'W-CP00-95E41RX-0562-TN-00A0', N'CP 95E41R LC-562 NAT TRAKNUS', CAST(N'2023-11-17T23:49:32.893' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'85', N'K-SCCV-270X045-11V0-QU-MFA0', N'STC CVR QUANTUM MF 270X45 MM', CAST(N'2023-11-17T23:48:23.640' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'850', N'W-CP00-95E41RX-0562-TR-00A0', N'CP 95E41R LC-562 NAT TRANSPORT', CAST(N'2023-11-17T23:49:32.973' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'851', N'W-CP00-D26XXXX-0313-IN-XPA0', N'CP D26 LC-313 NAT INCOE XP', CAST(N'2023-11-17T23:49:33.053' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'852', N'W-CP00-E41XXXX-0393-QU-00B0', N'CP E41 LC-393 NAT QUANTUM MII', CAST(N'2023-11-17T23:49:33.137' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'853', N'W-CP00-E41XXXX-0393-UL-00A0', N'CP E41 LC-393 NAT ULTRA-X', CAST(N'2023-11-17T23:49:33.217' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'854', N'W-CP00-E41XXXX-0393-UL-00B0', N'CP E41 LC-393 NAT ULTRA-X MII', CAST(N'2023-11-17T23:49:33.300' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'855', N'W-CP00-E41XXXX-0562-AM-HDA0', N'CP E41 LC-562 NAT AM H-DUTY', CAST(N'2023-11-17T23:49:33.380' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'856', N'W-CP00-E41XXXX-0562-AM-PRA0', N'CP E41 LC-562 NAT AM PREMIUM', CAST(N'2023-11-17T23:49:33.460' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'857', N'W-CP00-E41XXXX-0562-AP-DCA0', N'CP E41 LC-562 NAT ASPIRA DEEP', CAST(N'2023-11-17T23:49:33.540' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'858', N'W-CP00-E41XXXX-0562-AP-PRA0', N'CP E41 LC-562 NAT ASPIRA PRE', CAST(N'2023-11-17T23:49:33.620' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'859', N'W-CP00-E41XXXX-0562-AP-PRC0', N'CP E41 LC562 NAT ASPIRA PRE AO', CAST(N'2023-11-17T23:49:33.700' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'86', N'K-SCCV-270X045-11VJ-AS-MFA0', N'STIC CVR ASAHI MF 270X45 MM', CAST(N'2023-11-17T23:48:23.723' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'860', N'W-CP00-E41XXXX-0562-AS-HPA0', N'CP E41 LC-562 NAT ASAHI H-PERF', CAST(N'2023-11-17T23:49:33.783' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'861', N'W-CP00-E41XXXX-0562-AS-HPB0', N'CP E41 LC-562 NAT ASAHI HP MII', CAST(N'2023-11-17T23:49:33.863' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'862', N'W-CP00-E41XXXX-0562-AS-MCA0', N'CP E41 LC562 NAT ASAHI ME DEEP', CAST(N'2023-11-17T23:49:33.943' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'863', N'W-CP00-E41XXXX-0562-AS-MEA0', N'CP E41 LC-562 NAT ASAHI MEANS', CAST(N'2023-11-17T23:49:34.023' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'864', N'W-CP00-E41XXXX-0562-AU-00A0', N'CP E41 LC-562 NAT AURORA', CAST(N'2023-11-17T23:49:34.107' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'865', N'W-CP00-E41XXXX-0562-BL-00A0', N'CP E41 LC-562 NAT NO BRAND', CAST(N'2023-11-17T23:49:34.190' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'866', N'W-CP00-D31XXXX-0375-AM-HDA0', N'CP D31 LC-375 NAT AM H-DUTY', CAST(N'2023-11-17T23:49:34.270' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'867', N'W-CP00-D31XXXX-0375-AP-PRA0', N'CP D31 LC-375 NAT ASPIRA PRE', CAST(N'2023-11-17T23:49:34.350' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'868', N'W-CP00-D31XXXX-0375-AP-PRB0', N'CP D31 LC375 NAT ASPIRA PRE AO', CAST(N'2023-11-17T23:49:34.430' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'869', N'W-CP00-D31XXXX-0375-AS-HPA0', N'CP D31 LC-375 NAT ASAHI H-PERF', CAST(N'2023-11-17T23:49:34.513' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'87', N'K-SCCV-270X045-11VJ-IN-MFA0', N'STIC COVER INCOE MF 270X45 MM', CAST(N'2023-11-17T23:48:23.807' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'870', N'W-CP00-D31XXXX-0375-AS-HPB0', N'CP D31 LC-375 NAT ASAHI HP MII', CAST(N'2023-11-17T23:49:34.607' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'871', N'W-CP00-D31XXXX-0375-AU-00A0', N'CP D31 LC-375 NAT AURORA', CAST(N'2023-11-17T23:49:34.687' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'872', N'W-CP00-D31XXXX-0375-BL-00A0', N'CP D31 LC-375 NAT NO BRAND', CAST(N'2023-11-17T23:49:34.770' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'873', N'W-CP00-D31XXXX-0375-IN-GOB0', N'CP D31 LC-375 NAT INCOE GOLD', CAST(N'2023-11-17T23:49:34.850' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'874', N'W-CP00-D31XXXX-0375-IN-PPA0', N'CP D31 LC-375 NAT INCOE PPO', CAST(N'2023-11-17T23:49:34.930' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'875', N'W-CP00-D31XXXX-0375-IN-XPA0', N'CP D31 LC-375 NAT INCOE XP', CAST(N'2023-11-17T23:49:35.010' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'876', N'W-CP00-D31XXXX-0375-IN-XPC0', N'CP D31 LC-375 NAT INCOE XP UAE', CAST(N'2023-11-17T23:49:35.093' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'877', N'W-CP00-D31XXXX-0375-IN-XPD0', N'CP D31 LC-375 NAT INCOE XP MLY', CAST(N'2023-11-17T23:49:35.173' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'878', N'W-CP00-D31XXXX-0375-OH-HDB0', N'CP D31 LC-375 NAT OHAYO HD MII', CAST(N'2023-11-17T23:49:35.257' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'879', N'W-CP00-D31XXXX-0375-QU-00A0', N'CP D31 LC-375 NAT QUANTUM', CAST(N'2023-11-17T23:49:35.337' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'88', N'K-SCCV-270X045-11VJ-OH-MFA0', N'STIC COVER OHAYO MF 270X45 MM', CAST(N'2023-11-17T23:48:23.893' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'880', N'W-CP00-D31XXXX-0375-QU-00B0', N'CP D31 LC-375 NAT QUANTUM MII', CAST(N'2023-11-17T23:49:35.417' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'881', N'W-CP00-D31XXXX-0375-RA-00A0', N'CP D31 LC-375 NAT RAPTOR', CAST(N'2023-11-17T23:49:35.500' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'882', N'W-CP00-D31XXXX-0375-UL-00A0', N'CP D31 LC-375 NAT ULTRA-X', CAST(N'2023-11-17T23:49:35.580' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'883', N'W-CP00-D31XXXX-0421-AM-PRA0', N'CP D31 LC-421 NAT AM PREMIUM', CAST(N'2023-11-17T23:49:35.663' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'884', N'W-CP00-D31XXXX-0421-AP-PRA0', N'CP D31 LC-421 NAT ASPIRA PRE', CAST(N'2023-11-17T23:49:35.740' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'885', N'W-CP00-D31XXXX-0421-AP-PRB0', N'CP D31 LC421 NAT ASPIRA PRE AO', CAST(N'2023-11-17T23:49:35.823' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'886', N'W-CP00-D31XXXX-0421-AS-HPA0', N'CP D31 LC-421 NAT ASAHI H-PERF', CAST(N'2023-11-17T23:49:35.903' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'887', N'W-CP00-E41XXXX-0562-IN-PPA0', N'CP E41 LC-562 NAT INCOE PPO', CAST(N'2023-11-17T23:49:35.983' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'888', N'W-CP00-E41XXXX-0562-IN-XPA0', N'CP E41 LC-562 NAT INCOE XP', CAST(N'2023-11-17T23:49:36.083' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'889', N'W-CP00-E41XXXX-0562-IN-XPB0', N'CP E41 LC-562 NAT INCOE XP SUD', CAST(N'2023-11-17T23:49:36.177' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'89', N'K-SCCV-270X045-15V0-IN-MFB0', N'STC CVR INCOE MF UAE 270X45 MM', CAST(N'2023-11-17T23:48:23.990' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'890', N'W-CP00-E41XXXX-0562-IN-XPC0', N'CP E41 LC-562 NAT INCOE XP UAE', CAST(N'2023-11-17T23:49:36.253' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'891', N'W-CP00-E41XXXX-0562-IN-XPD0', N'CP E41 LC-562 NAT INCOE XP MLY', CAST(N'2023-11-17T23:49:36.333' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'892', N'W-CP00-E41XXXX-0562-OH-HDB0', N'CP E41 LC-562 NAT OHAYO HD MII', CAST(N'2023-11-17T23:49:36.417' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'893', N'W-CP00-E41XXXX-0562-QU-00A0', N'CP E41 LC-562 NAT QUANTUM', CAST(N'2023-11-17T23:49:36.497' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'894', N'W-CP00-E41XXXX-0562-QU-00B0', N'CP E41 LC-562 NAT QUANTUM MII', CAST(N'2023-11-17T23:49:36.577' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'895', N'W-CP00-E41XXXX-0562-RA-00A0', N'CP E41 LC-562 NAT RAPTOR', CAST(N'2023-11-17T23:49:36.660' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'896', N'W-CP00-E41XXXX-0562-UL-00A0', N'CP E41 LC-562 NAT ULTRA-X', CAST(N'2023-11-17T23:49:36.740' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'897', N'W-CP00-E41XXXX-0562-UL-00B0', N'CP E41 LC-562 NAT ULTRA-X MII', CAST(N'2023-11-17T23:49:36.820' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'898', N'W-CP00-F51XXXX-0672-AM-HDA0', N'CP F51 LC-672 NAT AM H-DUTY', CAST(N'2023-11-17T23:49:36.903' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'899', N'W-CP00-F51XXXX-0672-AM-PRA0', N'CP F51 LC-672 NAT AM PREMIUM', CAST(N'2023-11-17T23:49:36.983' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'9', N'K-TC10-S9XXXXX-GLFS-NL-0000', N'TOP COVER S9 BLACK', CAST(N'2023-11-17T23:48:17.050' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'90', N'K-SCCV-327X050-11V0-AP-MFA0', N'STIC CVR ASPIRA MF 327X50 MM', CAST(N'2023-11-17T23:48:24.080' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'900', N'W-CP00-E41XXXX-0393-QU-00A0', N'CP E41 LC-393 NAT QUANTUM', CAST(N'2023-11-17T23:49:37.063' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'901', N'W-CP00-115F51X-0672-TR-00A0', N'CP 115F51 LC-672 NAT TRANSPORT', CAST(N'2023-11-17T23:49:37.143' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'902', N'W-CP00-B20XXXX-0220-QU-00C0', N'CP B20 LC-220 NAT QUANTUM RFH', CAST(N'2023-11-17T23:49:37.223' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'903', N'W-CP00-B20XXXX-0220-UL-00A0', N'CP B20 LC-220 NAT ULTRA-X', CAST(N'2023-11-17T23:49:37.307' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'904', N'W-CP00-B20XXXX-0220-UL-00B0', N'CP B20 LC-220 NAT ULTRA-X MII', CAST(N'2023-11-17T23:49:37.383' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'905', N'W-CP00-B20XXXX-0273-AP-PRA0', N'CP B20 LC-273 NAT ASPIRA PRE', CAST(N'2023-11-17T23:49:37.467' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'906', N'W-CP00-B20XXXX-0273-AP-PRB0', N'CP B20 LC273 NAT ASPIRA PRE AO', CAST(N'2023-11-17T23:49:37.547' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'907', N'W-CP00-B20XXXX-0273-AS-HPA0', N'CP B20 LC-273 NAT ASAHI H-PERF', CAST(N'2023-11-17T23:49:37.630' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'908', N'W-CP00-B20XXXX-0273-AS-HPB0', N'CP B20 LC-273 NAT ASAHI HP MII', CAST(N'2023-11-17T23:49:37.707' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'909', N'W-CP00-B20XXXX-0273-AS-MEA0', N'CP B20 LC-273 NAT ASAHI MEANS', CAST(N'2023-11-17T23:49:37.787' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'91', N'K-SCCV-327X050-11V0-BL-MFA0', N'STC CVR NO BRAND MF 327X50 MM', CAST(N'2023-11-17T23:48:24.190' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'910', N'W-CP00-B20XXXX-0273-BL-00A0', N'CP B20 LC-273 NAT NO BRAND', CAST(N'2023-11-17T23:49:37.870' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'911', N'W-CP00-B20XXXX-0273-IN-GOB0', N'CP B20 LC-273 NAT INCOE GOLD', CAST(N'2023-11-17T23:49:37.957' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'912', N'W-CP00-B20XXXX-0273-IN-PPA0', N'CP B20 LC-273 NAT INCOE PPO', CAST(N'2023-11-17T23:49:38.047' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'913', N'W-CP00-B20XXXX-0273-IN-XPA0', N'CP B20 LC-273 NAT INCOE XP', CAST(N'2023-11-17T23:49:38.137' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'914', N'W-CP00-B20XXXX-0273-IN-XPB0', N'CP B20 LC-273 NAT INCOE XP SUD', CAST(N'2023-11-17T23:49:38.230' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'915', N'W-CP00-B20XXXX-0273-IN-XPC0', N'CP B20 LC-273 NAT INCOE XP UAE', CAST(N'2023-11-17T23:49:38.330' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'916', N'W-CP00-B20XXXX-0273-IN-XPD0', N'CP B20 LC-273 NAT INCOE XP MLY', CAST(N'2023-11-17T23:49:38.423' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'917', N'W-CP00-B20XXXX-0273-OH-HDB0', N'CP B20 LC-273 NAT OHAYO HD MII', CAST(N'2023-11-17T23:49:38.517' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'918', N'W-CP00-B20XXXX-0273-QU-00A0', N'CP B20 LC-273 NAT QUANTUM', CAST(N'2023-11-17T23:49:38.610' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'919', N'W-CP00-B20XXXX-0273-QU-00B0', N'CP B20 LC-273 NAT QUANTUM MII', CAST(N'2023-11-17T23:49:38.690' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'92', N'K-SCCV-327X050-11V0-LU-PRA0', N'STC COVER LUCAS PRE 327X50 MM', CAST(N'2023-11-17T23:48:24.273' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'920', N'W-CP00-B20XXXX-0273-RA-00A0', N'CP B20 LC-273 NAT RAPTOR', CAST(N'2023-11-17T23:49:38.770' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'921', N'W-CP00-B20XXXX-0273-UL-00A0', N'CP B20 LC-273 NAT ULTRA-X', CAST(N'2023-11-17T23:49:38.853' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'922', N'W-CP00-B20XXXX-0273-UL-00B0', N'CP B20 LC-273 NAT ULRA-X MII', CAST(N'2023-11-17T23:49:38.937' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'923', N'W-CP00-B24XXXX-0232-AP-PRA0', N'CP B24 LC-232 NAT ASPIRA PRE', CAST(N'2023-11-17T23:49:39.013' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'924', N'W-CP00-B24XXXX-0232-AP-PRB0', N'CP B24 LC232 NAT ASPIRA PRE AO', CAST(N'2023-11-17T23:49:39.090' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'925', N'W-CP00-B24XXXX-0232-AS-HPA0', N'CP B24 LC-232 NAT ASAHI H-PERF', CAST(N'2023-11-17T23:49:39.177' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'926', N'W-CP00-B24XXXX-0232-AS-HPB0', N'CP B24 LC-232 NAT ASAHI HP MII', CAST(N'2023-11-17T23:49:39.260' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'927', N'W-CP00-B24XXXX-0232-BL-00A0', N'CP B24 LC-232 NAT NO BRAND', CAST(N'2023-11-17T23:49:39.340' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'928', N'W-CP00-B24XXXX-0232-IN-PPA0', N'CP B24 LC-232 NAT INCOE PPO', CAST(N'2023-11-17T23:49:39.420' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'929', N'W-CP00-B24XXXX-0232-IN-XPA0', N'CP B24 LC232 NAT INCOE XP', CAST(N'2023-11-17T23:49:39.500' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'93', N'K-SCCV-327X050-11V0-OH-MFB0', N'STC CVR OHAYO MF UAE 327X50 MM', CAST(N'2023-11-17T23:48:24.353' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'930', N'W-CP02-D26XXXX-0310-AS-MEA0', N'CP D26 LC-310 BLA ASAHI MEN MF', CAST(N'2023-11-17T23:49:39.583' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'931', N'W-CP00-B24XXXX-0232-AS-MEA0', N'CP B24 LC-232 NAT ASAHI MEANS', CAST(N'2023-11-17T23:49:39.660' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'932', N'W-CP00-B24XXXX-0232-QU-00C0', N'CP B24 LC-232 NAT QUANTUM RFH', CAST(N'2023-11-17T23:49:39.743' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'933', N'W-CP00-D31XXXX-0300-IN-XPC0', N'CP D31 LC-300 NAT INCOE XP UAE', CAST(N'2023-11-17T23:49:39.823' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'934', N'W-CP00-NS60LXX-0232-NI-00A0', N'CP NS60L LC-232 NAT NIKO', CAST(N'2023-11-17T23:49:39.903' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'935', N'W-CP00-H52XXXX-1060-RA-00A0', N'CP H52 LC-1060 NAT RAPTOR', CAST(N'2023-11-17T23:49:39.987' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'936', N'W-CP00-145G51X-0880-TN-00A0', N'CP 145G51 LC-880 NAT TRAKNUS', CAST(N'2023-11-17T23:49:40.067' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'937', N'W-CP00-145G51X-0880-TR-00A0', N'CP 145G51 LC-880 NAT TRANSPORT', CAST(N'2023-11-17T23:49:40.190' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'938', N'W-CP00-190H52X-1060-TN-00A0', N'CP 190H52 LC-1060 NAT TRAKNUS', CAST(N'2023-11-17T23:49:40.273' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'939', N'W-CP00-190H52X-1060-TR-00A0', N'CP 190H52 LC1060 NAT TRANSPORT', CAST(N'2023-11-17T23:49:40.353' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'94', N'K-SCCV-327X050-11V0-QU-MFA0', N'STC CVR QUANTUM MF 327X50 MM', CAST(N'2023-11-17T23:48:24.440' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'940', N'W-CP00-34B20LX-0273-IN-GOA0', N'CP 34B20L LC273 NAT IN GOLD OE', CAST(N'2023-11-17T23:49:40.437' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'941', N'W-CP00-48D26RX-0313-TN-HDA0', N'CP 48D26R LC313 NAT TRAKNUS HD', CAST(N'2023-11-17T23:49:40.517' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'942', N'W-CP00-545XXXX-0338-AP-PRA0', N'CP 545 LC-338 NAT ASPIRA PRE', CAST(N'2023-11-17T23:49:40.600' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'943', N'W-CP00-545XXXX-0338-AS-HPA0', N'CP 545 LC-338 NAT ASAHI H-PERF', CAST(N'2023-11-17T23:49:40.680' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'944', N'W-CP00-545XXXX-0338-AS-HPB0', N'CP 545 LC-338 NAT ASAHI HP MII', CAST(N'2023-11-17T23:49:40.760' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'945', N'W-CP00-545XXXX-0338-BL-00A0', N'CP 545 LC-338 NAT NO BRAND', CAST(N'2023-11-17T23:49:40.840' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'946', N'W-CP00-115F51X-0672-TN-00A0', N'CP 115F51 LC-672 NAT TRAKNUS', CAST(N'2023-11-17T23:49:40.923' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'947', N'W-CP00-D23XXXX-0315-AP-PRB0', N'CP D23 LC315 NAT ASPIRA PRE AO', CAST(N'2023-11-17T23:49:41.003' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'948', N'W-CP00-545XXXX-0338-IN-XPA0', N'CP 545 LC-338 NAT INCOE XP', CAST(N'2023-11-17T23:49:41.083' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'949', N'W-CP00-545XXXX-0338-OH-HDB0', N'CP 545 LC-338 NAT OHAYO HD MII', CAST(N'2023-11-17T23:49:41.167' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'95', N'K-SCCV-327X050-11VE-AS-MFA0', N'STIC CVR ASAHI MF 327X50 MM', CAST(N'2023-11-17T23:48:24.527' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'950', N'W-CP00-545XXXX-0338-QU-00A0', N'CP 545 LC-338 NAT QUANTUM', CAST(N'2023-11-17T23:49:41.247' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'951', N'W-CP00-545XXXX-0338-QU-00B0', N'CP 545 LC-338 NAT QUANTUM MII', CAST(N'2023-11-17T23:49:41.327' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'952', N'W-CP00-545XXXX-0338-UL-00A0', N'CP 545 LC-338 NAT ULTRA-X', CAST(N'2023-11-17T23:49:41.407' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'953', N'W-CP00-545XXXX-0338-UL-00B0', N'CP 545 LC-338 NAT ULTRA-X MII', CAST(N'2023-11-17T23:49:41.490' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'954', N'W-CP00-555XXXX-0338-AP-PRA0', N'CP 555 LC-338 NAT ASPIRA PRE', CAST(N'2023-11-17T23:49:41.570' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'955', N'W-CP00-555XXXX-0338-AS-HPA0', N'CP 555 LC-338 NAT ASAHI H-PERF', CAST(N'2023-11-17T23:49:41.650' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'956', N'W-CP00-555XXXX-0338-AS-HPB0', N'CP 555 LC-338 NAT ASAHI HP MII', CAST(N'2023-11-17T23:49:41.730' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'957', N'W-CP00-555XXXX-0338-AS-MEA0', N'CP 555 LC-338 NAT ASAHI MEANS', CAST(N'2023-11-17T23:49:41.813' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'958', N'W-CP00-555XXXX-0338-BL-00A0', N'CP 555 LC-338 NAT NO BRAND', CAST(N'2023-11-17T23:49:41.893' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'959', N'W-CP00-555XXXX-0338-IN-PPA0', N'CP 555 LC-338 NAT INCOE PPO', CAST(N'2023-11-17T23:49:41.973' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'96', N'K-SCCV-327X050-11VE-IN-MFA0', N'STIC COVER INCOE MF 327X50 MM', CAST(N'2023-11-17T23:48:24.607' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'960', N'W-CP00-555XXXX-0338-IN-XPA0', N'CP 555 LC-338 NAT INCOE XP', CAST(N'2023-11-17T23:49:42.053' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'961', N'W-CP00-555XXXX-0338-IN-XPB0', N'CP 555 LC-338 NAT INCOE XP SUD', CAST(N'2023-11-17T23:49:42.133' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'962', N'W-CP00-555XXXX-0338-IN-XPC0', N'CP 555 LC-338 NAT INCOE XP UAE', CAST(N'2023-11-17T23:49:42.213' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'963', N'W-CP00-555XXXX-0338-IN-XPD0', N'CP 555 LC-338 NAT INCOE XP MLY', CAST(N'2023-11-17T23:49:42.293' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'964', N'W-CP00-555XXXX-0338-OH-HDB0', N'CP 555 LC-338 NAT OHAYO HD MII', CAST(N'2023-11-17T23:49:42.373' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'965', N'W-CP00-D26XXXX-0209-QU-00B0', N'CP D26 LC-209 NAT QUANTUM MII', CAST(N'2023-11-17T23:49:42.453' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'966', N'W-CP00-B24XXXX-0232-IN-XPB0', N'CP B24 LC232 NAT INCOE XP SUD', CAST(N'2023-11-17T23:49:42.533' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'967', N'W-CP00-B24XXXX-0232-IN-XPC0', N'CP B24 LC232 NAT INCOE XP UAE', CAST(N'2023-11-17T23:49:42.617' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'968', N'W-CP00-B24XXXX-0232-IN-XPD0', N'CP B24 LC232 NAT INCOE XP MLY', CAST(N'2023-11-17T23:49:42.697' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'969', N'W-CP00-B24XXXX-0232-OH-HDB0', N'CP B24 LC-232 NAT OHAYO HD MII', CAST(N'2023-11-17T23:49:42.780' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'97', N'K-SCCV-327X050-11VE-OH-MFA0', N'STIC COVER OHAYO MF 327X50 MM', CAST(N'2023-11-17T23:48:24.693' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'970', N'W-CP00-B24XXXX-0232-QU-00A0', N'CP B24 LC232 NAT QUANTUM', CAST(N'2023-11-17T23:49:42.860' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'971', N'W-CP00-B24XXXX-0232-QU-00B0', N'CP B24 LC232 NAT QUANTUM MII', CAST(N'2023-11-17T23:49:42.940' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'972', N'W-CP00-B24XXXX-0232-UL-00A0', N'CP B24 LC-232 NAT ULTRA-X', CAST(N'2023-11-17T23:49:43.020' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'973', N'W-CP00-B24XXXX-0232-UL-00B0', N'CP B24 LC-232 NAT ULTRA-X MII', CAST(N'2023-11-17T23:49:43.100' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'974', N'W-CP00-B24XXXX-0331-AP-PRA0', N'CP B24 LC-331 NAT ASPIRA PRE', CAST(N'2023-11-17T23:49:43.180' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'975', N'W-CP00-B20XXXX-0203-IN-XPA0', N'CP B20 LC-203 NAT INCOE XP', CAST(N'2023-11-17T23:49:43.263' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'976', N'W-CP00-B20XXXX-0203-IN-XPB0', N'CP B20 LC-203 NAT INCOE XP SUD', CAST(N'2023-11-17T23:49:43.340' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'977', N'W-CP00-B20XXXX-0203-IN-XPC0', N'CP B20 LC-203 NAT INCOE XP UAE', CAST(N'2023-11-17T23:49:43.420' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'978', N'W-CP00-B20XXXX-0203-IN-XPD0', N'CP B20 LC-203 NAT INCOE XP MLY', CAST(N'2023-11-17T23:49:43.500' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'979', N'W-CP00-B20XXXX-0203-OH-HDB0', N'CP B20 LC-203 NAT OHAYO HD MII', CAST(N'2023-11-17T23:49:43.583' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'98', N'K-SCCV-327X050-15V0-IN-MFB0', N'STC CVR INCOE MF UAE 327X50 MM', CAST(N'2023-11-17T23:48:24.773' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'980', N'W-CP00-B20XXXX-0203-QU-00A0', N'CP B20 LC-203 NAT QUANTUM', CAST(N'2023-11-17T23:49:43.663' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'981', N'W-CP00-B20XXXX-0203-QU-00B0', N'CP B20 LC-203 NAT QUANTUM MII', CAST(N'2023-11-17T23:49:43.747' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'982', N'W-CP00-B20XXXX-0203-UL-00A0', N'CP B20 LC-203 NAT ULTRA-X', CAST(N'2023-11-17T23:49:43.827' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'983', N'W-CP00-B20XXXX-0203-UL-00B0', N'CP B20 LC-203 NAT ULTRA-X MII', CAST(N'2023-11-17T23:49:43.907' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'984', N'W-CP00-B20XXXX-0220-AP-PRA0', N'CP B20 LC-220 NAT ASPIRA PRE', CAST(N'2023-11-17T23:49:43.983' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'985', N'W-CP00-B20XXXX-0220-AP-PRB0', N'CP B20 LC220 NAT ASPIRA PRE AO', CAST(N'2023-11-17T23:49:44.067' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'986', N'W-CP00-B20XXXX-0220-AS-HPA0', N'CP B20 LC-220 NAT ASAHI H-PERF', CAST(N'2023-11-17T23:49:44.150' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'987', N'W-CP00-B20XXXX-0220-AS-HPB0', N'CP B20 LC-220 NAT ASAHI HP MII', CAST(N'2023-11-17T23:49:44.233' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'988', N'W-CP00-B20XXXX-0220-AS-MEA0', N'CP B20 LC-220 NAT ASAHI MEANS', CAST(N'2023-11-17T23:49:44.313' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'989', N'W-CP00-B20XXXX-0220-BL-00A0', N'CP B20 LC-220 NAT NO BRAND', CAST(N'2023-11-17T23:49:44.393' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'99', N'K-SCCV-345X050-11V0-AP-MFA0', N'STIC CVR ASPIRA MF 345X50 MM', CAST(N'2023-11-17T23:48:24.857' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'990', N'W-CP00-B20XXXX-0220-IN-PPA0', N'CP B20 LC-220 NAT INCOE PPO', CAST(N'2023-11-17T23:49:44.477' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'991', N'W-CP00-B20XXXX-0220-IN-XPA0', N'CP B20 LC-220 NAT INCOE XP', CAST(N'2023-11-17T23:49:44.557' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'992', N'W-CP00-B20XXXX-0220-IN-XPB0', N'CP B20 LC-220 NAT INCOE XP SUD', CAST(N'2023-11-17T23:49:44.990' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'993', N'W-CP00-B20XXXX-0220-IN-XPC0', N'CP B20 LC-220 NAT INCOE XP UAE', CAST(N'2023-11-17T23:49:45.097' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'994', N'W-CP00-B20XXXX-0220-IN-XPD0', N'CP B20 LC-220 NAT INCOE XP MLY', CAST(N'2023-11-17T23:49:45.177' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'995', N'W-CP00-B20XXXX-0220-OH-HDB0', N'CP B20 LC-220 NAT OHAYO HD MII', CAST(N'2023-11-17T23:49:45.257' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'996', N'W-CP00-B20XXXX-0220-QU-00A0', N'CP B20 LC-220 NAT QUANTUM', CAST(N'2023-11-17T23:49:45.340' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'997', N'W-CP00-D31XXXX-0283-AP-PRA0', N'CP D31 LC-283 NAT ASPIRA PRE', CAST(N'2023-11-17T23:49:45.423' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'998', N'W-CP00-G51XXXX-0880-IN-XPB0', N'CP G51 LC-880 NAT INCOE XP SUD', CAST(N'2023-11-17T23:49:45.510' AS DateTime))
GO
INSERT [dbo].[part_number] ([id_pn], [pn], [description], [created_at]) VALUES (N'999', N'W-CP00-G51XXXX-0880-IN-XPC0', N'CP G51 LC-880 NAT INCOE XP UAE', CAST(N'2023-11-17T23:49:45.610' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[scope] ON 
GO
INSERT [dbo].[scope] ([id_scope], [scope], [created_at]) VALUES (1, N'PROJECT', CAST(N'2023-11-17T23:58:25.907' AS DateTime))
GO
INSERT [dbo].[scope] ([id_scope], [scope], [created_at]) VALUES (2, N'DISPOSAL', CAST(N'2023-11-17T23:58:29.897' AS DateTime))
GO
INSERT [dbo].[scope] ([id_scope], [scope], [created_at]) VALUES (3, N'SAMPLE', CAST(N'2023-11-17T23:58:42.633' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[scope] OFF
GO
SET IDENTITY_INSERT [dbo].[users] ON 
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (1, 1, 1, N'ADMIN', N'1234', N'ADMIN', CAST(N'2023-11-18T00:40:19.883' AS DateTime), CAST(N'2023-11-18T00:40:19.883' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (2, 1, 1, N'CIPTADI NUGROHO', N'1095', N'KASIE PPIC', CAST(N'2023-11-18T00:40:19.973' AS DateTime), CAST(N'2023-11-18T00:40:19.973' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (3, 1, 1, N'FAHMI KURNIAWAN', N'3404', N'ADMIN SEKSI PEMOHON', CAST(N'2023-11-18T00:40:20.070' AS DateTime), CAST(N'2023-11-18T00:40:20.070' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (4, 1, 2, N'GOFAR JULIO SAPUTRA', N'3661', N'PEMOHON', CAST(N'2023-11-18T00:40:20.177' AS DateTime), CAST(N'2023-11-18T00:40:20.177' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (5, 3, 2, N'ANDRIANA', N'3384', N'PEMOHON', CAST(N'2023-11-18T00:40:20.273' AS DateTime), CAST(N'2023-11-18T00:40:20.273' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (6, 3, 2, N'ARI TRI WIBOWO', N'4015', N'PEMOHON', CAST(N'2023-11-18T00:40:20.370' AS DateTime), CAST(N'2023-11-18T00:40:20.370' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (7, 3, 2, N'BAGAS MIFTHAHKUL HUDHA', N'4062', N'PEMOHON', CAST(N'2023-11-18T00:40:20.467' AS DateTime), CAST(N'2023-11-18T00:40:20.467' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (8, 3, 2, N'CEVI DOA SUKMA', N'4014', N'PEMOHON', CAST(N'2023-11-18T00:40:20.560' AS DateTime), CAST(N'2023-11-18T00:40:20.560' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (9, 3, 2, N'FREDY SEPTIAN', N'3368', N'PEMOHON', CAST(N'2023-11-18T00:40:20.657' AS DateTime), CAST(N'2023-11-18T00:40:20.657' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (10, 3, 2, N'IKRAR SATRIA HARTAWAN', N'3693', N'PEMOHON', CAST(N'2023-11-18T00:40:20.750' AS DateTime), CAST(N'2023-11-18T00:40:20.750' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (11, 3, 2, N'LATIF USMAN', N'2346', N'PEMOHON', CAST(N'2023-11-18T00:40:20.843' AS DateTime), CAST(N'2023-11-18T00:40:20.843' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (12, 3, 2, N'M. RIDWAN ARIFIN', N'4111', N'PEMOHON', CAST(N'2023-11-18T00:40:20.940' AS DateTime), CAST(N'2023-11-18T00:40:20.940' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (13, 3, 2, N'MUTHIA DUWINTA', N'3403', N'PEMOHON', CAST(N'2023-11-18T00:40:21.033' AS DateTime), CAST(N'2023-11-18T00:40:21.033' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (14, 3, 2, N'RIZKY TOYIBAH', N'2846', N'PEMOHON', CAST(N'2023-11-18T00:40:21.123' AS DateTime), CAST(N'2023-11-18T00:40:21.123' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (15, 4, 2, N'ADAM DARMAWAN HIDAYAT', N'3650', N'PEMOHON', CAST(N'2023-11-18T00:40:21.210' AS DateTime), CAST(N'2023-11-18T00:40:21.210' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (16, 4, 2, N'AHMAD FADILAH', N'1637', N'PEMOHON', CAST(N'2023-11-18T00:40:21.297' AS DateTime), CAST(N'2023-11-18T00:40:21.297' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (17, 4, 2, N'ANGGA SLAMET NUGRAHA', N'3399', N'PEMOHON', CAST(N'2023-11-18T00:40:21.390' AS DateTime), CAST(N'2023-11-18T00:40:21.390' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (18, 4, 2, N'ARIS ROYHAN', N'1028', N'PEMOHON', CAST(N'2023-11-18T00:40:21.477' AS DateTime), CAST(N'2023-11-18T00:40:21.477' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (19, 4, 2, N'HENDI SETIAWAN', N'2845', N'PEMOHON', CAST(N'2023-11-18T00:40:21.560' AS DateTime), CAST(N'2023-11-18T00:40:21.560' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (20, 4, 2, N'IMAN ARI WIBOWO', N'2872', N'PEMOHON', CAST(N'2023-11-18T00:40:21.657' AS DateTime), CAST(N'2023-11-18T00:40:21.657' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (21, 4, 2, N'MUHTAR INDRA PRABOWO', N'3517', N'PEMOHON', CAST(N'2023-11-18T00:40:21.747' AS DateTime), CAST(N'2023-11-18T00:40:21.747' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (22, 4, 2, N'RUSJA IRAWAN', N'0570', N'PEMOHON', CAST(N'2023-11-18T00:40:21.833' AS DateTime), CAST(N'2023-11-18T00:40:21.833' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (23, 2, 2, N'DESTA HENDRA PERMANA', N'2643', N'PEMOHON', CAST(N'2023-11-18T00:40:21.923' AS DateTime), CAST(N'2023-11-18T00:40:21.923' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (24, 2, 2, N'JOKO KRISTANTO', N'2503', N'PEMOHON', CAST(N'2023-11-18T00:40:22.010' AS DateTime), CAST(N'2023-11-18T00:40:22.010' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (25, 2, 2, N'NANANG AVIYANTO', N'3891', N'PEMOHON', CAST(N'2023-11-18T00:40:22.100' AS DateTime), CAST(N'2023-11-18T00:40:22.100' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (26, 2, 2, N'RAVI PUTRA PRATAMA', N'3690', N'PEMOHON', CAST(N'2023-11-18T00:40:22.193' AS DateTime), CAST(N'2023-11-18T00:40:22.193' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (27, 2, 2, N'RIZAL KUSNADI', N'3468', N'PEMOHON', CAST(N'2023-11-18T00:40:22.320' AS DateTime), CAST(N'2023-11-18T00:40:22.320' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (28, 2, 2, N'SRI SULISTYOWATI', N'1067', N'PEMOHON', CAST(N'2023-11-18T00:40:22.410' AS DateTime), CAST(N'2023-11-18T00:40:22.410' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (29, 5, 1, N'ARIS SUBAGIYO', N'1730', N'ADMIN WH ASAL', CAST(N'2023-11-18T00:40:22.507' AS DateTime), CAST(N'2023-11-18T00:40:22.507' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (30, 5, 1, N'ASEP SAEFUDIN', N'2350', N'ADMIN WH ASAL', CAST(N'2023-11-18T00:40:22.600' AS DateTime), CAST(N'2023-11-18T00:40:22.600' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (31, 5, 1, N'EKO NURTAUCHIT', N'2740', N'ADMIN WH ASAL', CAST(N'2023-11-18T00:40:22.690' AS DateTime), CAST(N'2023-11-18T00:40:22.690' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (32, 5, 1, N'RIAN ARDIANTO', N'2444', N'ADMIN WH ASAL', CAST(N'2023-11-18T00:40:22.780' AS DateTime), CAST(N'2023-11-18T00:40:22.780' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (33, 5, 1, N'WARIYADI', N'3087', N'ADMIN WH ASAL', CAST(N'2023-11-18T00:40:22.870' AS DateTime), CAST(N'2023-11-18T00:40:22.870' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (34, 5, 1, N'YUDI MULYADI', N'2138', N'ADMIN WH ASAL', CAST(N'2023-11-18T00:40:22.963' AS DateTime), CAST(N'2023-11-18T00:40:22.963' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (35, 7, 1, N'FATKHUROHMAN', N'3166', N'ADMIN WH ASAL', CAST(N'2023-11-18T00:40:23.057' AS DateTime), CAST(N'2023-11-18T00:40:23.057' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (36, 8, 1, N'RIDHI SYAM KURNIAWAN', N'3055', N'ADMIN WH ASAL', CAST(N'2023-11-18T00:40:23.147' AS DateTime), CAST(N'2023-11-18T00:40:23.147' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (37, 8, 1, N'SHANTO NUGROHO', N'1489', N'ADMIN WH ASAL', CAST(N'2023-11-18T00:40:23.237' AS DateTime), CAST(N'2023-11-18T00:40:23.237' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (38, 8, 1, N'FAJAR RAMADHAN', N'1806', N'ADMIN WH ASAL', CAST(N'2023-11-18T00:40:23.323' AS DateTime), CAST(N'2023-11-18T00:40:23.323' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (39, 8, 1, N'HARI SUNTORO', N'2813', N'ADMIN WH ASAL', CAST(N'2023-11-18T00:40:23.410' AS DateTime), CAST(N'2023-11-18T00:40:23.410' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (40, 8, 1, N'IMAM NUR HUDA', N'2622', N'ADMIN WH ASAL', CAST(N'2023-11-18T00:40:23.497' AS DateTime), CAST(N'2023-11-18T00:40:23.497' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (41, 1, 1, N'ANJAR WIDI SATRIA', N'1814', N'KASIE PEMOHON', CAST(N'2023-11-18T00:40:23.587' AS DateTime), CAST(N'2023-11-18T00:40:23.587' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (42, 1, 1, N'APRILIANTO CANDRA NUGROHO', N'2523', N'KASIE PEMOHON', CAST(N'2023-11-18T00:40:23.680' AS DateTime), CAST(N'2023-11-18T00:40:23.680' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (44, 1, 1, N'FAHRIZAL FITRA UTAMA', N'2863', N'KASIE PEMOHON', CAST(N'2023-11-18T00:40:23.870' AS DateTime), CAST(N'2023-11-18T00:40:23.870' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (45, 1, 1, N'INDRI AFRIYANTI', N'2649', N'KASIE PEMOHON', CAST(N'2023-11-18T00:40:23.960' AS DateTime), CAST(N'2023-11-18T00:40:23.960' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (46, 1, 1, N'KAUTZAR RIZKA IGAPUTRA', N'2526', N'KASIE PEMOHON', CAST(N'2023-11-18T00:40:24.047' AS DateTime), CAST(N'2023-11-18T00:40:24.047' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (47, 1, 1, N'YOHANES RICKY YUNIAR', N'2070', N'KASIE PEMOHON', CAST(N'2023-11-18T00:40:24.140' AS DateTime), CAST(N'2023-11-18T00:40:24.140' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (48, 3, 1, N'EVEI ADI KURNIAWAN', N'1697', N'KASIE PEMOHON', CAST(N'2023-11-18T00:40:24.230' AS DateTime), CAST(N'2023-11-18T00:40:24.230' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (49, 3, 1, N'KRESNA BAYU AJI', N'3477', N'KASIE PEMOHON', CAST(N'2023-11-18T00:40:24.320' AS DateTime), CAST(N'2023-11-18T00:40:24.320' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (50, 3, 1, N'LATIF USMAN', N'2346', N'KASIE PEMOHON', CAST(N'2023-11-18T00:40:24.407' AS DateTime), CAST(N'2023-11-18T00:40:24.407' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (51, 3, 1, N'SUCIPTO HENING', N'2862', N'KASIE PEMOHON', CAST(N'2023-11-18T00:40:24.497' AS DateTime), CAST(N'2023-11-18T00:40:24.497' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (52, 4, 1, N'BAGUS PURNOMO', N'3651', N'KASIE PEMOHON', CAST(N'2023-11-18T00:40:24.583' AS DateTime), CAST(N'2023-11-18T00:40:24.583' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (53, 4, 1, N'CIPTO TIGOR PRIBADI NAINGGOLAN', N'2593', N'KASIE PEMOHON', CAST(N'2023-11-18T00:40:24.673' AS DateTime), CAST(N'2023-11-18T00:40:24.673' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (54, 4, 1, N'M. FARRAS ABRAR', N'4171', N'KASIE PEMOHON', CAST(N'2023-11-18T00:40:24.763' AS DateTime), CAST(N'2023-11-18T00:40:24.763' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (55, 4, 1, N'PRADIPTA FAJAR YUNIARTO', N'2331', N'KASIE PEMOHON', CAST(N'2023-11-18T00:40:24.850' AS DateTime), CAST(N'2023-11-18T00:40:24.850' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (56, 4, 1, N'RYANDANU ALDY YUDHISTIRA', N'3659', N'KASIE PEMOHON', CAST(N'2023-11-18T00:40:24.947' AS DateTime), CAST(N'2023-11-18T00:40:24.947' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (57, 2, 1, N'AHMAD SYAFIQ', N'1971', N'KASIE PEMOHON', CAST(N'2023-11-18T00:40:25.037' AS DateTime), CAST(N'2023-11-18T00:40:25.037' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (58, 2, 1, N'ARI MUSTAKIM', N'3305', N'KASIE PEMOHON', CAST(N'2023-11-18T00:40:25.123' AS DateTime), CAST(N'2023-11-18T00:40:25.123' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (59, 2, 1, N'BAYU SURYADI', N'2524', N'KASIE PEMOHON', CAST(N'2023-11-18T00:40:25.213' AS DateTime), CAST(N'2023-11-18T00:40:25.213' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (60, 2, 1, N'MAYLANI TIARNA RIASMIN SIANIPAR', N'2322', N'KASIE PEMOHON', CAST(N'2023-11-18T00:40:25.300' AS DateTime), CAST(N'2023-11-18T00:40:25.300' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (61, 2, 1, N'MUHAMMAD FAUZI', N'2592', N'KASIE PEMOHON', CAST(N'2023-11-18T00:40:25.393' AS DateTime), CAST(N'2023-11-18T00:40:25.393' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (62, 5, 2, N'ANDIKA SAMULYA', N'2918', N'PENULIS JUMLAH DISETUJUI', CAST(N'2023-11-18T00:40:25.477' AS DateTime), CAST(N'2023-11-18T00:40:25.477' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (63, 5, 2, N'SULIS PERMANA', N'1168', N'PENULIS JUMLAH DISETUJUI', CAST(N'2023-11-18T00:40:25.570' AS DateTime), CAST(N'2023-11-18T00:40:25.570' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (64, 5, 2, N'SUWARNO (B)', N'1010', N'PENULIS JUMLAH DISETUJUI', CAST(N'2023-11-18T00:40:25.670' AS DateTime), CAST(N'2023-11-18T00:40:25.670' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (65, 5, 2, N'TARMONO', N'1509', N'PENULIS JUMLAH DISETUJUI', CAST(N'2023-11-18T00:40:25.760' AS DateTime), CAST(N'2023-11-18T00:40:25.760' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (66, 5, 2, N'UUS SUTISNA', N'3398', N'PENULIS JUMLAH DISETUJUI', CAST(N'2023-11-18T00:40:25.850' AS DateTime), CAST(N'2023-11-18T00:40:25.850' AS DateTime))
GO
INSERT [dbo].[users] ([id_user], [id_dept], [level], [username], [password], [keterangan], [created_at], [updated_at]) VALUES (67, 5, 2, N'YUDA AJI PRASETYO', N'2185', N'PENULIS JUMLAH DISETUJUI', CAST(N'2023-11-18T00:40:25.943' AS DateTime), CAST(N'2023-11-18T00:40:25.943' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[users] OFF
GO
SET IDENTITY_INSERT [dbo].[warehouse] ON 
GO
INSERT [dbo].[warehouse] ([id_wh], [wh], [created_at]) VALUES (1, N'KPENG', CAST(N'2023-11-18T00:03:05.937' AS DateTime))
GO
INSERT [dbo].[warehouse] ([id_wh], [wh], [created_at]) VALUES (2, N'KPRO1', CAST(N'2023-11-18T00:03:06.043' AS DateTime))
GO
INSERT [dbo].[warehouse] ([id_wh], [wh], [created_at]) VALUES (3, N'KPRO2', CAST(N'2023-11-18T00:03:06.140' AS DateTime))
GO
INSERT [dbo].[warehouse] ([id_wh], [wh], [created_at]) VALUES (4, N'KWMTX', CAST(N'2023-11-18T00:03:06.220' AS DateTime))
GO
INSERT [dbo].[warehouse] ([id_wh], [wh], [created_at]) VALUES (5, N'KFFGX', CAST(N'2023-11-18T00:03:06.303' AS DateTime))
GO
INSERT [dbo].[warehouse] ([id_wh], [wh], [created_at]) VALUES (6, N'KPCPQ', CAST(N'2023-11-18T00:03:06.510' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[warehouse] OFF
GO
ALTER TABLE [dbo].[departement] ADD  CONSTRAINT [DF_dapartement_created_at]  DEFAULT (getdate()) FOR [created_at]
GO
ALTER TABLE [dbo].[part_number] ADD  CONSTRAINT [DF_part_number_created_at]  DEFAULT (getdate()) FOR [created_at]
GO
ALTER TABLE [dbo].[request] ADD  CONSTRAINT [DF_request_created_at]  DEFAULT (getdate()) FOR [created_at]
GO
ALTER TABLE [dbo].[request] ADD  CONSTRAINT [DF_request_updated_at]  DEFAULT (getdate()) FOR [updated_at]
GO
ALTER TABLE [dbo].[scope] ADD  CONSTRAINT [DF_scope_created_at]  DEFAULT (getdate()) FOR [created_at]
GO
ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF_users_created_at]  DEFAULT (getdate()) FOR [created_at]
GO
ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF_users_updated_at]  DEFAULT (getdate()) FOR [updated_at]
GO
ALTER TABLE [dbo].[warehouse] ADD  CONSTRAINT [DF_warehouse_created_at]  DEFAULT (getdate()) FOR [created_at]
GO
ALTER TABLE [dbo].[users]  WITH CHECK ADD  CONSTRAINT [FK_users_dept] FOREIGN KEY([id_dept])
REFERENCES [dbo].[departement] ([id_dept])
GO
ALTER TABLE [dbo].[users] CHECK CONSTRAINT [FK_users_dept]
GO
